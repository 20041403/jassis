import { OrbitSpinner } from "epic-spinners";
var receivedPhrase = require("../../api/ReceivedPhrase.js");
var Response = require('../../api/Response.js');

export default {
  components: { OrbitSpinner },
  data() {
    return {
      count: {
        received: "Chưa có",
        responsed: "Chưa có",
        notyet: "Chưa có"
      },
      card: {
        id: 0,
        title: "CÁC CÂU ĐÃ NHẬN"
      },
      isLoading: true,
      data: [],
      modalsConfirmDelete: {
        isShow: false,
        icon: "fa fa-trash",
        currObject: {}
      },
      modalAddOrUpdate: {
        isShow: false,
        isEdit: true,
        icon: "ni ni-chat-round",
        title: "",
        warning: "",
        val1: "",
        val2: "",
        okDisable: "disable",
        hint1: "",
        currObject: {},
        currParent: {}
      },
    };
  },
  methods: {
    openInfo() {
      this.$toast.open({
        message: "Chưa hỗ trợ phần xem thông tìn!",
        type: "warning",
        position: "top-right"
      });
    },
    removeCurrent() {
      this.modalsConfirmDelete.isShow = false;
      Response.remove(this.modalsConfirmDelete.currObject.r_id).then(() => {
        this.changeCard();
        this.countAll();
        this.$toast.open({
          message: "Đã xóa!",
          type: "success",
          position: "top-right"
        });
      }).catch(e => {
        this.$toast.open({
          message: "Xóa không thành công!",
          type: "error",
          position: "top-right"
        });
      })
        .finally(() => {
        });
    },
    openDeleteConfirm(received) {
      this.modalsConfirmDelete.currObject = received;
      this.modalsConfirmDelete.isShow = true;
    },
    val1Change() {
      if (this.modalAddOrUpdate.val1.length <= 0) {
        this.modalAddOrUpdate.warning = "Vui lòng nhập mẫu câu trả lời!";
        this.modalAddOrUpdate.okDisable = "disabled";
      } else {
        this.modalAddOrUpdate.warning = "";
        this.modalAddOrUpdate.okDisable = null;
      }
    },
    val2Change() {
      if (this.modalAddOrUpdate.val2.length > 0) {
        try {
          var tmpJson = JSON.parse(this.modalAddOrUpdate.val2);
          this.modalAddOrUpdate.val2 = JSON.stringify(tmpJson, undefined, 4)
          this.modalAddOrUpdate.warning = "";
          this.modalAddOrUpdate.okDisable = null;
          if (tmpJson.event == undefined) {
            this.modalAddOrUpdate.warning = "JSON thiếu trường event, vui lòng bổ sung.";
            this.modalAddOrUpdate.okDisable = "disabled";
          }
        } catch (e) {
          this.modalAddOrUpdate.warning = "Không đúng chuẩn JSON. " + e;
          this.modalAddOrUpdate.okDisable = "disabled";
        }
      } else {
        this.modalAddOrUpdate.warning = "";
        this.modalAddOrUpdate.okDisable = null;
      }
    },
    addOrUpdate() {
      if (this.modalAddOrUpdate.val1.length <= 0) {
        this.modalAddOrUpdate.warning = "Vui lòng nhập nội dung câu trả lời trước";
        return;
      }
      let json = null;
      if (this.modalAddOrUpdate.val2.length > 0) {
        try {
          JSON.parse(this.modalAddOrUpdate.val2);
          json = this.modalAddOrUpdate.val2;
        } catch (e) {
          this.modalAddOrUpdate.warning = "Vui lòng nhập mã lệnh đúng định dạng JSON cho ô sự kiện";
          return;
        }
      }
      this.modalAddOrUpdate.isShow = false;
      if (this.modalAddOrUpdate.isEdit) {
        // Nếu là sửa
        Response.update(this.modalAddOrUpdate.currObject.r_id, this.modalAddOrUpdate.val1, json)
          .then(() => {
            this.changeCard();
            this.$toast.open({
              message: "Sửa thành công!",
              type: "success",
              position: "top-right"
            });
          }).catch(e => {
            this.$toast.open({
              message: "Sửa không thành công!",
              type: "error",
              position: "top-right"
            });
          })
          .finally(() => { });
      }
    },
    openEdit(receivedResponse) {
      receivedResponse = JSON.parse(receivedResponse);
      if (receivedResponse.status < 0) {
        this.$toast.open({
          message: "Không thể tiến hành chỉnh sửa khi phản hồi không có!",
          type: "error",
          position: "top-right"
        });
      } else {
        if (receivedResponse.result == null || receivedResponse.result == undefined || receivedResponse.result == "") {
          this.$toast.open({
            message: "Không thể tìm thấy phần phản hồi.",
            type: "error",
            position: "top-right"
          });
        } else if (receivedResponse.result.gottedIntent == null || receivedResponse.result.gottedIntent == undefined) {
          this.$toast.open({
            message: "Không thể tìm thấy ý định của phản hồi này.",
            type: "error",
            position: "top-right"
          });
        } else if (receivedResponse.result.gottedResponse == null || receivedResponse.result.gottedResponse == undefined) {
          this.$toast.open({
            message: "Không thể tìm thấy nội dung của phản hồi.",
            type: "error",
            position: "top-right"
          });
        } else {
          this.modalAddOrUpdate.currParent = receivedResponse.result.gottedIntent;
          let json = JSON.stringify(receivedResponse.result.gottedResponse.r_event);
          this.modalAddOrUpdate.currObject = receivedResponse.result.gottedResponse;
          this.modalAddOrUpdate.isShow = true;
          this.modalAddOrUpdate.isEdit = true;
          this.modalAddOrUpdate.title = `SỬA MẪU CÂU TRẢ LỜI (Mã số: ${receivedResponse.result.gottedResponse.r_id})`;
          this.modalAddOrUpdate.warning = "";
          this.modalAddOrUpdate.val1 = receivedResponse.result.gottedResponse.r_response;
          this.modalAddOrUpdate.val2 = json == "null" ? "" : JSON.stringify(receivedResponse.result.gottedResponse.r_event);
          this.modalAddOrUpdate.okDisable = null;
          this.modalAddOrUpdate.hint1 = "Vui lòng nhập mẫu câu trả lời";
        }
      }
    },
    processResultForShow(response) {
      response = JSON.parse(response);
      if (response.status < 0) {
        return response.result;
      } else {
        if (
          response.result.gottedResponse != null &&
          response.result.gottedResponse != undefined &&
          response.result.gottedResponse.r_response != null &&
          response.result.gottedResponse.r_response != undefined &&
          response.result.gottedResponse.r_response != ""
        ) {
          return response.result.gottedResponse.r_response;
        }
      }
    },
    changeCard(index) {
      if (this.card.id == index) return;
      else if (index == undefined) {
        index = this.card.id;
      }
      switch (index) {
        case 1: {
          this.isLoading = true;
          this.data = [];
          this.card.id = 1;
          this.card.title = "CÁC CÂU ĐÃ PHẢN HỒI";
          this.getResponsed();
          break;
        }
        case 2: {
          this.isLoading = true;
          this.data = [];
          this.card.id = 2;
          this.card.title = "CÁC CÂU CHƯA PHẢN HỒI";
          this.getNotYet();
          break;
        }
        default: {
          this.isLoading = true;
          this.data = [];
          this.card.id = 0;
          this.card.title = "CÁC CÂU ĐÃ NHẬN";
          this.getAll();
          break;
        }
      }
    },
    countAll() {
      receivedPhrase.countAll().then(r => {
        if (r.data.result <= 0) this.count.received = "Chưa có";
        else this.count.received = r.data.result + "";
      });
      receivedPhrase.countResponsed().then(r => {
        if (r.data.result <= 0) this.count.responsed = "Chưa có";
        else this.count.responsed = r.data.result + "";
      });
      receivedPhrase.countNotYet().then(r => {
        if (r.data.result <= 0) this.count.notyet = "Chưa có";
        else this.count.notyet = r.data.result + "";
      });
    },
    getAll() {
      receivedPhrase
        .getAll()
        .then(r => {
          this.data = r.data.result;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },
    getNotYet() {
      receivedPhrase
        .getNotYet()
        .then(r => {
          this.data = r.data.result;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },
    getResponsed() {
      receivedPhrase
        .getResponsed()
        .then(r => {
          this.data = r.data.result;
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
  },
  mounted() {
    this.countAll();
    this.getAll();
  }
};