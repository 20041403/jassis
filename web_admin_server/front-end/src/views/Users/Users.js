import { OrbitSpinner } from "epic-spinners";
var UserAccount = require("../../api/UserAccount.js");
import flatPickr from "vue-flatpickr-component";
import "flatpickr/dist/flatpickr.css";
import moment from "moment";
import storage from "../../utils/storage.js";
import JAxios from "../../utils/jaxios.js";
var PwdHasing = require("../../utils/pwd.js");

export default {
    components: {
        OrbitSpinner,
        flatPickr
    },
    data() {
        return {
            formData: {}, // for contain selected image
            deleteConfirm: {
                show: false,
                account: {},
                profile: {},
            },
            changePass: {
                show: false,
                account: {},
                pass: '',
                passReType: ''
            },
            info: {
                show: false,
                account: {},
                profile: {},
            },
            datePickerConfig: {
                altFormat: "d-m-Y",
                dateFormat: "d-m-Y",
                altInput: true
            },
            update: {
                isUpdateAvatar: false,
                show: false,
                account: {},
                profile: {},
                // https://flatpickr.js.org/options/
                input: {
                    surename: "",
                    name: "",
                    gender: {
                        code: -1,
                        name: "Vui lòng chọn giới tính"
                    },
                    birthday: "",
                    currentHome: "",
                    homeTown: "",
                    workAddress: "",
                    phone: "",
                }
            },
            roles: [],
            count: {
                all: "Chưa có",
                admin: "Chưa có",
                contentManager: "Chưa có"
            },
            card: {
                id: 0,
                title: "DANH SÁCH TẤT CẢ NGƯỜI DÙNG"
            },
            isLoading: true,
            data: []
        };
    },
    methods: {
        openChangePass(ua) {
            this.changePass.account = ua;
            this.changePass.pass = '';
            this.changePass.passReType = '';
            this.changePass.show = true;
        },
        openDeleteConfirm(ua) {
            this.deleteConfirm.account = ua;
            UserAccount.getProfile(ua.ua_id).then(r => {
                if (r.data.status == -1) {
                    this.$toast.open({
                        message: "Không thể tải được thông tin của người dùng này",
                        type: "error",
                        position: "top-right"
                    });
                } else {
                    this.deleteConfirm.show = true;
                    this.deleteConfirm.profile = r.data.result[0];
                }
            });
        },
        openInfo(ua) {
            this.info.account = ua;
            UserAccount.getProfile(ua.ua_id).then(r => {
                if (r.data.status == -1) {
                    this.$toast.open({
                        message: "Không thể tải được thông tin của người dùng này",
                        type: "error",
                        position: "top-right"
                    });
                } else {
                    this.info.show = true;
                    this.info.profile = r.data.result[0];
                    if (this.info.profile.up_avatar_addr != null &&
                        this.info.profile.up_avatar_addr != undefined &&
                        this.info.profile.up_avatar_addr.length > 10) {
                        if (this.info.profile.up_avatar_addr.indexOf("http://") != 0) {
                            this.info.profile.up_avatar_addr = `${JAxios.prototype.constructor.defaults.baseURL}${this.info.profile.up_avatar_addr}`;
                        }
                    }
                }
            });
        },
        openUpdate(ua) {
            this.update.isUpdateAvatar = false;
            this.update.account = ua;
            UserAccount.getProfile(ua.ua_id).then(r => {
                if (r.data.status == -1) {
                    this.$toast.open({
                        message: "Không thể tải được thông tin của người dùng này",
                        type: "error",
                        position: "top-right"
                    });
                } else {
                    this.update.show = true;
                    this.update.profile = r.data.result[0];
                    // Set Data to form
                    this.update.input.surename = this.update.profile.up_surename;
                    this.update.input.name = this.update.profile.up_name;
                    this.update.input.gender = this.getGender(
                        this.update.profile.up_gender
                    );
                    if (this.update.profile.up_birthday == null || this.update.profile.up_birthday == undefined || this.update.profile.up_birthday == "")
                        this.update.input.birthday = moment().format("DD-MM-YYYY")
                    else
                        this.update.input.birthday = moment(this.update.profile.up_birthday, "YYYY-MM-DD").format("DD-MM-YYYY");
                    this.update.input.currentHome = this.update.profile.up_home_curr_addr;
                    this.update.input.homeTown = this.update.profile.up_home_town_addr;
                    this.update.input.workAddress = this.update.profile.up_work_place_addr;
                    this.update.input.phone = this.update.profile.up_phone;
                    // set avatar
                    if (this.update.profile.up_avatar_addr != null &&
                        this.update.profile.up_avatar_addr != undefined &&
                        this.update.profile.up_avatar_addr.length > 10) {
                        if (this.update.profile.up_avatar_addr.indexOf("http://") != 0) {
                            this.update.profile.up_avatar_addr = `${JAxios.prototype.constructor.defaults.baseURL}${this.update.profile.up_avatar_addr}`;
                        }
                    }
                }
            });
        },
        loadCount() {
            // Count all user
            UserAccount.countAll().then(r => {
                if (r.data.result <= 0) this.count.all = "Chưa có";
                else this.count.all = r.data.result + "";
            });
            // Count Admin user 
            UserAccount.countUserByRoles(storage.roles.adminRoles).then(r => {
                if (r.data.result <= 0) this.count.admin = "Chưa có";
                else this.count.admin = r.data.result + "";
            });
            // Count Content Manager
            UserAccount.countUserByRoles(storage.roles.contentManager).then(r => {
                if (r.data.result <= 0) this.count.contentManager = "Chưa có";
                else this.count.contentManager = r.data.result + "";
            });
        },
        loadAll() {
            UserAccount.loadAll()
                .then(r => {
                    this.data = r.data.result;
                })
                .finally(() => {
                    this.isLoading = false;
                });
        },
        combineName(surename, name) {
            let combinedName = "";
            if (surename != undefined && surename != null && surename != "")
                combinedName += surename + " ";
            if (name != undefined && name != null && name != "") combinedName += name;
            if (combinedName == "") return "<Chưa đặt tên>";
            else return combinedName;
        },
        extractUserRoles(rolesCode) {
            for (let i = 0; i < this.roles.length; i++) {
                if (this.roles[i].code == rolesCode) {
                    return this.roles[i].name;
                }
            }
        },
        birthdayToText(inp) {
            if (inp == null || inp == undefined)
                return "";
            else
                return "ngày " + moment(inp, "YYYY-MM-DD").format("DD ++ MM -- YYYY").replace("++", "tháng").replace("--", "năm");
        },
        getCurrentDate() {
            let crrDate = new Date();
            let inpMoment = moment(crrDate);
            return inpMoment.format("DD/MM/YYYY");
        },
        getGender(gCode) {
            return storage.getGender(gCode);
        },
        updateProfile() {
            // Set data back to object
            this.update.profile.up_surename = this.update.input.surename;
            this.update.profile.up_name = this.update.input.name;
            this.update.profile.up_gender = this.update.input.gender.code;
            this.update.profile.up_birthday = this.update.input.birthday;
            this.update.profile.up_home_curr_addr = this.update.input.currentHome;
            this.update.profile.up_home_town_addr = this.update.input.homeTown;
            this.update.profile.up_work_place_addr = this.update.input.workAddress;
            this.update.profile.up_phone = this.update.input.phone;
            this.update.profile.ua_roles = this.update.account.ua_roles;

            // Update
            UserAccount.updateProfile(this.update.profile).then(r => {
                this.$toast.open({
                    message: "Cập nhật thông tin thành công!",
                    type: "success",
                    position: "top-right"
                });
                this.loadCount();
            }).catch(e => {
                this.$toast.open({
                    message: "Đã xảy ra lỗi trong quá trình cập nhât! Vui lòng thử lại",
                    type: "error",
                    position: "top-right"
                });
            }).finally(() => {
                this.update.show = false;
            });
            // Update avatar
            if (this.update.isUpdateAvatar) {
                UserAccount.updateAvatar(this.formData, this.update.account.ua_username, this.update.account.ua_id).then(r => {
                    this.$toast.open({
                        message: "Cập nhật ảnh đại diện thành công",
                        type: "success",
                        position: "top-right"
                    });                
                }).catch(e => {
                    this.$toast.open({
                        message: "Cập nhật ảnh đại diện không thành công",
                        type: "error",
                        position: "top-right"
                    });
                });    
            }

            // Update view in list
            for (let i = 0; i < this.data.length; i++) {
                if (this.data[i].ua_id == this.update.profile.up_id) {
                    this.data[i].up_surename = this.update.profile.up_surename;
                    this.data[i].up_name = this.update.profile.up_name;
                }
            }

        },
        onSelectedAvatar(e) {
            var files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            this.createImage(files[0]);
        },
        createImage(file) {
            var reader = new FileReader();

            this.formData = new FormData();
            this.formData.append('file', file);

            for (var key of this.formData.entries()) {
//                console.log(key[0] + ', ' + key[1]);
            }

            reader.onload = (e) => {
                this.update.profile.up_avatar_addr = e.target.result;
                this.update.isUpdateAvatar = true;
            };
            reader.readAsDataURL(file);
        },
        deleteAccountAndProfile() {
            UserAccount.deleteAccountAndProfile(this.deleteConfirm.account.ua_id).then(r => {
                if (r.data.status == 1) {
                    this.$toast.open({
                        message: "Xóa tài khoản thành công!",
                        type: "success",
                        position: "top-right"
                    });
                    this.data = this.data.filter(ua => ua.ua_id != this.deleteConfirm.account.ua_id);
                    this.loadCount();
                    this.deleteConfirm.show = false;
                } else {
                    throw new Error("Xóa tài khoản không thành công!");
                }
            }).catch(e => {
                this.$toast.open({
                    message: "Xóa tài khoản không thành công!",
                    type: "error",
                    position: "top-right"
                });
            });
        },
        loadUserByRoles(roleCodes) {
            UserAccount.getUserByRoles(roleCodes).then(r => {
                this.data = r.data.result;
            }).finally(() => {
                this.isLoading = false;
            });
        },
        changeCard(index) {
            if (this.card.id == index)
                return;
            switch (index) {
                case 1:
                    {
                        this.isLoading = true;
                        this.data = [];
                        this.card.id = 1;
                        this.card.title = "DANH SÁCH QUẢN TRỊ VIÊN";
                        this.loadUserByRoles(storage.roles.adminRoles);
                        break;
                    }
                case 2:
                    {
                        this.isLoading = true;
                        this.data = [];
                        this.card.id = 2;
                        this.card.title = "DANH SÁCH QUẢN TRỊ NỘI DUNG";
                        this.loadUserByRoles(storage.roles.contentManager);
                        break;
                    }
                default:
                    {
                        this.isLoading = true;
                        this.data = [];
                        this.card.id = 0;
                        this.card.title = "DANH SÁCH TẤT CẢ NGƯỜI DÙNG";
                        this.loadAll();
                        break;
                    }
            }
        },
        submitChangePass() {
            if (this.changePass.pass.length <= 0 || this.changePass.passReType.length <= 0) {
                this.$toast.open({
                    message: "Vui lòng nhập đầy đủ các ô",
                    type: "error",
                    position: "top-right"
                });
            } else if (this.changePass.pass.length < 8 || this.changePass.passReType.length < 8) {
                this.$toast.open({
                    message: "Mật khẩu không được bé hơn 8 ký tự",
                    type: "error",
                    position: "top-right"
                });
            } else if (this.changePass.pass != this.changePass.passReType) {
                this.$toast.open({
                    message: "Hai mật khẩu không trùng khớp",
                    type: "error",
                    position: "top-right"
                });
            } else {
                this.changePass.show = false;
                UserAccount.changePasswordByAdmin(this.changePass.account.ua_username, PwdHasing.hasing(this.changePass.pass))
                    .then(r => {
                        if (r.data.status == 1) {
                            this.$toast.open({
                                message: "Cập nhật mật khẩu thành công!",
                                type: "success",
                                position: "top-right"
                            });
                        } else {
                            throw new Error(r.data.result);
                        }
                    }).catch(e => {
                        this.$toast.open({
                            message: "Cập nhật mật khẩu không thành công!",
                            type: "error",
                            position: "top-right"
                        });
                    })
            }
        },
        getRoleNameByCode(RoleCode) {
            for (let i = 0; i < this.roles.length; i++) {
                if (this.roles[i].code == RoleCode) {
                    return this.roles[i].name;
                }
            }
        }
    },
    created() {
        UserAccount.getAllRoles().then(r => {
            this.roles = r.data.result;
            this.loadCount();
            this.loadAll();
        });
    },
    mounted() { }
};