import { OrbitSpinner } from "epic-spinners";
var audioStored = require("../../api/AudioStored.js");

export default {
  components: { OrbitSpinner },
  data() {
    return {
      lockPlayBtn: null,
      count: {
        allGender: "Chưa có",
        maleVoice: "Chưa có",
        femaleVoice: "Chưa có"
      },
      card: {
        id: 0,
        title: "TẤT CẢ"
      },
      isLoading: true,
      data: [],
      modalsConfirmDelete: {
        isShow: false,
        icon: "fa fa-trash",
        currObject: {}
      },
      modalAddOrUpdate: {
        isShow: false,
        isEdit: true,
        icon: "ni ni-chat-round",
        title: "",
        warning: "",
        val1: "",
        val2: "",
        okDisable: "disable",
        hint1: "",
        currObject: {},
        currParent: {}
      },
    };
  },
  methods: {
    playBase64Audio(audio) {
      this.lockPlayBtn = "disable";
      if (audio.as_id == null || audio.as_id == undefined || audio.as_id < 0) {
        this.$toast.open({
          message: "Đã xảy ra lỗi, vui lòng thử lại.",
          type: "warning",
          position: "top-right"
        });
      } else {
        audioStored.getAudio(audio.as_id).then(r => {
          if (r.data.status < 0 || r.data.result.as_base64_audio == undefined) {
            this.$toast.open({
              message: "Tài nguyên này không chứa âm thanh.",
              type: "error",
              position: "top-right"
            });
          } else {
            let base64Content = r.data.result.as_base64_audio;
            var snd = new Audio(`data:audio/${audio.as_audio_encoding.toLowerCase()};base64,${base64Content}`);
            snd.play();
          }

        }).catch(e => {
          this.$toast.open({
            message: "Mở tài nguyên âm thanh không thành công.",
            type: "error",
            position: "top-right"
          });
        }).finally(() => {
          this.lockPlayBtn = null;
        });
      }
    },
    getVoiceName(str) {
      if (str == "vi-VN-Wavenet-A") {
        return "Giọng nữ - Miền Bắc";
      } else if (str == "vi-VN-Wavenet-B") {
        return "Giọng nam - Miền Bắc";
      } else if (str == "vi-VN-Wavenet-C") {
        return "Giọng nữ - Miền Nam";
      } else if (str == "vi-VN-Wavenet-D") {
        return "Giọng nam - Miền Nam";
      } else {
        return "Không xác định";
      }
    },
    openInfo() {
      // this.$toast.open({
      //   message: "Chưa hỗ trợ phần xem thông tìn!",
      //   type: "warning",
      //   position: "top-right"
      // });
    },
    removeCurrent() {
      this.modalsConfirmDelete.isShow = false;
      audioStored.remove(this.modalsConfirmDelete.currObject.as_id).then(() => {
        this.countAll();
        this.changeCard();
        this.$toast.open({
          message: "Đã xóa!",
          type: "success",
          position: "top-right"
        });
      }).catch(e => {
        this.$toast.open({
          message: "Xóa không thành công!",
          type: "error",
          position: "top-right"
        });
      })
        .finally(() => {
        });
    },
    openDeleteConfirm(obj) {
      this.modalsConfirmDelete.currObject = obj;
      this.modalsConfirmDelete.isShow = true;
    },
    val1Change() {
      if (this.modalAddOrUpdate.val1.length <= 0) {
        this.modalAddOrUpdate.warning = "Vui lòng nhập mẫu câu trả lời!";
        this.modalAddOrUpdate.okDisable = "disabled";
      } else {
        this.modalAddOrUpdate.warning = "";
        this.modalAddOrUpdate.okDisable = null;
      }
    },
    val2Change() {
      if (this.modalAddOrUpdate.val2.length > 0) {
        try {
          var tmpJson = JSON.parse(this.modalAddOrUpdate.val2);
          this.modalAddOrUpdate.val2 = JSON.stringify(tmpJson, undefined, 4)
          this.modalAddOrUpdate.warning = "";
          this.modalAddOrUpdate.okDisable = null;
          if (tmpJson.event == undefined) {
            this.modalAddOrUpdate.warning = "JSON thiếu trường event, vui lòng bổ sung.";
            this.modalAddOrUpdate.okDisable = "disabled";
          }
        } catch (e) {
          this.modalAddOrUpdate.warning = "Không đúng chuẩn JSON. " + e;
          this.modalAddOrUpdate.okDisable = "disabled";
        }
      } else {
        this.modalAddOrUpdate.warning = "";
        this.modalAddOrUpdate.okDisable = null;
      }
    },
    addOrUpdate() {
      if (this.modalAddOrUpdate.val1.length <= 0) {
        this.modalAddOrUpdate.warning = "Vui lòng nhập nội dung câu trả lời trước";
        return;
      }
      let json = null;
      if (this.modalAddOrUpdate.val2.length > 0) {
        try {
          JSON.parse(this.modalAddOrUpdate.val2);
          json = this.modalAddOrUpdate.val2;
        } catch (e) {
          this.modalAddOrUpdate.warning = "Vui lòng nhập mã lệnh đúng định dạng JSON cho ô sự kiện";
          return;
        }
      }
      this.modalAddOrUpdate.isShow = false;
      if (this.modalAddOrUpdate.isEdit) {
        // Nếu là sửa
        Response.update(this.modalAddOrUpdate.currObject.r_id, this.modalAddOrUpdate.val1, json)
          .then(() => {
            this.changeCard();
            this.$toast.open({
              message: "Sửa thành công!",
              type: "success",
              position: "top-right"
            });
          }).catch(e => {
            this.$toast.open({
              message: "Sửa không thành công!",
              type: "error",
              position: "top-right"
            });
          })
          .finally(() => { });
      }
    },
    openEdit(allGenderResponse) {
      allGenderResponse = JSON.parse(allGenderResponse);
      if (allGenderResponse.status < 0) {
        this.$toast.open({
          message: "Không thể tiến hành chỉnh sửa khi phản hồi không có!",
          type: "error",
          position: "top-right"
        });
      } else {
        if (allGenderResponse.result == null || allGenderResponse.result == undefined || allGenderResponse.result == "") {
          this.$toast.open({
            message: "Không thể tìm thấy phần phản hồi.",
            type: "error",
            position: "top-right"
          });
        } else if (allGenderResponse.result.gottedIntent == null || allGenderResponse.result.gottedIntent == undefined) {
          this.$toast.open({
            message: "Không thể tìm thấy ý định của phản hồi này.",
            type: "error",
            position: "top-right"
          });
        } else if (allGenderResponse.result.gottedResponse == null || allGenderResponse.result.gottedResponse == undefined) {
          this.$toast.open({
            message: "Không thể tìm thấy nội dung của phản hồi.",
            type: "error",
            position: "top-right"
          });
        } else {
          this.modalAddOrUpdate.currParent = allGenderResponse.result.gottedIntent;
          let json = JSON.stringify(allGenderResponse.result.gottedResponse.r_event);
          this.modalAddOrUpdate.currObject = allGenderResponse.result.gottedResponse;
          this.modalAddOrUpdate.isShow = true;
          this.modalAddOrUpdate.isEdit = true;
          this.modalAddOrUpdate.title = `SỬA MẪU CÂU TRẢ LỜI (Mã số: ${allGenderResponse.result.gottedResponse.r_id})`;
          this.modalAddOrUpdate.warning = "";
          this.modalAddOrUpdate.val1 = allGenderResponse.result.gottedResponse.r_response;
          this.modalAddOrUpdate.val2 = json == "null" ? "" : JSON.stringify(allGenderResponse.result.gottedResponse.r_event);
          this.modalAddOrUpdate.okDisable = null;
          this.modalAddOrUpdate.hint1 = "Vui lòng nhập mẫu câu trả lời";
        }
      }
    },
    processResultForShow(response) {
      response = JSON.parse(response);
      if (response.status < 0) {
        return response.result;
      } else {
        if (
          response.result.gottedResponse != null &&
          response.result.gottedResponse != undefined &&
          response.result.gottedResponse.r_response != null &&
          response.result.gottedResponse.r_response != undefined &&
          response.result.gottedResponse.r_response != ""
        ) {
          return response.result.gottedResponse.r_response;
        }
      }
    },
    changeCard(index) {
      if (this.card.id == index) return;
      else if (index == undefined) {
        index = this.card.id;
      }
      switch (index) {
        case 1: {
          this.isLoading = true;
          this.data = [];
          this.card.id = 1;
          this.card.title = "GIỌNG NAM";
          this.getMaleVoice();
          break;
        }
        case 2: {
          this.isLoading = true;
          this.data = [];
          this.card.id = 2;
          this.card.title = "GIỌNG NỮ";
          this.getFemaleVoice();
          break;
        }
        default: {
          this.isLoading = true;
          this.data = [];
          this.card.id = 0;
          this.card.title = "TẤT CẢ";
          this.getAll();
          break;
        }
      }
    },
    countAll() {
      audioStored.countAll().then(r => {
        if (r.data.result <= 0) this.count.allGender = "Chưa có";
        else this.count.allGender = r.data.result + "";
      });
      audioStored.countMaleVoice().then(r => {
        if (r.data.result <= 0) this.count.maleVoice = "Chưa có";
        else this.count.maleVoice = r.data.result + "";
      });
      audioStored.countFemaleVoice().then(r => {
        if (r.data.result <= 0) this.count.femaleVoice = "Chưa có";
        else this.count.femaleVoice = r.data.result + "";
      });
    },
    getAll() {
      audioStored
        .getAll()
        .then(r => {
          this.data = r.data.result;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },
    getFemaleVoice() {
      audioStored
        .getFemaleVoice()
        .then(r => {
          this.data = r.data.result;
        })
        .finally(() => {
          this.isLoading = false;
        });
    },
    getMaleVoice() {
      audioStored
        .getMaleVoice()
        .then(r => {
          this.data = r.data.result;
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
  },
  mounted() {
    this.countAll();
    this.getAll();
  }
};