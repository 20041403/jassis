import { OrbitSpinner } from "epic-spinners";
var Variable = require('../../api/Variable.js')

export default {
    components: {
        OrbitSpinner
    },
    data() {
        return {
            card: {
                id: 0,
                title: "CƠ BẢN"
            },
            isLoading: true,
            data: {
                show: [],
                base: [],
                condition: [],
                calculate: []
            }
        };
    },
    methods: {
        changeCard(id) {
            switch (id) {
                case 1:
                    this.card.id = 1;
                    this.card.title = "BIỂU THỨC ĐIỀU KIỆN";
                    this.data.show = this.data.condition;
                    break;
                case 2:
                    this.card.id = 2;
                    this.card.title = "BIỂU THỨC TÍNH TOÁN";
                    this.data.show = this.data.calculate;
                    break;
                default:
                    this.card.id = 0;
                    this.card.title = "BIỂU THỨC CƠ BẢN";
                    this.data.show = this.data.base;
                    break;
            }
        },
        loadAll() {
            // Load all base variable
            Variable.getAllBaseVariable().then(r => {
                this.data.base = r.data;
                this.data.show = this.data.base;
            }).finally(() => {
                // Load all condition variable
                Variable.getAllConditionVariable().then(r => {
                    this.data.condition = r.data;
                }).finally(() => {
                    // Load all calculate variable
                    Variable.getAllCalculateVariable().then(r => {
                        this.data.calculate = r.data;
                    }).finally(()=>{
                        this.isLoading = false;
                    });
                });
            });
        }
    },
    mounted() {
        this.loadAll();
    }
};