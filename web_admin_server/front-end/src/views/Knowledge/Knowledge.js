import AgentTable from './Agent/AgentTable.vue';
import IntentTable from './Intent/IntentTable.vue'

var Agent = require('../../api/Agent.js');
var Intent = require('../../api/Intent.js');
var Phrase = require('../../api/Phrase.js');
var Response = require('../../api/Response.js');

export default {
    components: {
        AgentTable,
        IntentTable,
    },
    data() {
        return {
            selectedCard: 0,
            attAgent: {
                total: "Chưa có"
            },
            attIntent: {
                total: "Chưa có"
            },
            attPhrase: {
                total: "Chưa có"
            },
            attResponse: {
                total: "Chưa có"
            },
            selectedAgentId: -1
        };
    },
    methods: {
        loadCount() {
            Agent.countAll().then(r=>{
                this.attAgent.total = (r.data.result == 0 ? "Chưa có" : r.data.result);
            });
            Intent.countAll().then(r=>{
                this.attIntent.total = (r.data.result == 0 ? "Chưa có" : r.data.result);
            });
            Phrase.countAll().then(r=>{
                this.attPhrase.total = (r.data.result == 0 ? "Chưa có" : r.data.result)
            });
            Response.countAll().then(r=> {
                this.attResponse.total = (r.data.result == 0 ? "Chưa có" : r.data.result);
            })
        },
        changeSelectedCard(index) {
            if (this.selectedCard == 2 && index == 1)
            {
                this.$eventBus.$emit('goback');
            } else if (this.selectedCard == 0) {
                this.selectedAgent = {};
            }
            this.selectedCard = index;
        }
    },
    mounted() {
        this.loadCount();
        this.$eventBus.$on('agentSelected', (event) => {
            this.selectedAgent = event.data;
            this.selectedCard = 1;
        });
    }
};