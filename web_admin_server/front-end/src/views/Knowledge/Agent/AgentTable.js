import { OrbitSpinner } from "epic-spinners";
import moment from "moment";
var Agent = require('../../../api/Agent.js');

export default {
    components: {
        OrbitSpinner
    },
    name: "agent-table",
    data() {
        return {
            isLoadingAgent: true,
            agents: [],
            modals: {
                isEdit: false,
                currAgentId: -1,
                addAgentModal: false,
                title: "Thêm mới phạm trù",
                ic: "fa fa-plus mr-2",
                fieldAName: "",
                fieldADescription: "",
                warning: "",
                finishDisable: "disabled"
            },
            detailsAgent: {
                show: false,
                currentAgent: {}
            },
            pagination: {
                value: 1
            },
            confrimDelete: {
                isShow: false,
                currentAgent: {},
            }
        };
    },
    methods: {
        openDeleteConfirm(crrAgent) {
            this.confrimDelete.currentAgent = crrAgent;
            this.confrimDelete.isShow = true;
        },
        openAdd() {
            this.modals.isEdit = false;
            this.modals.addAgentModal = true;
            this.modals.title = "Thêm mới phạm trù";
            this.modals.ic = "fa fa-plus mr-2";
            this.modals.fieldAName = "";
            this.modals.fieldADescription = "";
            this.modals.warning = "";
            this.modals.finishDisable = "disabled";
        },
        openEdit(curr) {
            this.modals.finishDisable = null;
            this.modals.title = "Chỉnh sửa phạm trù";
            this.modals.isEdit = true;
            this.modals.addAgentModal = true;
            this.modals.ic = "ni ni-ruler-pencil mr-2";
            this.modals.fieldAName = curr.a_name;
            this.modals.fieldADescription = curr.a_description;
            this.modals.warning = "";
            this.modals.currAgentId = curr.a_id;
        },
        loadAgents() {
            this.agents = [];
            Agent.getAll().then(r => {
                this.agents = r.data.result;
            })
                .finally(() => {
                    this.isLoadingAgent = false;
                });
        },
        async addOrUpdateAgent() {
            this.modals.addAgentModal = false;
            if (!this.modals.isEdit) {
                await Agent.add(this.modals.fieldAName, this.modals.fieldADescription).then(r => {
                    if (r.data.status + "" == "1") {
                        // Thêm thành công
                        this.$emit('reloadTotalCount');
                    } else {
                        // Thêm không thành công
                    }
                })
                    .catch(e => {
                        alert(e);
                    })
                    .finally(() => {
                        this.modals.fieldAName = "";
                        this.modals.fieldADescription = "";
                        this.loadAgents();
                    });
            } else {
                await Agent.update(this.modals.currAgentId, this.modals.fieldAName, this.modals.fieldADescription).then(r => {
                    if (r.data.status + "" == "1") {
                        // Thêm thành công
                    } else {
                        // Thêm không thành công
                    }
                })
                    .catch(e => {
                        alert(e);
                    })
                    .finally(() => {
                        this.modals.currAgentId = -1;
                        this.modals.fieldAName = "";
                        this.modals.fieldADescription = "";
                        this.loadAgents();
                    });
            }
        },
        inpDesChange() {
            if (this.modals.fieldADescription.length > 0) this.modals.warning = "";
            else
                this.modals.warning =
                    "Bạn nên nhập mô tả để giúp phạn trù trở nên rõ ràng hơn. Tuy nhiên vẫn có thể bỏ qua.";
        },
        inpNameChange() {
            if (this.modals.fieldAName.length <= 0) {
                this.modals.warning = "Vui lòng điền tên của phạm trù!";
                this.modals.finishDisable = "disabled";
            } else {
                this.modals.warning = "";
                this.modals.finishDisable = null;
            }
        },
        viewAgentDetail(crrAgent) {
            this.detailsAgent.currentAgent = crrAgent;
            this.detailsAgent.show = true;
        },
        removeAgent() {
            this.confrimDelete.isShow = false;
            Agent.remove(this.confrimDelete.currentAgent.a_id).then(r => {
                // Xóa thành công
                this.$emit('reloadTotalCount');
            }).finally(() => {
                this.loadAgents();
            });
        },
        extractTime(inpTime) {
            let crrDate = new Date(inpTime);
            let inpMoment = moment(crrDate);
            return inpMoment.format("h:mm:ss DD/MM/YYYY");
        },
        selectedAgent(agent) {
            this.$eventBus.$emit('agentSelected', { data: agent });
        }
    },
    created() {
        this.loadAgents();
    },
    mounted() {
    },
    computed: {
    },
    watch: {
        "pagination.value": function (newVal, oldVal) {
            // let curr = (parseInt(newVal) - 1) * 10;
            // this.pagination.currentItemShow = this.agents.slice(curr, curr + 10);
        }
    }
};