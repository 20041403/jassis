import PhraseTable from './Phrase/PhraseTable.vue';
import ResponseTable from './Response/ResponseTable.vue'

var Intent = require('../../../api/Intent.js');

export default {
    components: {
        PhraseTable,
        ResponseTable
    },
    props: {
        parentId: {
            parentId: Number,
            default: -1,
            description: "Chứa mã của loại hình cha, ở đây là Intent"
        }
    },
    name: "phrase-response-table",
    data() {
        return {
            currentIntent: {}
        };
    },
    methods: {
        getIntentDetail() {
            Intent.get(this.$props.parentId).then(r=>{
                this.currentIntent = r.data.result[0];
            });
        }
    },
    created() {
    },
    mounted() {
        this.getIntentDetail();
     }
};