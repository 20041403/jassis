import { OrbitSpinner } from "epic-spinners";
import moment from 'moment';

var Response = require('../../../../api/Response.js');

export default {
    components: {
        OrbitSpinner,
    },
    name: "response-table",
    props: {
        parentId: {
            parentId: Number,
            default: -1,
            description: "Chứa mã của loại hình cha, ở đây là Intent"
        }
    },
    data() {
        return {
            isLoading: true,
            currData: [],
            pagination: {
                value: 1
            },
            modalAddOrUpdate: {
                isShow: false,
                isEdit: false,
                icon: "ni ni-chat-round",
                title: "",
                warning: "",
                val1: "",
                val2: "",
                okDisable: "disable",
                hint1: "",
                currObject: {},
                currParent: {}
            },
            modalsConfirmDelete: {
                isShow: false,
                icon: "fa fa-trash",
                currObject: {}
            },
            pr: {
                selectedParent: {},
            }
        };
    },
    methods: {
        load() {
            if (this.$props.parentId == undefined || this.$props.parentId < 0) {
                Response.getAll().then(r => {
                    this.currData = r.data.result;
                }).finally(() => {
                    this.isLoading = false;
                });
            } else {
                Response.getByIntent(this.$props.parentId).then(r => {
                    this.currData = r.data.result;
                }).finally(() => {
                    this.isLoading = false;
                });
            }
        },
        openAdd() {
            this.modalAddOrUpdate.isShow = true;
            this.modalAddOrUpdate.isEdit = false;
            this.modalAddOrUpdate.title = "THÊM MẪU CÂU TRẢ LỜI";
            this.modalAddOrUpdate.warning = "";
            this.modalAddOrUpdate.val1 = "";
            this.modalAddOrUpdate.val2 = "";
            this.modalAddOrUpdate.okDisable = "disable";
            this.modalAddOrUpdate.hint1 = "Vui lòng nhập mẫu câu trả lời";
            this.modalAddOrUpdate.currParent = {};
        },
        openEdit(currObject) {
            let json = JSON.stringify(currObject.r_event);
            this.modalAddOrUpdate.currObject = currObject;
            this.modalAddOrUpdate.isShow = true;
            this.modalAddOrUpdate.isEdit = true;
            this.modalAddOrUpdate.title = `SỬA MẪU CÂU TRẢ LỜI (Mã số: ${currObject.r_id})`;
            this.modalAddOrUpdate.warning = "";
            this.modalAddOrUpdate.val1 = currObject.r_response;
            this.modalAddOrUpdate.val2 = json == "null" ? "": JSON.stringify(currObject.r_event);
            this.modalAddOrUpdate.okDisable = null;
            this.modalAddOrUpdate.hint1 = "Vui lòng nhập mẫu câu trả lời";
        },
        val1Change() {
            if (this.modalAddOrUpdate.val1.length <= 0) {
                this.modalAddOrUpdate.warning = "Vui lòng nhập mẫu câu trả lời!";
                this.modalAddOrUpdate.okDisable = "disabled";
            } else {
                this.modalAddOrUpdate.warning = "";
                this.modalAddOrUpdate.okDisable = null;
            }
        },
        val2Change() {
            if (this.modalAddOrUpdate.val2.length > 0)
            {
                try {
                    var tmpJson = JSON.parse(this.modalAddOrUpdate.val2);
                    this.modalAddOrUpdate.val2 = JSON.stringify(tmpJson, undefined, 4)
                    this.modalAddOrUpdate.warning = "";
                    this.modalAddOrUpdate.okDisable = null;
                    if (tmpJson.event == undefined) {
                        this.modalAddOrUpdate.warning = "JSON thiếu trường event, vui lòng bổ sung.";
                        this.modalAddOrUpdate.okDisable = "disabled";                        
                    }
                } catch(e) {
                    this.modalAddOrUpdate.warning = "Không đúng chuẩn JSON. " + e;
                    this.modalAddOrUpdate.okDisable = "disabled";
                }
            } else {
                this.modalAddOrUpdate.warning = "";
                this.modalAddOrUpdate.okDisable = null;
            }
        },
        addOrUpdate() {
            if (this.modalAddOrUpdate.val1.length <= 0) {
                this.modalAddOrUpdate.warning = "Vui lòng nhập nội dung câu trả lời trước";
                return;
            }
            let json = null;
            if (this.modalAddOrUpdate.val2.length > 0)
            {
                try {
                    JSON.parse(this.modalAddOrUpdate.val2);
                    json = this.modalAddOrUpdate.val2;
                } catch(e) {
                    this.modalAddOrUpdate.warning = "Vui lòng nhập mã lệnh đúng định dạng JSON cho ô sự kiện";
                    return;
                }
            } 
            this.modalAddOrUpdate.isShow = false;
            if (this.modalAddOrUpdate.isEdit) {
                // Nếu là sửa
                Response.update(this.modalAddOrUpdate.currObject.r_id, this.modalAddOrUpdate.val1, json)
                    .then(() => {
                        this.load(this.$props.parentId);
                    })
                    .finally(() => { });
            } else {
                // Nếu là thêm mới
                Response.add(this.modalAddOrUpdate.val1, json, this.$props.parentId)
                    .then(() => {
                        this.load(this.$props.parentId);
                        this.$emit('reloadTotalCount');
                    })
                    .finally(() => {
                    });
            }
        },
        openConfirmDelete(obj) {
            this.modalsConfirmDelete.currObject = obj;
            this.modalsConfirmDelete.isShow = true;
        },
        removeCurrent() {
            this.modalsConfirmDelete.isShow = false;
            Response.remove(this.modalsConfirmDelete.currObject.r_id).then(() => {
                this.load();
                this.$emit('reloadTotalCount');
            })
                .finally(() => {
                });
        }
    },
    created() {
        this.load();
    },
    mounted() { }
};