import { OrbitSpinner } from "epic-spinners";
import moment from 'moment';

var Phrase = require('../../../../api/Phrase.js');

export default {
    components: {
        OrbitSpinner,
    },
    name: "phrase-table",
    props: {
        parentId: {
            parentId: Number,
            default: -1,
            description: "Chứa mã của loại hình cha, ở đây là Intent"
        }
    },
    data() {
        return {
            isLoading: true,
            currData: [],
            pagination: {
                value: 1
            },
            modalsEditOrUpdate: {
                isShow: false,
                isEdit: false,
                icon: "ni ni-air-baloon",
                title: "",
                warning: "",
                val1: "",
                okDisable: "disable",
                hint1: "",
                currObject: {},
                currParent: {}
            },
            modalsConfirmDelete: {
                isShow: false,
                icon: "fa fa-trash",
                currObject: {}
            },
            pr: {
                selectedParent: {},
            }
        };
    },
    methods: {
        load() {
            if (this.$props.parentId == undefined || this.$props.parentId < 0) {
                Phrase.getAll().then(r => {
                    this.currData = r.data.result;
                }).finally(() => {
                    this.isLoading = false;
                });
            } else {
                Phrase.getByIntent(this.$props.parentId).then(r => {
                    this.currData = r.data.result;
                }).finally(() => {
                    this.isLoading = false;
                });
            }
        },
        openAdd() {
            this.modalsEditOrUpdate.isShow = true;
            this.modalsEditOrUpdate.isEdit = false;
            this.modalsEditOrUpdate.title = "THÊM MẪU CÂU HỎI";
            this.modalsEditOrUpdate.warning = "";
            this.modalsEditOrUpdate.val1 = "";
            this.modalsEditOrUpdate.okDisable = "disable";
            this.modalsEditOrUpdate.hint1 = "Vui lòng nhập mẫu câu hỏi";
            this.modalsEditOrUpdate.currParent = {};
        },
        openEdit(currObject) {
            this.modalsEditOrUpdate.currObject = currObject;
            this.modalsEditOrUpdate.isShow = true;
            this.modalsEditOrUpdate.isEdit = true;
            this.modalsEditOrUpdate.title = `SỬA MẪU CÂU HỎI (Mã số: ${currObject.p_id})`;
            this.modalsEditOrUpdate.warning = "";
            this.modalsEditOrUpdate.val1 = currObject.p_phrase;
            this.modalsEditOrUpdate.okDisable = null;
            this.modalsEditOrUpdate.hint1 = "Vui lòng nhập mẫu câu hỏi";
        },
        val1Change() {
            if (this.modalsEditOrUpdate.val1.length <= 0) {
                this.modalsEditOrUpdate.warning = "Vui lòng nhập mẫu câu hỏi!";
                this.modalsEditOrUpdate.okDisable = "disabled";
            } else {
                this.modalsEditOrUpdate.warning = "";
                this.modalsEditOrUpdate.okDisable = null;
            }
        },
        addOrUpdate() {
            if (this.modalsEditOrUpdate.val1.length <= 0) {
                this.modalsEditOrUpdate.warning = "Vui lòng nhập nội dung câu hỏi trước";
                return;
            }
            this.modalsEditOrUpdate.isShow = false;
            if (this.modalsEditOrUpdate.isEdit) {
                // Nếu là sửa
                Phrase.update(this.modalsEditOrUpdate.val1, this.modalsEditOrUpdate.currObject.p_id)
                    .then(() => {
                        this.load(this.$props.parentId);
                    })
                    .finally(() => { });
            } else {
                // Nếu là thêm mới
                Phrase.add(this.modalsEditOrUpdate.val1, this.$props.parentId)
                    .then(() => {
                        this.load(this.$props.parentId);
                        this.$emit('reloadTotalCount');
                    })
                    .finally(() => {
                    });
            }
        },
        openConfirmDelete(obj) {
            this.modalsConfirmDelete.currObject = obj;
            this.modalsConfirmDelete.isShow = true;
        },
        removeCurrent() {
            this.modalsConfirmDelete.isShow = false;
            Phrase.remove(this.modalsConfirmDelete.currObject.p_id).then(() => {
                this.load();
                this.$emit('reloadTotalCount');
            })
                .finally(() => {
                });
        }
    },
    created() {
        this.load();
    },
    mounted() { }
};