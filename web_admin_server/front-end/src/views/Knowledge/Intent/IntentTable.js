import { OrbitSpinner } from "epic-spinners";
import moment from 'moment'
import PhraseResponseTable from '../PhraseResponse/PhraseResponse.vue'

var Intent = require('../../../api/Intent.js');
var Agent = require('../../../api/Agent.js');


export default {
    components: {
        OrbitSpinner,
        PhraseResponseTable
    },
    name: "intent-table",
    props:
    {
        agent: {
            agent: Object,
            default: -1,
            description: "Chứa mã của phạm trù được truyền sang"
        }
    },
    data() {
        return {
            isLoading: true,
            currData: [],
            pagination: {
                value: 1
            },
            modalsEditOrUpdate: {
                isShow: false,
                isEdit: false,
                icon: "ni ni-tag",
                title: "THÊM Ý ĐỊNH",
                warning: "",
                val1: "",
                val2: "",
                okDisable: "disable",
                hint1: "",
                hint2: "",
                currObject: {},
                currParent: {}
            },
            modalsDetail: {
                isShow: false,
                icon: "ni ni-tag",
                currObject: {}
            },
            modalsConfirmDelete: {
                isShow: false,
                icon: "fa fa-trash",
                currObject: {}
            },
            modalsSelectParent: {
                isDisableButton: null,
                isShow: false,
                icon: "ni ni-hat-3",
                parents: [],
                currObject: {}
            },
            pr: {
                selectedParent: {},
            }
        };
    },
    methods: {
        load(aId) {
            if (this.$props.agent != undefined && this.$props.agent.a_id != undefined && this.$props.agent.a_id >= 0) {
                aId = this.$props.agent.a_id;
                this.modalsSelectParent.currObject = this.$props.agent;
            }
            if (aId == undefined || aId < 0) {
                Intent.getAll().then(r => {
                    this.currData = r.data.result;
                }).finally(() => {
                    this.isLoading = false;
                });
            } else {
                Intent.getByAgent(aId).then(r => {
                    this.currData = r.data.result;
                }).finally(() => {
                    this.isLoading = false;
                });
            }
        },
        openAdd() {
            this.modalsEditOrUpdate.isShow = true;
            this.modalsEditOrUpdate.isEdit = false;
            this.modalsEditOrUpdate.title = "THÊM Ý ĐỊNH";
            this.modalsEditOrUpdate.warning = "";
            this.modalsEditOrUpdate.val1 = "";
            this.modalsEditOrUpdate.val2 = "";
            this.modalsEditOrUpdate.okDisable = "disable";
            this.modalsEditOrUpdate.hint1 = "Vui lòng nhập tên của ý định";
            this.modalsEditOrUpdate.hint2 = "Vui lòng nhập mô tả cho ý định này...";
            this.modalsEditOrUpdate.currParent = {};
        },
        openEdit(currObject) {
            this.modalsEditOrUpdate.currObject = currObject;
            this.modalsEditOrUpdate.isShow = true;
            this.modalsEditOrUpdate.isEdit = true;
            this.modalsEditOrUpdate.title = `SỬA Ý ĐỊNH (Mã số: ${currObject.i_id})`;
            this.modalsEditOrUpdate.warning = "";
            this.modalsEditOrUpdate.val1 = currObject.i_name;
            this.modalsEditOrUpdate.val2 = currObject.i_description;
            this.modalsEditOrUpdate.okDisable = null;
            this.modalsEditOrUpdate.hint1 = "Vui lòng nhập tên của ý định";
            this.modalsEditOrUpdate.hint2 = "Vui lòng nhập mô tả cho ý định này...";
            this.modalsEditOrUpdate.currParent = this.modalsSelectParent.parents.filter(
                function (item) {
                    return item.a_id == currObject.a_id;
                }
            )[0];
        },
        val2Change() {
            if (this.modalsEditOrUpdate.val2.length > 0) this.modalsEditOrUpdate.warning = "";
            else
                this.modalsEditOrUpdate.warning =
                    "Bạn nên nhập mô tả để giúp ý định trở nên rõ ràng hơn. Tuy nhiên vẫn có thể bỏ qua.";
        },
        val1Change() {
            if (this.modalsEditOrUpdate.val1.length <= 0) {
                this.modalsEditOrUpdate.warning = "Vui lòng điền tên của ý định!";
                this.modalsEditOrUpdate.okDisable = "disabled";
            } else {
                this.modalsEditOrUpdate.warning = "";
                this.modalsEditOrUpdate.okDisable = null;
            }
        },
        addOrUpdate() {
            if (this.modalsEditOrUpdate.currParent.a_id == undefined) {
                this.modalsEditOrUpdate.warning = "Vui lòng chọn phạm trù cho ý định này";
                return;
            }
            this.modalsEditOrUpdate.isShow = false;
            if (this.modalsEditOrUpdate.isEdit) {
                // Nếu là sửa
                Intent.update(this.modalsEditOrUpdate.currObject.i_id, this.modalsEditOrUpdate.val1, this.modalsEditOrUpdate.val2, this.modalsEditOrUpdate.currParent.a_id)
                    .then(() => {
                        this.load(this.modalsDetail.currObject.a_id);
                    })
                    .finally(() => { });
            } else {
                // Nếu là thêm mới
                Intent.add(this.modalsEditOrUpdate.val1, this.modalsEditOrUpdate.val2, this.modalsEditOrUpdate.currParent.a_id)
                    .then(() => {
                        this.load(this.modalsDetail.currObject.a_id);
                        this.$emit('reloadTotalCount');
                    })
                    .finally(() => {
                    });
            }
        },
        extractTime(inpTime) {
            let crrDate = new Date(inpTime);
            let inpMoment = moment(crrDate);
            return inpMoment.format("h:mm:ss DD/MM/YYYY");
        },
        openDetail(obj) {
            this.modalsDetail.currObject = obj;
            this.modalsDetail.isShow = true;
        },
        openConfirmDelete(obj) {
            this.modalsConfirmDelete.currObject = obj;
            this.modalsConfirmDelete.isShow = true;
        },
        openParentSelect() {
            this.modalsSelectParent.isDisableButton = "disable";
            this.loadAllParent();
            this.modalsSelectParent.isShow = true;
        },
        loadAllParent() {
            Agent.getAll().then(r => {
                this.modalsSelectParent.parents = r.data.result;
            })
                .finally(() => {
                    this.modalsSelectParent.isDisableButton = null;
                });
        },
        selectThisParent(parent) {
            this.modalsSelectParent.currObject = parent;
            this.modalsSelectParent.isShow = false;
            this.load(parent.a_id);
        },
        selectAllParent() {
            this.modalsSelectParent.currObject = {};
            this.modalsSelectParent.isShow = false;
            this.load();
        },
        dropboxSelectedParent(parent) {
            this.modalsEditOrUpdate.currParent = parent;
            this.modalsEditOrUpdate.warning = "";
        },
        removeCurrent() {
            this.modalsConfirmDelete.isShow = false;
            Intent.remove(this.modalsConfirmDelete.currObject.i_id).then(() => {
                this.$emit('reloadTotalCount');
                this.load();
            })
                .finally(() => {
                });
        },
        openIntent(curr) {
            this.pr.selectedParent = curr;
            this.$emit('selectedIntent');
        },
        goBack() {
            this.pr.selectedParent = {}
        }
    },
    created() {
        this.load();
        this.loadAllParent();
        this.$eventBus.$on('goback', this.goBack);
    },
    mounted() {
    }
};