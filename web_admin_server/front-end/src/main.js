/*!

=========================================================
* Vue Argon Dashboard - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'
import VueCookies from 'vue-cookies'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/index.css';
Vue.use(VueToast);
Vue.use(VueCookies)

Vue.config.productionTip = false
Vue.use(ArgonDashboard)
const EventBus = new Vue()
// const VueCookies = require('vue-cookies');
// Vue.prototype.$cookies = VueCookies;
Vue.prototype.$eventBus = EventBus

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
