import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
var UserAccount = require('./api/UserAccount.js')
import Storage from './utils/storage.js'

Vue.use(Router)

let router = new Router({
  linkExactActiveClass: 'active',
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: 'login',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboard',
          component: () => import(/* webpackChunkName: "demo" */ './views/Dashboard.vue')
        },
        {
          path: '/knowledge',
          name: 'knowledge',
          component: () => import('./views/Knowledge/Knowledge.vue'),
          meta: {
            auth: true,
            isAdmin: true
          }
        },
        {
          path: '/expression',
          name: 'expression',
          component: () => import('./views/Expression/Expression.vue'),
          meta: {
            auth: true,
            isAdmin: true
          }
        },
        {
          path: '/communication-history',
          name: 'communication-history',
          component: () => import('./views/CommunicationHistory/CommunicationHistory.vue'),
          meta: {
            auth: true,
            isAdmin: true
          }
        },
        {
          path: '/audio-stored',
          name: 'audio-stored',
          component: () => import('./views/AudioStored/AudioStored.vue'),
          meta: {
            auth: true,
            isAdmin: true
          }
        },
        {
          path: '/users',
          name: 'users',
          component: () => import('./views/Users/Users.vue'),
          meta: {
            auth: true,
            isAdmin: true
          }
        },
        {
          path: '/icons',
          name: 'icons',
          component: () => import(/* webpackChunkName: "demo" */ './views/Icons.vue')
        },
        {
          path: '/profile',
          name: 'profile',
          component: () => import(/* webpackChunkName: "demo" */ './views/UserProfile.vue')
        },
        {
          path: '/maps',
          name: 'maps',
          component: () => import(/* webpackChunkName: "demo" */ './views/Maps.vue')
        },
        {
          path: '/tables',
          name: 'tables',
          component: () => import(/* webpackChunkName: "demo" */ './views/Tables.vue')
        }
      ]
    },
    {
      path: '/',
      redirect: 'login',
      component: AuthLayout,
      children: [
        {
          path: '/login',
          name: 'login',
          component: () => import('./views/Login.vue'),
          meta: {
            guest: true
          }
        },
        {
          path: '/register',
          name: 'register',
          component: () => import('./views/Register.vue'),
          meta: {
            guest: true
          }
        }
      ]
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.auth) || to.matched.some(record => record.meta.guest)) {
    await UserAccount.tokenVerify().then(r => {
      let user = r.data.result;
      if (to.matched.some(record => record.meta.auth)) {
        if (to.matched.some(record => record.meta.isAdmin)) {
          UserAccount.getAccount(user.id).then(r => {
            if (Storage.checkIsRoles(Storage.roles.adminRoles, r.data.result[0].ua_roles)) {
              next()
            }
            else {
              next({ name: 'register' })
            }
          });
        } else {
          next()
        }
      } else {
        // Nếu đã đăng nhập mà còn cố vô mấy trang đăng ký
        if (to.name == 'login' || to.name == 'register') {
          UserAccount.getAccount(user.id).then(r => {
            if (Storage.checkIsRoles(Storage.roles.adminRoles, r.data.result[0].ua_roles)) {
              next({ name: 'knowledge' })
            }
            else {
              next()
            }
          });
        }
      }
    }).catch(e => {
      if (e.response != undefined && e.response.status != undefined && e.response.status == 401 && from.name != "login") {
        if (to.path != '/login' && to.path != '/register') {
          next('/login')
        } else {
          next();
        }
      } else
        next();
    })
  } else {
    next({ name: 'dashboard' })
  }
})

export default router