import JAxios from '../utils/jaxios';
import API from "../api/API";

export function getAll() {
    return JAxios.get(API.base64Audio.getAll);
}

export function getMaleVoice() {
    return JAxios.get(API.base64Audio.getMaleVoice);
}

export function getFemaleVoice() {
    return JAxios.get(API.base64Audio.getFemaleVoice);
}

export function getAudio(audioId) {
    return JAxios.get(API.base64Audio.getAudio,{
        params:  {
            as_id: audioId
        }
    });
}
export function countAll() {
    return JAxios.get(API.base64Audio.countAll);
}
export function countMaleVoice() {
    return JAxios.get(API.base64Audio.countMaleVoice);
}
export function countFemaleVoice() {
    return JAxios.get(API.base64Audio.countFemaleVoice);
}
export function remove(id) {
    return JAxios.delete(API.base64Audio.delete, {
        params: {
            as_id: id
        }
    });
}