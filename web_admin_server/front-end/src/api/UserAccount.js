import JAxios from '../utils/jaxios';
import API from "./API";

export function add(username, password, email) {
    return JAxios.post(API.userAccount.add, {
        ua_username: username,
        ua_password: password,
        ua_email: email
    });
}

export function checkEmail(email) {
    return JAxios.get(API.userAccount.checkEmail, {
        params: {
            email: email
        }
    });
}

export function checkUsername(username) {
    return JAxios.get(API.userAccount.checkUsername, {
        params: {
            username: username
        }
    });
}

export function login(usernameOrEmail, password) {
    if (usernameOrEmail.includes("@")) {
        return JAxios.post(API.userAccount.login, {
            email: usernameOrEmail,
            password: password,
        });
    } else {
        return JAxios.post(API.userAccount.login, {
            username: usernameOrEmail,
            password: password,
        });
    }
}

export function logout() {
    return JAxios.get(API.userAccount.logout);
}

export function tokenVerify() {
    return JAxios.get(API.userAccount.tokenVerify);
}

export function changePassword(oldPass, newPass) {
    return JAxios.put(API.userAccount.changePassword, {
        oldPass: oldPass,
        newPass: newPass
    });
}

export function loadAll() {
    return JAxios.get(API.userAccount.getAll);
}

export function countAll() {
    return JAxios.get(API.userAccount.count);
}

export function getAllRoles() {
    return JAxios.get(API.userAccount.getAllRoles);
}

export function getProfile(uId) {
    return JAxios.get(API.userAccount.getProfileById, {
        params: {
            up_id: uId
        }
    });
}

export function getAccount(uId) {
    return JAxios.get(API.userAccount.getUserAccountById, {
        params: {
            ua_id: uId
        }
    })
}

export function getUserByRoles(roleCodes) {
    return JAxios.get(API.userAccount.getUserByRoles, {
        params: {
            role_codes: `{${roleCodes.toString()}}`
        }
    })
}

export function updateProfile(uProfile) {
    return JAxios.put(API.userAccount.updateProfile, uProfile);
}

export function deleteAccountAndProfile(uId) {
    return JAxios.delete(API.userAccount.delete, {
        params: {
            ua_id: uId
        }
    })
}

export function countUserByRoles(roleCodes) {
    return JAxios.get(API.userAccount.countUserByRoles, {
        params: {
            role_codes: `{${roleCodes.toString()}}`
        }
    })
}

export function changePasswordByAdmin(ua_username, ua_password) {
    return JAxios.put(API.userAccount.changePasswordByAdmin, {
        ua_username: ua_username,
        ua_password: ua_password
    })
}

export function updateAvatar(imageData, username, id) {
    return JAxios.put(API.userAccount.updateAvatar, imageData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        params: {
            username,
            id
        }
    })
}