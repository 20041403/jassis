import JAxios from '../utils/jaxios';
import API from "./API";

export function add(rResponse, rEvent, iId) {
    return JAxios.post(API.response.add, {
        r_response: rResponse,
        r_event: rEvent, 
        i_id: iId
    });
}

export function getAll() {
    return JAxios.get(API.response.getAll);
}

export function get(rId) {
    return JAxios.get(API.response.get, {
        params: {
            r_id: rId
        }
    });
}

export function getByIntent(iId) {
    return JAxios.get(API.response.getAll, {
        params: {
            i_id: iId
        }
    });
}

export function countAll() {
    return JAxios.get(API.response.count);
}

export function update(rId, rResponse, rEvent) {
    return JAxios.put(API.response.update, {
        r_id: rId,
        r_response: rResponse,
        r_event: rEvent
    });
}

export function remove(rId) {
    return JAxios.delete(API.response.delete, {
        params: {
            r_id: rId
        }
    });
}