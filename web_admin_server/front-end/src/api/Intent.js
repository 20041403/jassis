import JAxios from '../utils/jaxios';
import API from "../api/API";

export function add(iName, iDescription, aId) {
    return JAxios.post(API.intent.add, {
        i_name: iName,
        i_description: iDescription,
        a_id: aId
    });
}

export function getAll() {
    return JAxios.get(API.intent.getAll);
}

export function get(iId) {
    return JAxios.get(API.intent.get, {
        params: {
            i_id: iId
        }
    });
}

export function getByAgent(aId) {
    return JAxios.get(API.intent.getAll, {
        params: {
            a_id: aId
        }
    });
}

export function countAll() {
    return JAxios.get(API.intent.count);
}

export function update(iId, iName, iDescription, aId) {
    return JAxios.put(API.intent.update, {
        i_id: iId,
        i_name: iName,
        i_description: iDescription,
        a_id: aId
    });
}

export function remove(iId) {
    return JAxios.delete(API.intent.delete, {
        params: {
            i_id: iId
        }
    });
}