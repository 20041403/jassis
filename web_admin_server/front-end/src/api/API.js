export default {
    app: {
        google_play_store: "",
        app_store: ""
    },
    agent: {
        count: "agent/count",
        add: "agent/add",
        get: "agent/get",
        getAll: "agent/get_all",
        totalIntents: "agent/total_intents",
        totalPhrases: "agent/total_phrases",
        totalResponses: "agent/total_responses",
        update: "agent/update",
        delete: "agent/delete",
    },
    intent: {
        count: "intent/count",
        add: "intent/add",
        get: "intent/get",
        getAll: "intent/get_all",
        update: "intent/update",
        delete: "intent/delete",
    },
    phrase: {
        count: "phrase/count",
        add: "phrase/add",
        get: "phrase/get",
        getAll: "phrase/get_all",
        update: "phrase/update",
        delete: "phrase/delete",
    },
    response: {
        count: "response/count",
        add: "response/add",
        get: "response/get",
        getAll: "response/get_all",
        update: "response/update",
        delete: "response/delete",
    },
    userAccount: {
        count: "user_account/count",
        add: "user_account/add",
        login: "user_account/login",
        logout: "user_account/logout",
        changePassword: "user_account/change_password",
        tokenVerify: "user_account/token_verify",
        checkEmail: "user_account/check_email",
        checkUsername: "user_account/check_username",
        getAll: "user_account/get_all",
        getAllRoles: "user_account/get_all_roles",
        getProfileById: "user_account/profile",
        getUserAccountById: "user_account/account",
        updateProfile: "user_account/update_profile",
        delete: "user_account/delete",
        getUserByRoles: "user_account/account_by_roles",
        countUserByRoles: "user_account/count_user_by_roles",
        changePasswordByAdmin: "user_account/change_password_by_admin",
        updateAvatar: "user_account/update_avatar",
    },
    receivedPhrase: {
        countAll: "received_phrase/count_all",
        countResponsed: "received_phrase/count_responsed",
        countNotYet: "received_phrase/count_not_yet",
        getAll: "received_phrase/get_all",
        getResponsed: "received_phrase/get_responsed",
        getNotYet: "received_phrase/get_not_yet"
    },
    base64Audio: {
        getAll: "base64_audio/get_all",
        getAudio: "base64_audio/get_audio", 
        countAll: "base64_audio/count_all",
        countFemaleVoice: "base64_audio/count_female_voice",
        countMaleVoice: "base64_audio/count_male_voice",
        getMaleVoice: "base64_audio/get_male_voice",
        getFemaleVoice: "base64_audio/get_female_voice",
        delete: "base64_audio/delete"
    },
    variable: {
        getAllBaseVariable: "variable/get_base_variable",
        getAllConditionVariable: "variable/get_condition_variable",
        getAllCalculateVariable: "variable/get_calculate_variable"
    }
};