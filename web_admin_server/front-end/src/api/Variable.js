import JAxios from '../utils/jaxios';
import API from "../api/API";

export function getAllBaseVariable() {
    return JAxios.get(API.variable.getAllBaseVariable);
}

export function getAllConditionVariable() {
    return JAxios.get(API.variable.getAllConditionVariable);
}

export function getAllCalculateVariable() {
    return JAxios.get(API.variable.getAllCalculateVariable);
}