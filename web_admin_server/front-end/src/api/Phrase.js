import JAxios from '../utils/jaxios';
import API from "./API";

export function add(pPhrase, iId) {
    return JAxios.post(API.phrase.add, {
        p_phrase: pPhrase,
        i_id: iId
    });
}

export function getAll() {
    return JAxios.get(API.phrase.getAll);
}

export function get(pId) {
    return JAxios.get(API.phrase.get, {
        params: {
            p_id: pId
        }
    });
}

export function getByIntent(iId) {
    return JAxios.get(API.phrase.getAll, {
        params: {
            i_id: iId
        }
    });
}

export function countAll() {
    return JAxios.get(API.phrase.count);
}

export function update(pPhrase, pId) {
    return JAxios.put(API.phrase.update, {
        p_phrase: pPhrase,
        p_id: pId
    });
}

export function remove(pId) {
    return JAxios.delete(API.phrase.delete, {
        params: {
            p_id: pId
        }
    });
}