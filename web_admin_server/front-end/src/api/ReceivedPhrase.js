import JAxios from '../utils/jaxios.js';
import API from "./API";

export function countAll() {
    return JAxios.get(API.receivedPhrase.countAll);
}
export function countNotYet() {
    return JAxios.get(API.receivedPhrase.countNotYet);
}
export function countResponsed() {
    return JAxios.get(API.receivedPhrase.countResponsed);
}
export function getAll() {
    return JAxios.get(API.receivedPhrase.getAll);
}
export function getNotYet() {
    return JAxios.get(API.receivedPhrase.getNotYet);
}
export function getResponsed() {
    return JAxios.get(API.receivedPhrase.getResponsed);
}