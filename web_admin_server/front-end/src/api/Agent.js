import JAxios from '../utils/jaxios';
import API from "../api/API";

export function add(aName, aDescription) {
    return JAxios.post(API.agent.add, {
        a_name: aName,
        a_description: aDescription
    });
}

export function getAll() {
    return JAxios.get(API.agent.getAll);
}

export function get(aId) {
    return JAxios.get(API.agent.get, {
        params: {
            a_id: aId
        }
    });
}

export function countAll() {
    return JAxios.get(API.agent.count);
}

export function update(aId, aName, aDescription) {
    return JAxios.put(API.agent.update, {
        a_id: aId,
        a_name: aName,
        a_description: aDescription
    });
}

export function remove(aId) {
    return JAxios.delete(API.agent.delete, {
        params: {
            a_id: aId
        }
    });
}