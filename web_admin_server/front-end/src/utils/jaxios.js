import axios from 'axios'

var JAxios = axios.create({ // Không có dấu '/' ở cuối
    baseURL: "http://45.76.188.27:2000",
    // baseURL: "http://localhost:2000",
    timeout: 5000
});

JAxios.interceptors.request.use((config) => {
    let token = window.$cookies.get("access_token");
    if (token) {
        config.headers['access_token'] = token;
    }
    return config;
})

export default JAxios;