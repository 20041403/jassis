export default {
    gender: [
        {
            code: -1,
            name: "Chưa cập nhật",
            call: "Người này"
        },
        {
            code: 0,
            name: "Nam",
            call: "Anh ấy"
        },
        {
            code: 1,
            name: "Nữ",
            call: "Cô ấy"
        },
        {
            code: 2,
            name: "Giữ bí mật",
            call: "Người này"
        }
    ],
    getGender: function (gCode) {
        for (let i = 0; i < this.gender.length; i++) {
            if (gCode == null || gCode == undefined) {
                return {
                    code: -1,
                    name: "Vui lòng chọn giới tính",
                    call: "Người này"
                };
            } else if (gCode == this.gender[i].code)
                return this.gender[i];
        }
    },
    roles: {
        adminRoles: [100, 1000000],
        contentManager: [200]
    },
    checkIsRoles: function (roles, current_user_roles) {
        for (let i = 0; i , roles.length;i++)
        {
            if (current_user_roles == roles[i])
            return true;
        }
        return false;
    }
}