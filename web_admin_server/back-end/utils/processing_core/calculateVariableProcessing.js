const config = require('./config.js');
const CalculateVariable = require('./calculateVariable.js');

module.exports = (msg) => {
    for (let i = 0; i < CalculateVariable.length;i++)
    {
        if (msg.indexOf(CalculateVariable[i].regex))
        {
            msg = CalculateVariable[i].get(msg);
        }
    }
    return msg;
}