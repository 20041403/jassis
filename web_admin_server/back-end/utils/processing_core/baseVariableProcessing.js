const config = require('./config.js');
const BaseVariable = require('./baseVariable.js');

module.exports = (msg) => {
    for (let i = 0; i < BaseVariable.length;i++)
    {
        if (msg.indexOf(BaseVariable[i].regex))
        {
            msg = BaseVariable[i].get(msg);
        }
    }
    return msg;
}