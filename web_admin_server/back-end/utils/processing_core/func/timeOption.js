exports.getCurrentTime = () => {
    let time = new Date();
    return time.getHours() + " giờ " + time.getMinutes() + " phút";
}

exports.getHours = () => {
    let time = new Date();
    return time.getHours() + " giờ";
}

exports.getMinutes = () => {
    let time = new Date();
    return time.getMinutes() + " phút";
}

exports.getSeconds = () => {
    let time = new Date();
    return time.getSeconds() + " giây";
}

exports.getDate = () => {
    let time = new Date();
    return "ngày " + time.getDate() + " tháng " + time.getMonth() + " năm " + time.getFullYear();
}

exports.getOnlyDate = () => {
    let time = new Date();
    return "ngày " + time.getDate();
}

exports.getMonth = () => {
    let time = new Date();
    return "tháng " + time.getMonth();
}

exports.getYear = () => {
    let time = new Date();
    return "năm " + time.getFullYear();
}

exports.isLeapYear = () => {
    // Năm nhuận là năm chia hết cho 4,
    // Nếu đã chia hết cho 100 thì cũng phải chia hết cho 400
    let time = new Date();
    if (time.getYear() % 4 == 0) {
        if (time.getYear() % 100 == 0) {
            if (time.getYear() % 400 == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    } else {
        return false;
    }
}

exports.getYearName = () => {
    can = ['Canh', 'Tân', 'Nhâm,', 'Quý', 'Giáp', 'Ất', 'Bính', 'Đinh', 'Mậu', 'Kỷ'];
    chi = ['Thân', 'Dậu', 'Tuất', 'Hợi', 'Tí', 'Sửu', 'Dần', 'Mão', 'Thìn', 'Tị', 'Ngọ', "Mùi"];
    let time = new Date();
    return can[time.getFullYear() % 10] + " " + chi[time.getFullYear() % 12];
}

exports.getDay = () => {
    var days = ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ 7'];
    let time = new Date();
    return days[time.getDay()];
}

exports.getCurrentTime12 = () => {
    let time = new Date();
    let endStr = "";
    if (this.isMorning()) {
        endStr = "sáng";
    } else if (this.isNoon()) {
        endStr = "trưa";
    } else if (this.isAfternoon()) {
        endStr = "chiều";
    } else if (this.isEvening()) {
        endStr = "tối";
    } else if (this.isNight()) {
        if (time.getHours() < 4) {
            endStr = "mờ sáng";
        } else if (time.getHours() >= 20 && time.getHours() <= 23) {
            endStr = "tối";
        } else {
            endStr = "khuya";
        }
    }
    return (time.getHours() < 12 ? time.getHours() : time.getHours() - 12) + " giờ " + time.getMinutes() + " phút " + endStr;
}

exports.isMorning = () => {
    let time = new Date();
    return (time.getHours() >= 4 && time.getHours() < 10);
}

exports.isNoon = () => {
    let time = new Date();
    return (time.getHours() >= 10 && time.getHours() < 13);
}

exports.isAfternoon = () => {
    let time = new Date();
    return (time.getHours() >= 13 && time.getHours() < 17);
}

exports.isEvening = () => {
    let time = new Date();
    return (time.getHours() >= 17 && time.getHours() < 20);
}

exports.isNight = () => {
    let time = new Date();
    return (time.getHours() >= 20 || time.getHours() < 4);
}