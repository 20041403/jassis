exports.getDataFromConditon = (msg, regexToFind, regexToRemove) => {
    let m;
    while ((m = regexToFind.exec(msg)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regexToFind.lastIndex) {
            regexToFind.lastIndex++;
        }

        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
            matchClone = match; // Nhân bản biểu thức tìm được
            matchClone = matchClone.replace(regexToRemove, ""); // Tiến hành tách lấy kết quả
            // Thay thế biến đó bằng kết quả vào trong chuỗi cho đến khi hết biểu thức
            while (msg.indexOf(match) >= 0) {
                msg = msg.replace(match, matchClone);
            }
        });
    }
    return msg.trim();
}