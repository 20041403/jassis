const fs = require('fs');
const path = require('path');

func = {}

fs.readdirSync(__dirname).forEach((file) => {
    let current_path = path.join(__dirname, file);
    if (current_path != __filename) {
        temp = require(current_path);
        Object.assign(func, temp);
    }
})

module.exports = func;