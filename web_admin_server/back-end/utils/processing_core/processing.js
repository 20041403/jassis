const config = require('./config.js');
const BaseVariableProcessing = require('./baseVariableProcessing.js');
const ConditionVariable = require('./conditionVariableProcessing.js');
const CalculateVariable = require('./calculateVariableProcessing.js');

module.exports = (msg) => {
    if (msg.match(config.format)) {
        // Nếu có định dạng của dấu hiệu thực thi biến thì tiến hành thực thi
        msg = BaseVariableProcessing(msg); // Bộ xử lý biến cơ bản
        msg = ConditionVariable(msg);   // Bộ xử lý biến điều kiện
        msg = CalculateVariable(msg);   // Bộ xử lý các biểu thức biến tính toán
        msg = msg.replace(/`{|}`/g, "");
    } else {
        // Nếu không có dấu hiệu chỉ định biểu thức thực thi biến thì không làm gì cả
    }
    // Xóa 2 dấu cách
    while (msg.indexOf("  ") >= 0) {
        msg = msg.replace(/  /g, "");
    }
    // Xóa cách đầu cuối và trả về
    return msg.trim(); // Trả về kết quả sau cùng sau khi được xử lý
}