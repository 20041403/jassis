const config = require('./config.js');
const ConditionVariable = require('./conditionVariable.js');

module.exports = (msg) => {
    for (let i = 0; i < ConditionVariable.length;i++)
    {
        if (msg.match(ConditionVariable[i].regex))
        {
            msg = ConditionVariable[i].get(msg);
        }
    }
    return msg;
}