const Func = require('./func/func.js');

module.exports = [
    {
        code: "@CURRENT_TIME",
        regex: "@CURRENT_TIME",
        description: "Lấy thời gian hiện tại từ máy chủ. Dạng: Giờ:Phút:Giây (24 tiếng).",
        get: function (msg) {
            return msg.replace(this.code, Func.getCurrentTime());
        }
    },
    {
        code: "@CURRENT_TIME_12",
        regex: "@CURRENT_TIME_12",
        description: "Lấy thời gian hiện tại từ máy chủ. Dạng: Giờ:Phút:Giây (12 tiếng).",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@CURRENT_HOUR",
        regex: "@CURRENT_HOUR",
        description: "Lấy giờ hiện tại từ máy chủ. Dạng: Giờ 12 tiếng.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@CURRENT_MINUTE",
        regex: "@CURRENT_MINUTE",
        description: "Lấy phút hiện tại từ máy chủ.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@CURRENT_SECOND",
        regex: "@CURRENT_SECOND",
        description: "Lấy giây hiện tại từ máy chủ.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@CURRENT_DATE",
        regex: "@CURRENT_DATE",
        description: "Lấy ngày tháng năm hiện tại từ máy chủ. Dạng: Ngày/Tháng/Năm.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@CURRENT_DAY_NAME",
        regex: "@CURRENT_DAY_NAME",
        description: "Lấy tên hiện tại của ngày hôm nay.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@CURRENT_USER_ADDRESS",
        regex: "@CURRENT_USER_ADDRESS",
        description: "Địa chỉ mà người dùng đang tại đó.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@USER_COUNTRY",
        regex: "@USER_COUNTRY",
        description: "Quốc gia cuối dùng người dùng đặt chân đến. (Nếu ở một chỗ thì lấy quốc gia của người dùng)",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@USER_PROVINCE",
        regex: "@USER_PROVINCE",
        description: "Lấy tên tỉnh thành người dùng đang tại đó.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@USER_CITY",
        regex: "@USER_CITY",
        description: "Lấy tên thành phố hoặc thị trấn mà người dùng đang tại đó người dùng đang tại đó.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@USER_HOME_ADDRESS",
        regex: "@USER_HOME_ADDRESS",
        description: "Địa chỉ thường trú của người dùng.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
    {
        code: "@USER_WORK_ADDRESS",
        regex: "@USER_WORK_ADDRESS",
        description: "Địa chỉ nơi làm việc của người dùng.",
        get: function (msg) {
            return msg.replace(this.code, "hihi haha");
        }
    },
]