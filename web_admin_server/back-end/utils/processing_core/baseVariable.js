const Func = require('./func/func.js');

module.exports = [
    {
        code: "@CURRENT_TIME",
        regex: /@CURRENT_TIME\b/gm,
        remove: /@CURRENT_TIME\b/g,
        description: "Lấy thời gian hiện tại từ máy chủ. Dạng: Giờ:Phút:Giây (24 tiếng).",
        get: function (msg) {
            return msg.replace(this.regex, Func.getCurrentTime());
        }
    },
    {
        code: "@CURRENT_TIME_12",
        regex: /@CURRENT_TIME_12\b/gm,
        remove: /@CURRENT_TIME_12\b/g,
        description: "Lấy thời gian hiện tại từ máy chủ. Dạng: Giờ:Phút:Giây (12 tiếng).",
        get: function (msg) {
            return msg.replace(this.regex, Func.getCurrentTime12());
        }
    },
    {
        code: "@CURRENT_HOURS",
        regex: /@CURRENT_HOURS\b/gm,
        remove: /@CURRENT_HOURS\b/g,
        description: "Lấy giờ hiện tại từ máy chủ. Dạng: Giờ 24 tiếng.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getHours());
        }
    },
    {
        code: "@CURRENT_MINUTES",
        regex: /@CURRENT_MINUTES\b/gm,
        remove: /@CURRENT_MINUTES\b/g,
        description: "Lấy phút hiện tại từ máy chủ.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getMinutes());
        }
    },
    {
        code: "@CURRENT_SECONDS",
        regex: /@CURRENT_SECONDS\b/gm,
        remove: /@CURRENT_SECONDS\b/g,
        description: "Lấy giây hiện tại từ máy chủ.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getSeconds());
        }
    },
    {
        code: "@CURRENT_DATE",
        regex: /@CURRENT_DATE\b/gm,
        remove: /@CURRENT_DATE\b/g,
        description: "Lấy ngày tháng năm hiện tại từ máy chủ. Dạng: Ngày X Tháng X Năm XXXX.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getDate());
        }
    },
    {
        code: "@CURRENT_ONLY_DATE",
        regex: /@CURRENT_ONLY_DATE\b/gm,
        remove: /@CURRENT_ONLY_DATE\b/g,
        description: "Chỉ lấy ngày hiện tại từ máy chủ. Dạng: Ngày X.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getOnlyDate());
        }
    },
    {
        code: "@CURRENT_MONTH",
        regex: /@CURRENT_MONTH\b/gm,
        remove: /@CURRENT_MONTH\b/g,
        description: "Chỉ lấy tháng hiện tại từ máy chủ. Dạng: Tháng X.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getMonth());
        }
    },
    {
        code: "@CURRENT_YEAR",
        regex: /@CURRENT_YEAR\b/gm,
        remove: /@CURRENT_YEAR\b/g,
        description: "Chỉ lấy năm hiện tại từ máy chủ. Dạng: Năm XXXX.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getYear());
        }
    },
    {
        code: "@CURRENT_YEAR_NAME",
        regex: /@CURRENT_YEAR_NAME\b/gm,
        remove: /@CURRENT_YEAR_NAME\b/g,
        description: "Lấy tên của năm hiện tại.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getYearName());
        }
    },
    {
        code: "@CURRENT_DAY_NAME",
        regex: /@CURRENT_DAY_NAME\b/gm,
        remove: /@CURRENT_DAY_NAME\b/g,
        description: "Lấy tên hiện tại của ngày hôm nay.",
        get: function (msg) {
            return msg.replace(this.regex, Func.getDay());
        }
    },
    {
        code: "@CURRENT_USER_ADDRESS",
        regex: /@CURRENT_USER_ADDRESS\b/gm,
        remove: /@CURRENT_USER_ADDRESS\b/g,
        description: "Địa chỉ mà người dùng đang tại đó.",
        get: function (msg) {
            return msg.replace(this.regex, "hihi haha");
        }
    },
    {
        code: "@USER_COUNTRY",
        regex: /@USER_COUNTRY\b/gm,
        remove: /@USER_COUNTRY\b/g,
        description: "Quốc gia cuối dùng người dùng đặt chân đến. (Nếu ở một chỗ thì lấy quốc gia của người dùng)",
        get: function (msg) {
            return msg.replace(this.regex, "hihi haha");
        }
    },
    {
        code: "@USER_PROVINCE",
        regex: /@USER_PROVINCE\b/gm,
        remove: /@USER_PROVINCE\b/g,
        description: "Lấy tên tỉnh thành người dùng đang tại đó.",
        get: function (msg) {
            return msg.replace(this.regex, "hihi haha");
        }
    },
    {
        code: "@USER_CITY",
        regex: /@USER_CITY\b/gm,
        remove: /@USER_CITY\b/g,
        description: "Lấy tên thành phố hoặc thị trấn mà người dùng đang tại đó người dùng đang tại đó.",
        get: function (msg) {
            return msg.replace(this.regex, "hihi haha");
        }
    },
    {
        code: "@USER_HOME_ADDRESS",
        regex: /@USER_HOME_ADDRESS\b/gm,
        remove: /@USER_HOME_ADDRESS\b/g,
        description: "Địa chỉ thường trú của người dùng.",
        get: function (msg) {
            return msg.replace(this.regex, "hihi haha");
        }
    },
    {
        code: "@USER_WORK_ADDRESS",
        regex: /@USER_WORK_ADDRESS\b/gm,
        remove: /@USER_WORK_ADDRESS\b/g,
        description: "Địa chỉ nơi làm việc của người dùng.",
        get: function (msg) {
            return msg.replace(this.regex, "hihi haha");
        }
    },
]