const Func = require('./func/func.js');

module.exports = [
    {
        code: "@IS_MORNING[Nội dung khi là buổi sáng]",
        regex: /@IS_MORNING\[.+\]/gm,
        remove: /@IS_MORNING\[|\]/g,
        description: "Nếu là buổi sáng, thì thực hiện nội dung trong dấu ngoặc vuông",
        get: function (msg) {
            if (!Func.isMorning()) {
                return msg.replace(this.regex, "");
            }
            else {
                return Func.getDataFromConditon(msg, this.regex, this.remove);
            }
        }
    },
    {
        code: "@IS_NOON[Nội dung khi là buổi trưa]",
        regex: /@IS_NOON\[.+\]/gm,
        remove: /@IS_NOON\[|\]/g,
        description: "Nếu là buổi trưa, thì thực hiện nội dung trong dấu ngoặc vuông",
        get: function (msg) {
            if (!Func.isNoon()) {
                return msg.replace(this.regex, "");
            }
            else {
                return Func.getDataFromConditon(msg, this.regex, this.remove);
            }
        }
    },
    {
        code: "@IS_AFTERNOON[Nội dung khi là buổi chiểu]",
        regex: /@IS_AFTERNOON\[.+\]/gm,
        remove: /@IS_AFTERNOON\[|\]/g,
        description: "Nếu là buổi chiều, thì thực hiện nội dung trong dấu ngoặc vuông",
        get: function (msg) {
            if (!Func.isAfternoon()) {
                return msg.replace(this.regex, "");
            }
            else {
                return Func.getDataFromConditon(msg, this.regex, this.remove);
            }
        }
    },
    {
        code: "@IS_EVENING[Nội dung khi là buổi chiểu tối]",
        regex: /@IS_EVENING\[.+\]/gm,
        remove: /@IS_EVENING\[|\]/g,
        description: "Nếu là buổi chiều tối, thì thực hiện nội dung trong dấu ngoặc vuông",
        get: function (msg) {
            if (!Func.isEvening()) {
                return msg.replace(this.regex, "");
            }
            else {
                return Func.getDataFromConditon(msg, this.regex, this.remove);
            }
        }
    },
    {
        code: "@IS_NIGHT[Nội dung khi là buổi tối]",
        regex: /@IS_NIGHT\[.+\]/gm,
        remove: /@IS_NIGHT\[|\]/g,
        description: "Nếu là buổi tối, thì thực hiện nội dung trong dấu ngoặc vuông",
        get: function (msg) {
            if (!Func.isNight()) {
                return msg.replace(this.regex, "");
            }
            else {
                return Func.getDataFromConditon(msg, this.regex, this.remove);
            }
        }
    },
    {
        code: "@CURRENT_IS_LEAP_YEAR[Nội dung khi là năm nhuận]",
        regex: /@CURRENT_IS_LEAP_YEAR\[.+\]\b/gm,
        remove: /@CURRENT_IS_LEAP_YEAR\b/g,
        description: "Chỉ lấy năm hiện tại từ máy chủ. Dạng: Năm XXXX.",
        get: function (msg) {
            if (!Func.isLeapYear()) {
                return msg.replace(this.regex, "");
            }
            else {
                return Func.getDataFromConditon(msg, this.regex, this.remove);
            }
        }
    },
]