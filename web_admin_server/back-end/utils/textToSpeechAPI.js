const sequelize = require("../models/sequelize");
var queries = require("../models/queries.js");
var axios = require('axios');

let audioEncoding = {
    // Không mã hóa
    AUDIO_ENCODING_UNSPECIFIED: "AUDIO_ENCODING_UNSPECIFIED",
    // Mã hóa kiểu tuyến tính 16-bit
    LINEAR16: "LINEAR16",
    // Mã hóa mp3 thông dụng
    MP3: "MP3",
    // Mã hóa thích hợp cho các thiết bị android, ios và một số trình duyệt
    OGG_OPUS: "OGG_OPUS",
}
exports.audioEncoding = audioEncoding;

// Danh sách các giọng mặc định
let voiceSoundType = {
    // Giọng nữ miền bắc
    northernFemaleVoice: "vi-VN-Wavenet-A",
    // Giọng nam miền bắc
    northernMaleVoice: "vi-VN-Wavenet-B",
    // Giọng nữ miền nam
    southernFemaleVoice: "vi-VN-Wavenet-C",
    // Giọng nam miền nam
    southernMaleVoice: "vi-VN-Wavenet-D",
}
exports.voiceSoundType = voiceSoundType;

// Cấu hình cho việc lấy phản hồi văn bản thành giọng nói
let audioConfig = {
    audioEncoding: audioEncoding.MP3,
    pitch: 1.0,
    speakingRate: 1.0
};
exports.audioConfig = audioConfig;

let voice = {
    languageCode: "vi-VN",
    name: voiceSoundType.northernFemaleVoice
};
exports.voice = voice;

exports.getBase64Audio = async (msg) => {
    const url = `${process.env.CLOUD_SPEECH}?key=${process.env.CLOUD_TEXT_TO_SPEECH_API_KEY}`;
    return axios.request({
        url,
        method: 'POST',
        data: {
            audioConfig,
            input: {
                text: msg
            },
            voice
        }
    });
}