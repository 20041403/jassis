exports.list = [
    {
        code: -1,
        name: "Người dùng"
    },
    {
        code: 100,
        name: "Quản trị thành viên"
    },
    {
        code: 200,
        name: "Quản trị nội dung"
    },
    {
        code: 1000000,
        name: "Quản trị viên cấp cao"
    }
]

exports.superAdminRoles = "1000000";
exports.contentManagerRoles = "200";
exports.adminRoles = "100,1000000";

exports.checkIsHaveRoles = function(roles, current_user_roles) {
    let arr = roles.split(',');
    for (let i = 0; i < arr.length;i++)
    {
        if (current_user_roles == arr[i])
            return true;
    }
    return false;
}