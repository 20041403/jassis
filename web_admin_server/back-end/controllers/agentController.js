// API for INTENT using CRUD models
const sequelize = require("../models/sequelize");
var queries = require("../models/queries");
const jwt = require('jsonwebtoken');

// CONFIG
exports.root = "agent";

// CREATE
exports.add = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.a_name == undefined || req.body.a_description == undefined) {
    //     res.status(400).send( // Bad request
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Agent.add, {
            replacements: {
                a_name: req.body.a_name,
                a_description: req.body.a_description
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// READ
exports.countAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        sequelize.query(queries.Agent.count).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.get = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.a_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Agent.getById, {
            replacements: {
                a_id: req.query.a_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        sequelize.query(queries.Agent.getAllDetails).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// UPDATE
exports.update = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.a_name == undefined || req.body.a_description == undefined || req.body.a_update_at == undefined || req.body.a_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Agent.update, {
            replacements: {
                a_name: req.body.a_name,
                a_description: req.body.a_description,
                a_update_at: req.body.a_update_at,
                a_id: req.body.a_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// DELETE
exports.delete = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.a_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Agent.delete, {
            replacements: {
                a_id: req.query.a_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}



