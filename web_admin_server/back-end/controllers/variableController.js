const baseVariable = require('../utils/processing_core/baseVariable.js');
const conditionVariable = require('../utils/processing_core/conditionVariable.js');
const calculateVariable = require('../utils/processing_core/calculateVariable.js');

exports.root = "variable";

exports.getAllBaseVariable = async(req, res) => {
    res.status(200).send(baseVariable);
}
exports.getAllConditionVariable = async(req, res) => {
    res.status(200).send(conditionVariable);
}
exports.getAllCalculateVariable = async(req, res) => {
    res.status(200).send(calculateVariable);
}