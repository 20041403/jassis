// API for RESPONSE using CRUD models
const sequelize = require("../models/sequelize");
var queries = require("../models/queries.js");
const jwt = require('jsonwebtoken');
var textToSpeech = require('../utils/textToSpeechAPI.js');
var handleSentences = require('../utils/handleSentences.js');
const processing = require('../utils/processing_core/processing.js');

// CONFIG
exports.root = "received_phrase";

exports.getAll = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.receivedPhrase.getAll).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getResponsed = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.receivedPhrase.getResponsed).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getNotYet = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.receivedPhrase.getNotYet).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.countAll = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.receivedPhrase.countAll).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0].count
            })
        });
    } catch (err) {
        res.status(200).send({
            status: -1,
            result: "Không thể tiến hành đếm tổng số câu đã nhận"
        })
    }
};

exports.countResponsed = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.receivedPhrase.countResponsed).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0].count
            })
        });
    } catch (err) {
        res.status(200).send({
            status: -1,
            result: "Không thể tiến hành đếm tổng số câu đã phản hồi"
        })
    }
};

exports.countNotYet = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.receivedPhrase.countNotYet).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0].count
            })
        });
    } catch (err) {
        res.status(200).send({
            status: -1,
            result: "Không thể tiến hành đếm tổng số câu chưa phản hồi"
        })
    }
};

// Phương thức này dành cho việc lưu lại các dữ liệu đã được xử lý khi nhận được yêu cầu từ người dùng - được gọi kèm với phương thức tìm câu trả lời thích hợp bên dưới
async function insertToReceived(rd_user_query, rd_response, rd_query_by, rd_query_coordinate, rd_current_place_name, rd_is_success) {
    // Xóa bớt thuộc tính truyền audio vì quá dài
    delete rd_response.result.gottedResponseAudio;

    // Sau đó tiến hành chuyển JSON thành chuỗi
    rd_user_query = JSON.stringify(rd_user_query);
    rd_response = JSON.stringify(rd_response);

    // Tiến hành kiểm tra và khởi tạo các giá trị mặc định
    if (rd_user_query == null || rd_user_query == undefined || rd_user_query == "") {
        rd_user_query = '';
    }
    if (rd_query_coordinate == null || rd_query_coordinate == undefined || rd_query_coordinate == "") {
        rd_query_coordinate = 'Không thể lấy được toạ độ';
    }
    if (rd_current_place_name == null || rd_current_place_name == undefined || rd_current_place_name == "") {
        rd_current_place_name = 'Không thể lấy được tên địa điểm';
    }

    // Tiến hành xử lý và thêm vào cơ sở dữ liệu
    try {
        sequelize.query(queries.receivedPhrase.insertToReceived, {
            replacements: {
                rd_user_query,
                rd_response,
                rd_query_by,
                rd_query_coordinate,
                rd_current_place_name,
                rd_is_success
            }
        }).then(r => { });
    } catch (err) { }
}

// Phương thức chuyển văn bản thành giọng nói
exports.textToSpeech = async (req, res) => {
    if (req.query == undefined || req.query.msg == undefined)
    {
        res.status(200).send({
            status: -1,
            result: "Giá trị truyền vào không hợp lệ",
        });
        return;
    }
    // Processing
    try {
        this.findSpeechAnswer(req.query.msg).then(gottedResponseAudio => {
            res.status(200).send({
                status: 1,
                result: {
                    as_audio_encoding: textToSpeech.audioConfig.audioEncoding,
                    as_pitch: textToSpeech.audioConfig.pitch,
                    as_speaking_rate: textToSpeech.audioConfig.speakingRate,
                    as_language_code: textToSpeech.voice.languageCode,
                    as_voice_sound_name: textToSpeech.voice.name,
                    as_text_code: handleSentences.convertToBase64AudioKey(req.query.msg),
                    gottedResponseAudio
                }
            });
        });
    } catch (err) {
        res.status(200).send({
            status: -1,
            result: "Không thể tiến hành chuyển đổi",
            err: err.message
        })
    }
}

// Phương thức tìm phản hồi bằng giọng nói
exports.findSpeechAnswer = (msg) => {
    return new Promise((resolve, reject) => {
        let gottedResponseAudio = '';
        // Kiểm tra xem câu này đã có trong cơ sở dữ liệu chưa, nếu có tối thì lấy
        sequelize.query(queries.base64Audio.find, {
            replacements: {
                as_audio_encoding: textToSpeech.audioConfig.audioEncoding,
                as_pitch: textToSpeech.audioConfig.pitch,
                as_speaking_rate: textToSpeech.audioConfig.speakingRate,
                as_language_code: textToSpeech.voice.languageCode,
                as_voice_sound_name: textToSpeech.voice.name,
                as_text_code: handleSentences.convertToBase64AudioKey(msg)
            }
        }).then(r5 => {
            if (r5 == null || r5 == undefined || r5.length <= 0 || r5[0] == undefined || r5[0].length <= 0 || r5[0][0] == undefined) {
                // Không có thì chuyển sang request cái mới
                textToSpeech.getBase64Audio(msg).then(r6 => {
                    if (r6 == null || r6 == undefined || r6.data == null || r6.data == undefined || r6.data == "" || r6.data.audioContent == null || r6.data.audioContent == undefined || r6.data.audioContent == "") {
                        // Nếu không thể lấy được lời nói cho câu trả lời, thì để mục đó trống
                        gottedResponseAudio = '';
                    } else {
                        // Nếu đã lấy được rồi thì đặt mục đó là chuỗi base64 nhận được
                        gottedResponseAudio = r6.data.audioContent;

                        // Tiến hành lưu câu chưa có vô CSDL
                        try {
                            sequelize.query(queries.base64Audio.add, {
                                replacements: {
                                    as_audio_encoding: textToSpeech.audioConfig.audioEncoding,
                                    as_pitch: textToSpeech.audioConfig.pitch,
                                    as_speaking_rate: textToSpeech.audioConfig.speakingRate,
                                    as_language_code: textToSpeech.voice.languageCode,
                                    as_voice_sound_name: textToSpeech.voice.name,
                                    as_base64_audio: gottedResponseAudio,
                                    as_text: msg,
                                    as_text_code: handleSentences.convertToBase64AudioKey(msg)
                                }
                            }).then(r => {
                            });
                        } catch (err6) {
                            if (err6) reject(err6)
                        }
                    }
                }).finally(() => {
                    // Sau khi hoàn tất mọi thứ, tiến hành gửi phản hồi đầy đủ về cho client   
                    //onsole.log(gottedResponseAudio);   
                    resolve(gottedResponseAudio);
                });

            } else {
                gottedResponseAudio = r5[0][0].as_base64_audio;
                //console.log(gottedResponseAudio);                                                     
                resolve(gottedResponseAudio);
            }
        });
    });
}

// Phương thức này dành cho việc tìm câu phản hồi cho người dùng
exports.findAnswer = async (req, res) => {
    // Danh sách các mục cần lưu lại dành cho việc trả kết quả phản hồi
    let userTalked = req.body.phrase; // Lưu lại câu mà người dùng nói và đã được gửi lên
    let userId = -1; // Lưu id của người gửi, mặc định nếu người dùng chưa đăng nhập thì có id là -1 - Lấy từ token
    let coordinate = req.body.coordinate; // Lưu toạ độ của người gửi
    let currentPlaceName = req.body.current_place_name; // Lưu tên của địa điểm hiện tại mà người dùng thực hiện để gửi yêu cầu lên

    // Danh sách biến để lưu trữ dữ liệu lấy được
    let gottedPhrase = {}; // Chứa câu hỏi được cho là trùng khớp
    let gottedIntent = {}; // Chứa ý định của câu hỏi đó
    let gottedAgent = {}; // Chứa phạm trù của câu nói đó
    let gottedResponse = {}; // Chứa phản hồi sẽ dùng để trả lời cho người dùng thông qua câu họ nói

    // Chứa phản hồi
    let responseBody = {};

    // Chứa các câu mẫu
    let notFoundPhrase = ["Xin lỗi, tôi không hiểu", "Xin lỗi tôi chưa hiểu", "Bạn có thể nói lại theo một cách khác được không?"];
    let notFoundIntent = ["Xin lỗi, tôi không rõ ý định của câu này", "Ý định của câu này là gì nhỉ? Bạn hãy thử nói theo một cách khác xem", "Bạn có thể nói lại theo một cách khác được không?"];
    let notFoundAgent = ["Xin lỗi, tôi không rõ câu này thuộc phạm trù nào", "Phạm trù của câu này là gì nhỉ? Bạn hãy thử nói theo một cách khác xem", "Bạn có thể nói lại theo một cách khác được không?"];
    let notFoundResponse = ["Xin lỗi, tôi chưa học về vấn đề này", "Tôi chưa được huấn luyện để trả lời cho câu hỏi này", "Xin lỗi, tôi chưa thể phản hồi về vấn đề này được"];

    // Tiến hành chuẩn hóa câu đầu vào
    userTalked = handleSentences.removeExtraSpaces(userTalked); // Xóa dấu cách thừa trong câu

    try {
        // Tiến hành xử lý các trường hợp để tìm kiếm phản hồi thích hợp
        if (userTalked == null || userTalked == undefined || userTalked == "") {
            // Nếu câu cán người dùng gửi lên là không có, thì tiến hành phản hồi lỗi lại
            let wrongPhraseResponse = ["Xin lỗi, tôi không thế biết được bạn đang muốn nói gì."];
            responseBody = {
                status: -1,
                result: wrongPhraseResponse[Math.floor(Math.random() * wrongPhraseResponse.length)],
                err: "Giá trị của câu không được truyền khi gửi yêu cầu"
            };
            insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
            res.status(200).send(responseBody);
        } else {
            // Nếu câu cán đầy đủ, có giá trị. Tiến hành phân tích xem nó thuộc mẫu câu nào
            sequelize.query(queries.receivedPhrase.findAnswer, {
                replacements: {
                    // Tiến hành xóa các dấu không phải chữ và số như dấu ?, !,... để câu tường minh hơn
                    phrase_full: userTalked,
                    phrase: handleSentences.removeCharacterNotNumberAndLetter(userTalked).replace(/ /g, " | ")
                }
            }).then((r) => {
                if (r == null || r == undefined || r.length <= 0 || r[0].length <= 0 || r[0][0] == undefined) {
                    // Nếu không tìm thấy bất kỳ câu hỏi nào khớp với câu hỏi này trong cơ sở dữ liệu thì tiến hành quẳng lỗi
                    responseBody = {
                        status: -100,
                        result: notFoundPhrase[Math.floor(Math.random() * notFoundPhrase.length)],
                        err: "Không tìm thấy câu nào phù hợp"
                    };
                    insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
                    res.status(200).send(responseBody);
                    return;
                } else {
                    // Nếu đã tìm được câu khớp với câu mà ngừoi dùng gửi lên nhất, thì tiến hành các bước sau
                    // Lưu mẫu câu tìm được lại
                    gottedPhrase = r[0][0];

                    // Tiến hành lấy ý định bao hàm của câu đó
                    try {
                        sequelize.query(queries.Intent.getById, {
                            replacements: {
                                i_id: gottedPhrase.i_id
                            }
                        }).then(r2 => {
                            if (r2 == null || r2 == undefined || r2.length <= 0 || r2[0].length <= 0 || r2[0][0] == undefined) {
                                // Nếu không tìm thấy ý dịnh của câu này trong cơ sở dữ liệu thì tiến hành quăng lỗi
                                responseBody = {
                                    status: -101,
                                    result: notFoundIntent[Math.floor(Math.random() * notFoundIntent.length)],
                                    err: err.message
                                };
                                insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
                                res.status(200).send(responseBody);
                                return;
                            } else {
                                // Nếu đã tìm được ý định của câu này, thì tiến hành lưu lại và thực hiện bước tiếp theo là lấy phạm trù của nó
                                gottedIntent = r2[0][0];

                                // Tiến hành lấy phạm trù của câu này
                                try {
                                    sequelize.query(queries.Agent.getById, {
                                        replacements: {
                                            a_id: gottedIntent.a_id
                                        }
                                    }).then(r3 => {
                                        if (r3 == null || r3 == undefined || r3.length <= 0 || r3[0].length <= 0 || r3[0][0] == undefined) {
                                            // Nếu không tìm thấy phạm trù chứa câu và ý định này, thì tiến hành quăng lỗi
                                            responseBody = {
                                                status: -102,
                                                result: notFoundAgent[Math.floor(Math.random() * notFoundAgent.length)],
                                                err: err.message
                                            };
                                            insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
                                            res.status(200).send(responseBody);
                                            return;
                                        } else {
                                            // Nếu đã tìm được phạm trù chứa câu và ý định này, thì tiến hành lưu lại, và tìm câu trả lời thích hợp
                                            gottedAgent = r3[0][0];

                                            // Tiến hành lấy câu trả lời thích hợp (Ngẫu nhiên theo lệnh truy vấn)
                                            try {
                                                sequelize.query(queries.receivedPhrase.getRandomResponseViaIntentId, {
                                                    replacements: {
                                                        i_id: gottedIntent.i_id
                                                    }
                                                }).then(r4 => {
                                                    if (r4 == null || r4 == undefined || r4.length <= 0 || r4[0].length <= 0 || r4[0][0] == undefined) {
                                                        // Nếu không tìm thấy câu trả lời phù hợp, thì tiến hành quăng lỗi
                                                        responseBody = {
                                                            status: -102,
                                                            result: notFoundResponse[Math.floor(Math.random() * notFoundResponse.length)],
                                                            err: "Không tìm thấy câu trả lời phù hợp"
                                                        };
                                                        insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
                                                        res.status(200).send(responseBody);
                                                    } else {
                                                        // Nếu đã lấy được, thì tiến hành lưu lại
                                                        gottedResponse = r4[0][0];

                                                        // Tiến hành xử lý kết quả trả về (Biên dịch ra kết quả)
                                                        gottedResponse.r_response = processing(gottedResponse.r_response);

                                                        // Tiến hành chuyển đổi câu trả lời thành âm thanh tiếng Việt
                                                        this.findSpeechAnswer(gottedResponse.r_response).then(gottedResponseAudio => {
                                                            /* DÀNH CHO VIỆC THÀNH PHẨM - CÓ PHẦN LẤY ÂM THANH */
                                                            responseBody = {
                                                                status: 1,
                                                                result: {
                                                                    userTalked,
                                                                    gottedAgent,
                                                                    gottedIntent,
                                                                    gottedPhrase,
                                                                    gottedResponse,
                                                                    gottedResponseAudio
                                                                }
                                                            };
                                                            insertToReceived(req.body, JSON.parse(JSON.stringify(responseBody)), userId, coordinate, currentPlaceName, true);
                                                            res.status(200).send(responseBody);
                                                        });
                                                    }
                                                });
                                            } catch (err4) {
                                                // Nếu bắt đuợc lỗi nào đó, thì chắc chắn là không tìm được câu trả lời
                                                responseBody = {
                                                    status: -102,
                                                    result: notFoundResponse[Math.floor(Math.random() * notFoundResponse.length)],
                                                    err: "Không tìm thấy câu trả lời"
                                                };
                                                insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
                                                res.status(200).send(responseBody);
                                            }
                                        }
                                    });
                                } catch (err3) {
                                    // Nếu bắt đuợc lỗi nào đó, thì chắc chắn là không tìm được câu trả lời
                                    responseBody = {
                                        status: -102,
                                        result: notFoundAgent[Math.floor(Math.random() * notFoundAgent.length)],
                                        err: "Không tìm thấy phạm trù"
                                    };
                                    insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
                                    res.status(200).send(responseBody);
                                }
                            }

                        });
                    } catch (err2) {
                        // Nếu bắt đuợc lỗi nào đó, thì chắc chắn là không tìm được câu trả lời
                        responseBody = {
                            status: -101,
                            result: notFoundIntent[Math.floor(Math.random() * notFoundIntent.length)],
                            err: "Không tìm thấy ý định của câu nói"
                        };
                        insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
                        res.status(200).send(responseBody);
                    }
                }
            })
        }
    } catch (err) {
        // Nếu bắt đuợc lỗi nào đó, thì chắc chắn là không tìm được câu trả lời
        responseBody = {
            status: -100,
            result: notFoundPhrase[Math.floor(Math.random() * notFoundPhrase.length)],
            err: err.message
        };
        insertToReceived(req.body.phrase, responseBody, userId, coordinate, currentPlaceName, false);
        res.status(200).send(responseBody);
    }
}


exports.delete = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.receivedPhrase.delete, {
            replacements: {
                rd_id: req.query.rd_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}



