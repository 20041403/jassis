const sequelize = require("../models/sequelize");
var queries = require("../models/queries");
const jwt = require('jsonwebtoken');

exports.root = "base64_audio";

exports.getAll = async (req, res) => {
    try {
        sequelize.query(queries.base64Audio.getAll).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể thực hiện yêu cầu lấy danh sách kho lời thoại"
            }
        )
    }
}

exports.getFemaleVoice = async (req, res) => {
    try {
        sequelize.query(queries.base64Audio.getFemaleVoice).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể thực hiện yêu cầu lấy danh sách kho lời thoại của nữ"
            }
        )
    }
}

exports.getMaleVoice = async (req, res) => {
    try {
        sequelize.query(queries.base64Audio.getMaleVoice).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể thực hiện yêu cầu lấy danh sách kho lời thoại của nam"
            }
        )
    }
}

exports.countAll = async (req, res) => {
    try {
        sequelize.query(queries.base64Audio.count).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0].count
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể thực hiện yêu cầu đếm tổng số lời thoại đã có"
            }
        )
    }
}

exports.countFemaleVoice = async (req, res) => {
    try {
        sequelize.query(queries.base64Audio.countFemaleVoice).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0].count
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể thực hiện yêu cầu đếm tổng số lời thoại của nữ đã có"
            }
        )
    }
}

exports.countMaleVoice = async (req, res) => {
    try {
        sequelize.query(queries.base64Audio.countMaleVoice).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0].count
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể thực hiện yêu cầu đếm tổng số lời thoại của nam đã có"
            }
        )
    }
}

exports.getBase64AudioViaId = async (req, res) => {
    try {
        sequelize.query(queries.base64Audio.getBase64AudioViaId, {
            replacements: {
                as_id: req.query.as_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể lấy nội dung lời thoại để phát"
            }
        )
    }
}

exports.delete = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.base64Audio.delete, {
            replacements: {
                as_id: req.query.as_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

