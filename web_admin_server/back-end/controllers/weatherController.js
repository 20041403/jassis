const jwt = require('jsonwebtoken');
const axios = require('axios').default;

exports.root = "weather";
exports.forecast = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: "Xác thực không thành công!"
            }
        );
        return;
    }
    if (req.query.ua_current_coordinate == undefined) {
        res.status(200).send(
            {
                status: -1,
                result: "Truy vấn thiếu tọa độ để xác định vị trí cần dự báo thời tiết."
            }
        );
        return;
    }
    let forecastUrl = `https://api.darksky.net/forecast/${process.env.WEATHER_DARKSKY_APIKEY}/${req.query.ua_current_coordinate}?units=si`;
    // Tiến hành dự báo thời tiết
    axios.get(forecastUrl).then(r => {
        res.status(200).send(
            {
                status: 1,
                result: r.data
            }
        )
    }).catch(e => {
        res.status(200).send(
            {
                status: -1,
                result: "Không thể lấy được thông tin dự báo thời tiết.\n" + e.message
            }
        );
    })
}