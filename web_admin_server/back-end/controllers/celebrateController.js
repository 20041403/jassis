const fs = require('fs');
const path = require('path');
let data = fs.readFileSync(path.join(__dirname, './../data/celebrate.json'), 'utf8');
const celebrates = JSON.parse(data);
const format = /^([1-9]|1[0-9]|2[0-9]|3(0|1))-([1-9]|1[0-2])-\d{4}$/gm;

// CONFIG
exports.root = "celebrate";

exports.find = async(req, res) => {
    if (req.query.date == undefined || !req.query.date.match(format))
    {
        res.status(200).send(
            {
                status: -1,
                result: "Ngày tháng năm không đúng định dạng."
            }
        )
    } else {
        let found = [];
        for (let i = 0; i < celebrates.length;i++) {
            if (celebrates[i].historyDate != undefined && (celebrates[i].historyDate.indexOf(req.query.date.replace(/\d{4}/g,"")) == 0 || 
            celebrates[i].historyDate == req.query.date.replace(/-\d{4}/g,"")))
            {
                found.push(celebrates[i]);
            }
        }
        res.status(200).send(
            {
                status: 1,
                result: found
            }
        )
    }
}