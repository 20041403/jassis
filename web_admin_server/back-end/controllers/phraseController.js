// API for PHRASE using CRUD models
const sequelize = require("../models/sequelize");
var queries = require("../models/queries.js");
const jwt = require('jsonwebtoken');

// CONFIG
exports.root = "phrase";

// CREATE
exports.add = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.p_phrase == undefined || req.body.i_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Phrase.add, {
            replacements: {
                p_phrase: req.body.p_phrase,
                i_id: req.body.i_id,
                p_create_by: jwtPayload.id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// READ
exports.countAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        sequelize.query(queries.Phrase.count).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.get = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.p_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Phrase.getById, {
            replacements: {
                p_id: req.query.p_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        if (req.query.i_id == undefined) {
            sequelize.query(queries.Phrase.getAllDetails).then((r) => {
                res.status(200).send({
                    status: 1,
                    result: r[0]
                });
            })
        } else {
            sequelize.query(queries.Phrase.getAllDetailsByIntent, {
                replacements: {
                    i_id: req.query.i_id
                }
            }).then((r) => {
                res.status(200).send({
                    status: 1,
                    result: r[0]
                });
            })
        }
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// UPDATE
exports.update = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.p_phrase == undefined || req.query.p_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Phrase.update, {
            replacements: req.body
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// DELETE
exports.delete = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.p_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Phrase.delete, {
            replacements: {
                p_id: req.query.p_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}



