// API for INTENT using CRUD models
const sequelize = require("../models/sequelize");
var queries = require("../models/queries");
const jwt = require('jsonwebtoken');
var PwdHasing = require('../utils/pwd.js');
var Roles = require('../utils/roles.js');
var fs = require('fs');
const uuidv4 = require('uuid/v4')
const pump = require('pump')
const path = require('path')

// CONFIG
exports.root = "user_account";

// CREATE
exports.add = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        // nếu đã có cookie rồi, thức tức đây là đăng ký spam
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
        res.status(409).send(
            {
                status: -1,
                result: -1,
            }
        );
    } catch (err) {
        // OK, có thể đăng ký
    }
    // // Check argument
    // if (req.body.ua_username == undefined || req.body.ua_password == undefined || req.body.ua_email == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.UserAccount.add, {
            replacements: {
                ua_username: req.body.ua_username,
                ua_password: PwdHasing.hasing(req.body.ua_password, jwtPayload.username),
                ua_email: req.body.ua_email
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}
// READ
exports.logout = async (req, res) => {
    try {
        res.setCookie('access_token', null, {
            expires: new Date(Date.now() - 365 * 24 * 60 * 60 * 1000 * 10), // 10 năm
            httpOnly: true,
            path: '/'
        })
            .status(200).send({
                status: 1,
                result: "Success"
            });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.login = async (req, res) => {
    // Processing
    try {
        // Check type of login
        let isUserNameLogin = true;
        if (req.body.username != undefined && req.body.password != undefined) {
            isUserNameLogin = true;
        } else if (req.body.email != undefined && req.body.password != undefined) {
            isUserNameLogin = false;
        } else {
            res.status(400).send(
                {
                    status: -1,
                    result: -1
                }
            );
            return;
        }
        // Use to get user
        // If loggin using username
        if (isUserNameLogin) {
            sequelize.query(queries.UserAccount.findUserViaUsernameAndPassword, {
                replacements: {
                    username: req.body.username,
                    password: PwdHasing.hasing(req.body.password, req.body.username)
                }
            }).then((r) => {
                let gottedUser = (r[0].length == 1 ? r[0][0] : null);
                if (gottedUser == null) {
                    res.status(200).send(
                        {
                            status: -1,
                            result: -1
                        }
                    )
                } else {
                    const payload = {
                        id: gottedUser.ua_id,
                        username: gottedUser.ua_username,
                        email: gottedUser.ua_email
                    }
                    const token = jwt.sign(payload, process.env.JWT_SECRET);
                    res.setCookie('access_token', token, {
                        expires: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000 * 10), // 10 năm
                        httpOnly: true,
                        path: '/'
                    })
                        .header('access_token', token)
                        .status(200).send({
                            status: 1,
                            result: gottedUser,
                            token: token
                        });
                }
            });
        }

        // If loggin using email
        else {
            sequelize.query(queries.UserAccount.getUsernameViaEmail, {
                replacements: {
                    ua_email: req.body.email
                }
            }).then(r => {
                if (r[0].length <= 0) {
                    throw("Không tìm thấy username cho email này");
                } else if (r[0][0].ua_username == null || r[0][0].ua_username == undefined) {
                    throw("Không tìm thấy username cho email này");
                } else {
                    sequelize.query(queries.UserAccount.findUserViaEmailAndPassword, {
                        replacements: {
                            email: req.body.email,
                            password: PwdHasing.hasing(req.body.password, r[0][0].ua_username)
                        }
                    }).then((r) => {
                        let gottedUser = (r[0].length == 1 ? r[0][0] : null);
                        if (gottedUser == null) {
                            res.status(200).send(
                                {
                                    status: -1,
                                    result: -1
                                }
                            )
                        } else {
                            const payload = {
                                id: gottedUser.ua_id,
                                username: gottedUser.ua_username,
                                email: gottedUser.ua_email
                            }
                            const token = jwt.sign(payload, process.env.JWT_SECRET);
                            res.setCookie('access_token', token, {
                                expires: new Date(Date.now() + 365 * 24 * 60 * 60 * 1000 * 10), // 10 năm
                                httpOnly: true,
                                path: '/'
                            })
                                .header('access_token', token)
                                .status(200).send({
                                    status: 1,
                                    result: gottedUser,
                                    token: token
                                });
                        }
                    });
                }
            });
        }
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.tokenVerify = async (req, res) => {
    // Authentication
    try {
        const access_token = req.headers.access_token;
        let jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
        res.status(200).send(
            {
                status: 1,
                result: jwtPayload
            }
        )
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: "Xác thực không thành công"
            }
        );
    }
};

exports.countAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }

    // Processing
    try {
        sequelize.query(queries.UserAccount.count).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.countUserByRoles = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }

    // Processing
    try {
        sequelize.query(queries.UserAccount.countUserByRoles, {
            replacements: {
                role_codes: req.query.role_codes
            }
        }).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.getAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }

    // Processing
    try {
        sequelize.query(queries.UserAccount.getAll).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0]
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.getProfileById = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }

    // Processing
    try {
        if (req.query.up_id == null)
            throw("No value");
        sequelize.query(queries.UserAccount.getProfileById, {
            replacements: {
                up_id: req.query.up_id
            }
        }).then((r) => {
            if (r[0].length == 0) {
                // nếu người dùng chưa có profile nào, tạo mới 1 cái
                sequelize.query(queries.UserAccount.createProfileByUserId, {
                    replacements: {
                        up_id: req.query.up_id
                    }
                }).then((r) => {
                    sequelize.query(queries.UserAccount.getProfileById, {
                        replacements: {
                            up_id: req.query.up_id
                        }
                    }).then((r) => {
                        res.status(200).send(
                            {
                                status: 1,
                                result: r[0]
                            }
                        )
                    });
                });
            } else {
                // Đã có rồi thì trả về
                res.status(200).send(
                    {
                        status: 1,
                        result: r[0]
                    }
                )
            }
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.getUserAccountById = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }

    // Processing
    try {
        sequelize.query(queries.UserAccount.getUserAccountById, {
            replacements: {
                ua_id: req.query.ua_id
            }
        }).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0]
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.getUserByRoles = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        sequelize.query(queries.UserAccount.getListUser, {
            replacements: {
                role_codes: req.query.role_codes
            }
        }).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0]
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.getAllRoles = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }

    // Processing
    res.status(200).send(
        {
            status: 1,
            result: Roles.list
        }
    );
};

exports.checkEmail = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.UserAccount.checkEmail, {
            replacements: {
                ua_email: req.query.email
            }
        }).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.checkUsername = async (req, res) => {
    // Processing
    try {
        sequelize.query(queries.UserAccount.checkUsername, {
            replacements: {
                ua_username: req.query.username
            }
        }).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

// UPDATE
exports.changePassword = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.old_pass == undefined || req.body.new_pass == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.UserAccount.changePassword, {
            replacements: {
                ua_id: jwtPayload.id,
                old_pass: PwdHasing.hasing(req.body.old_pass, jwtPayload.username),
                new_pass: PwdHasing.hasing(req.body.new_pass, jwtPayload.username)
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getUserImage = async (req, res) => {
    let username = req.query.username;
    let name = req.query.name;
    try {
        if (username == undefined || name == undefined) {
            throw("Biến truyền vào không đảm bảo");
        } else {
            let avatarDir = path.join(process.cwd(), 'public', `${username}_images`);
            let buffer = fs.readFileSync(path.join(avatarDir, name));
            res.type(`image/${name.split('.').pop()}`).send(buffer);
        }
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1,
                err: err.message
            }
        )
    }
}

exports.updateAvatar = async (req, res) => {
    // Processing
    try {
        if (req.query.username != undefined) {
            let username = req.query.username;
            // Tạo folder chứa ảnh riêng cho mỗi người dùng
            let avatarDir = path.join(process.cwd(), 'public', `${username}_images`);
            const mp = req.multipart(handler, function (err) {
                res.status(200).send({
                    status: 1,
                    result: "Cập nhật ảnh đại diện thành công!"
                });
            })

            function handler(field, file, filename, encoding, mimetype) {
                // Nếu người dùng này chưa có folder riêng thì tạo mới một cái
                if (!fs.existsSync(avatarDir)) {
                    fs.mkdirSync(avatarDir);
                }
                // Tạo tên mới cho file
                let newFileName = `${username}_${uuidv4()}.${filename.split('.').pop()}`;
                // Lưu file
                pump(file, fs.createWriteStream(`${avatarDir}/${newFileName}`))
                // Cập nhật link vào csdl
                try {
                    sequelize.query(queries.UserAccount.updateAvatar, {
                        replacements:
                        {
                            up_avatar_addr: `/public/images?username=${username}&name=${newFileName}`,
                            up_id: req.query.id
                        }
                    }).then((r) => {
                    })
                } catch (err) {
                }
            }
        } else {
            throw("Không tìm thấy tên tài khoản.");
        }
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getCurrentUserCoordinate = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: "Mã xác thực không hợp lệ"
            }
        );
        return;
    }
    // Processing
    try {
        sequelize.query(queries.UserAccount.getCurrentUserCoordinate, {
            replacements: {
                ua_id: jwtPayload.id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0][0]
            });
        });
    } catch (err2) {
        res.status(200).send(
            {
                status: -1,
                result: "Thực hiện truy vấn không thành công!"
            }
        );
    }
}

exports.updateCurrentUserCoordinate = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Nếu không truyền tọa độ lên thì tiến hành lấy tọa độ cũ đã có trong cơ sở dữ liệu về để apply vào
    if (req.body == undefined || req.body.ua_current_coordinate == null ||req.body.ua_current_coordinate == undefined || req.body.ua_current_coordinate == "")
    {
        this.getCurrentUserCoordinate(req, res);
        return;
    }
    // Processing
    try {
        sequelize.query(queries.UserAccount.updateCurrentUserCoordinate, {
            replacements: {
                ua_current_coordinate: req.body.ua_current_coordinate,
                ua_id: jwtPayload.id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        });
    } catch (err) {
        this.getCurrentUserCoordinate(req, res);
    }
}

exports.updateProfile = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.up_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.UserAccount.updateProfile, {
            replacements: req.body
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.changePasswordByAdmin = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.ua_username == undefined || req.body.ua_password == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.UserAccount.getUserAccountById, {
            replacements: {
                ua_id: jwtPayload.id
            }
        }).then(r => {
            let userAccount = r[0][0];
            if (!Roles.checkIsHaveRoles(Roles.adminRoles, userAccount.ua_roles)) {
                res.status(200).send(
                    {
                        status: -1,
                        result: "Không có quyền"
                    }
                );
                return;
            } else {
                sequelize.query(queries.UserAccount.changePasswordByAdmin, {
                    replacements: {
                        ua_username: req.body.ua_username,
                        ua_password: PwdHasing.hasing(req.body.ua_password, req.body.ua_username)
                    }
                }).then((r) => {
                    res.status(200).send({
                        status: 1,
                        result: "Đổi mật khẩu thành công"
                    });
                })
            }
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.delete = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.ua_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        // (jwtPayload.role_code != 100 && jwtPayload.role_code != 1000000)
        sequelize.query(queries.UserAccount.getUserAccountById, {
            replacements: {
                ua_id: jwtPayload.id
            }
        }).then(r => {
            let userAccount = r[0][0];
            if (!Roles.checkIsHaveRoles(Roles.adminRoles, userAccount.ua_roles)) {
                res.status(200).send(
                    {
                        status: -1,
                        result: "Không có quyền"
                    }
                );
                return;
            } else {
                sequelize.query(queries.UserAccount.deleteAccountBySuperAdmin, {
                    replacements: {
                        ua_id: req.query.ua_id
                    }
                }).then((r) => {
                    res.status(200).send({
                        status: 1,
                        result: "Xóa thành công"
                    });
                })
            }
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}
