// API for RESPONSE using CRUD models
const sequelize = require("../models/sequelize");
var queries = require("../models/queries.js");
const jwt = require('jsonwebtoken');

// CONFIG
exports.root = "response";

// CREATE
exports.add = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.r_response == undefined || req.body.r_event == undefined || req.body.i_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Response.add, {
            replacements: req.body
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// READ
exports.countAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        sequelize.query(queries.Response.count).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.get = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.r_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Response.getById, {
            replacements: {
                r_id: req.query.r_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        if (req.query.i_id == undefined) {
            sequelize.query(queries.Response.getAllDetails).then((r) => {
                res.status(200).send({
                    status: 1,
                    result: r[0]
                });
            })
        } else {
            sequelize.query(queries.Response.getAllDetailsByIntent, {
                replacements: {
                    i_id: req.query.i_id
                }
            }).then((r) => {
                res.status(200).send({
                    status: 1,
                    result: r[0]
                });
            })
        }

    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// UPDATE
exports.update = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.r_response == undefined || req.body.r_event == undefined || req.body.r_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Response.update, {
            replacements: req.body
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// DELETE
exports.delete = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.r_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Response.delete, {
            replacements: {
                r_id: req.query.r_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}
