// API for INTENT using CRUD models
const sequelize = require("../models/sequelize");
var queries = require("../models/queries");
const jwt = require('jsonwebtoken');

// CONFIG
exports.root = "intent";

// CREATE 
exports.add = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.i_id == undefined || req.body.a_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Intent.add, {
            replacements: req.body
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// READ
exports.countAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        sequelize.query(queries.Intent.count).then((r) => {
            res.status(200).send(
                {
                    status: 1,
                    result: r[0][0].count
                }
            )
        });
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
};

exports.get = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.i_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Intent.getById, {
            replacements: {
                i_id: req.query.i_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: r[0]
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

exports.getAll = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // Processing
    try {
        if (req.query.a_id == undefined) {
            sequelize.query(queries.Intent.getAllDetails).then((r) => {
                res.status(200).send({
                    status: 1,
                    result: r[0]
                });
            })
        } else {
            sequelize.query(queries.Intent.getAllDetailsByAgent, {
                replacements: {
                    a_id: req.query.a_id
                }
            }).then((r) => {
                res.status(200).send({
                    status: 1,
                    result: r[0]
                });
            })
        }
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// UPDATE
exports.update = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.body.i_id == undefined || req.body.i_name == undefined || req.body.i_description == undefined || req.body.a_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Intent.update, {
            replacements: req.body
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}

// DELETE
exports.delete = async (req, res) => {
    const access_token = req.headers.access_token;
    let jwtPayload;
    // Authentication
    try {
        jwtPayload = jwt.verify(access_token, process.env.JWT_SECRET);
    } catch (err) {
        res.status(401).send(
            {
                status: -1,
                result: -1
            }
        );
        return;
    }
    // // Check argument
    // if (req.query.i_id == undefined) {
    //     res.status(400).send(
    //         {
    //             status: -1,
    //             result: -1
    //         }
    //     );
    //     return;
    // }
    // Processing
    try {
        sequelize.query(queries.Intent.delete, {
            replacements: {
                i_id: req.query.i_id
            }
        }).then((r) => {
            res.status(200).send({
                status: 1,
                result: req.body
            });
        })
    } catch (err) {
        res.status(200).send(
            {
                status: -1,
                result: -1
            }
        )
    }
}



