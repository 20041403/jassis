exports.Agent = {
    getAllDetails: "SELECT agent.*, total_intents.count AS i_total, total_phrases.count AS p_total, total_responses.count AS r_total FROM agent LEFT JOIN (SELECT a_id, count(a_id) FROM intent GROUP BY a_id) AS total_intents ON agent.a_id = total_intents.a_id LEFT JOIN (SELECT intent.a_id, count(intent.a_id) FROM intent, phrase WHERE intent.i_id = phrase.i_id GROUP BY a_id) AS total_phrases ON agent.a_id = total_phrases.a_id LEFT JOIN (SELECT intent.a_id, count(intent.a_id) FROM intent, response WHERE intent.i_id = response.i_id GROUP BY intent.a_id) AS total_responses ON agent.a_id  = total_responses.a_id;",
    add: "INSERT INTO agent(a_name, a_description) VALUES(:a_name,:a_description)",
    delete: "DELETE FROM agent WHERE a_id = :a_id",
    update: "UPDATE agent SET a_name = :a_name, a_description = :a_description, a_update_at = current_timestamp WHERE a_id = :a_id",
    count: "SELECT COUNT(*) FROM agent",
    getById: "SELECT agent.*, total_intents.count as i_total, total_phrases.count as p_total, total_responses.count as r_total FROM agent LEFT JOIN (SELECT a_id, count(a_id) FROM intent GROUP BY a_id ) AS total_intents ON agent.a_id = total_intents.a_id LEFT JOIN (SELECT a_id, count(phrase.i_id) FROM intent, phrase GROUP BY a_id,phrase.i_id ) AS total_phrases ON total_intents.a_id = total_phrases.a_id LEFT JOIN (SELECT a_id, count(response.i_id) FROM intent, response GROUP BY a_id,response.i_id ) AS total_responses ON total_phrases.a_id = total_responses.a_id WHERE agent.a_id = :a_id;"
}

exports.Intent = {
    getAllDetails: "SELECT intent.*, agent.a_name, total_phrases.count as p_total, total_responses.count as r_total FROM intent LEFT JOIN agent ON intent.a_id = agent.a_id LEFT JOIN (SELECT i_id, count(i_id) FROM phrase GROUP BY i_id ) AS total_phrases ON intent.i_id = total_phrases.i_id LEFT JOIN (SELECT i_id, count(i_id) FROM response GROUP BY response.i_id ) AS total_responses ON total_phrases.i_id = total_responses.i_id;",
    getAllDetailsByAgent: "SELECT intent.*, agent.a_name, total_phrases.count as p_total, total_responses.count as r_total FROM intent LEFT JOIN agent ON intent.a_id = agent.a_id LEFT JOIN (SELECT i_id, count(i_id) FROM phrase GROUP BY i_id ) AS total_phrases ON intent.i_id = total_phrases.i_id LEFT JOIN (SELECT i_id, count(i_id) FROM response GROUP BY response.i_id ) AS total_responses ON total_phrases.i_id = total_responses.i_id WHERE intent.a_id = :a_id;",
    add: "INSERT INTO intent(i_name, i_description, a_id) VALUES(:i_name, :i_description, :a_id)",
    delete: "DELETE FROM intent WHERE i_id = :i_id",
    update: "UPDATE intent SET i_name = :i_name, i_description = :i_description, a_id = :a_id WHERE i_id = :i_id",
    count: "SELECT COUNT(*) FROM intent",
    getById: "SELECT intent.*, agent.a_name, total_phrases.count as p_total, total_responses.count as r_total FROM intent LEFT JOIN agent ON intent.a_id = agent.a_id LEFT JOIN (SELECT i_id, count(i_id) FROM phrase GROUP BY i_id ) AS total_phrases ON intent.i_id = total_phrases.i_id LEFT JOIN (SELECT i_id, count(i_id) FROM response GROUP BY response.i_id ) AS total_responses ON total_phrases.i_id = total_responses.i_id WHERE intent.i_id = :i_id;"
}

exports.Phrase = {
    getAllDetails: "SELECT * FROM phrase",
    getAllDetailsByIntent: "SELECT * FROM phrase WHERE i_id = :i_id",
    add: "INSERT INTO phrase(p_phrase, i_id, p_create_by) VALUES (:p_phrase, :i_id, :p_create_by)",
    delete: "DELETE FROM phrase WHERE p_id = :p_id",
    update: "UPDATE phrase SET p_phrase = :p_phrase WHERE p_id = :p_id",
    count: "SELECT COUNT(*) FROM phrase",
    getById: "SELECT * FROM phrase WHERE p_id = :p_id"
}

exports.Response = {
    getAllDetails: "SELECT * FROM response",
    getAllDetailsByIntent: "SELECT * FROM response WHERE i_id = :i_id",
    add: "INSERT INTO response(r_response, r_event, i_id) VALUES(:r_response, :r_event, :i_id);",
    delete: "DELETE FROM response WHERE r_id = :r_id",
    update: "UPDATE response SET r_response = :r_response, r_event = :r_event WHERE r_id = :r_id",
    count: "SELECT COUNT(*) FROM response",
    getById: "SELECT * FROM response WHERE r_id = :r_id"
}

exports.UserAccount = {
    add: "INSERT INTO user_account(ua_username, ua_password, ua_email) VALUES (:ua_username, :ua_password, :ua_email);",
    findUserViaUsernameAndPassword: "SELECT * FROM fc_find_user_via_up(:username, :password)",
    findUserViaEmailAndPassword: "SELECT * FROM fc_find_user_via_ep(:email, :password)",
    changePassword: "UPDATE user_account SET ua_password = :new_pass WHERE ua_id = :ua_id AND ua_password=:old_pass;",
    count: "SELECT COUNT(*) FROM user_account;",
    checkEmail: "SELECT COUNT(*) FROM user_account WHERE ua_email = :ua_email;",
    checkUsername: "SELECT COUNT(*) FROM user_account WHERE ua_username = :ua_username",
    getAll: "SELECT ua_id, up_surename, up_name, ua_username, ua_email, ua_create_at, ua_update_at, ua_roles FROM user_account LEFT JOIN user_profile ON user_account.ua_id = user_profile.up_id",
    getProfileById: "SELECT * FROM user_profile WHERE up_id = :up_id",
    getUserAccountById: "SELECT ua_id, up_surename, up_name, ua_username, ua_email, ua_create_at, ua_update_at, ua_roles FROM user_account LEFT JOIN user_profile ON user_account.ua_id = user_profile.up_id WHERE user_account.ua_id = :ua_id",
    createProfileByUserId: "INSERT INTO user_profile(up_id) VALUES(:up_id)",
    getUsernameViaEmail: "SELECT ua_username FROM user_account WHERE ua_email = :ua_email",
    updateProfile: "UPDATE user_profile SET up_surename=:up_surename, up_name=:up_name, up_birthday=:up_birthday, up_phone=:up_phone, up_home_curr_addr=:up_home_curr_addr, up_home_town_addr=:up_home_town_addr, up_work_place_addr=:up_work_place_addr, up_gender=:up_gender WHERE up_id=:up_id;UPDATE user_account SET ua_roles = :ua_roles WHERE ua_id = :up_id;",
    updateAvatar: "UPDATE user_profile SET up_avatar_addr=:up_avatar_addr WHERE up_id=:up_id;",
    updateCurrentUserCoordinate: "UPDATE user_account SET ua_current_coordinate=:ua_current_coordinate WHERE ua_id=:ua_id;",
    getCurrentUserCoordinate: "SELECT ua_current_coordinate FROM user_account WHERE ua_id=:ua_id",
    deleteAccountBySuperAdmin: "DELETE FROM user_account WHERE ua_id = :ua_id;DELETE FROM user_profile WHERE up_id = :ua_id;",
    getListUser: "SELECT ua_id, up_surename, up_name, ua_username, ua_email, ua_create_at, ua_update_at, ua_roles FROM user_account LEFT JOIN user_profile ON user_account.ua_id = user_profile.up_id WHERE user_account.ua_roles = ANY(:role_codes)",
    countUserByRoles: "SELECT COUNT(*) FROM user_account LEFT JOIN user_profile ON user_account.ua_id = user_profile.up_id WHERE user_account.ua_roles = ANY(:role_codes)",
    changePasswordByAdmin: "UPDATE user_account SET ua_password = :ua_password WHERE ua_username = :ua_username"
}

exports.receivedPhrase = {
    findAnswer: "SELECT * FROM phrase WHERE lower(p_phrase) = lower(:phrase_full) OR p_tsv @@ to_tsquery(:phrase) limit 5;",
    getRandomResponseViaIntentId: "SELECT * FROM response WHERE i_id = :i_id ORDER BY RANDOM() LIMIT 1",
    insertToReceived: "INSERT INTO received(rd_user_query, rd_response, rd_query_by, rd_query_coordinate, rd_current_place_name, rd_is_success) VALUES (:rd_user_query, :rd_response, :rd_query_by, :rd_query_coordinate, :rd_current_place_name, :rd_is_success);",
    countAll: "SELECT COUNT(*) FROM received",
    countResponsed: "SELECT COUNT(*) FROM received WHERE rd_is_success = true",
    countNotYet: "SELECT COUNT(*) FROM received WHERE rd_is_success = false",
    getAll: "select received.*, user_account.ua_username from received LEFT JOIN user_account ON rd_query_by = ua_id",
    getResponsed: "select received.*, user_account.ua_username from received LEFT JOIN user_account ON rd_query_by = ua_id WHERE rd_is_success = true",
    getNotYet: "select received.*, user_account.ua_username from received LEFT JOIN user_account ON rd_query_by = ua_id WHERE rd_is_success = false",
    delete: "DELETE FROM received WHERE rd_id = :rd_id"
}

exports.base64Audio = {
    add: "INSERT INTO audio_stored(as_audio_encoding, as_pitch, as_speaking_rate, as_language_code, as_voice_sound_name, as_base64_audio, as_text, as_text_code) VALUES (:as_audio_encoding, :as_pitch, :as_speaking_rate, :as_language_code, :as_voice_sound_name, :as_base64_audio, :as_text, :as_text_code);",
    find: "SELECT as_base64_audio from audio_stored WHERE as_audio_encoding = :as_audio_encoding AND as_pitch = :as_pitch AND as_speaking_rate = :as_speaking_rate AND as_language_code = :as_language_code AND as_voice_sound_name = :as_voice_sound_name AND as_text_code = :as_text_code;",
    getAll: "SELECT as_id, as_audio_encoding, as_pitch, as_speaking_rate, as_language_code, as_voice_sound_name, as_text, as_text_code FROM audio_stored;",
    getBase64AudioViaId: "SELECT as_base64_audio FROM audio_stored WHERE as_id=:as_id;",
    count: "SELECT COUNT(*) FROM audio_stored;",
    countMaleVoice: "select COUNT(*) from audio_stored WHERE as_voice_sound_name = 'vi-VN-Wavenet-B' OR as_voice_sound_name = 'vi-VN-Wavenet-D'",
    countFemaleVoice: "select COUNT(*) from audio_stored WHERE as_voice_sound_name = 'vi-VN-Wavenet-A' OR as_voice_sound_name = 'vi-VN-Wavenet-C'",
    getMaleVoice: "select as_id, as_audio_encoding, as_pitch, as_speaking_rate, as_language_code, as_voice_sound_name, as_text, as_text_code from audio_stored WHERE as_voice_sound_name = 'vi-VN-Wavenet-B' OR as_voice_sound_name = 'vi-VN-Wavenet-D'",
    getFemaleVoice: "select as_id, as_audio_encoding, as_pitch, as_speaking_rate, as_language_code, as_voice_sound_name, as_text, as_text_code from audio_stored WHERE as_voice_sound_name = 'vi-VN-Wavenet-A' OR as_voice_sound_name = 'vi-VN-Wavenet-C'",
    delete: "DELETE FROM audio_stored WHERE as_id = :as_id"
}