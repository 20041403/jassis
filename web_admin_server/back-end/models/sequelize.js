const Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.DATABASE_NAME, process.env.DATABASE_USER_NAME, process.env.DATABASE_PASSWORD, {
    dialect: process.env.DATABASE_DIALECT,    
    host: process.env.DATABASE_ADDRESS,
    port: process.env.DATABASE_PORT,
    logging : false
});
module.exports = sequelize;