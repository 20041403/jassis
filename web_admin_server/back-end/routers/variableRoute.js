const variableController = require('../controllers/variableController.js');

const variableRoute = [
    {
        method: "GET",
        path: `/${variableController.root}/get_base_variable`,
        handler: variableController.getAllBaseVariable
    },
    {
        method: "GET",
        path: `/${variableController.root}/get_condition_variable`,
        handler: variableController.getAllConditionVariable
    },
    {
        method: "GET",
        path: `/${variableController.root}/get_calculate_variable`,
        handler: variableController.getAllCalculateVariable
    },
]

module.exports = variableRoute;