const receivedPhraseController = require('../controllers/receivedPhraseController.js');

const receivedPhraseRoute = [{
        method: "POST",
        path: `/${receivedPhraseController.root}/find`,
        handler: receivedPhraseController.findAnswer
    },
    {
        method: "GET",
        path: `/${receivedPhraseController.root}/count_all`,
        handler: receivedPhraseController.countAll
    },
    {
        method: "GET",
        path: `/${receivedPhraseController.root}/count_responsed`,
        handler: receivedPhraseController.countResponsed,
    },
    {
        method: "GET",
        path: `/${receivedPhraseController.root}/count_not_yet`,
        handler: receivedPhraseController.countNotYet,
    },
    {
        method: "GET",
        path: `/${receivedPhraseController.root}/get_all`,
        handler: receivedPhraseController.getAll,
    },
    {
        method: "GET",
        path: `/${receivedPhraseController.root}/get_responsed`,
        handler: receivedPhraseController.getResponsed,
    },
    {
        method: "GET",
        path: `/${receivedPhraseController.root}/get_not_yet`,
        handler: receivedPhraseController.getNotYet,
    },
    {
        method: "GET",
        path: `/${receivedPhraseController.root}/text_to_speech`,
        handler: receivedPhraseController.textToSpeech,
    },
    {
        method: "DELETE",
        path: `/${receivedPhraseController.root}/delete`,
        handler: receivedPhraseController.delete,
    }
]

module.exports = receivedPhraseRoute;