const base64AudioController = require('../controllers/base64AudioController.js');

const base64AudioRoute = [
    {
        method: "GET",
        path: `/${base64AudioController.root}/get_male_voice`,
        handler: base64AudioController.getMaleVoice
    },
    {
        method: "GET",
        path: `/${base64AudioController.root}/get_female_voice`,
        handler: base64AudioController.getFemaleVoice
    },
    {
        method: "GET",
        path: `/${base64AudioController.root}/get_all`,
        handler: base64AudioController.getAll
    },
    {
        method: "GET",
        path: `/${base64AudioController.root}/get_audio`,
        handler: base64AudioController.getBase64AudioViaId
    },
    {
        method: "GET",
        path: `/${base64AudioController.root}/count_all`,
        handler: base64AudioController.countAll
    },
    {
        method: "GET",
        path: `/${base64AudioController.root}/count_female_voice`,
        handler: base64AudioController.countFemaleVoice
    },
    {
        method: "GET",
        path: `/${base64AudioController.root}/count_male_voice`,
        handler: base64AudioController.countMaleVoice
    },
    {
        method: "DELETE",
        path: `/${base64AudioController.root}/delete`,
        handler: base64AudioController.delete
    }
]

module.exports = base64AudioRoute;