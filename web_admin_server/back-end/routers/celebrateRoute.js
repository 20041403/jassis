const celebrateController = require('../controllers/celebrateController.js');

const celebrateRoute = [
    {
        method: "GET",
        path: `/${celebrateController.root}/find`,
        handler: celebrateController.find
    },
]

module.exports = celebrateRoute;