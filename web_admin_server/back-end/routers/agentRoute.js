const agentController = require('../controllers/agentController');

const agentRoute = [
    {
        method: "GET",
        path: `/${agentController.root}/count`,
        handler: agentController.countAll
    },
    {
        method: "POST",
        path: `/${agentController.root}/add`,
        handler: agentController.add
    },
    {
        method: "GET",
        path: `/${agentController.root}/get_all`,
        handler: agentController.getAll
    },
    {
        method: "GET",
        path: `/${agentController.root}/get`,
        handler: agentController.get
    },
    {
        method: "PUT",
        path: `/${agentController.root}/update`,
        handler: agentController.update
    },
    {
        method: "DELETE",
        path: `/${agentController.root}/delete`,
        handler: agentController.delete
    }
]
module.exports = agentRoute;