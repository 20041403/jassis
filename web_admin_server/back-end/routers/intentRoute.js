const intentController = require('../controllers/intentController');

const intentRoute = [
    {
        method: "GET",
        path: `/${intentController.root}/count`,
        handler: intentController.countAll
    },
    {
        method: "POST",
        path: `/${intentController.root}/add`,
        handler: intentController.add
    },
    {
        method: "GET",
        path: `/${intentController.root}/get_all`,
        handler: intentController.getAll
    },
    {
        method: "GET",
        path: `/${intentController.root}/get`,
        handler: intentController.get
    },
    {
        method: "PUT",
        path: `/${intentController.root}/update`,
        handler: intentController.update
    },
    {
        method: "DELETE",
        path: `/${intentController.root}/delete`,
        handler: intentController.delete
    }
]
module.exports = intentRoute;