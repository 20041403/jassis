const userAccountController = require('../controllers/userAccountController.js');

const userAccountRoute = [
    {
        method: "GET",
        path: `/${userAccountController.root}/get_all`,
        handler: userAccountController.getAll
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/count`,
        handler: userAccountController.countAll
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/count_user_by_roles`,
        handler: userAccountController.countUserByRoles
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/get_all_roles`,
        handler: userAccountController.getAllRoles
    },
    {
        method: "POST",
        path: `/${userAccountController.root}/add`,
        handler: userAccountController.add
    },
    {
        method: "POST",
        path: `/${userAccountController.root}/login`,
        handler: userAccountController.login
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/logout`,
        handler: userAccountController.logout
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/token_verify`,
        handler: userAccountController.tokenVerify
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/check_username`,
        handler: userAccountController.checkUsername
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/check_email`,
        handler: userAccountController.checkEmail
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/profile`,
        handler: userAccountController.getProfileById
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/account`,
        handler: userAccountController.getUserAccountById
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/account_by_roles`,
        handler: userAccountController.getUserByRoles
    },
    {
        method: "GET",
        path: '/public/images',
        handler: userAccountController.getUserImage
    },
    {
        method: "GET",
        path: `/${userAccountController.root}/current_coordinate`,
        handler: userAccountController.getCurrentUserCoordinate
    },
    {
        method: "PUT",
        path: `/${userAccountController.root}/current_coordinate`,
        handler: userAccountController.updateCurrentUserCoordinate
    },
    {
        method: "PUT",
        path: `/${userAccountController.root}/change_password`,
        handler: userAccountController.changePassword
    },
    {
        method: "PUT",
        path: `/${userAccountController.root}/change_password_by_admin`,
        handler: userAccountController.changePasswordByAdmin
    },
    {
        method: "PUT",
        path: `/${userAccountController.root}/update_profile`,
        handler: userAccountController.updateProfile
    },
    {
        method: "PUT",
        path: `/${userAccountController.root}/update_avatar`,
        handler: userAccountController.updateAvatar
    },
    {
        method: "DELETE",
        path: `/${userAccountController.root}/delete`,
        handler: userAccountController.delete
    },
]
module.exports = userAccountRoute;