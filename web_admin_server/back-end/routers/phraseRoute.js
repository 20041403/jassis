const phraseController = require('../controllers/phraseController');

const phraseRoute = [
    {
        method: "GET",
        path: `/${phraseController.root}/count`,
        handler: phraseController.countAll
    },
    {
        method: "POST",
        path: `/${phraseController.root}/add`,
        handler: phraseController.add
    },
    {
        method: "GET",
        path: `/${phraseController.root}/get_all`,
        handler: phraseController.getAll
    },
    {
        method: "GET",
        path: `/${phraseController.root}/get`,
        handler: phraseController.get
    },
    {
        method: "PUT",
        path: `/${phraseController.root}/update`,
        handler: phraseController.update
    },
    {
        method: "DELETE",
        path: `/${phraseController.root}/delete`,
        handler: phraseController.delete
    }
]
module.exports = phraseRoute;