const fs = require('fs');
const path = require('path');


routes = []

fs.readdirSync(__dirname).forEach((file) => {
    let current_path = path.join(__dirname, file);
    if (current_path != __filename) {
        temp = require(current_path);
        routes.push(...temp);
    }
})

module.exports = routes;