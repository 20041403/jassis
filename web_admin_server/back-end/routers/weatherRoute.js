const weatherController = require('../controllers/weatherController.js');

const weatherRoute = [
    {
        method: "GET",
        path: `/${weatherController.root}/forecast`,
        handler: weatherController.forecast
    }
]

module.exports = weatherRoute;