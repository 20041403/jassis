const responseController = require('../controllers/responseController');

const responseRoute = [
    {
        method: "GET",
        path: `/${responseController.root}/count`,
        handler: responseController.countAll
    },
    {
        method: "POST",
        path: `/${responseController.root}/add`,
        handler: responseController.add
    },
    {
        method: "GET",
        path: `/${responseController.root}/get_all`,
        handler: responseController.getAll
    },
    {
        method: "GET",
        path: `/${responseController.root}/get`,
        handler: responseController.get
    },
    {
        method: "PUT",
        path: `/${responseController.root}/update`,
        handler: responseController.update
    },
    {
        method: "DELETE",
        path: `/${responseController.root}/delete`,
        handler: responseController.delete
    }
]
module.exports = responseRoute;