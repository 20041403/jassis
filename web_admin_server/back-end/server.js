require('dotenv').config();
var fs = require('fs');
const fastify = require("fastify")({
    // logger : true,
});
const routes = require("./routers/index");

const path = require('path')

// KHởi tạo mục cho phép download file tĩnh
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/public/', // optional: default '/'
});
// Nếu thư mục chứa các file tĩnh chưa có thì tiến hành tạo mới
if (!fs.existsSync(path.join(__dirname, 'public'))) {
  fs.mkdirSync(path.join(__dirname, 'public'));
}
// Kết thúc phần khởi tạo chứa file

fastify.register(require('fastify-multipart'))

fastify.register(require('fastify-cors'), { 
  // put your options here
})

fastify.register(require('fastify-cookie'))


// Declare a route
routes.forEach((x)=>{
    fastify.route(x);
})

// Run the server!
fastify.listen(process.env.PORT, '0.0.0.0', function(err, address) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
  console.log(`Server listen on ${process.env.PORT}`);
  fastify.log.info(`server listening on ${address}`);
});
