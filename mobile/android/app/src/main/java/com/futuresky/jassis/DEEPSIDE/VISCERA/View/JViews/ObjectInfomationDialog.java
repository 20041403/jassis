package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectOverview;
import com.futuresky.jassis.R;
import com.meetic.marypopup.MaryPopup;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import es.dmoral.toasty.Toasty;

public class ObjectInfomationDialog {
    public static boolean isShow = false;
    public static LinearLayout ivCloseButton;
    public static void Show(final Activity mContext, ObjectOverview mObjects, View from) {
        // Infilate layout
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.module_layout_show_object_definition, null);
        // Create Dialog
        final MaryPopup popup = MaryPopup.with(mContext)
                .content(v)
                .from(from)
                .blackOverlayColor(0)
                .backgroundColor(0)
                .center(true)
                .draggable(true)
                .scaleDownDragging(true)
                .closeDuration(500)
                .cancellable(false);
        // 1. Find and init view
        SliderLayout images = v.findViewById(R.id.object_avatar);
        // ---- Init slide layout
        for (int i = 0; i < mObjects.getImages().size(); i++) {
            SliderView sliderView = new DefaultSliderView(mContext);
            sliderView.setImageUrl(mObjects.getImages().get(i).getImageLink());
            sliderView.setDescription(String.format("Ảnh %d/%d", i+1, mObjects.getImages().size()));
            int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    Toasty.info(mContext, mObjects.getImages().get(finalI).getDescription());
//                    Toast.makeText(mContext, "Thao tác với ảnh đối tượng chưa khả dụng ở phiên bản này!", Toast.LENGTH_SHORT).show();
                }
            });
            images.addSliderView(sliderView);
        }
        // 2. Init title
        TextView tvTitle = v.findViewById(R.id.object_name);
        tvTitle.setText(mObjects.getName());
        // 3. Set content
        TextView wvContent =  v.findViewById(R.id.webView_showContent);
        wvContent.setText(mObjects.getContent());
        // 4. Set Close button
        ivCloseButton = v.findViewById(R.id.close_object_overview);
        ivCloseButton.setOnClickListener(v1 -> {
            if (popup.isOpened())
                popup.close(true);
            isShow = false;
        });
        popup.show();
        isShow = true;
    }
}
