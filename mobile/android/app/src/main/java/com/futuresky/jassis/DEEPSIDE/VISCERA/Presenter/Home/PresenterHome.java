package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Home;

import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.TimerForSwitchOnServicesDeepListen;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.UpdateLoactionServices;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.CustomAnimation.FlipAnimation;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.LongMsgContentShow;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.ObjectInfomationDialog;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;

import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener.OpenConnection;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Home.SpeechRecognizerManager.mInterfaceSpeechRecognizer;

public class PresenterHome {
    public final static String TAG = PresenterHome.class.getSimpleName();
    ActivityHome mContext;
    public SpeechRecognizerManager mSpeechRecognizerManager;

    public PresenterHome(ActivityHome mContext) {
        this.mContext = mContext;
        InitSpeechRecognizerManager();
        InitEvents();
        FinishTalk();

        // Start websocket connection
        OpenConnection();
        // Start location services
        mContext.startService(new Intent(mContext, UpdateLoactionServices.class));
    }

    private void ExitButton() {
        mContext.cbExitButton.setOnClickListener(v -> {

            SweetAlertDialog aks = new SweetAlertDialog(mContext)
                    .setTitleText("ĐÓNG HỆ THỐNG JASSIS?")
                    .setConfirmButton("Đóng JASSIS", sweetAlertDialog -> {
                        if (TimerForSwitchOnServicesDeepListen.waitingObj != null) {
                            TimerForSwitchOnServicesDeepListen.waitingObj.cancel();
                            TimerForSwitchOnServicesDeepListen.waitingObj.purge();
                            TimerForSwitchOnServicesDeepListen.waitingObj = null;
                        }
                        if (mContext.mJassisListenServices != null)
                            mContext.mJassisListenServices.destroyListen();
                        mContext.mPresenter.mSpeechRecognizerManager.destroyListen();
                        SystemClock.sleep(80);
                        sweetAlertDialog.cancel();
                        mContext.finish();
                    })
                    .setCancelButton("Hủy", null);
            aks.create();
            aks.show();
        });
    }

    private void InitSpeechRecognizerManager() {
        mSpeechRecognizerManager = new SpeechRecognizerManager(mContext);
    }

    private void InitEvents() {
        TouchScreenEvents();
        ExitButton();
        MenuButton();
        UserTalkMsgClick();
        AITalkingClick();
    }

    private void AITalkingClick() {
        mContext.findViewById(R.id.ai_talking_wrapped)
                .setOnLongClickListener(view -> {
                    LongMsgContentShow.Show(mContext, mContext.stvWhatAITalk.getText().toString(), mContext.rlRootLayout);
                    return true;
                });
    }

    private void UserTalkMsgClick() {
        mContext.ltvWhatUserTalk.setOnLongClickListener(view -> {
            LongMsgContentShow.Show(mContext, mContext.ltvWhatUserTalk.getText().toString(), mContext.rlRootLayout);
            return true;
        });
    }

    private void MenuButton() {
        mContext.cbMenuButton.setOnClickListener(v -> {
            mContext.mInitBoomMenu.bmb.boom();
        });
    }

    public void StartListen() {
        Log.w(TAG, "StartListen: Tiến hành bắt đầu lắng nghe!");
        if (mInterfaceSpeechRecognizer != null)
            mSpeechRecognizerManager.destroyListen();
        InitSpeechRecognizerManager();
        mSpeechRecognizerManager.startListening();
        mContext.ivStateMicroIcon.startAnimation(new FlipAnimation());
    }

    public void StopListen() {
        Log.w(TAG, "StartListen: Dừng việc lắng nghe!");
        mSpeechRecognizerManager.destroyListen();
        FinishTalk();
        mContext.ivStateLogoIcon.startAnimation(new FlipAnimation());
    }

    private void TouchScreenEvents() {
        mContext.lnlayoutTouchScreen.setOnClickListener(v -> {
//            Log.w(TAG, "TouchScreenEvents: Đã nhấn vào nút nói lúc giá trị cờ bằng = " + (mInterfaceSpeechRecognizer != null));
            if (mInterfaceSpeechRecognizer != null && mContext.prgbarVoiceShow.getVisibility() == View.VISIBLE) {
                FinishTalk();
                StopListen();
            } else {// if (mInterfaceSpeechRecognizer == null || mContext.prgbarVoiceShow.getVisibility() == View.INVISIBLE) {
                ShowTalk();
                StartListen();
            }
        });
        mContext.lnlayoutTouchScreen.setOnLongClickListener(v -> {
            if (mInterfaceSpeechRecognizer != null && mContext.prgbarVoiceShow.getVisibility() == View.VISIBLE) {
                FinishTalk();
                StopListen();
            }
            mContext.StartUserInput();
            return true;
        });
    }

    public void ShowTalk() {
        final Timer[] TimerWaitFinishTalk = {new Timer()};
        TimerWaitFinishTalk[0].schedule(new TimerTask() {
            @Override
            public void run() {
//                Log.w(TAG, "run: Đợi đến khi CÁC view được cập nhật");
                while (mContext.stvWhatAITalk == null ||
                        mContext.prgbarVoiceShow == null ||
                        mContext.ftvHintText == null ||
                        mContext.rbAnimationVoice == null ||
                        mContext.rbAnimation == null
                )
                    SystemClock.sleep(5);
//                Log.w(TAG, "run: View đã cập nhật xong! Tiến hành hiển thị giao diện START TALK");

                mContext.prgbarVoiceShow.post(() -> {
                    mContext.prgbarVoiceShow.setVisibility(View.VISIBLE);
                });
                mContext.ftvHintText.post(() -> {
                    mContext.ftvHintText.setTexts(R.array.tap_to_stop);
                    mContext.ftvHintText.restart();
                });
                mContext.rbAnimationVoice.post(() -> {
                    mContext.rbAnimationVoice.setVisibility(View.VISIBLE);
                    mContext.rbAnimationVoice.startRippleAnimation();
                });
                mContext.rbAnimation.post(() -> {
                    mContext.rbAnimation.setVisibility(View.INVISIBLE);
                    mContext.rbAnimation.stopRippleAnimation();
                });
                // Hủy bỏ timer
                TimerWaitFinishTalk[0].cancel();
                TimerWaitFinishTalk[0].purge();
                TimerWaitFinishTalk[0] = null;
            }
        }, 15, 100);
        // Tạm ngưng nhạc nếu có
        if (BODY.MUSIC_PLAYER != null && BODY.MUSIC_PLAYER.mediaPlayer.isPlaying() && !BODY.MUSIC_PLAYER.IS_PAUSE) {
            BODY.MUSIC_PLAYER.ivPlayerControlButton.performClick();
            Log.w(TAG, "StartListen: Đã tạm ngưng nhạc!");
        }
        // Hủy xem đối tượng nếu có
        if (ObjectInfomationDialog.isShow && ObjectInfomationDialog.ivCloseButton != null) {
            ObjectInfomationDialog.ivCloseButton.performClick();
        }
    }

    public void FinishTalk() {
        final Timer[] TimerWaitFinishTalk = {new Timer()};
        TimerWaitFinishTalk[0].schedule(new TimerTask() {
            @Override
            public void run() {
//                Log.w(TAG, "run: Đợi đến khi CÁC view được cập nhật");
                while (mContext.stvWhatAITalk == null ||
                        mContext.prgbarVoiceShow == null ||
                        mContext.ftvHintText == null ||
                        mContext.rbAnimationVoice == null ||
                        mContext.rbAnimation == null
                )
                    SystemClock.sleep(5);
//                Log.w(TAG, "run: View đã cập nhật xong! Tiến hành hiển thị giao diện FINISH TALK");

                mContext.prgbarVoiceShow.post(() -> {
                    mContext.prgbarVoiceShow.setVisibility(View.INVISIBLE);
                });
                mContext.ftvHintText.post(() -> {
                    mContext.ftvHintText.setTexts(R.array.tap_to_talk);
                    mContext.ftvHintText.restart();
                });
                mContext.rbAnimationVoice.post(() -> {
                    mContext.rbAnimationVoice.setVisibility(View.INVISIBLE);
                    mContext.rbAnimationVoice.stopRippleAnimation();
                });
                mContext.rbAnimation.post(() -> {
                    mContext.rbAnimation.setVisibility(View.VISIBLE);
                    mContext.rbAnimation.startRippleAnimation();
                });
                // Hủy bỏ timer
                TimerWaitFinishTalk[0].cancel();
                TimerWaitFinishTalk[0].purge();
                TimerWaitFinishTalk[0] = null;
            }
        }, 15, 100);
        // Tiến hành tiếp tục hát nếu còn
        if (BODY.MUSIC_PLAYER != null && BODY.MUSIC_PLAYER.IS_PAUSE) {
            BODY.MUSIC_PLAYER.ivPlayerControlButton.performClick();
            Log.w(TAG, "StartListen: Tiếp tục phát nhạc!");
        }
    }
}
