package com.futuresky.jassis.DEEPSIDE.VISCERA.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.futuresky.jassis.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }
}
