package com.futuresky.jassis.DEEPSIDE.VISCERA.Modal;

import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectMessage;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.SimpleFriendInfo;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.MessageBox.MessageList;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.SimpleFriendList.SimpleFriendList;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.TNLib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class EchoWebsockerListener extends WebSocketListener {
    public static WebSocket ws;
    public static String wsid = "";
    public final static String TAG = EchoWebsockerListener.class.getSimpleName();
    private static final int NORMAL_CLOSURE_STATUS = 1000;

    Timer timer = null;

    public static void CloseAll() {
        // Nếu trước đó đã có kết nối, kết nói này là kết nối trùng lặp, tiến hành xóa bỏ
        if (ws != null) {
            // Đóng kết nối, đề phòng đang bắt tay
            ws.close(NORMAL_CLOSURE_STATUS, "Overlap");
            // Hủy kết nối đó
            ws.cancel();
            // Giải phóng web socket
            ws = null;
            // NGhỉ 50ms
            SystemClock.sleep(50);
        }
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
//        webSocket.send("What's up ?");
//        webSocket.send(ByteString.decodeHex("deadbeef"));
//        webSocket.close(NORMAL_CLOSURE_STATUS, "Goodbye !");
    }

    public static void OpenConnection() {
        // Khởi tạo đối tượng để yếu cầu đến websocket server
        Request request = new Request.Builder().url(NETWORK_CONFIG.WEBSOCKET_SERVER_ADDRESS).build();
        Log.w(TAG, "OpenConnection: " + NETWORK_CONFIG.WEBSOCKET_SERVER_ADDRESS);
        Log.w(TAG, "OPENCONNECTION: " + NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN);
        // Khởi tạo lớp lắng nghe
        EchoWebsockerListener listener = new EchoWebsockerListener();
        // Khởi tạo client hiện hành
        OkHttpClient client = new OkHttpClient();
        //
        CloseAll();
        // Khởi tạo kết nối websocket đến server và lắng nghe nó
        ws = client.newWebSocket(request, listener);
        // Gửi chuỗi kết nối xác thực đến websocket server trước khi có thể thực hiện các thao tác khác
        OpenCnn();
    }

    public static void OpenCnn() {
        try {
            JSONObject cnnObj = new JSONObject();
            cnnObj.put("action", OptionType.CONNECT);
            cnnObj.put("token", NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN);
            ws.send(cnnObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.w(TAG, "onMessage: " + "Receiving bytes : " + bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null);
        Log.w(TAG, "onClosing: " + "Closing : " + code + " / " + reason);
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        Log.e(TAG, "onFailure: " + "Error : " + t.getMessage());
        CloseAll();
        if (timer == null && ws == null) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    OpenConnection();
                    if (ws != null)
                    {
                        timer.cancel();
                        timer.purge();
                        timer = null;
                    }
                }
            }, 500, 500);
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        Log.w(TAG, "onMessage: " + "Receiving : " + text);
        try {
            JSONObject response = new JSONObject(text);
            String actionType = response.getString("action");
            if (actionType == null || actionType.isEmpty()) {
                throw new Exception("Kết quả trả về rỗng!");
            } else if (actionType.toUpperCase().equals(OptionType.CONNECT)) {
                try {
                    JSONObject result = response.getJSONObject("result");
                    if (result.getInt("status_code") != 1) {
                        throw new Exception("Thiết lập kết nối không thành công!");
                    } else {
                        wsid = result.getString("ws_id");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (actionType.toUpperCase().equals(OptionType.GETSIMPLEFRIENDLIST)) {
                try {
                    JSONObject result = response.getJSONObject("result");
                    if (result.getInt("status_code") != 1) {
                        throw new Exception("Thiết lập kết nối không thành công!");
                    } else {
                        JSONArray clients = result.getJSONArray("clients");
                        if (clients.length() <= 0)
                            return;
                        for (int i = 0; i < clients.length(); i++) {
                            JSONObject client = clients.getJSONObject(i);
                            if (SimpleFriendList.data != null && SimpleFriendList.data.size() > 0) {
                                for (int j = 0; j < SimpleFriendList.data.size(); j++) {
                                    if (SimpleFriendList.data.get(j).getUserId() == client.getInt("user_id")) {
                                        SimpleFriendList.data.get(j).UpdateStatus(client.getString("key"));
                                        if (SimpleFriendList.simpleFriendListAdapter != null) {
                                            BODY.mContext.runOnUiThread(() -> {
                                                SimpleFriendList.simpleFriendListAdapter.notifyDataSetChanged();
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (actionType.toUpperCase().equals(OptionType.SENDMESSAGE)) {
                try {
                    JSONObject result = response.getJSONObject("result");
                    if (result.getInt("status_code") != 1) {
                        throw new Exception("Thiết lập kết nối không thành công!");
                    } else {
                        String content = result.getString("content");
                        content = new String(Base64.decode(content, Base64.DEFAULT), StandardCharsets.UTF_8);
                        String finalContent = content;

                        // Json
                        JSONObject msg = new JSONObject(content);
                        if (MessageList.isShow && msg.getInt("sender_id") == MessageList.Current_user_id)
                            MessageList.ReceivedMessage(msg);
                        else {
                            if (SimpleFriendList.data != null && SimpleFriendList.data.size() > 0) {
                                for (int j = 0; j < SimpleFriendList.data.size(); j++) {
                                    if (SimpleFriendList.data.get(j).getUserId() == msg.getInt("sender_id")) {
                                        SimpleFriendList.data.get(j).addUnreadMessage(
                                                new ObjectMessage(0, msg.getString("msg"), msg.getInt("sender_id"), msg.getString("send_at"), TNLib.Using.GetCurrentTimeStamp())
                                        );
                                        if (SimpleFriendList.simpleFriendListAdapter != null) {
                                            BODY.mContext.runOnUiThread(() -> {
                                                SimpleFriendList.simpleFriendListAdapter.notifyDataSetChanged();
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (actionType.toUpperCase().equals(OptionType.OFFLINE)) {
                try {
                    JSONObject result = response.getJSONObject("result");
                    if (result.getInt("status_code") != 1) {
                        throw new Exception("Thiết lập kết nối không thành công!");
                    } else {
                        JSONObject user = result.getJSONObject("user");
                        if (SimpleFriendList.data != null && SimpleFriendList.data.size() > 0) {
                            for (int j = 0; j < SimpleFriendList.data.size(); j++) {
                                if (SimpleFriendList.data.get(j).getUserId() == user.getInt("user_id")) {
                                    SimpleFriendList.data.get(j).UpdateStatus("");
                                    if (SimpleFriendList.simpleFriendListAdapter != null) {
                                        BODY.mContext.runOnUiThread(() -> {
                                            SimpleFriendList.simpleFriendListAdapter.notifyDataSetChanged();
                                        });
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (actionType.toUpperCase().equals(OptionType.ONLINE)) {
                try {
                    JSONObject result = response.getJSONObject("result");
                    if (result.getInt("status_code") != 1) {
                        throw new Exception("Thiết lập kết nối không thành công!");
                    } else {
                        JSONObject user = result.getJSONObject("user");
                        if (SimpleFriendList.data != null && SimpleFriendList.data.size() > 0) {
                            for (int j = 0; j < SimpleFriendList.data.size(); j++) {
                                if (SimpleFriendList.data.get(j).getUserId() == user.getInt("user_id")) {
                                    // Chứa tạm giá trị
                                    SimpleFriendInfo temp = SimpleFriendList.data.get(j);
                                    temp.UpdateStatus(user.getString("key"));
                                    // Xóa giá trị cũ
                                    SimpleFriendList.data.remove(j);
                                    // Thêm lại người dùng vừa online vào đầu
                                    SimpleFriendList.data.add(0, temp);

                                    if (SimpleFriendList.simpleFriendListAdapter != null) {
                                        BODY.mContext.runOnUiThread(() -> {
                                            SimpleFriendList.simpleFriendListAdapter.notifyDataSetChanged();
                                        });
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class OptionType {
        public final static String CONNECT = "CONNECT";
        public final static String GETSIMPLEFRIENDLIST = "GETSIMPLEFRIENDLIST";
        public final static String SENDMESSAGE = "SENDMESSAGE";
        public final static String ONLINE = "ONLINE";
        public final static String OFFLINE = "OFFLINE";
        public final static String ONLOCATIONCHANGE = "ONLOCATIONCHANGE";
    }
}
