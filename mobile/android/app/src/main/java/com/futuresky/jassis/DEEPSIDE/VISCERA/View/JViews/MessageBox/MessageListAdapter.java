package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.MessageBox;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectMessage;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.SimpleFriendInfo;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;

import java.util.ArrayList;
import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.RecyclerViewHolder> {
    private List<ObjectMessage> data = new ArrayList<>();

    public MessageListAdapter(List<ObjectMessage> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = null;
        if (i != NETWORK_CONFIG.CLIENT.USER_ID)
            view = inflater.inflate(R.layout.module_layout_message_records_frined, viewGroup, false);
        else
            view = inflater.inflate(R.layout.module_layout_message_records_current_user, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getSender_id();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int i) {
        holder.msgContent.setText(data.get(i).getMsg());
        holder.timeContent.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView msgContent;
        TextView timeContent;
        public RecyclerViewHolder(@NonNull View v) {
            super(v);
            msgContent = v.findViewById(R.id.x1580_msg_content);
            timeContent = v.findViewById(R.id.x1580_msg_time);
        }
    }
}
