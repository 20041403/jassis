package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.futuresky.jassis.INTERFACE.BODY;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationProcessing {
    public final static String SYSTEM_PACKAGE_NAME = "android";
    public final static String TAG = ApplicationProcessing.class.getSimpleName();
    public static String OpenAppliaction(String applicationName) {
        applicationName = applicationName.toLowerCase().trim();
        final PackageManager pm = BODY.mContext.getPackageManager();

        String appName = "";
        String packageName = "";
        HashMap<String, String> similars = new HashMap<>();

        //get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
//            if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0)
//                continue;
            String app = packageInfo.loadLabel(BODY.mContext.getPackageManager()).toString();
            Log.e(TAG, "Search application: [" + applicationName + "] ? [" + app.toLowerCase() + "] (" + packageInfo.packageName + ")");
            if (applicationName.equals(app.toLowerCase()))
            {
                Log.w(TAG, "OpenAppliaction: Founded >>" + app);
                appName = app;
                packageName = packageInfo.packageName;
                break;
            } else if (applicationName.contains(app.toLowerCase()))
            {
                Log.w(TAG, "OpenAppliaction: Founded ||>>" + app);
                similars.put(app, packageInfo.packageName);
            }
        }
        if (packageName.isEmpty()) {
            if (similars.size()>=1) {
                String tempStr = applicationName.replace(" ","");
                int minDifferent = 0;
                for (Map.Entry<String, String> similar : similars.entrySet())
                {
                    int tempMinDiff =  tempStr.replaceAll(similar.getKey().trim().toLowerCase().replace(" ", "|"),"").length();
                    Log.w(TAG, "OpenAppliaction_AfterReplace:" + tempStr.replaceAll(similar.getKey().trim().toLowerCase().replace(" ", "|"),""));
                    if (packageName.isEmpty())
                    {
                        appName = similar.getKey();
                        packageName = similar.getValue();
                        minDifferent = tempMinDiff;
                        Log.w(TAG, "OpenAppliaction_SET_1s: " + appName + " " + packageName + " " + minDifferent);
                    } else if (tempMinDiff < minDifferent)
                    {
                        appName = similar.getKey();
                        packageName = similar.getValue();
                        minDifferent = tempMinDiff;
                        Log.w(TAG, "OpenAppliaction_SET_1s: " + appName + " " + packageName + " " + minDifferent);
                    }
                }
                // Thực hiện tính toán mức độ trùng khớp
            } else if (similars.size() <1 ) {
                // Khi không có ứng dụng nào tương thính, tiến hành thông báo cho người dùng
                Log.w(TAG, "Không có ứng dụng tương thích [" + applicationName + "]");
                return "";
            }
        } else {
            Log.w(TAG, "OpenAppliaction: PackageName not empty! [" + packageName + "]");
        }
        Log.w(TAG, "Mở [" + appName + "] >> [" + packageName + "]");
        Intent LaunchApp = pm.getLaunchIntentForPackage(packageName);
        if (LaunchApp != null) {
            BODY.mContext.startActivity(LaunchApp);
        }
        return appName;
    }
    public static boolean isSystemApp(String packageName) {
        PackageManager pm = (PackageManager) BODY.mContext.getPackageManager();
        try {
            PackageInfo pi_app = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            PackageInfo pi_sys = pm.getPackageInfo("android", PackageManager.GET_SIGNATURES);
            return (pi_app != null
                    && pi_app.signatures != null
                    && pi_sys.signatures[0].equals(pi_app.signatures[0]));
        }catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "exception", e);
            return false;
        }
    }
    public static boolean isPrivilegedApp(ApplicationInfo ai) {
        try {
            Method method = ApplicationInfo.class.getDeclaredMethod("isPrivilegedApp");
            return (Boolean)method.invoke(ai);
        } catch(Exception e) {
            Log.d(TAG, "exception", e);
            return false;
        }
    }
}
