package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.BaseLibraries;

import java.util.HashMap;

public class StandardSubject {
    private static String dest = "";
    public static String DoIt(String src) {
        dest = src.toLowerCase();
        HashMap<String, String> libs = new HashMap<>();
        //region Data
        libs.put("tao", "tôi");
        libs.put("tui", "tôi");
        libs.put("tớ", "tôi");
        libs.put("mình", "tôi");
        libs.put("hỗ trợ", "giúp");
        libs.put("cho em", "cho tôi");
        libs.put("cho chị", "cho tôi");
        libs.put("cho chú", "cho tôi");
        libs.put("cho thầy", "cho tôi");
        libs.put("cho anh", "cho tôi");
        libs.put("cho bác", "cho tôi");
        libs.put("cho cô", "cho tôi");
        libs.put("cho bố", "cho tôi");
        libs.put("giúp em", "giúp tôi");
        libs.put("giúp chị", "giúp tôi");
        libs.put("giúp chú", "giúp tôi");
        libs.put("giúp thầy", "giúp tôi");
        libs.put("giúp anh", "giúp tôi");
        libs.put("giúp bác", "giúp tôi");
        libs.put("giúp cô", "giúp tôi");
        libs.put("giúp bố", "giúp tôi");
        libs.put("hiểu", "biết");
        //endregion
        libs.forEach((keyString, valueString) -> {
            dest = dest.replace(keyString, valueString);
        });
        return  dest;
    }
}

