package com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home;

import android.Manifest;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PersistableBundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.BRAIN.RESPONSE;
import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Home.PresenterHome;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.JassisDeepListenCallback;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.JassisDeepListenServices;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.AccountLogRes.ModulLoginDialog;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.CustomAnimation.FlipAnimation;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;
import com.hanks.htextview.scale.ScaleTextView;
import com.hanks.htextview.typer.TyperTextView;
import com.jacksonandroidnetworking.JacksonParserFactory;
import com.meetic.marypopup.MaryPopup;
import com.skyfishjy.library.RippleBackground;
import com.tomer.fadingtextview.FadingTextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;

public class ActivityHome extends AppCompatActivity implements JassisDeepListenCallback {
    public final static String TAG = ActivityHome.class.getSimpleName();

    //region For hidd to fullscreen
    public static boolean IS_RUNNING = true;
    public static boolean IS_MATCHED_CALL = false;
    public static PowerManager pm;

    Handler mHandler = new Handler(Looper.myLooper()) {
    };

    private void HideActionBarAndStatusBarForFullScreen() {
        int w = getWindow().getDecorView().getWidth();
        int h = getWindow().getDecorView().getHeight();

        if ((float) w / h < 2) {
            if (getSupportActionBar() != null)
                getSupportActionBar().hide();
//            mHandler.postDelayed(() -> getWindow().getDecorView().setSystemUiVisibility(
//                    //View.SYSTEM_UI_FLAG_FULLSCREEN |
//                            //View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
//                            //View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
//                            //View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//            ), 50);
        }
    }

    private void AutoHide() {
        int w = getWindow().getDecorView().getWidth();
        int h = getWindow().getDecorView().getHeight();

        if ((float) w / h < 2) {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    HideActionBarAndStatusBarForFullScreen();
                }
            }, 1000, 200);
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        HideActionBarAndStatusBarForFullScreen();
    }

    @Override
    protected void onResume() {
        IS_RUNNING = true;
        Log.w("ActivityHome", "Tiếp tục ứng dụng.");
        super.onResume();
        HideActionBarAndStatusBarForFullScreen();
    }

    @Override
    protected void onStart() {
        IS_RUNNING = true;
        Log.w("ActivityHome", "Bắt đầu");
        super.onStart();
    }

    @Override
    protected void onPause() {
        Log.w("ActivityHome", "Tạm dừng ứng dụng");
        IS_RUNNING = false;
//        BODY.Mouth.ShutUp();
        super.onPause();
    }

    @Override
    protected void onStop() {
        IS_RUNNING = false;
        Log.w("ActivityHome", "Stop ứng dụng!!");
//        BODY.Mouth.ShutUp();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        IS_RUNNING = false;
        Log.w("ActivityHome", "Hủy bỏ ứng dụng!");
        BODY.Mouth.ShutUp();
        BODY.Mouth.Destroy();
        EchoWebsockerListener.CloseAll();
        super.onDestroy();
    }

    //endregion

    //Services
    public JassisDeepListenServices mJassisListenServices;
    private boolean bound = false;

    // Declare view here
    public RelativeLayout rlRootLayout;
    public RippleBackground rbAnimation;
    public FadingTextView ftvHintText;
    public ProgressBar prgbarVoiceShow;
    public LinearLayout lnlayoutTouchScreen;
    public ImageView ivStateLogoIcon;
    public RippleBackground rbAnimationVoice;
    public ImageView ivStateMicroIcon;

    public ScaleTextView stvWhatAITalk;
    public TyperTextView ltvWhatUserTalk;
    public ImageView cbExitButton;
    public ImageView cbMenuButton;

    // Declare presenter
    public PresenterHome mPresenter;
    // Declare initBoom menu
    public InitBoomMenu mInitBoomMenu;
    ////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        new BODY(this);

        NETWORK_CONFIG.CLIENT.GetClientInfo(this);

        InitViews();
        InitPresenters();
        InitBoomMenus();
        StartLogoAnimation();

        ConfingNetwork();

        //StartDeepListen();
        //ActivityHome.this.runOnUiThread(TimerForSwitchOnServicesDeepListen::WaitAndWork);

        AutoHide();
        RequestPermission();
    }


    private void ConfingNetwork() {
        AndroidNetworking.initialize(getApplicationContext());
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
    }

    private void StartLogoAnimation() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (ivStateLogoIcon != null)
                    ivStateLogoIcon.post(() -> ivStateLogoIcon.startAnimation(new FlipAnimation()));
            }
        }, 15000, 10000);
        rbAnimationVoice.startRippleAnimation();
        rbAnimation.startRippleAnimation();
    }


    private void InitPresenters() {
        mPresenter = new PresenterHome(this);
    }

    public void InitViews() {
        rlRootLayout = findViewById(R.id.root_screen);
        rbAnimation = findViewById(R.id.content);
        ftvHintText = findViewById(R.id.fadingtextview_hint_text);
        InitProgressbar();
        lnlayoutTouchScreen = findViewById(R.id.linearlayout_touch_screen);
        ivStateLogoIcon = findViewById(R.id.state_logo_icon);
        rbAnimationVoice = findViewById(R.id.voice_effect);
        ivStateMicroIcon = findViewById(R.id.state_voice_icon);
        stvWhatAITalk = findViewById(R.id.ai_talking);
        ltvWhatUserTalk = findViewById(R.id.user_talking);
        cbExitButton = findViewById(R.id.exit_button);
        cbMenuButton = findViewById(R.id.menu_button);
        //ObjectInfomationDialog.Show(this, ObjectOverview.getDemo(), rlRootLayout);
    }

    private void InitProgressbar() {
        prgbarVoiceShow = findViewById(R.id.progressbar_voice_progress);
        prgbarVoiceShow.setIndeterminate(false);
        prgbarVoiceShow.setMax(10);
    }

    public void StartUserInput() {
        View v = LayoutInflater.from(ActivityHome.this).inflate(R.layout.layout_user_input_text_request, null);

        final MaryPopup mMaryPopup = MaryPopup.with(ActivityHome.this)
                .cancellable(false)
                .closeDuration(300)
                .content(v)
//                .backgroundColor(Color.TRANSPARENT)
//                .blackOverlayColor(Color.TRANSPARENT)
                .draggable(true)
                .fadeOutDragging(true)
                .center(false)
                .from(rlRootLayout);
        // Set event for close button
        v.findViewById(R.id.layout_user_input_request_send_button)
                .setOnClickListener(cx -> {
                    String content = ((EditText) v.findViewById(R.id.layout_user_input_request_edittext)).getText().toString();
                    this.runOnUiThread(() -> {
                        new RESPONSE.WORK(this).execute(content);
                        ltvWhatUserTalk.animateText(content);
                    });
                    mMaryPopup.close(true);
                });
        v.findViewById(R.id.layout_user_input_request_close)
                .setOnClickListener(vvf -> {
                    mMaryPopup.close(true);
                });
        // Set event for send button
        mMaryPopup.show();
    }

    //region For services callback
    private ServiceConnection mServicesConnection;

    /* Defined by its callback */
    @Override
    public void userCalledMe() {
        Log.w(TAG, "userCalledMe: Đã vào phương thức khi người dùng gọi tên AI");
        String[] reply = {"Vâng!", "Vâng! Bạn cần gì?", "Vâng! Tôi đây"};
        BODY.MOUNTH.ONLY_TALK(reply[new Random().nextInt(reply.length)]);
        final Timer[] TimerWaitFinishTalk = {new Timer()};
        TimerWaitFinishTalk[0].schedule(new TimerTask() {
            @Override
            public void run() {
                Log.w(TAG, "run: Đợi Jassis nói");
                while (!BODY.MOUNTH.isTalking())
                    SystemClock.sleep(1);
                Log.w(TAG, "run: Jassis đã nói, đợi nó nói xong");
                while (BODY.MOUNTH.isTalking())
                    SystemClock.sleep(10);
                Log.w(TAG, "run: Sau khi Jassis nói xong, tiến hành hủy timer và bắt đầu nghe!");
                TimerWaitFinishTalk[0].cancel();
                TimerWaitFinishTalk[0].purge();
                TimerWaitFinishTalk[0] = null;
                Log.w(TAG, "run: Kết thúc thành công Timer, Đợi một khoảng thời gian");
                SystemClock.sleep(50);
                ActivityHome.this.runOnUiThread(() -> mPresenter.StartListen());
            }
        }, 300, 1000);
    }

    public void StartDeepListen() {
        Intent intent = new Intent(this, JassisDeepListenServices.class);
        if (isMyServiceRunning(JassisDeepListenServices.class)) {
            Log.d("VANDAY", "Service vẫn đang chạy!");
            stopService(intent);
            return;
        }
        IS_MATCHED_CALL = false;
        if (mServicesConnection != null)
            mServicesConnection = null;
        mServicesConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                bound = true;
                // cast the IBinder and get JassisDeepListenServices innstance
                JassisDeepListenServices.LocalBinder binder = (JassisDeepListenServices.LocalBinder) service;
                mJassisListenServices = binder.getServices();
                mJassisListenServices.setCallbacks(ActivityHome.this);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };
        bindService(intent, mServicesConnection, Context.BIND_AUTO_CREATE);
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    //endregion

    private void RequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{
                    Manifest.permission.WAKE_LOCK,
                    Manifest.permission.DISABLE_KEYGUARD,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.VIBRATE,
                    Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, JAssistant.ALL_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == JAssistant.ALL_PERMISSION_CODE) {
            boolean isOK = true;
            if (grantResults.length > 0) {
                for (int gr : grantResults)
                    if (gr != PackageManager.PERMISSION_GRANTED) {
                        isOK = false;
                        break;
                    }
            } else isOK = false;
            if (isOK) {
                if (NETWORK_CONFIG.CLIENT.USER_ID < 1) {
                    new ModulLoginDialog().Show(this, rlRootLayout);
                    Log.w(TAG, "onRequestPermissionsResult: Chưa đăng nhập");
                } else
                    new ModulLoginDialog().Login(NETWORK_CONFIG.CLIENT.USER_NAME, "");
                Log.w(TAG, "onRequestPermissionsResult: OK!");
            } else {
                Toasty.error(this, "Xin lỗi! bạn không cấp đủ quyền hạn cho tôi.", Toast.LENGTH_LONG, true).show();
                finish();
            }
        }
    }

    private void InitBoomMenus() {
        mInitBoomMenu = new InitBoomMenu(this);
    }
}
