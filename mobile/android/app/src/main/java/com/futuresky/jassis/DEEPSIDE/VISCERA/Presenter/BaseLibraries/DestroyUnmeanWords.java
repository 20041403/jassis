package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.BaseLibraries;

import java.util.HashMap;

public class DestroyUnmeanWords {
    private static String dest = "";
    public static String DoIt(String src) {
        dest = src.toLowerCase();
        HashMap<String, String> libs = new HashMap<>();
        //region Data
        libs.put("đi", "");
        libs.put("nha", "");
        libs.put("nhé", "");
        libs.put("ha", "");
        libs.put("nè", "");
        libs.put("nhá", "");
        libs.put("nghe", "");
        libs.put("nghen", "");
        libs.put("nhoa", "");
        libs.put("nhỉ", "");
        libs.put("vậy", "");
        //endregion
        libs.forEach((keyString, valueString) -> {
            dest = dest.replace(keyString, valueString);
        });
        return  dest;
    }
}
