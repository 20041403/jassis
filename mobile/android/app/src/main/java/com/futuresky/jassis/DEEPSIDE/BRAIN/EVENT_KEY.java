package com.futuresky.jassis.DEEPSIDE.BRAIN;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.Song;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.FindObjectOverview;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.FindSong;
import com.futuresky.jassis.INTERFACE.BODY;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing.ApplicationProcessing.OpenAppliaction;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing.ApplicationProcessing.isPrivilegedApp;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing.ApplicationProcessing.isSystemApp;


public class EVENT_KEY {
    public final static String TAG = EVENT_KEY.class.getSimpleName();
    public static boolean PROCESSING(String content, RESPONSE response, String mixedReply) {
        if (response.phrase.event_key.equals("OPEN_APPLICATION")) {
            // Talk for waiting at the moment
            String[] wait = new String[]{"Đang xử lý! Vui lòng đợi trong giây lát!", "Vui lòng đợi tôi một vài giây", "Đang quét ứng dụng tương thích"};
            BODY.MOUNTH.TALK(wait[new Random().nextInt(wait.length)]);

            // Processing
            final String varName = "${app_name}";
            String AppliactionName = content.replaceAll(response.phrase.matched.replace(varName, "|"), "").trim();
            String appName = OpenAppliaction(AppliactionName);
            String[] ExtractResponse = response.response.matched.replace(varName, appName).split("\\|\\|");
            if (ExtractResponse.length < 2)
                ExtractResponse = new String[2];
            if (!appName.isEmpty())
                BODY.MOUNTH.TALK(ExtractResponse[0]);
            else
                BODY.MOUNTH.TALK(ExtractResponse[1]);
            return true;
        }
        //-------------------
        if (response.phrase.event_key.equals("LIST_ALL_APPLICATION")) {
            // Talk for waiting at the moment
            String[] wait = new String[]{"Đang xử lý! Vui lòng đợi trong giây lát!", "Vui lòng đợi tôi một vài giây", "Đang quét tất cả các ứng dụng"};
            BODY.MOUNTH.TALK(wait[new Random().nextInt(wait.length)]);

            // Processing
            final PackageManager pm = BODY.mContext.getPackageManager();
            List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

            // Store appliction
            StringBuilder CurrentContent = new StringBuilder();
            HashMap<String, String> apps = new HashMap<>();

            for (ApplicationInfo packageInfo : packages) {
                if (isSystemApp(packageInfo.packageName) ||
                        isPrivilegedApp(packageInfo))
                    continue;
                String app = packageInfo.loadLabel(BODY.mContext.getPackageManager()).toString();
                apps.put(app, packageInfo.packageName);
            }

            int count = 0;
            String Reply = "";
            if (apps.size() > 1) {
                for (Map.Entry<String, String> app : apps.entrySet()) {
                    count++;
                    if (count > apps.size())
                        CurrentContent.append("Và cuối cùng là ").append(app.getKey());
                    else
                        CurrentContent.append(count).append(". ").append(app.getKey()).append(".\n");
                }

                // Annoucement
                if (response.response.matched.isEmpty()) {
                    Reply = "Bạn có tất cả " + apps.size() + " ứng dụng bao gồm các ứng dụng sau:\n" + CurrentContent.toString();
                } else
                    response.response.matched.replace("${sys_app_list}", CurrentContent.toString())
                            .replace("${sys_app_list.COUNT}", apps.size() + "");
            } else {
                if (apps.size() == 1)
                    Reply = "Bạn chỉ có một ứng dụng đó là " + apps.entrySet().iterator().next().getKey();
                else
                    Reply = "Bạn không có ứng dụng nào được cài đặt từ bên ngoài!";
            }
            // TALK
            BODY.MOUNTH.TALK(Reply);
            return true;
        }
        //--------------------------
        if (response.phrase.event_key.equals("COUT_APPLICATION")) {
            // Talk for waiting at the moment
            String[] wait = new String[]{"Đang xử lý! Vui lòng đợi trong giây lát!", "Vui lòng đợi tôi một vài giây", "Đang quét tất cả các ứng dụng"};
            BODY.MOUNTH.TALK(wait[new Random().nextInt(wait.length)]);

            // Processing
            final PackageManager pm = BODY.mContext.getPackageManager();
            List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

            int sysAppCount = 0;
            int normalAppCout = 0;
            // Store appliction
            for (ApplicationInfo packageInfo : packages) {
                if (isSystemApp(packageInfo.packageName) ||
                        isPrivilegedApp(packageInfo))
                    sysAppCount++;
                else
                    normalAppCout++;
            }
            String Reply = "";
            if (response.response.matched.isEmpty())
                Reply = "Bạn có tất cả " + (sysAppCount + normalAppCout) + " ứng dụng. Trong đó, có " +
                        sysAppCount + " ứng dụng hệ thống và " + normalAppCout + " ứng dụng có thể thao tác được!";
            else
                Reply = response.response.matched.replace("${sys_sys_app_count}", sysAppCount + "")
                        .replace("${sys_normal_app_count}", "" + normalAppCout);
            // TALK
            BODY.MOUNTH.TALK(Reply);
            return true;
        }
        //---------------------
        if (response.phrase.event_key.equals("PLAY_SONG")) {
            Log.i(TAG, "PROCESSING: Đặt kênh là chơi nhạc để chiếm lĩnh kênh hiện hành");
            JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.MUSIC_PLAYER;
            // Get song name
            String SongName = content.replaceAll("^" + response.phrase.matched.replace("${song_name}", "|") + "$", "").trim();
            // Talk for waiting at the moment
            String[] wait = new String[]{"Đang tìm kiếm", "Vui lòng đợi tôi một vài giây", "Đang quét bài hát tương thích",
                    "Hãy đợi một vài giây", "Đang tìm kiếm bài hát " + SongName};
            BODY.MOUNTH.TALK(wait[new Random().nextInt(wait.length)]);
            // Tiến hành tìm kiếm
            new FindSong().execute(SongName, "", "");
            return true;
        }
        //----------------------
        if (response.phrase.event_key.equals("FIND_OBJECT_INFO")) {
            Log.i(TAG, "PROCESSING: Đổi sang kênh khác");
            JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.OTHER;
            // Lấy tên đối tượng
            String ObjectName = content.replaceAll("^" + response.phrase.matched.replace("${object_name}", "|") + "$", "").trim();
            // Talk for waiting at the moment
            String[] wait = new String[]{"Đang tìm đối tượng tương thích", "Vui lòng đợi tôi một vài giây", "Đang tìm kiếm đối tượng",
                    "Hãy đợi một vài giây", "Đang tìm " + ObjectName, ObjectName + " ư. Để tôi tìm xem", "OK! Hãy đợi tôi"};
            BODY.MOUNTH.TALK(wait[new Random().nextInt(wait.length)]);
            // Tiến hành tìm kiếm đối tượng
            new FindObjectOverview().execute(ObjectName, "", "");
            return true;
        }
        return false;
    }
}
