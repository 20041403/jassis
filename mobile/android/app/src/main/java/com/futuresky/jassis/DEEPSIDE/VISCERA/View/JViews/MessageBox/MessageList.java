package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.MessageBox;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectMessage;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.SimpleFriendInfo;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.SimpleFriendList.SimpleFriendList;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;
import com.futuresky.jassis.TNLib;
import com.meetic.marypopup.MaryPopup;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener.ws;

public class MessageList {
    public static int Current_user_id = -1;
    public static boolean isShow = false;
    public static int i = 0;
    public static List<ObjectMessage> data;
    public static MessageListAdapter messageListAdapter;


    public static List<ObjectMessage> getAllSimpleFriends() {
        return new ArrayList<>();
    }

    public static void Show(final Activity mContext, View from, SimpleFriendInfo simpleFriendInfo) {
        Current_user_id = simpleFriendInfo.getUserId();
        if (data != null) {
            data.clear();
            data = null;
        }
        if (messageListAdapter != null)
            messageListAdapter = null;
        // Infilate layout
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.module_layout_message, null);
        // Create Dialog
        final MaryPopup popup = MaryPopup.with(mContext)
                .content(v)
                .from(from)
                .blackOverlayColor(0)
                .backgroundColor(0)
                .center(false)
                .draggable(false)
                .scaleDownDragging(false)
                .closeDuration(300)
                .cancellable(false);
        // 1. Find and init view
        // A. Close button
        ImageView closeBtn = v.findViewById(R.id.module_close_message);
        closeBtn.setOnClickListener(v1 -> {
            popup.close(true);
            isShow = false;
        });
        //
        v.findViewById(R.id.module_close_bg_message_btn)
                .setOnClickListener(df -> {
                    closeBtn.performClick();
                });
        // B. Title name
        TextView frName = v.findViewById(R.id.module_message_frname);
        frName.setText(simpleFriendInfo.getDisplayName());
        // C. Display image
        CircleImageView avatar = v.findViewById(R.id.module_message_fravatar);
        // Set avatar
        Glide.with(BODY.mContext)
                .load(Uri.parse(simpleFriendInfo.getAvatarLink()))
                .placeholder(BODY.mContext.getResources().getDrawable(R.drawable.empty, null))
                .into(avatar);
        // D. RecycleView
        RecyclerView messages = v.findViewById(R.id.module_messages);
        // Init data
        data = new ArrayList<>();
        //
        data = getAllSimpleFriends();
        messageListAdapter = new MessageListAdapter(data);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        messages.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {
                if (data.size() > 0)
                    v.findViewById(R.id.module_layout_no_msg).setVisibility(View.INVISIBLE);
                else
                    v.findViewById(R.id.module_layout_no_msg).setVisibility(View.VISIBLE);
            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {
                if (data.size() > 0)
                    v.findViewById(R.id.module_layout_no_msg).setVisibility(View.INVISIBLE);
                else
                    v.findViewById(R.id.module_layout_no_msg).setVisibility(View.VISIBLE);
            }
        });

        messages.setLayoutManager(layoutManager);
        messages.setAdapter(messageListAdapter);

        // E. Edittext - message content
        EditText msgContent = v.findViewById(R.id.module_message_inp_content);

        // F. Send button
        ImageView sendBtn = v.findViewById(R.id.module_message_send_btn);
        sendBtn.setOnClickListener(vx -> {
            String content = msgContent.getText().toString().trim();
            if (content.isEmpty()) {
                Toasty.warning(mContext, "Nội dung tin nhắn không được trống", Toast.LENGTH_SHORT, true).show();
                return;
            }

            // Send message
            try {
                // Create JSON object for message
                JSONObject msgObj = new JSONObject();
                msgObj.put("msg", content);
                msgObj.put("sender_id", NETWORK_CONFIG.CLIENT.USER_ID);
                msgObj.put("send_at", TNLib.Using.GetCurrentTimeStamp());

                // Send to server

                JSONObject msg = new JSONObject();
                msg.put("action", "sendmessage");
                msg.put("content", Base64.encodeToString(msgObj.toString().getBytes(StandardCharsets.UTF_8), Base64.DEFAULT));
                msg.put("towsid", simpleFriendInfo.getSocketId());
                ws.send(msg.toString());

                data.add(new ObjectMessage(i++, content, NETWORK_CONFIG.CLIENT.USER_ID, "", ""));
                // Thông báo đến data set
                messageListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
                Toasty.error(mContext, "Tin nhắn chưa gửi được.", Toast.LENGTH_SHORT, true).show();
                return;
            }

            // Sau khi xử lý, xóa tin nhắn cũ
            msgContent.setText("");
            messages.scrollToPosition(data.size() - 1);
        });
        ///////// LOAD HAVED MESSAGE
        if (simpleFriendInfo.getUnread().size() > 0) {
            for (ObjectMessage objectMessage : simpleFriendInfo.getUnread()) {
                data.add(objectMessage);
            }
            BODY.mContext.runOnUiThread(() -> {
                messageListAdapter.notifyDataSetChanged();
            });
            simpleFriendInfo.setUnread(new ArrayList<>());
            if (SimpleFriendList.data.remove(simpleFriendInfo)) {
                SimpleFriendList.data.add(simpleFriendInfo);
                SimpleFriendList.simpleFriendListAdapter.notifyDataSetChanged();
                Log.w("MSGLIST", "Cập nhật thành công hệ thống");
            } else
                Log.w("MSGLIST", "Cập nhật không thành công!");
        }
        /////////////////////////////////////
        popup.show();
        isShow = true;
    }

    public static void ReceivedMessage(JSONObject msgObj) {
        try {
            data.add(new ObjectMessage(i++, msgObj.getString("msg"), msgObj.getInt("sender_id"), msgObj.getString("send_at"), TNLib.Using.GetCurrentTimeStamp()));
            BODY.mContext.runOnUiThread(() -> {
                messageListAdapter.notifyDataSetChanged();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
