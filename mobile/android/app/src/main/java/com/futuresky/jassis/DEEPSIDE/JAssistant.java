package com.futuresky.jassis.DEEPSIDE;

public class JAssistant {
    // Thời gian đợi sau khi nói xong
    public final static long TIME_WAIT_AFTER_LISTEN = 3000L;
    // Tên của trợ lý ảo
    public static String NAME = "Hello";

    //-------
    // Độ cao giọng nói của trợ lý
    public static float PITCH = (float) 1.0;
    // Tốc độ nói (nhanh hay chậm của trợ lý)
    public static float SPEECH_RATE = (float) 1.0;
    // Mã ngôn ngữ cho trợ lý ảo
    public static String LANGUAGE_CODE = "vi-VN";
    // Loại chất giọng (Kiểu giọng cho trợ lý)
    public static String VOICE_NAME = VOICE.Southern_female_voice;
    // Loại mã hóa âm thanh
    public static class AUDIO_ENCODING {
        // Không mã hóa
        public final static String AUDIO_ENCODING_UNSPECIFIED = "AUDIO_ENCODING_UNSPECIFIED";
        // Mã hóa kiểu tuyến tính 16-bit
        public final static String LINEAR16 = "LINEAR16";
        // Mã hóa mp3 thông dụng
        public final static String MP3 = "MP3";
        // Mã hóa thích hợp cho các thiết bị android, ios và một số trình duyệt
        public final static String OGG_OPUS = "OGG_OPUS";
    }
    // Các kiểu chất giọng hiện có của trợ lý
    public static class VOICE {
        // Giọng nữ miền bắc
        public final static String Northern_female_voice = "vi-VN-Wavenet-A";
        // Giọng nam miền bắc
        public final static String Northern_male_voice = "vi-VN-Wavenet-B";
        // Giọng nữ miền nam
        public final static String Southern_female_voice = "vi-VN-Wavenet-C";
        // Giọng nam miền nam
        public final static String Southern_male_voice = "vi-VN-Wavenet-D";
    }
    //-------
    // Kênh hiện tại của tiến trình lắng nghe
    public enum CHANNEL {
        // Đang ở kênh lắng nghe nền
        DEEP_LISTEN,
        // Đang ở kênh tương tác với người dùng
        INTERFACE,
        // Đang phát nhạc
        MUSIC_PLAYER,
        // Đang ở chế độ khác,
        OTHER
    }
    // Đặt kênh hiện hành là kênh LẮNG NGHE SÂU
    public static CHANNEL CURRENT_CHANNEL = CHANNEL.DEEP_LISTEN;

    //-------
    // Mã để request tất cả quyền là 1998
    public static int ALL_PERMISSION_CODE = 1998;
    public static int LOCATION_PERMISSION = 1999;
}
