package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ImageView;

import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.UpdateLoactionServices;
import com.futuresky.jassis.R;
import com.futuresky.jassis.TNLib;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("Registered")
public class ConnectModule extends FragmentActivity implements OnMapReadyCallback {
    public final static String TAG = ConnectModule.class.getSimpleName();
    public static GoogleMap mMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.module_layout_connect);
        // For main
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        // For close button
        ImageView closeBtn = findViewById(R.id.module_layout_connect_close_btn);
        closeBtn.setOnClickListener(v1 -> {
            finish();
        });
        findViewById(R.id.module_layout_connect_close_bg_btn)
                .setOnClickListener(v2 -> {
                    closeBtn.performClick();
                });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mMap == null)
            mMap = googleMap;
//        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//        mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //mMap.setMyLocationEnabled(true);
        }

        MarkerOptions myMarkderOptions = new MarkerOptions()
                .position(new LatLng(0, 0))
                .title("Tôi")
                .icon(BitmapDescriptorFactory.fromBitmap(TNLib.Using.getCircularBitmap(NETWORK_CONFIG.CLIENT.USER_AVATAR, 100)));

        Marker myMarker = mMap.addMarker(myMarkderOptions);
        // Add a marker in Sydney, Australia, and move the camera.
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (UpdateLoactionServices.mLastLocation != null) {
                    LatLng myLocation = new LatLng(UpdateLoactionServices.mLastLocation.getLatitude(),
                            UpdateLoactionServices.mLastLocation.getLongitude());
                    Log.w(TAG, "run: Cập nhật vị trí và ảnh hiện tại ở: " + myLocation);
                    runOnUiThread(() -> {
//                        mMap.clear();
                        myMarker.setPosition(myLocation);
                    });
                }
            }
        }, 500, 500);
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.addMarker(new MarkerOptions().position(new LatLng(-34.3432343, 151)).title("Marker in Sydney"));
//        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
