package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing;

import android.os.AsyncTask;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectFeature;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetResource {
    public final static String TAG = GetResource.class.getSimpleName();

    public static String GetMP3Link(String songlink) throws Exception {
        String ur = "https://getlink.songhanh.com.vn/index.php?link=" + songlink;
        URL url = new URL(ur);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // set method
        conn.setRequestMethod("GET");
        // set request header
        conn.setRequestProperty("User-Agent", "Mozilla/5.0");// (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/79.0.108 Chrome/73.0.3683.108 Safari/537.36");
        conn.setRequestProperty("Content-type", "text/html");

        int responseCode = conn.getResponseCode();
        if (responseCode == 200) {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Matcher matcher = Pattern.compile("<a.target=(.*?)<\\/a>", Pattern.MULTILINE)
                    .matcher(response.toString());
            while (matcher.find())
            {
                Log.w(TAG, "GetMP3Link: " + matcher.group(0));
                String mp3Link = "";
                if (matcher.group(0).contains("https://"))
                    mp3Link = "https://";
                else
                    mp3Link = "http://";
                mp3Link += matcher.group(0).replaceAll("<a.(.*?)http(.*?):\\/\\/|[\\\"]{0,1}>(.*?)<\\/a>", "").trim();
                Log.d(TAG, "GetMP3Link: Found -> \n" + mp3Link);
                return mp3Link;
            }
            // print result
        } else {
            Log.d(TAG, "GetMP3Link: Không thể tiếp tục, vì nhận phản hồi là " + responseCode);
        }
        return new String();
    }

    public static String GetMP3LinkNhacCuaTui(String songlink) throws Exception {
        URL url = new URL(songlink);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // set method
        conn.setRequestMethod("GET");
        // set request header
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/79.0.108 Chrome/73.0.3683.108 Safari/537.36");
        conn.setRequestProperty("Content-type", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");

        int responseCode = conn.getResponseCode();
        if (responseCode == 200) {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Matcher matcher = Pattern.compile("https\\:\\/\\/www\\.nhaccuatui\\.com\\/flash\\/xml\\?html5=true\\x26key1=[a-z0-9]{32}")
                    .matcher(response.toString());
            if (matcher.find())
            {
                URL url2 = new URL(matcher.group(0).trim());
                HttpURLConnection conn2 = (HttpURLConnection) url2.openConnection();

                // set method
                conn2.setRequestMethod("GET");
                // set request header
                conn2.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/79.0.108 Chrome/73.0.3683.108 Safari/537.36");
                conn2.setRequestProperty("Content-type", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                int responseCode2 = conn.getResponseCode();
                if (responseCode2 == 200) {
                    BufferedReader in2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
                    String inputLine2;
                    StringBuffer response2 = new StringBuffer();
                    while((inputLine2 = in2.readLine()) != null) {
                        response2.append(inputLine2);
                    }
                    in.close();
                    Matcher matcher2 = Pattern.compile("https:\\/\\/(.*?)\\.mp3\\?(.*?)\\]")
                            .matcher(response2.toString());
                    if (matcher2.find())
                    {
                        String result = matcher2.group(0).substring(0,matcher2.group(0).length()-1);
                        return result;
                    }
                } else {
                    System.out.println("Trả về 2: " + responseCode);
                }
            }
            System.out.println("Done!");
        } else {
            System.out.println("Trả về: " + responseCode);
        }
        return "";
    }

    public static ArrayList<String> GetMP3Links(String songlink) throws Exception {
        ArrayList<String> mp3Links = new ArrayList();
        String ur = "https://getlink.songhanh.com.vn/index.php?link=" + songlink;
        URL url = new URL(ur);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // set method
        conn.setRequestMethod("GET");
        // set request header
        conn.setInstanceFollowRedirects(false); /* added line */
        conn.setRequestProperty("User-Agent", "Mozilla/5.0");
        conn.setRequestProperty("Content-type", "text/html");

        int responseCode = conn.getResponseCode();
        if (responseCode == 200) {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Matcher matcher = Pattern.compile("<a.target=_blank.href=(.*?)<\\/a>", Pattern.MULTILINE)
                    .matcher(response.toString());
            while (matcher.find())
            {
                mp3Links.add(matcher.group(0).replaceAll("<a.target=_blank.href=|>(.*?)<\\/a>", "").trim());
            }
            // print result
        } else {
        }
        return mp3Links;
    }

    public static ArrayList<ObjectFeature> GetObjectImages(String ObjectName) throws Exception {
        return GetObjectImages(ObjectName, -1);
    }

    public static ArrayList<ObjectFeature> GetObjectImages(String ObjectName, int number) throws Exception {
        ArrayList<ObjectFeature> objectImages = new ArrayList();
        String ur = "https://www.google.com/search?q=" + URLEncoder.encode(ObjectName, "UTF-8") + "&tbm=isch";
        Log.w(TAG, "GetObjectImages: GOOGLE to " + ur);
        URL url = new URL(ur);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // set method
        conn.setRequestMethod("GET");
        // set request header
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/79.0.108 Chrome/73.0.3683.108 Safari/537.36");
        conn.setRequestProperty("Content-type", "text/html");

        int responseCode = conn.getResponseCode();
        if (responseCode == 200) {
            Log.w(TAG, "GetObjectImages: Tìm ảnh phản hồi 200 OK");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Matcher matcher = Pattern.compile("<div.class=.rg_meta.notranslate\">(.*?)<\\/div>", Pattern.MULTILINE)
                    .matcher(response.toString());
            while (matcher.find())
            {
                String objJSON = matcher.group(0).replaceAll("<div.class=.rg_meta.notranslate.>|<\\/div>", "")
                        .trim();
                Log.d(TAG, "GetObjectImages: " + objJSON);
                objectImages.add(new ObjectFeature(objJSON));
                if (objectImages.size() >= number && number > 0)
                    break;
            }
            // print result
        } else {
            Log.w(TAG, "GetObjectImages: KHÔNG PHẢI 200K");
        }
        return objectImages;
    }
    public static String GetObjectTitle(String title) {
        try {
            String ur = "http://www.google.com/search?q=" + URLEncoder.encode(title, "UTF-8");
            URL url = new URL(ur);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // set method
            conn.setRequestMethod("GET");
            // set request header
            conn.setRequestProperty("User-Agent", "Mozilla/5.0");
            conn.setRequestProperty("Content-type", "text/html");

            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                String content = "";
                Matcher matcher = Pattern.compile("<div.class=.g.><h3(.*?)<div.class=.s.>(.*?)<br><\\/div><\\/div>", Pattern.MULTILINE)
                        .matcher(response.toString());
                String link = "";
                String songname = "";
                String[] linkImage;
                while (matcher.find())
                {
                    content = matcher.group(0).replaceAll("<div.class=\"g\"><h3.class=.r.>|<\\/h3>(.*)", "");
                    Matcher matcher2 = Pattern.compile("((<(.*?)vi.wikipedia.org\\/wiki\\/)(.*?)((\\x26)(.*?)<\\/a>))|((<(.*?)vi.wikipedia.org\\/wiki\\/)(.*?)(.>(.*?)<\\/a>))")
                            .matcher(content);
                    if (matcher2.find())
                    {
                        String objectTile = matcher2.group(0).replaceAll("((<(.*?)vi.wikipedia.org\\/wiki\\/)|((\\x26)(.*?)<\\/a>))|((<(.*?)vi.wikipedia.org\\/wiki\\/)|(.>(.*?)<\\/a>))", "");

                        return URLDecoder.decode(objectTile, "UTF-8");
                    }

                }
                Log.e(TAG, "GetObjectTitle: Chạy hết while");
                return "";
                // print result
            } else {
                Log.e(TAG, "GetObjectTitle: Kết nối trả về mã " + responseCode);
            }
            Log.e(TAG, "GetObjectTitle: Không tìm thấy đối tượng đó");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
