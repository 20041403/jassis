package com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home;

import android.content.Intent;
import android.graphics.Color;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.AccountLogRes.WelcomeNoneLogin;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.ConnectModule;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.Settings.ModuleSettingDialog;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.SimpleFriendList.SimpleFriendList;
import com.futuresky.jassis.R;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;

import org.json.JSONObject;

import java.io.File;

public class InitBoomMenu {
    ActivityHome mContext;
    public BoomMenuButton bmb;
    int ColorAll = Color.parseColor("#FFFFFF");

    public InitBoomMenu(ActivityHome mContext) {
        this.mContext = mContext;
        bmb = mContext.findViewById(R.id.btn_bom_menu_hidden);
        bmb.setBackground(mContext.getDrawable(R.drawable.xoa_sau));
        bmb.setUse3DTransformAnimation(true);
        ModuleForBoomMenu();
    }

    public void ModuleForBoomMenu() {
        // 1. Thời tiết
        TextOutsideCircleButton.Builder weather = new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.weather_ook)
                .normalText("Thời tiết")
                .textSize(14)
                .rippleEffect(true)
                .normalColor(ColorAll);
        bmb.addBuilder(weather);
        // 2. Nhắc nhở
        TextOutsideCircleButton.Builder remind = new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.sticky_note)
                .normalText("Nhắc nhở")
                .textSize(14)
                .rippleEffect(true)
                .normalColor(ColorAll);
        bmb.addBuilder(remind);
        // 3. Bài hát yêu thích
        TextOutsideCircleButton.Builder lovesongs = new TextOutsideCircleButton.Builder()
                .normalText("Bài hát yêu thích")
                .textSize(14)
                .normalImageRes(R.drawable.music_player)
                .rippleEffect(true)
                .normalColor(ColorAll)
                .listener(ss->{
//                    AndroidNetworking.upload("")
//                            .addMultipartFile("image",new File(""))
//                            .addMultipartParameter("key","value")
//                            .setTag("uploadTest")
//                            .setPriority(Priority.HIGH)
//                            .build()
//                            .setUploadProgressListener(new UploadProgressListener() {
//                                @Override
//                                public void onProgress(long bytesUploaded, long totalBytes) {
//                                    // do anything with progress
//                                }
//                            })
//                            .getAsJSONObject(new JSONObjectRequestListener() {
//                                @Override
//                                public void onResponse(JSONObject response) {
//                                    // do anything with response
//                                }
//                                @Override
//                                public void onError(ANError error) {
//                                    // handle error
//                                }
//                            });
                });
        bmb.addBuilder(lovesongs);
        // 4. Các đối tượng đã tìm kiếm
        TextOutsideCircleButton.Builder definedObjects = new TextOutsideCircleButton.Builder()
                .normalText("Lịch sử tra cứu")
                .textSize(14)
                .normalImageRes(R.drawable.reporting_and_history)
                .rippleEffect(true)
                .normalColor(ColorAll);
        bmb.addBuilder(definedObjects);
        // 5. Máy tính
        TextOutsideCircleButton.Builder calculator = new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.calculator_icon)
                .normalText("Máy tính")
                .textSize(14)
                .rippleEffect(true)
                .normalColor(ColorAll);
        bmb.addBuilder(calculator);
        // 6. Vị trí hiện tại
        TextOutsideCircleButton.Builder currentLocation = new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.location_maps)
                .normalText("Vị trí hiện tại")
                .textSize(14)
                .rippleEffect(true)
                .normalColor(ColorAll);
        bmb.addBuilder(currentLocation);
        // 7. Bạn bè
        TextOutsideCircleButton.Builder friends = new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.network_cnn)
                .normalText("Kết nối")
                .textSize(14)
                .rippleEffect(true)
                .normalColor(ColorAll)
                .listener(ls -> {
                    mContext.startActivity(new Intent(mContext, ConnectModule.class));
                });
        bmb.addBuilder(friends);
        // 8. Tin nhắn
        TextOutsideCircleButton.Builder messages = new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.chat)
                .normalText("Trò chuyện")
                .textSize(14)
                .rippleEffect(true)
                .normalColor(ColorAll)
                .listener(evnt -> {
                    SimpleFriendList.Show(mContext, mContext.rlRootLayout);
                });
        bmb.addBuilder(messages);
        // 9. Cài đặt
        TextOutsideCircleButton.Builder settings = new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.user_setting)
                .normalText("Cá nhân")
                .textSize(14)
                .rippleEffect(true)
                .normalColor(ColorAll)
                .listener(ls -> {
                    if (NETWORK_CONFIG.CLIENT.USER_ID < 0) {
                        WelcomeNoneLogin.Show(mContext, mContext.rlRootLayout);
                    } else {
                        ModuleSettingDialog.Show(mContext, mContext.rlRootLayout);
                    }
                });
        bmb.addBuilder(settings);
    }
}
