package com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects;

import java.util.ArrayList;

public class ObjectOverview {
    ArrayList<ObjectFeature> images = new ArrayList<>();
    String name = "";
    String content = "";
    String homeLink = "";

    public ObjectOverview() {}
    public ObjectOverview(ArrayList<ObjectFeature> images, String name, String content) {
        this.images = images;
        this.name = name;
        this.content = content;
    }

    public ArrayList<ObjectFeature> getImages() {
        return images;
    }

    public void setImages(ArrayList<ObjectFeature> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
