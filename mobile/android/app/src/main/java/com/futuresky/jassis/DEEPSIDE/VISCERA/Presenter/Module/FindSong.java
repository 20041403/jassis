package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectFeature;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.Song;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.ModuleMediaPlayer;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.TNLib;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing.GetResource.GetMP3Link;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing.GetResource.GetMP3LinkNhacCuaTui;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing.GetResource.GetObjectImages;


public class FindSong extends AsyncTask<String, Integer, Song> {
    public final static String TAG = FindSong.class.getSimpleName();
    String success = "";
    String notfound = "";

    public Song Find(String songName) throws Exception {
        Song song = new Song();
        String ur = "http://www.google.com/search?q=" + URLEncoder.encode(songName, "UTF-8") + "&gws_rd=ssl";
        Log.w(TAG, "Thực hiện request đến: " + ur);
        URL url = new URL(ur);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // set method
        conn.setRequestMethod("GET");
        // set request header
        conn.setInstanceFollowRedirects(false); /* added line */
        conn.setRequestProperty("User-Agent", "Mozilla/5.0");// (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/79.0.108 Chrome/73.0.3683.108 Safari/537.36");
        conn.setRequestProperty("Content-type", "text/html");

        int responseCode = conn.getResponseCode();
        if (responseCode == 200) {
            Log.w(TAG, "Find: 200 OK");
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //--------
            Matcher matcher2 = Pattern.compile("http(.*?)\\/\\/((www.nhaccuatui.com\\/bai-hat)|(zingmp3.vn\\/bai-hat))\\/(.*?).html")
                    .matcher(response.toString());
            while (matcher2.find()) {
                Log.d(TAG, "Find__2: " + matcher2.group(0));
                String link = "";
                try {
                    link = GetMP3Link(matcher2.group(0));
                    if (link.isEmpty())
                        link = GetMP3LinkNhacCuaTui(matcher2.group(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Nếu không tìm thấy đường dẫn nhạc thì tiếp tục với link khác
                if (link.isEmpty())
                    continue;
                else {
                    Log.w("FindSong", "Link: " + link);
                    song.setLocation(link);
//                    Log.w(TAG, "Find: Tiến hành tìm các hình ảnh liên quan đến bài hát");
//                    ArrayList<ObjectFeature> avatars = GetObjectImages(song.getTitle() + " - " + song.getCreator(), 1);
//                    Log.w("FindSong", "Avatar: " + avatars.get(0));
//                    if (avatars.size() > 0)
//                        song.setAvatar(avatars.get(0).getImageLink());
                    return song;
                }
            }
            //--------
            Log.e("FindSong", "Done!");
            // print result
        } else {
            Log.e("FindSong", "KHÔNG PHẢN HỒI 200 OK ->" + responseCode);
        }
        return song;
    }

    @Override
    protected Song doInBackground(String... strings) {
        try {
            if (strings[1] != null)
                success = strings[1];
            if (strings[2] != null)
                notfound = strings[2];
            return Find(strings[0].toLowerCase() + " mp3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Song();
    }

    @Override
    protected void onPostExecute(Song song) {
        if (song != null && !song.getLocation().isEmpty()) {
            if (success == null && !success.isEmpty()) {
                BODY.MOUNTH.TALK(success);
            } else {
            }
            if (BODY.MUSIC_PLAYER != null) {
                BODY.MUSIC_PLAYER.lnClose.performClick();
                Log.w(TAG, "onPostExecute: Đóng bài cũ trước khi thực hiện phát bài mới!");
                SystemClock.sleep(15);
            }
            BODY.MUSIC_PLAYER = new ModuleMediaPlayer(song);
        } else {
            if (notfound == null && !notfound.isEmpty()) {
                BODY.MOUNTH.TALK(notfound);
            } else {
                String[] notfoundit = {"Không tìm thấy bài này!", "Xin lỗi! Tôi không thể tìm thấy bài hát bạn yêu cầu!", "Xin lỗi! Không tìm thấy bài hát"};
                BODY.MOUNTH.TALK(notfoundit[new Random().nextInt(notfoundit.length)]);
            }
            Log.w(TAG, "onPostExecute: Không tìm thấy bài hát, thực hiện giải phóng kênh");
            JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.INTERFACE;
        }
        super.onPostExecute(song);
    }
}
