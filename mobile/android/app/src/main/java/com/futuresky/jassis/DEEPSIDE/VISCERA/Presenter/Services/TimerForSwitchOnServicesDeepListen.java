package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services;

import android.os.SystemClock;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Home.SpeechRecognizerManager;
import com.futuresky.jassis.INTERFACE.BODY;

import java.util.Timer;
import java.util.TimerTask;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome.IS_MATCHED_CALL;

public class TimerForSwitchOnServicesDeepListen {
    public final static long TIME_WAIT = 5000L;
    public final static String TAG = TimerForSwitchOnServicesDeepListen.class.getSimpleName();
    public static Timer waitingObj;
    public static long startCountMilis = 999999999;

    public static void WaitAndWork() {
        waitingObj = new Timer();
        waitingObj.schedule(new TimerTask() {
            @Override
            public void run() {
                if (BODY.mContext == null ||
                        BODY.mContext.mJassisListenServices == null ||
                        BODY.mContext.mPresenter == null ||
                        BODY.mContext.mPresenter.mSpeechRecognizerManager == null ||
                        (JAssistant.CURRENT_CHANNEL != JAssistant.CHANNEL.INTERFACE && JAssistant.CURRENT_CHANNEL != JAssistant.CHANNEL.DEEP_LISTEN) ||
                        BODY.MOUNTH.isTalking()) {
                    startCountMilis = System.currentTimeMillis();
                    Log.w(TAG, "run: KÊNH HIỆN TẠI " + JAssistant.CURRENT_CHANNEL);
                    return;
                }
                if (JassisDeepListenServices.mServicesSpeechRecognizer != null &&
                        SpeechRecognizerManager.mInterfaceSpeechRecognizer != null) {
                    Log.w(TAG, "run: Cả hai phương thức lắng nghe cùng tồn tại, thực hiện ưu tiên cho lắng nghe kênh INTERFACE");
                    BODY.mContext.runOnUiThread(() -> {
                        BODY.mContext.mJassisListenServices.destroyListen();
                    });
                    startCountMilis = System.currentTimeMillis();
                } else if (JassisDeepListenServices.mServicesSpeechRecognizer != null) {
                    if (JAssistant.CURRENT_CHANNEL != JAssistant.CHANNEL.DEEP_LISTEN) {
                        Log.i(TAG, "run: Đóng nghe nền, do thực hiện trái kênh@");
                        BODY.mContext.runOnUiThread(() -> {
                            BODY.mContext.mJassisListenServices.destroyListen();
                        });
                    }
                    Log.w(TAG, "run: Lắng nghe ẩn hiện đang chạy, không cần thực hiện khởi động");
                    startCountMilis = System.currentTimeMillis();
                } else if (SpeechRecognizerManager.mInterfaceSpeechRecognizer != null) {
                    if (JAssistant.CURRENT_CHANNEL == JAssistant.CHANNEL.DEEP_LISTEN) {
                        Log.i(TAG, "run: Đóng mục nghe tương tác do sai kênh");
                        BODY.mContext.mPresenter.mSpeechRecognizerManager.destroyListen();
                    }
                    Log.w(TAG, "run: Lắng nghe tại kênh INTERFACE nền chưa cần khởi động");
                    startCountMilis = System.currentTimeMillis();
                } else {
                    if (System.currentTimeMillis() - startCountMilis > TIME_WAIT) {
                        Log.d(TAG, "run: Đã vượt quá thời gian chờ, tiến hành khởi động lắng nghe dưới nền");
                        startCountMilis = System.currentTimeMillis();
                        if (JAssistant.CURRENT_CHANNEL != JAssistant.CHANNEL.DEEP_LISTEN) {
                            Log.w(TAG, "run: Hiện tại đang ở kênh " + JAssistant.CURRENT_CHANNEL
                                    + " tiến hành chuyển sang kênh DEEP_LISTEN để mở đường cho kênh chạy");
                            JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.DEEP_LISTEN;
                        }
                        if (IS_MATCHED_CALL) {
                            Log.w(TAG, "run: Phù hợp với tên gọi đang ở TRUE, nhưng giá trị này đã cũ, tiến hành đưa về giá trị mặc định");
                            IS_MATCHED_CALL = false;
                        }
                        BODY.mContext.runOnUiThread(() -> BODY.mContext.mJassisListenServices.startListening());
                    } else
                        Log.e(TAG, "run: Hiện tại không có mục nào đang nghe, kênh đang rảnh, tiến hành đếm thời gian chờ!");
                }
            }
        }, 1000, 1000);
    }
}
