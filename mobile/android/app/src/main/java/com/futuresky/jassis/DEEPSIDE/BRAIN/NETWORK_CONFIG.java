package com.futuresky.jassis.DEEPSIDE.BRAIN;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;

import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener;

public class NETWORK_CONFIG {
    public final static String CLOUD_TEXT_TO_SPEECH_API_KEY = "AIzaSyAm2syIah1RqLbw38R6SCxPBuET-EZPNBA";
    public final static String CLOUD_SPEECH = "https://texttospeech.googleapis.com/v1beta1/text:synthesize";

    public final static String API_PROTOCOL = "http://";
    public final static String SERVER_ADDRESS = "jassis-server.southeastasia.cloudapp.azure.com";
    public final static int API_PORT = 3000;
    public final static String GET_RESPONSE_API = API_PROTOCOL + SERVER_ADDRESS + ":" + API_PORT + "/sentence/query";
    public final static String APPEN_SPEECH_API = API_PROTOCOL + SERVER_ADDRESS + ":" + API_PORT + "/speech/add";
    public final static String GET_EXISTED_SPEECH_API = API_PROTOCOL + SERVER_ADDRESS + ":" + API_PORT + "/speech/getexisted";
    public final static String GET_ALL_VARIABLE_API = API_PROTOCOL + SERVER_ADDRESS + ":" + API_PORT + "/varible/getall";
    public final static String CHECK_LOGIN_API = API_PROTOCOL + SERVER_ADDRESS + ":" + API_PORT + "/socialuser/checkuser";
    public final static String GET_ALL_FRIENDS_FROM_USER_API = API_PROTOCOL + SERVER_ADDRESS + ":" + API_PORT + "/friend/getfriendfromuser";

    public final static int WS_PORT = 8080;
    public final static String WS_PROTOCOL = "ws://";
    public final static String WEBSOCKET_SERVER_ADDRESS = WS_PROTOCOL + SERVER_ADDRESS + ":" + WS_PORT;

    public static class CLIENT {
        public final static String NETWORK_CLIENT = "NETWORK_CLIENT";
        public static Bitmap USER_AVATAR = null;
        public static int USER_ID = -1;
        public static String USER_CURRENT_TOKEN = "";
        public static String USER_DISPLAY_NAME = "";
        public static String USER_AVATAR_SRC = "";
        public static String USER_NAME = "";
        public static String USER_ENCODED_PASS = "";
        public static String USER_BACKGROUND = "";

        public static void ClearUserInfo(Context mContext) {
            USER_ID = -1;
            USER_CURRENT_TOKEN = "";
            USER_DISPLAY_NAME = "";
            USER_AVATAR_SRC = "";
            USER_ENCODED_PASS = "";
            USER_BACKGROUND = "";
            EchoWebsockerListener.wsid = "";
            UpdateClientInfo(mContext);
        }

        public static void GetClientInfo(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORK_CLIENT, Context.MODE_PRIVATE);
            USER_ID = sharedPreferences.getInt("USER_ID", -1);
            USER_NAME = sharedPreferences.getString("USER_NAME", "");
            USER_ENCODED_PASS = sharedPreferences.getString("USER_ENCODED_PASS", "");
        }

        public static void UpdateClientInfo(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORK_CLIENT, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("USER_ID", USER_ID);
            editor.putString("USER_NAME", USER_NAME);
            editor.putString("USER_ENCODED_PASS", USER_ENCODED_PASS);
            editor.apply();
        }

        public static void UpdateToken(Context mContext) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(NETWORK_CLIENT, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("USER_CURRENT_TOKEN", USER_CURRENT_TOKEN);
            editor.apply();
        }
    }
}
