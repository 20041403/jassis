package com.futuresky.jassis;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TNLib {
    public final static String TAG = "TNLib";

    public static class Location {
        public static Address getAddress(Context mContext, double lat, double lng) {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                Address obj = addresses.get(0);
                return obj;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "getAddress: " + e.getMessage());
            }
            return null;
        }
    }

    public static class Using {
        public static final String MD5(final String s) {
            final String MD5 = "MD5";
            try {
                // Create MD5 Hash
                MessageDigest digest = java.security.MessageDigest
                        .getInstance(MD5);
                digest.update(s.getBytes());
                byte messageDigest[] = digest.digest();

                // Create Hex String
                StringBuilder hexString = new StringBuilder();
                for (byte aMessageDigest : messageDigest) {
                    String h = Integer.toHexString(0xFF & aMessageDigest);
                    while (h.length() < 2)
                        h = "0" + h;
                    hexString.append(h);
                }
                return hexString.toString();

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return "";
        }
        public static float CalFromString(String inp) {
            String xy[] = inp.split("\\+|\\-|\\*|\\/|\\%|\\:");
            float x = Integer.parseInt(xy[0]),
                    y = Integer.parseInt(xy[1]),
                    result = 0;
            if (inp.matches("\\d+\\+\\d+"))
                result = x + y;
            else if (inp.matches("\\d+\\-\\d+"))
                result = x - y;
            else if (inp.matches("\\d+\\*\\d+"))
                result = x * y;
            else if (inp.matches("\\d+(\\/|\\:)\\d+"))
                result = x / y;
            else if (inp.matches("\\d+\\%\\d+"))
                result = x % y;
            return result;
        }
        public static String[] WeekDays = {"Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"};
        public static boolean HashMapIsContainKey(String key, HashMap<String, String> hm) {
            for (Map.Entry<String, String> entry : hm.entrySet()) {
                if (key.equals(entry.getKey())) {
                    return true;
                }
            }
            return false;
        }
        public static String UpperPerWord(String cnt) {
            cnt = cnt.replace("\t", " ").trim();
            while (cnt.contains("  "))
                cnt = cnt.replace("  ", " ");
            String[] temp = cnt.split(" ");
            cnt = "";
            for (int i = 0; i < temp.length; i++) {
                temp[i] = temp[i].substring(0,1).toUpperCase() + temp[i].substring(1).toLowerCase();
                cnt += temp[i] + " ";
            }
            return cnt.trim();
        }
        public static void OpenUri(Context mContext, Uri _uri) {
            Toast.makeText(mContext, "Đang mở", Toast.LENGTH_SHORT).show();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, _uri);
            mContext.startActivity(browserIntent);
        }
        public static boolean OpenPDF(Context mContext, String FilePath) {
            File file = new File(FilePath);
            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(Uri.fromFile(file),"application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open File");
            try {
                mContext.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
                Log.e(TAG, "OpenPDF: " + e.getMessage());
                return false;
            }
        }
        public static boolean SaveRawToDownloadsDirectory(Context mContext, int mRawID, String mFileName, boolean mOpenNow) {
            boolean status = SaveRawToDownloadsDirectory(mContext,mRawID,mFileName,mOpenNow);
            if (status && mOpenNow) {
                String mDownloadsDirectoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                File file = new File(mDownloadsDirectoryPath + File.separator + mFileName);
                OpenUri(mContext, Uri.fromFile(file));
                return status;
            } else
                return status;
        }
        public static boolean SaveRawToDownloadsDirectory(Context mContext, int mRawID, String FileName) {
            InputStream inp = null;
            FileOutputStream out = null;
            try {
                inp = mContext.getResources().openRawResource(mRawID);
                String mDownloadsDirectoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                Log.d(TAG, "SaveRawToDownloadsDirectory: PATH: " + mDownloadsDirectoryPath + File.separator + FileName);
                out = new FileOutputStream(
                        new File(mDownloadsDirectoryPath + File.separator + FileName)
                );
                byte[] buffer = new byte[inp.available()];
                int singleByte;
                while ((singleByte = inp.read(buffer, 0, buffer.length)) != -1) {
                    out.write(buffer, 0, singleByte);
                }
            } catch (Exception e) {
                Log.e(TAG, "SaveRawToDownloadsDirectory: " + e.getMessage());
                return false;
            } finally {
                if (inp != null) {
                    try {
                        inp.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "SaveRawToDownloadsDirectory: " + e.getMessage());
                        return false;
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "SaveRawToDownloadsDirectory: " + e.getMessage());
                        return false;
                    }
                }
            }
            return true;
        }

        public static boolean DeleteFile(String Path) {
            File file = new File(Path);
            return file.delete();
        }

        public static String[] StringArrayListToStringArray(ArrayList<String> arr) {
            String[] strarr = new String[arr.size()];
            for (int i = 0; i < arr.size(); i++)
                strarr[i] = arr.get(i);
            return strarr;
        }
        public static String[] StringArrayListToStringArray(String[] arr) {
            String[] strarr = new String[arr.length];
            for (int i = 0; i < arr.length; i++)
                strarr[i] = arr[i];
            return strarr;
        }

        public static void ChangeLanguage(Context mContext, String code) {
            Locale locale = new Locale(code);
            Resources resources = mContext.getResources();
            Configuration configuration = resources.getConfiguration();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                configuration.setLocale(locale);
            }
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }

        public static String DateTimeStringReverseFromTimeStamp(String timestamp) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = formatter.format(new Date(Long.parseLong(timestamp)));
            return dateString;
        }

        public static String DateTimeStringFromTimeStamp(String timestamp) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String dateString = formatter.format(new Date(Long.parseLong(timestamp)));
            return dateString;
        }

        public static boolean IsMyServiceRunning(Context mContext, Class<?> serviceClass) {
            ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
            return false;
        }

        public static String GetReverseCurrentDateString() {
            Calendar calendarX = Calendar.getInstance();
            String str = calendarX.get(Calendar.YEAR) + "-" + (calendarX.get(Calendar.MONTH) + 1) + "-"
                    + calendarX.get(Calendar.DAY_OF_MONTH);
            return str;
        }

        public static String MyCalendarToReverseString(Calendar calendarX) {
            String str = calendarX.get(Calendar.YEAR) + "-" + (calendarX.get(Calendar.MONTH) + 1) +
                    "-" + calendarX.get(Calendar.DAY_OF_MONTH) + " " + calendarX.get(Calendar.HOUR_OF_DAY) + ":" +
                    calendarX.get(Calendar.MINUTE) + ":" + calendarX.get(Calendar.SECOND);
            return str;
        }

        public static String MyCalendarToString(Calendar calendarX) {
            @SuppressLint("DefaultLocale")
            String result = String.format("%02d:%02d:%02d %02d-%02d-%04d", calendarX.get(Calendar.HOUR_OF_DAY),
                    calendarX.get(Calendar.MINUTE), calendarX.get(Calendar.SECOND),
                    calendarX.get(Calendar.DAY_OF_MONTH), calendarX.get(Calendar.MONTH) + 1, calendarX.get(Calendar.YEAR));
            return result;
        }

        public static String GetNowTimeString() {
            Calendar calendarX = Calendar.getInstance();
            return MyCalendarToString(calendarX);
        }

        public static String GetCurrentTimeStamp() {
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            return ts;
        }

        public static String StringListToSingalString(ArrayList<String> arr) {
            String res = "";
            for (int i = 0; i < arr.size(); i++) {
                res += arr.get(i) + " ";
            }
            Log.d(TAG, "StringListToSingalString: >" + res);
            return res;
        }

        public static String Base64FromImageFile(String _Path) {
            Bitmap bm = BitmapFromFilePath(_Path);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            return encoded;
        }

        public static Bitmap BitmapFromFilePath(String _Path) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(_Path, options);
            return bitmap;
        }

        public static boolean SaveImage(Bitmap bm, String fn, String destDir) {
            // Check and create dir if nessasary
            File dest = new File(destDir);
            if (!dest.exists()) {
                return false;
            }
            File desF = new File(dest + File.separator + fn);
            if (desF.exists()) {
                desF.delete();
            }
            try {
                FileOutputStream out = new FileOutputStream(desF);
                out.write(Using.BitmapToBytes(bm));
                out.flush();
                out.close();
                return true;
            } catch (Exception e) {
                Log.e(TAG, "SaveImage: " + e.getMessage());
                return false;
            }

        }

        public static void createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {

            File direct = new File(Environment.getExternalStorageDirectory() + "/DirName");

            if (!direct.exists()) {
                File wallpaperDirectory = new File("/sdcard/DirName/");
                wallpaperDirectory.mkdirs();
            }

            File file = new File(new File("/sdcard/DirName/"), fileName);
            if (file.exists()) {
                file.delete();
            }
            try {
                FileOutputStream out = new FileOutputStream(file);
                imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public static String createDirectoryAndSaveFile(byte[] data, String directoryName, String fileName) {

            String PathX = Environment.getExternalStorageDirectory() + directoryName;
            File direct = new File(PathX);

            if (!direct.exists()) {
                File wallpaperDirectory = new File(PathX);
                wallpaperDirectory.mkdirs();
            }

            File file = new File(new File(PathX), fileName.replace("-","-"));
            file.deleteOnExit();
            try {
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(data);
                fos.close();
                return file.getPath();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        public static Bitmap BytesToBitmap(byte[] bytes) {
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }

        public static Bitmap getCircularBitmap(Bitmap bitmap, int maxsize) {
            bitmap = ResizeBitmap(bitmap, maxsize);
            Bitmap output;

            if (bitmap.getWidth() > bitmap.getHeight()) {
                output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            } else {
                output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            float r = 0;

            if (bitmap.getWidth() > bitmap.getHeight()) {
                r = bitmap.getHeight() / 2;
            } else {
                r = bitmap.getWidth() / 2;
            }

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(r, r, r, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return output;
        }
        public static Bitmap ResizeBitmap(Bitmap image, int maxSize) {
            int width = image.getWidth();
            int height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }

            return Bitmap.createScaledBitmap(image, width, height, true);
        }

        public static Bitmap RotateBitmap(Bitmap bitmapX, float angle) {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Bitmap.createBitmap(bitmapX, 0, 0, bitmapX.getWidth(), bitmapX.getHeight(), matrix, true);
        }

        public static byte[] BitmapToBytes(Bitmap bm) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            bm.recycle();
            return stream.toByteArray();
        }

        public static String getContent(String _URL) {
            String result = "";
            try {
                result = new getContentFromURL().execute(_URL).get();
            } catch (Exception e) {
                result = "Không thực hiện tải được " + e.getMessage();
            }
            Log.d(TAG, "getContent: " + result);
            return result;
        }

        public static Bitmap getBitmap(String _URL) {
            try {
                return new getBitmapFromURL().execute(_URL).get();
            } catch (Exception e) {
                Log.e(TAG, "getBitmap: " + e.getMessage());
                return null;
            }
        }

        public static String DateToString(Date inp) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss'Z'dd/MM/yyyy'T'");
            try {
                String result = simpleDateFormat.format(inp);
                return result;
            } catch (Exception e) {
                Log.e(TAG, "DateToString: " + e.getMessage());
                return "";
            }
        }

        public static Date StringToDateTime(String inp) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss'Z'dd/MM/yyyy'T'");
            try {
                Date myDate = simpleDateFormat.parse(inp);
                return myDate;
            } catch (Exception e) {
                Log.e(TAG, "StringToDateTime: " + e.getMessage());
                return null;
            }
        }

        public static String DecodeUnicode(String UnicodeEncodedString) {
            Matcher matcher = Pattern.compile("\\\\u[a-z0-9]{4}", Pattern.MULTILINE).matcher(UnicodeEncodedString);
            while (matcher.find())
            {
                String temp = "" + (char)Integer.parseInt(matcher.group(0).replace("\\u",  ""),16);
                UnicodeEncodedString = UnicodeEncodedString.replace(matcher.group(0), temp);
            }
            return UnicodeEncodedString;
        }
        public static Date StringToDate(String inp) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            try {
                Date myDate = simpleDateFormat.parse(inp);
                return myDate;
            } catch (Exception e) {
                return null;
            }
        }

        public static byte[] DrawableToBytes(Context mContext, int id) {
            return BitmapToBytes(DrawableToBitmap(mContext, id));
        }

        public static Bitmap DrawableToBitmap(Context mContext, int id) {
            return ((BitmapDrawable) mContext.getResources().getDrawable(id, null)).getBitmap();
        }

        public static class NameOfYear {
            private static String Chi[] = {"Thân", "Dậu", "Tuất", "Hợi", "Tí", "Sửu", "Dần", "Mão", "Thìn", "Tị", "Ngọ", "Mùi"};
            private static String Can[] = {"Canh", "Tân", "Nhâm", "Quý", "Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ"};
            public static String Get(int Nam)
            {
                return Can[Nam%10] + " " + Chi[Nam%12];
            }
        }
    }

    public static class getContentFromURL extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("User-Agent", "Mozilla/5.0");
                return InputStreamToString(conn.getInputStream());
            } catch (Exception e) {
                Log.e(TAG, "<getContentFromURL> " + e.getMessage());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    public static class getBitmapFromURL extends AsyncTask<String, Integer, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... imageUrl) {
            try {
                URL url = new URL(imageUrl[0].replace("https", "http"));
                return BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }
    }
    //region InputStream to String
    public static String InputStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            Log.e(TAG, "> " + e.getMessage());
        }
        return sb.toString();
    }
    //endregion
}