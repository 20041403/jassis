package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.Song;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;
import com.futuresky.jassis.TNLib;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.JassisDeepListenServices.mAudioManager;

public class ModuleMediaPlayer {
    RelativeLayout rlModuleWrap;
    ImageView ivSongAvatar;
    public ImageView ivPlayerControlButton;
    ProgressBar prbPlayProgress;
    TextView tvPercentOfProgress;
    TextView tvSongTitle;
    public MediaPlayer mediaPlayer;
    public Boolean IS_PAUSE = false;
    public LinearLayout lnClose;
    Timer timer;

    public ModuleMediaPlayer(Song song) {
        // Init view
        rlModuleWrap = BODY.mContext.findViewById(R.id.module_mediaplayer);
        ivSongAvatar = BODY.mContext.findViewById(R.id.player_song_avatar);
        ivPlayerControlButton = BODY.mContext.findViewById(R.id.player_control_button);
        prbPlayProgress = BODY.mContext.findViewById(R.id.player_progress_bar);
        tvPercentOfProgress = BODY.mContext.findViewById(R.id.player_progress_percent);
        tvSongTitle = BODY.mContext.findViewById(R.id.player_song_tile);
        lnClose = BODY.mContext.findViewById(R.id.player_close);
        // Init Song meta data
        InitSongMetaDatas(song);
        // Set play button back
        ivPlayerControlButton.setImageDrawable(BODY.mContext.getDrawable(R.drawable.ic_play));
        // Show it
        rlModuleWrap.setVisibility(View.VISIBLE);
        // Restore to default
        tvPercentOfProgress.setText("0%");
        prbPlayProgress.setProgress(0);
        // Set up mediaplayer
        if (mediaPlayer == null)
            mediaPlayer = new MediaPlayer();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(song.getLocation());
            mediaPlayer.prepare();
            // Set for progree
            prbPlayProgress.setMax(mediaPlayer.getDuration());
            // set running event
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Event startbutton
        ivPlayerControlButton.setOnClickListener(v -> {
            if (!mediaPlayer.isPlaying() && mediaPlayer.getCurrentPosition() < mediaPlayer.getDuration()) {
                IS_PAUSE = false;
                // Gán kênh hiện tại là kênh phát nhạc
                JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.MUSIC_PLAYER;
                // Dừng việc lắng nghe nếu có
                if (BODY.mContext.mJassisListenServices != null)
                    BODY.mContext.mJassisListenServices.stopListen();
                BODY.mContext.mPresenter.mSpeechRecognizerManager.stopListen();
                // Mở âm thanh
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, AudioManager.FLAG_PLAY_SOUND);
                }
                // Tiến hành chơi nhạc
                mediaPlayer.start();
                StartTimer();
                ivPlayerControlButton.setImageDrawable(BODY.mContext.getDrawable(R.drawable.ic_pause));
            } else {
                try {
                    if (mediaPlayer != null && mediaPlayer.isPlaying())
                        mediaPlayer.pause();
                } catch (Exception e) {
                    // Không làm gì cả
                }
                if (!mediaPlayer.isPlaying())
                    IS_PAUSE = true;
                ivPlayerControlButton.setImageDrawable(BODY.mContext.getDrawable(R.drawable.ic_play));
            }
        });
        lnClose.setOnClickListener(v -> {
            IS_PAUSE = false;
            mediaPlayer.stop();
            mediaPlayer.reset();
            ivPlayerControlButton.setImageDrawable(BODY.mContext.getDrawable(R.drawable.ic_play));
            rlModuleWrap.setVisibility(View.INVISIBLE);
            ivSongAvatar.setImageDrawable(BODY.mContext.getDrawable(R.drawable.empty));
            // Delete
            BODY.MUSIC_PLAYER = null;
            // Chuyển cờ sang tương tác thông thường
            JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.INTERFACE;
        });
        // On player complete
        mediaPlayer.setOnCompletionListener(mp -> {
            lnClose.performClick();
        });
        ivPlayerControlButton.performClick();
    }

    private void InitSongMetaDatas(Song song) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(song.getLocation());
        try {
            byte[] avatar = mediaMetadataRetriever.getEmbeddedPicture();
            if (avatar == null) {
                // Set avatar
                Glide.with(BODY.mContext)
                        .load(Uri.parse(song.getAvatar()))
                        .placeholder(BODY.mContext.getResources().getDrawable(R.drawable.empty, null))
                        .into(ivSongAvatar);
            } else {
                Bitmap songImage = BitmapFactory.decodeByteArray(avatar, 0, avatar.length);
                ivSongAvatar.setImageBitmap(songImage);
            }
            String songTitle = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            String singer = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            if (singer != null && singer.isEmpty())
                song.setCreator(singer);
            song.setTitle(songTitle);
            tvSongTitle.setText(song.getTitle() + " - " + song.getCreator());
            String[] notfoundit = {"Đang phát bài " + song.getTitle(), "Cùng nghe " + song.getCreator() + " hát nào", "Bài hát " + song.getTitle() + " đang được phát",
                    "Hãy cùng thưởng thức giọng ca của " + song.getCreator()};
            BODY.MOUNTH.TALK(notfoundit[new Random().nextInt(notfoundit.length)]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Set song name

    }

    void StartTimer() {
        if (timer != null)
            return;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        // Mở âm thanh
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (mAudioManager.isStreamMute(AudioManager.STREAM_MUSIC))
                                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, AudioManager.FLAG_PLAY_SOUND);
                        }
                        String progress = mediaPlayer.getCurrentPosition() * 100 / mediaPlayer.getDuration() + "%";
                        prbPlayProgress.post(() -> prbPlayProgress.setProgress(mediaPlayer.getCurrentPosition()));
                        tvPercentOfProgress.post(() -> {
                            tvPercentOfProgress.setText(progress);
                        });
                    } else {
                        if (!IS_PAUSE)
                            timer.cancel();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 50, 50);
    }
}
