package com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjectFeature {
    int cb = -1, cl = -1, cr = -1, itg = -1, oh = -1, ow = -1, rt = -1, th = -1, tw = -1;
    String id = "", isu = "", ity = "", ou = "", pt = "", rh = "", rid = "", ru = "", s = "", sc = "", st = "", tu = "";

    public ObjectFeature(String JSONs) {
        try {
            JSONObject me = new JSONObject(JSONs);
            if (!me.isNull("cb") && me.has("cb"))
                cb = me.getInt("cb");
            if (!me.isNull("cl") && me.has("cl"))
                cl = me.getInt("cl");
            if (!me.isNull("cr") && me.has("cr"))
                cr = me.getInt("cr");
            if (!me.isNull("id") && me.has("id"))
                id = me.getString("id");
            if (!me.isNull("isu") && me.has("isu"))
                isu = me.getString("isu");
            if (!me.isNull("itg") && me.has("itg"))
                itg = me.getInt("itg");
            if (!me.isNull("ity") && me.has("ity"))
                ity = me.getString("ity");
            if (!me.isNull("oh") && me.has("oh"))
                oh = me.getInt("oh");
            if (!me.isNull("ou") && me.has("ou"))
                ou = me.getString("ou");
            if (!me.isNull("ow") && me.has("ow"))
                ow = me.getInt("ow");
            if (!me.isNull("pt") && me.has("pt"))
                pt = me.getString("pt");
            if (!me.isNull("rh") && me.has("rh"))
                rh = me.getString("rh");
            if (!me.isNull("rid") && me.has("rid"))
                rid = me.getString("rid");
            if (!me.isNull("rt") && me.has("rt"))
                rt = me.getInt("rt");
            if (!me.isNull("ru") && me.has("ru"))
                ru = me.getString("ru");
            if (!me.isNull("s") && me.has("s"))
                s = me.getString("s");
            if (!me.isNull("sc") && me.has("sc"))
                sc = me.getString("sc");
            if (!me.isNull("st") && me.has("st"))
                st = me.getString("st");
            if (!me.isNull("th") && me.has("th"))
                th = me.getInt("th");
            if (!me.isNull("tu") && me.has("tu"))
                tu = me.getString("tu");
            if (!me.isNull("tw") && me.has("tw"))
                tw = me.getInt("tw");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getCb() {
        return cb;
    }

    public void setCb(int cb) {
        this.cb = cb;
    }

    public int getCl() {
        return cl;
    }

    public void setCl(int cl) {
        this.cl = cl;
    }

    public int getCr() {
        return cr;
    }

    public void setCr(int cr) {
        this.cr = cr;
    }

    public int getItg() {
        return itg;
    }

    public void setItg(int itg) {
        this.itg = itg;
    }

    public int getOh() {
        return oh;
    }

    public void setOh(int oh) {
        this.oh = oh;
    }

    public int getOw() {
        return ow;
    }

    public void setOw(int ow) {
        this.ow = ow;
    }

    public int getRt() {
        return rt;
    }

    public void setRt(int rt) {
        this.rt = rt;
    }

    public int getTh() {
        return th;
    }

    public void setTh(int th) {
        this.th = th;
    }

    public int getTw() {
        return tw;
    }

    public void setTw(int tw) {
        this.tw = tw;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsu() {
        return isu;
    }

    public void setIsu(String isu) {
        this.isu = isu;
    }

    public String getIty() {
        return ity;
    }

    public void setIty(String ity) {
        this.ity = ity;
    }

    public String getOu() {
        return ou;
    }

    public void setOu(String ou) {
        this.ou = ou;
    }

    public String getPt() {
        return pt;
    }

    public void setPt(String pt) {
        this.pt = pt;
    }

    public String getRh() {
        return rh;
    }

    public void setRh(String rh) {
        this.rh = rh;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getRu() {
        return ru;
    }

    public void setRu(String ru) {
        this.ru = ru;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    public String getSt() {
        return st;
    }

    public void setSt(String st) {
        this.st = st;
    }

    public String getTu() {
        return tu;
    }

    public void setTu(String tu) {
        this.tu = tu;
    }

    public String getImageLink() {
        return getOu();
    }

    public String getDescription() {
        return getPt();
    }

    public String getSourceSite() {
        return getRu();
    }
}
