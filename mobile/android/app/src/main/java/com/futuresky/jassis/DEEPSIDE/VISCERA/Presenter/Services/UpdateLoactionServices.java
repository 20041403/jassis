package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener;

import org.json.JSONObject;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener.ws;

public class UpdateLoactionServices extends Service {
    public UpdateLoactionServices() {
    }

    private static final String TAG = UpdateLoactionServices.class.getSimpleName();
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    public static Location mLastLocation;

    private class LocationListener implements android.location.LocationListener {

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            if (mLastLocation == null)
                mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            UpdateCurrentLocation(location);
            mLastLocation.set(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };


    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.e(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.e(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.e(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    public static void UpdateCurrentLocation(Location location) {
        Log.w(TAG, "onMyLocationClick: Vị trí hiện tại là: " + location);
        try {
            JSONObject userLocation = new JSONObject();
            userLocation.put("log", location.getLongitude());
            userLocation.put("lat", location.getLatitude());

            JSONObject requestUpdateLocation = new JSONObject();
            requestUpdateLocation.put("current_position", userLocation);
            requestUpdateLocation.put("host_user_id", NETWORK_CONFIG.CLIENT.USER_ID);

            JSONObject cnnObj = new JSONObject();
            cnnObj.put("action", EchoWebsockerListener.OptionType.ONLOCATIONCHANGE);
            cnnObj.put("current_location", requestUpdateLocation);
            Log.w(TAG, "onLocationChanged send JSON: \n" + cnnObj.toString());

            ws.send(cnnObj.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
