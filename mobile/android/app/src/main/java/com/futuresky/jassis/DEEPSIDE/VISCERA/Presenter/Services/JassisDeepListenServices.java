package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;
import com.futuresky.jassis.INTERFACE.BODY;

import java.util.ArrayList;
import java.util.TimerTask;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Home.SpeechRecognizerManager.mInterfaceSpeechRecognizer;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome.IS_MATCHED_CALL;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome.pm;

public class JassisDeepListenServices extends Service {
    public final static String TAG = JassisDeepListenServices.class.getSimpleName();
    protected Intent mServicesSpeechRecognizerIntent;
    protected String language = "vi";
    public static SpeechRecognizer mServicesSpeechRecognizer;
    // binder given to clients
    private final IBinder binder = new LocalBinder();
    // Registed callbacks
    private JassisDeepListenCallback mJassisDeepListenCallbacks;

    public class LocalBinder extends Binder {
        public JassisDeepListenServices getServices() {
            return JassisDeepListenServices.this;
        }
    }

    //
    public static AudioManager mAudioManager;

    Intent mCurrentIntent;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void Init() {
        // Setting audio manager
        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // Create new intent
        mServicesSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getPackageName());
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, language);
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, language);
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, language);
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true); // For streaming result
        mServicesSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 100L);
    }

    protected void InitSpeechRecognizer() {
        if (mServicesSpeechRecognizer != null) {
            stopListen();
            mServicesSpeechRecognizer = null;
        }
        mServicesSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mServicesSpeechRecognizer.setRecognitionListener(new SpeechRecognitionListener());
    }

    public void startListening() {
        if (JAssistant.CURRENT_CHANNEL != JAssistant.CHANNEL.DEEP_LISTEN) {
            destroyListen();
            return;
        }
        if (IS_MATCHED_CALL)
            return;
        if (mInterfaceSpeechRecognizer == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, AudioManager.FLAG_PLAY_SOUND);
            }
            stopListen();
            destroyListen();
            InitSpeechRecognizer();
            mServicesSpeechRecognizer.startListening(mServicesSpeechRecognizerIntent);
        }
    }

    public void stopListen() {
        if (mInterfaceSpeechRecognizer != null) {
            if (mServicesSpeechRecognizer != null) {
                mServicesSpeechRecognizer.cancel();
                mServicesSpeechRecognizer.stopListening();
            }
        }
    }

    public void destroyListen() {
        stopListen();
        if (mServicesSpeechRecognizer != null) {
            mServicesSpeechRecognizer.destroy();
        }
        mServicesSpeechRecognizer = null;
    }

    private class SpeechRecognitionListener implements RecognitionListener {
        @Override
        public void onReadyForSpeech(Bundle params) {
        }

        @Override
        public void onBeginningOfSpeech() {
        }

        @Override
        public void onRmsChanged(float rmsdB) {
        }

        @Override
        public void onBufferReceived(byte[] buffer) {
        }

        @Override
        public void onEndOfSpeech() {
            if (!IS_MATCHED_CALL && JAssistant.CURRENT_CHANNEL == JAssistant.CHANNEL.DEEP_LISTEN) {
                startListening();
            } else {
                // leave
            }
        }


        @Override
        public void onError(int error) {
            if (!IS_MATCHED_CALL && JAssistant.CURRENT_CHANNEL == JAssistant.CHANNEL.DEEP_LISTEN) {
                startListening();
            } else {
                // leave
            }
        }

        @Override
        public void onResults(Bundle results) {
            if (results != null) {
                ArrayList<String> resultArray = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                assert resultArray != null;
                Called(resultArray.get(0));
                Log.w(TAG, "Kết quả nghe thầm lặng: " + resultArray.get(0));
            }
        }

        void Called(String inp) {
            if (mJassisDeepListenCallbacks != null && inp.toLowerCase().contains(JAssistant.NAME.toLowerCase())) {
                Log.w(TAG, ">>> ĐÃ NGHE THẤY NGƯỜI KHÁC GỌI MÌNH!");
                mServicesSpeechRecognizer.cancel();
                stopListen();
                destroyListen();
                Thread x = new Thread(() -> {
                    SystemClock.sleep(20);
                    Log.w(TAG, "onResults: Tiến hành thực hiện gọi ngược");
                    new CallbackActions().execute();
                });
                x.start();
                return;
            } else {
                if (!IS_MATCHED_CALL && JAssistant.CURRENT_CHANNEL == JAssistant.CHANNEL.DEEP_LISTEN) {
                    startListening();
                } else {
                    // leave
                }
            }
        }

        @Override
        public void onPartialResults(Bundle partialResults) { // Streamming result
            if (partialResults != null) {
                ArrayList<String> texts = partialResults.getStringArrayList("android.speech.extra.UNSTABLE_TEXT");
                assert texts != null;
                if (texts != null) {
                    Called(texts.get(0));
                    Log.w(TAG, "Nghe được tức thì: " + texts.get(0));
                }
            } else {
                Log.w(TAG, "Nghe được tức thì không khả dụng, do partialResult là rỗng!");
            }
        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }
    }

    private class CallbackActions extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            IS_MATCHED_CALL = true;
            stopListen();
            if (!ActivityHome.IS_RUNNING) {
                PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), TAG);
                wakeLock.acquire(10 * 60 * 1000L);

                Intent intent = new Intent(JassisDeepListenServices.this, ActivityHome.class);
                intent.setAction(Intent.ACTION_SCREEN_ON);
                intent.setAction(Intent.ACTION_MAIN);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                JassisDeepListenServices.this.startActivity(intent);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            while (true) {
                if (IS_MATCHED_CALL && ActivityHome.IS_RUNNING && mInterfaceSpeechRecognizer == null && pm.isInteractive())
                    break;
                while (!pm.isInteractive()) ;
                SystemClock.sleep(10);
            }
            Log.w(TAG, "doInBackground: Đã đủ điều kiện để thực hiện lắng nghe!!");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.w(TAG, "onPreExecute: Đợi cho đến khi màn hình bật và màn hình chính ở chế độ hoạt động");
            mJassisDeepListenCallbacks.userCalledMe();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        mCurrentIntent = intent;
        Init();
        startListening();
        return binder;
    }

    public void setCallbacks(JassisDeepListenCallback callbacks) {
        mJassisDeepListenCallbacks = callbacks;
    }
}
