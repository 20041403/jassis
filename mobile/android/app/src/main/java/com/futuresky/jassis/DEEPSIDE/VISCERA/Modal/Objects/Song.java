package com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects;

public class Song {
    String title = "";
    String creator = "";
    String location = "";
    String lyric = "";
    String bgImage = "";
    String avatar = "";
    String corverImage = "";
    String singerInfo = "";

    public Song() {

    }
    public static Song getDemo() {
        return new Song("Ex's Hate Me",
                "B Ray, Masew, AMee",
                "http://mp3-s1-zmp3.zadn.vn/27d8b4be9bf972a72be8/2815297979810517552?authen=exp=1557500685~acl=/27d8b4be9bf972a72be8/*~hmac=b1450d045e7adea0f67ef7cc79997580",
                "http://lrc-nct.nixcdn.com/2019/02/16/4/1/4/1/1550295026824.lrc",
                "http://avatar-nct.nixcdn.com/singer/avatar/2019/02/27/7/d/f/a/1551250667338_600.jpg",
                "https://znews-photo.zadn.vn/w480/Uploaded/qfssu/2019_02_15/51734072_2044833409147553_5499681549236305920_n_thumb.jpg",
                "http://avatar-nct.nixcdn.com/song/2019/02/13/7/c/9/3/1550063179723_500.jpg",
                "http://www.nhaccuatui.com/nghe-si-bray.html");
    }

    public Song(String title, String creator, String location, String lyric, String bgImage, String avatar, String corverImage, String singerInfo) {
        this.title = title;
        this.creator = creator;
        this.location = location;
        this.lyric = lyric;
        this.bgImage = bgImage;
        this.avatar = avatar;
        this.corverImage = corverImage;
        this.singerInfo = singerInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    public String getBgImage() {
        return bgImage;
    }

    public void setBgImage(String bgImage) {
        this.bgImage = bgImage;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCorverImage() {
        return corverImage;
    }

    public void setCorverImage(String corverImage) {
        this.corverImage = corverImage;
    }

    public String getSingerInfo() {
        return singerInfo;
    }

    public void setSingerInfo(String singerInfo) {
        this.singerInfo = singerInfo;
    }
}
