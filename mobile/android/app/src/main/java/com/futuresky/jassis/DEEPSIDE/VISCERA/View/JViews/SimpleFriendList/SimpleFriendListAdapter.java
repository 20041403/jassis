package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.SimpleFriendList;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.SimpleFriendInfo;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.MessageBox.MessageList;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;

import java.util.ArrayList;
import java.util.List;

public class SimpleFriendListAdapter extends RecyclerView.Adapter<SimpleFriendListAdapter.RecyclerViewHolder> {
    private List<SimpleFriendInfo> data = new ArrayList<>();

    public SimpleFriendListAdapter(List<SimpleFriendInfo> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.layout_item_friend_online, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int i) {
        holder.frDisplayName.setText(data.get(i).getDisplayName());
        if (data.get(i).isStatus()) {
            holder.frStatus.setVisibility(View.VISIBLE);
        } else {
            holder.frStatus.setVisibility(View.INVISIBLE);
        }
        // Set avatar
        Glide.with(BODY.mContext)
                .load(Uri.parse(data.get(i).getAvatarLink()))
                .placeholder(BODY.mContext.getResources().getDrawable(R.drawable.empty, null))
                .into(holder.frAvatar);
        // Event
        holder.item.setOnClickListener(v -> {
            // Thực hiện nối id của 2 người dùng này lại và tiến hành cho phép gửi tin nhắn
            MessageList.Show(BODY.mContext, BODY.mContext.rlRootLayout, data.get(i));
        });
        // Set item
        if (data.get(i).getUnread().size() <= 0) {
            holder.frMessageCount.setText("0");
            holder.frMessageCount.setVisibility(View.INVISIBLE);
        } else {
            holder.frMessageCount.setVisibility(View.VISIBLE);
            if (data.get(i).getUnread().size() <= 5)
                holder.frMessageCount.setText(data.get(i).getUnread().size() + "");
            else
                holder.frMessageCount.setText("5+");
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout item;
        ImageView frAvatar;
        TextView frDisplayName;
        LinearLayout frStatus;
        TextView frMessageCount;

        public RecyclerViewHolder(@NonNull View v) {
            super(v);
            item = v.findViewById(R.id.friend_item);
            frAvatar = v.findViewById(R.id.friend_avatar);
            frDisplayName = v.findViewById(R.id.friend_display_name);
            frStatus = v.findViewById(R.id.friend_online_status);
            frMessageCount = v.findViewById(R.id.friend_message_count);
        }
    }
}
