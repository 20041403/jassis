package com.futuresky.jassis.DEEPSIDE.VISCERA.View.AccountLogRes;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;
import com.futuresky.jassis.TNLib;
import com.meetic.marypopup.MaryPopup;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener.OpenConnection;
import static com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener.wsid;

public class ModulLoginDialog {
    public final static String TAG = ModulLoginDialog.class.getSimpleName();
    public static boolean isShow = false;
    public static MaryPopup popup;

    public void Show(final Activity mContext, View from) {
        if (popup != null) {
            if (popup.isOpened())
                popup.close(true);
            popup = null;
        }
        // Infilate layout
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.module_layout_login, null);
        // Create Dialog
        popup = MaryPopup.with(mContext)
                .content(v)
                .from(from)
                .blackOverlayColor(0)
                .backgroundColor(0)
                .center(true)
                .draggable(true)
                .scaleDownDragging(true)
                .closeDuration(500)
                .cancellable(false);
        // 1. Find and init view
        EditText userName = v.findViewById(R.id.module_layout_login_username);
        EditText userPass = v.findViewById(R.id.module_layout_login_password);
        Button loginButton = v.findViewById(R.id.module_layout_login_login_btn);
        loginButton.setOnClickListener(z -> {
            if (userName.getText().toString().isEmpty() || userPass.getText().toString().isEmpty()) {
                Toasty.error(mContext, "Vui lòng nhập đầy đủ thông tin.", Toast.LENGTH_SHORT, true).show();
                return;
            }
            // Lưu tạm vào biến
            String usrName = userName.getText().toString();
            String usrPass = userPass.getText().toString();
            // Xóa giá trị của password
            userPass.setText("");
            // Tiến hành gửi request
            new LoginAsync().execute(usrName, usrPass);
        });

        ///////////////////////////////////
        popup.show();
        isShow = true;
    }

    public void Login(String username, String pass) {
        new LoginAsync().execute(username, pass);
    }

    @SuppressLint("StaticFieldLeak")
    public class LoginAsync extends AsyncTask<String, Integer, Boolean> {
        boolean isHiddenLogin = false;
        int id = -1;
        String userName = "";
        String encodePass = "";
        String displayName = "";
        String avatarLink = "";
        String token = "";

        LoginAsync() {

        }

        SweetAlertDialog loadingDialog;
        LoginAsync loginAsync;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loginAsync = this;
            loadingDialog = new SweetAlertDialog(BODY.mContext)
                    .setTitleText("Đang đăng nhập")
                    .setCancelButton("Hủy", z -> {
                        loginAsync.cancel(true);
                    })
                    .showCancelButton(true);
            loadingDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
            loadingDialog.setCancelable(false);
            loadingDialog.create();
            loadingDialog.show();
        }

        @Override
        protected void onCancelled(Boolean aBoolean) {
            super.onCancelled(aBoolean);
            if (aBoolean == null || aBoolean) {
                if (loadingDialog != null && loadingDialog.isShowing()) {
                    loadingDialog.cancel();
                }
                Toasty.success(BODY.mContext, "Đã hủy đăng nhập!", Toast.LENGTH_SHORT, true).show();
            } else {
                Toasty.error(BODY.mContext, "Không thể hủy đăng nhập ngay được!", Toast.LENGTH_SHORT, true).show();
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (loadingDialog != null && loadingDialog.isShowing()) {
                loadingDialog.cancel();
            }
            if (aBoolean) {
                Log.w(TAG, "onPostExecute: Đã đăng nhập thành công rồi huhuhhu!");
                DownloadUserAvatar();
                if (isHiddenLogin)
                    return;
                Toasty.success(BODY.mContext, "Đăng nhập thành công!", Toast.LENGTH_SHORT, true).show();
                popup.close(true);
            } else {
                if (!isHiddenLogin)
                    Toasty.error(BODY.mContext, "Sai tài khoản hoặc mật khẩu! Vui lòng kiểm tra lại.", Toast.LENGTH_SHORT, true).show();
                else
                    Show(BODY.mContext, BODY.mContext.rlRootLayout);
            }
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            HttpURLConnection client = null;
            StringBuilder response = new StringBuilder();
            if (strings[1].isEmpty())
                isHiddenLogin = true;
            try {
                userName = strings[0];
                if (!isHiddenLogin)
                    encodePass = TNLib.Using.MD5(strings[1] + strings[1] + strings[0]);
                // Create json parameter
                JSONObject JSONbody = new JSONObject();
                JSONbody.put("usr_name", strings[0]);
                JSONbody.put("usr_pass", (!strings[1].equals("") ?
                        encodePass :
                        NETWORK_CONFIG.CLIENT.USER_ENCODED_PASS));

                URL url = new URL(NETWORK_CONFIG.CHECK_LOGIN_API);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("POST");
                client.setConnectTimeout(1000);
                client.setRequestProperty("User-Agent", "Mozilla/5.0");
                client.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                client.setRequestProperty("Accept", "application/json");
                client.setDoInput(true);
                client.setDoOutput(true);

                OutputStream os = client.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, StandardCharsets.UTF_8));
                writer.write(JSONbody.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = client.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                } else {
                    response = new StringBuilder();
                }
            } catch (Exception e) {
                Log.e(TAG, "ERROR: " + e.getMessage());
                e.printStackTrace();
                return false;
            } finally {
                if (client != null) // Make sure the connection is not null.
                    client.disconnect();
            }
            Log.w(TAG, "RESPONSE:\n" + response);
            String JSONstring = response.toString().trim();
            if (JSONstring.equals("-1"))
                return false;
            else {
                try {
                    JSONObject s = new JSONObject(JSONstring);

                    JSONObject info_user = s.getJSONObject("info_user");

                    token = s.getString("token");
                    NETWORK_CONFIG.CLIENT.USER_ID = info_user.getInt("id");
                    NETWORK_CONFIG.CLIENT.USER_DISPLAY_NAME = info_user.getString("display_name");
                    NETWORK_CONFIG.CLIENT.USER_AVATAR_SRC = info_user.getString("avatar_src");
                    if (info_user.has("background_link"))
                        NETWORK_CONFIG.CLIENT.USER_BACKGROUND = info_user.getString("background_link");

                    NETWORK_CONFIG.CLIENT.USER_NAME = userName;
                    NETWORK_CONFIG.CLIENT.USER_ENCODED_PASS = encodePass;
                    NETWORK_CONFIG.CLIENT.UpdateClientInfo(BODY.mContext);
                    NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN = token;

                    Log.w(TAG, "Login success: \n" +
                            "USER_ID:" + NETWORK_CONFIG.CLIENT.USER_ID + "\n" +
                            "USER_DISPLAY_NAME: " + NETWORK_CONFIG.CLIENT.USER_DISPLAY_NAME + "\n" +
                            "USSER_AVATAR: " + NETWORK_CONFIG.CLIENT.USER_AVATAR_SRC + "\n" +
                            "TOKEN: " + NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN);
                    if (wsid.isEmpty())
                        OpenConnection();
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }
        }
    }

    public static void DownloadUserAvatar() {
        Log.w(TAG, "DownloadUserAvatar: Ở đây cmnr");
        NETWORK_CONFIG.CLIENT.USER_AVATAR = TNLib.Using.getBitmap(NETWORK_CONFIG.CLIENT.USER_AVATAR_SRC);
        if (NETWORK_CONFIG.CLIENT.USER_AVATAR == null)
            NETWORK_CONFIG.CLIENT.USER_AVATAR = TNLib.Using.DrawableToBitmap(BODY.mContext, R.drawable.noavatar);
    }

}
