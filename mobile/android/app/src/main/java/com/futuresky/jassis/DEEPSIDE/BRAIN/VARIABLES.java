package com.futuresky.jassis.DEEPSIDE.BRAIN;

import android.util.Log;

import com.futuresky.jassis.TNLib;

import java.nio.channels.Pipe;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VARIABLES {
    public final static List<String> LIST = Arrays.asList(
            "${number}",
            "${date}",
            "${day}",
            "${month}",
            "${year}",
            "${weekdays}",
            "${time}");

    private static String GetSystemValue(String input) {
        String sys1 = "${sys_app_list}";
        if (input.contains(sys1)) {

        }

        // -------------------------------------------------------------------

        String sys2 = "${sys_current_datetime}";
        if (input.contains(sys2)) {
            Calendar tempCalen = Calendar.getInstance();
            input = input.replace(sys2, tempCalen.get(Calendar.HOUR_OF_DAY) + " giờ " +
                    tempCalen.get(Calendar.MINUTE) + " phút - Ngày " +
                    tempCalen.get(Calendar.DAY_OF_MONTH) + "/" +
                    (tempCalen.get(Calendar.MONTH) + 1) + "/" +
                    tempCalen.get(Calendar.YEAR));
        }

        // -------------------------------------------------------------------

        String sys3 = "${sys_current_hour}";
        if (input.contains(sys3)) {
            input = input.replace(sys3, Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + " giờ");
        }

        // -------------------------------------------------------------------

        String sys4 = "${sys_current_minute}";
        if (input.contains(sys4)) {
            input = input.replace(sys3, Calendar.getInstance().get(Calendar.MINUTE) + " phút");
        }

        // -------------------------------------------------------------------

        String sys5 = "${sys_current_second}";
        if (input.contains(sys5)) {
            input = input.replace(sys3, Calendar.getInstance().get(Calendar.SECOND) + "giây");
        }

        // -------------------------------------------------------------------

        String sys6 = "${sys_current_time}";
        if (input.contains(sys6)) {
            Calendar tempCalen = Calendar.getInstance();
            input = input.replace(sys6, tempCalen.get(Calendar.HOUR_OF_DAY) + " giờ " +
                    tempCalen.get(Calendar.MINUTE) + " phút");
        }

        // -------------------------------------------------------------------

        String sys7 = "${sys_current_weekday}";
        if (input.contains(sys7)) {
            // Calculate normalize
            String check_sys_crr_wd_have_cal = "\\x24\\{\\{\\=\\x24\\{sys_current_weekday\\}(\\+|\\-|\\*|\\/|\\%|\\:)\\d+\\}\\}";
            Calendar current_calendar = Calendar.getInstance();
            int CurrentWeekDay = current_calendar.get(Calendar.DAY_OF_WEEK);
            Matcher matcher = Pattern.compile(check_sys_crr_wd_have_cal, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                String condition_value = matcher
                        .group(0)
                        .replace(" ", "")
                        .replace("${{=", "")
                        .replace("}}", "")
                        .trim();
                condition_value = condition_value.replace("${sys_current_weekday}", "0");
                int amount = (int) TNLib.Using.CalFromString(condition_value);
                Log.w("CONDITIOIN", condition_value + " = " + amount);
                current_calendar.add(Calendar.DAY_OF_MONTH, amount);
                input = input.replaceFirst(check_sys_crr_wd_have_cal, TNLib.Using.WeekDays[current_calendar.get(Calendar.DAY_OF_WEEK) - 1]);
            }
            // Calculate with date
            String regex = "\\x24\\{\\{\\=.*.\\>\\>\\s{0,1}\\x24\\{sys_current_weekday\\}\\}\\}";
            Matcher matcher2 = Pattern.compile(regex).matcher(input);
            while (matcher2.find()) {
                String extracted[] = matcher2.group(0)
                        .trim()
                        .replace(" ", "")
                        .replace("${{=", "")
                        .replace("}}", "")
                        .trim().split(">>");
                extracted[0] = extracted[0].trim().toLowerCase()
                        .replaceAll("ngày|tháng|năm", " ")
                        .trim();
                String DMY[] = extracted[0].split(" ");
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, Integer.parseInt(DMY[2]));
                calendar.set(Calendar.MONTH, Integer.parseInt(DMY[1]) - 1);
                calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(DMY[0]));
                input = input.replaceFirst(regex, TNLib.Using.WeekDays[calendar.get(Calendar.DAY_OF_WEEK) - 1]);
            }
            input = input.replace(sys7, TNLib.Using.WeekDays[CurrentWeekDay - 1]);
        }

        // -------------------------------------------------------------------

        String sys8 = "${sys_current_date}";
        if (input.contains(sys8)) {
            // Calculate normalize
            String check_sys_crr_wd_have_cal = "\\x24\\{\\{\\=\\x24\\{sys_current_date\\}(\\+|\\-|\\*|\\/|\\%|\\:)\\d+\\}\\}";
            Calendar current_calendar = Calendar.getInstance();
            int CurrentDay = current_calendar.get(Calendar.DAY_OF_MONTH);
            int CurrentMonth = current_calendar.get(Calendar.MONTH);
            int  CurrentYear = current_calendar.get(Calendar.YEAR);
            Matcher matcher = Pattern.compile(check_sys_crr_wd_have_cal, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                String condition_value = matcher
                        .group(0)
                        .replace(" ", "")
                        .replace("${{=", "")
                        .replace("}}", "")
                        .trim();
                condition_value = condition_value.replace("${sys_current_date}", "0");
                int amount = (int) TNLib.Using.CalFromString(condition_value);
                Log.w("CONDITIOIN", condition_value + " = " + amount);
                current_calendar.add(Calendar.DAY_OF_MONTH, amount);
                input = input.replaceFirst(check_sys_crr_wd_have_cal, "ngày " + current_calendar.get(Calendar.DAY_OF_MONTH)
                        + " tháng " + (current_calendar.get(Calendar.MONTH) + 1)
                        + " năm " + current_calendar.get(Calendar.YEAR));
            }
            input = input.replace(sys8, "ngày " + CurrentDay
                    + " tháng " + CurrentMonth
                    + " năm " + CurrentYear);
        }

        // -------------------------------------------------------------------

        String sys9 = "${sys_current_day}";
        if (input.contains(sys9)) {
            // Calculate normalize
            String check_sys_crr_wd_have_cal = "\\x24\\{\\{\\=\\x24\\{sys_current_day\\}(\\+|\\-|\\*|\\/|\\%|\\:)\\d+\\}\\}";
            Calendar current_calendar = Calendar.getInstance();
            int CurrentDay = current_calendar.get(Calendar.DAY_OF_MONTH);
            int CurrentMonth = current_calendar.get(Calendar.MONTH);
            int  CurrentYear = current_calendar.get(Calendar.YEAR);
            Matcher matcher = Pattern.compile(check_sys_crr_wd_have_cal, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                String condition_value = matcher
                        .group(0)
                        .replace(" ", "")
                        .replace("${{=", "")
                        .replace("}}", "")
                        .trim();
                condition_value = condition_value.replace("${sys_current_day}", "0");
                int amount = (int) TNLib.Using.CalFromString(condition_value);
                Log.w("CONDITIOIN", condition_value + " = " + amount);
                current_calendar.add(Calendar.DAY_OF_MONTH, amount);
                input = input.replaceFirst(check_sys_crr_wd_have_cal, "ngày " + current_calendar.get(Calendar.DAY_OF_MONTH));
            }
            input = input.replace(sys9, "ngày " + CurrentDay);
        }

        // -------------------------------------------------------------------

        String sys10 = "${sys_current_month}";
        if (input.contains(sys10)) {
            // Calculate normalize
            String check_sys_crr_wd_have_cal = "\\x24\\{\\{\\=\\x24\\{sys_current_month\\}(\\+|\\-|\\*|\\/|\\%|\\:)\\d+\\}\\}";
            Calendar current_calendar = Calendar.getInstance();
            int CurrentDay = current_calendar.get(Calendar.DAY_OF_MONTH);
            int CurrentMonth = current_calendar.get(Calendar.MONTH);
            int  CurrentYear = current_calendar.get(Calendar.YEAR);
            Matcher matcher = Pattern.compile(check_sys_crr_wd_have_cal, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                String condition_value = matcher
                        .group(0)
                        .replace(" ", "")
                        .replace("${{=", "")
                        .replace("}}", "")
                        .trim();
                condition_value = condition_value.replace("${sys_current_month}", "0");
                int amount = (int) TNLib.Using.CalFromString(condition_value);
                Log.w("CONDITIOIN", condition_value + " = " + amount);
                current_calendar.add(Calendar.MONTH, amount);
                input = input.replaceFirst(check_sys_crr_wd_have_cal, "tháng " + (current_calendar.get(Calendar.MONTH) + 1));
            }
            input = input.replace(sys10, "tháng " + (CurrentMonth + 1));
        }

        // -------------------------------------------------------------------

        String sys11 = "${sys_current_year}";
        if (input.contains(sys11)) {
            // Calculate normalize
            String check_sys_crr_wd_have_cal = "\\x24\\{\\{\\=\\x24\\{sys_current_year\\}(\\+|\\-|\\*|\\/|\\%|\\:)\\d+\\}\\}";
            Calendar current_calendar = Calendar.getInstance();
            int CurrentDay = current_calendar.get(Calendar.DAY_OF_MONTH);
            int CurrentMonth = current_calendar.get(Calendar.MONTH);
            int  CurrentYear = current_calendar.get(Calendar.YEAR);
            Matcher matcher = Pattern.compile(check_sys_crr_wd_have_cal, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                String condition_value = matcher
                        .group(0)
                        .replace(" ", "")
                        .replace("${{=", "")
                        .replace("}}", "")
                        .trim();
                condition_value = condition_value.replace("${sys_current_year}", "0");
                int amount = (int) TNLib.Using.CalFromString(condition_value);
                Log.w("CONDITIOIN", condition_value + " = " + amount);
                current_calendar.add(Calendar.YEAR, amount);
                input = input.replaceFirst(check_sys_crr_wd_have_cal, "năm " + current_calendar.get(Calendar.YEAR));
            }
            input = input.replace(sys11, "năm " + CurrentYear);
        }

        // -------------------------------------------------------------------

        String sys12 = "\\x24\\{sys_current_month\\((\\d+){0,1}(\\+|\\-|\\*|\\/){0,1}\\d+\\)\\.NUMBER_OF_DAY\\}";
        if (input.matches("(.+){0,1}" + sys12 + "(.+){0,1}")) {
            // Calculate normalize
            Calendar current_calendar = Calendar.getInstance();
            input = input.replaceAll("\\x24\\{sys_current_month\\(0+\\)\\.NUMBER_OF_DAY\\}", current_calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + "");
            Matcher matcher = Pattern.compile(sys12, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                int amount = 0;
                Matcher matcher1 = Pattern.compile("\\d+(\\+|\\-|\\*|\\/)\\d+").matcher(matcher.group(0));
                if (matcher1.find()) {
                    amount = (int) TNLib.Using.CalFromString(matcher1.group(0));
                } else {
                    Matcher matcher2 = Pattern.compile("(\\+|\\-|\\*|\\/){0,1}\\d+").matcher(matcher.group(0));
                    if (matcher2.find())
                        amount = Integer.parseInt(matcher2.group(0));
                }
                current_calendar.add(Calendar.DAY_OF_MONTH, amount);
                input = input.replaceFirst(sys12, current_calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + "");
            }
        }

        // -------------------------------------------------------------------

        String sys13 = "\\x24\\{sys_current_month\\((\\d+){0,1}(\\+|\\-|\\*|\\/){0,1}\\d+\\)\\.NUMBER_OF_WEEK\\}";
        if (input.matches("(.+){0,1}" + sys13 + "(.+){0,1}")) {
            // Calculate normalize
            Calendar current_calendar = Calendar.getInstance();
            input = input.replaceAll("\\x24\\{sys_current_month\\(0+\\)\\.NUMBER_OF_WEEK\\}",
                    current_calendar.getActualMaximum(Calendar.DAY_OF_WEEK) + "");
            Matcher matcher = Pattern.compile(sys13, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                int amount = 0;
                Matcher matcher1 = Pattern.compile("\\d+(\\+|\\-|\\*|\\/)\\d+").matcher(matcher.group(0));
                if (matcher1.find()) {
                    amount = (int) TNLib.Using.CalFromString(matcher1.group(0));
                } else {
                    Matcher matcher2 = Pattern.compile("(\\+|\\-|\\*|\\/){0,1}\\d+").matcher(matcher.group(0));
                    if (matcher2.find())
                        amount = Integer.parseInt(matcher2.group(0));
                }
                current_calendar.add(Calendar.DAY_OF_MONTH, amount);
                input = input.replaceFirst(sys13, current_calendar.getActualMaximum(Calendar.DAY_OF_WEEK) + "");
            }
        }

        // -------------------------------------------------------------------

        String sys14 = "\\x24\\{sys_current_year\\((\\d+){0,1}(\\+|\\-|\\*|\\/){0,1}\\d+\\)\\.NUMBER_OF_DAY\\}";
        if (input.matches("(.+){0,1}" + sys14 + "(.+){0,1}")) {
            // Calculate normalize
            Calendar current_calendar = Calendar.getInstance();
            input = input.replaceAll("\\x24\\{sys_current_year\\(0+\\)\\.NUMBER_OF_DAY\\}",
                    current_calendar.getActualMaximum(Calendar.DAY_OF_YEAR) + "");
            Matcher matcher = Pattern.compile(sys14, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                int amount = 0;
                Matcher matcher1 = Pattern.compile("\\d+(\\+|\\-|\\*|\\/)\\d+").matcher(matcher.group(0));
                if (matcher1.find()) {
                    amount = (int) TNLib.Using.CalFromString(matcher1.group(0));
                } else {
                    Matcher matcher2 = Pattern.compile("(\\+|\\-|\\*|\\/){0,1}\\d+").matcher(matcher.group(0));
                    if (matcher2.find())
                        amount = Integer.parseInt(matcher2.group(0));
                }
                current_calendar.add(Calendar.DAY_OF_YEAR, amount);
                input = input.replaceFirst(sys14, current_calendar.getActualMaximum(Calendar.DAY_OF_YEAR) + "");
            }
        }

        // -------------------------------------------------------------------

        String sys15 = "\\x24\\{sys_current_year\\((\\d+){0,1}(\\+|\\-|\\*|\\/){0,1}\\d+\\)\\.NUMBER_OF_WEEK\\}";
        if (input.matches("(.+){0,1}" + sys15 + "(.+){0,1}")) {
            // Calculate normalize
            Calendar current_calendar = Calendar.getInstance();
            input = input.replaceAll("\\x24\\{sys_current_year\\(0+\\)\\.NUMBER_OF_WEEK\\}",
                    current_calendar.getActualMaximum(Calendar.WEEK_OF_YEAR) + "");
            Matcher matcher = Pattern.compile(sys15, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                int amount = 0;
                Matcher matcher1 = Pattern.compile("\\d+(\\+|\\-|\\*|\\/)\\d+").matcher(matcher.group(0));
                if (matcher1.find()) {
                    amount = (int) TNLib.Using.CalFromString(matcher1.group(0));
                } else {
                    Matcher matcher2 = Pattern.compile("(\\+|\\-|\\*|\\/){0,1}\\d+").matcher(matcher.group(0));
                    if (matcher2.find())
                        amount = Integer.parseInt(matcher2.group(0));
                }
                current_calendar.add(Calendar.DAY_OF_YEAR, amount);
                input = input.replaceFirst(sys15, current_calendar.getActualMaximum(Calendar.WEEK_OF_YEAR) + "");
            }
        }

        // -------------------------------------------------------------------

        String sys16 = "\\x24\\{sys_current_year\\((\\d+){0,1}(\\+|\\-|\\*|\\/){0,1}\\d+\\)\\.NAME\\}";
        if (input.matches("(.+){0,1}" + sys16 + "(.+){0,1}")) {
            // Calculate normalize
            Calendar current_calendar = Calendar.getInstance();
            input = input.replaceAll("\\x24\\{sys_current_year\\(0+\\)\\.NAME\\}",
                     TNLib.Using.NameOfYear.Get(current_calendar.get(Calendar.YEAR)));
            Matcher matcher = Pattern.compile(sys16, Pattern.MULTILINE).matcher(input);
            while (matcher.find()) {
                int amount = 0;
                Matcher matcher1 = Pattern.compile("\\d+(\\+|\\-|\\*|\\/)\\d+").matcher(matcher.group(0));
                if (matcher1.find()) {
                    amount = (int) TNLib.Using.CalFromString(matcher1.group(0));
                } else {
                    Matcher matcher2 = Pattern.compile("(\\+|\\-|\\*|\\/){0,1}\\d+").matcher(matcher.group(0));
                    if (matcher2.find())
                        amount = Integer.parseInt(matcher2.group(0));
                }
                current_calendar.add(Calendar.YEAR, amount);
                input = input.replaceFirst(sys16, TNLib.Using.NameOfYear.Get(current_calendar.get(Calendar.YEAR)));
            }
        }
        return input;
    }

    static String ReplaceByRealValue(String input, HashMap<String, String> PairValue) {
        for (Map.Entry<String, String> entry : PairValue.entrySet()) {
            input = input.replace(entry.getKey(), entry.getValue());
        }
        return GetSystemValue(input);
    }

    static HashMap<String, String> StandardVaiablePairs(HashMap<String, String> pairs) {
        for (Map.Entry<String, String> pair : pairs.entrySet()) {
            String standarded = StandardValue(pair.getKey(), pair.getValue());
            pairs.put(pair.getKey(), standarded);
            Log.w("PAIR STADARDED", pair.getKey() + " -> " + standarded);
        }
        return pairs;
    }

    private static String StandardValue(String variable, String value) {
        switch (LIST.indexOf(variable)) {
            case 0: { // ${number}
                String temp = value.toLowerCase()
                        .replace("không", "0")
                        .replace("một", "1")
                        .replace("hai", "2")
                        .replace("ba", "3")
                        .replace("bốn", "4")
                        .replace("năm", "5")
                        .replace("sáu", "6")
                        .replace("bảy", "7")
                        .replace("tám", "8")
                        .replace("chín", "0");
                Matcher matcher = Pattern.compile("\\d+").matcher(temp);
                while (matcher.find())
                    value = matcher.group(0);
                break;
            }
            case 1: { // ${date}
                Matcher matcher = Pattern.compile("\\d{1,2} tháng \\d{1,2} năm \\d{4}|\\d{1,2} tháng \\d{1,2}( \\d{4}){0,1}").matcher(value);
                while (matcher.find()) {
                    String temp = matcher.group(0).replaceAll(" tháng | năm | ", "-");
                    if (temp.matches("\\d{1,2}\\-\\d{1,2}") && !temp.matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
                        temp += "-" + Calendar.getInstance().get(Calendar.YEAR);
                    }
                    if (temp.matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
                        String[] el = temp.split("-");
                        value = "ngày " + el[0] + " tháng " + el[1] + " năm " + el[2];
                    }
                }
                break;
            }
            case 2: { // ${day}
                Matcher matcher = Pattern.compile("ngày \\d{1,2}").matcher(value);
                while (matcher.find())
                    value = matcher.group(0);
                break;
            }
            case 3: { // ${month}
                Matcher matcher = Pattern.compile("tháng \\d{1,2}").matcher(value);
                while (matcher.find())
                    value = matcher.group(0);
                break;
            }
            case 4: { // ${year}
                Matcher matcher = Pattern.compile("năm \\d{1,2}").matcher(value);
                while ((matcher.find()))
                    value = matcher.group(0);
                break;
            }
            case 5: { // ${weekdays}
                Matcher matcher = Pattern.compile("([Tt]hứ ([Hh]ai\\b|[Bb]a\\b|[Tt]ư\\b|[Nn]ăm\\b|[Ss]áu\\b|[Bb]ảy\\b))|[Cc]hủ [Nn]hật")
                        .matcher(value);
                while (matcher.find())
                    value = matcher.group(0);
                break;
            }
            case 6: { // ${time}
                Matcher matcher = Pattern.compile("\\d{1,2} giờ \\d{1,2}|\\d{1,2}\\:\\d{1,2}")
                        .matcher(value);
                while (matcher.find())
                    value = matcher.group(0);
                break;
            }
        }
        return value;
    }
}
