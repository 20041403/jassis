package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectFeature;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectOverview;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing.GetResource;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.ObjectInfomationDialog;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.TNLib;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindObjectOverview extends AsyncTask<String, Integer, ObjectOverview> {
    long currentTime = 99999;
    String success = "";
    String notfound = "";
    public final static String TAG = FindObjectOverview.class.getSimpleName();

    public ObjectOverview FindObjectOverview(String standardObjectInfo) {
        ObjectOverview objectOverview = new ObjectOverview();
        try {
            String ur = "https://vi.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=" + standardObjectInfo;
            URL url = new URL(ur);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // set method
            conn.setRequestMethod("GET");
            // set request header
            conn.setRequestProperty("User-Agent", "Mozilla/5.0");
            conn.setRequestProperty("Content-type", "text/html");

            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                Matcher matcher = Pattern.compile("(\"title\":\"(.*?)\")|(\"extract\":\"(.*?)\"\\})", Pattern.MULTILINE)
                        .matcher(response.toString());
                while (matcher.find()) {
                    if (matcher.group(0).matches(("\"title\":\"(.*?)\""))) {
                        String get = matcher.group(0)
                                .replace("\\\"", "")
                                .replace("\n", "")
                                .replaceAll("(\"title\":\"|\")", "");
                        if (!get.isEmpty()) {
                            objectOverview.setName(TNLib.Using.DecodeUnicode(get));
                        }
                        Log.e(TAG, "FindObjectOverview-Title: " + objectOverview.getName());

                    } else {
                        String get = matcher.group(0)
                                .replace("\\\"", "\"")
                                .replace("\\n", "\n")
                                .replaceAll("(\"extract\":\"|\")", "");
                        if (!get.isEmpty()) {
                            String content = TNLib.Using.DecodeUnicode(get);
                            objectOverview.setContent(content);
                        }
                        Log.e(TAG, "FindObjectOverview-Content: " + objectOverview.getContent());
                    }

                }
                // Kiểm tra, nếu có tiêu đề thì tiến hành lấy ảnh, nội dung không quan trọng
                if (!objectOverview.getName().isEmpty()) {
                    ArrayList<ObjectFeature> objectFeatures = GetResource.GetObjectImages(objectOverview.getName(), 5);
                    if (objectFeatures.size() > 0)
                        objectOverview.setImages(objectFeatures);
                }

                Log.w(TAG, "FindObjectOverview: Done!");
            } else {
                Log.w(TAG, "FindObjectOverview: Trả về " + responseCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objectOverview;
    }

    @Override
    protected ObjectOverview doInBackground(String... strings) {
        if (strings.length > 1)
            success = strings[1];
        if (strings.length > 2)
            notfound = strings[2];
        String standardObjectTitle = GetResource.GetObjectTitle(strings[0]);
        Log.w(TAG, "doInBackground: GET OBJECT NAME IS " + standardObjectTitle);
        return FindObjectOverview(standardObjectTitle);
    }

    @Override
    protected void onPostExecute(ObjectOverview objectOverview) {
        if (objectOverview != null) {
            if (objectOverview.getName().isEmpty()) {
                String[] noObj = {"Không tìm thấy thông tin.", "Xin lỗi, tôi không thể tìm thấy thông tin!", "Đối tượng bạn yêu cầu hiện không có"};
                BODY.MOUNTH.TALK(noObj[new Random().nextInt(noObj.length)]);
            } else if (objectOverview.getContent().isEmpty()) {
                if (objectOverview.getImages().size() > 0) {
                    String[] noContentButHaveImages = {"Đối tượng này hiện chưa được cung cấp thông tin cụ thể, nhưng hiện có một số hình ảnh.",
                            "Hiện chưa có nội dung cụ thể, nhưng tôi có một số hình ảnh cho bạn.", "Dưới đây là một số hình ảnh về vấn đề mà bạn yêu cầu"};
                    BODY.MOUNTH.TALK(noContentButHaveImages[new Random().nextInt(noContentButHaveImages.length)]);
                    // Hiển thị tiêu đề và ảnh cho người dùng xem
                    ObjectInfomationDialog.Show(BODY.mContext, objectOverview, BODY.mContext.rlRootLayout);
                } else {
                    String onlyTitle[] = {"Rất tiếc, đối tượng này chưa có thông tin cụ thể", "Thông tin về đối tượng này chưa được cập nhật!", "Hiện tại đối tượng này vần chưa có thông tin rõ ràng"};
                    BODY.MOUNTH.TALK(onlyTitle[new Random().nextInt(onlyTitle.length)]);
                }
            } else {
                // Hiển thị tiêu đề và ảnh cho người dùng xem, có cả nội dung
                ObjectInfomationDialog.Show(BODY.mContext, objectOverview, BODY.mContext.rlRootLayout);
                // Thực hiện lấy câu đầu tiên trong nội dung
                Matcher matcher1 = Pattern.compile("^(.*?)[.?!]\\s|[\\n]", Pattern.MULTILINE).matcher(objectOverview.getContent());
                BODY.ShowAITalk("Đã hiển thị về " + objectOverview.getName());
                if (matcher1.find()) {
                    String first = matcher1.group(0);
                    String[] content = {first.replaceAll("\\((.*?)\\)",""), "Hãy xem thông tin về '" + objectOverview.getName() + "' trên màn hình"};
                    BODY.MOUNTH.ONLY_TALK(content[new Random().nextInt(content.length)]);
                } else {
                    String[] introduction = {"Bạn có thể xem chi tiết hình ảnh và nội dung bên dưới", "Đây là hình ảnh và tóm tắt thông tin về đố tượng bạn yêu cầu",
                            "Hãy xem thông tin chi tiết trên màn hình"};
                    BODY.MOUNTH.ONLY_TALK(introduction[new Random().nextInt(introduction.length)]);
                }
            }
            Timer waitTalkFinish = new Timer();
            waitTalkFinish.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (BODY.MOUNTH.isTalking())
                    {
                        currentTime = System.currentTimeMillis();
                        return;
                    }
                    else
                    {
                        if (System.currentTimeMillis() - currentTime > 3000)
                        {
                            waitTalkFinish.cancel();
                            waitTalkFinish.purge();
                            Log.w(TAG, "run: Dường như đã nói xong về đối tượng, đổi kênh sang kênh tương tác thông thường");
                            JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.INTERFACE;
                        }
                    }
                }
            }, 500, 3000);
        }
        super.onPostExecute(objectOverview);
    }
}
