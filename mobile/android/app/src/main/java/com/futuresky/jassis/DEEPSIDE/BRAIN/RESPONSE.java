package com.futuresky.jassis.DEEPSIDE.BRAIN;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;
import com.futuresky.jassis.INTERFACE.BODY;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

public class RESPONSE {
    public final static String TAG = RESPONSE.class.getSimpleName();
    public static String reply = "";
    public static String CURRENT_USER_INPUT_SENTENCE = "";

    private static void PROCESSING(ActivityHome mContext, String JSONString) {
        if (!JSONString.isEmpty() && JSONString.length() > 25) {
            // Receive and extract Response
            RESPONSE mCurrentResponse = new RESPONSE(JSONString);
            // Extract value of per varriable
            HashMap<String, String> Pairs = GetPairVariable(CURRENT_USER_INPUT_SENTENCE, mCurrentResponse.phrase.matched);
            // Standard value for per variable
            Pairs = VARIABLES.StandardVaiablePairs(Pairs);
            Log.w(TAG, "PAIR STADARDED: " + Pairs.toString());

            // Check for response
            Matcher matcher = Pattern.compile(STANDARD_REGEX.CHECK_IS_CONDITION_CLAUSE).matcher(mCurrentResponse.response.matched);
            if (matcher.find()) {
                // Nếu đây là mệnh đề điều kiện
                String condition_value = matcher.group(0).trim().replace("${{?", "").replace("}}", "").trim();
                if (condition_value.matches("^\\x24\\{.*.\\}$")) {
                    // Nếu giá trị điều kiện là một biến, thực hiện phân giải biến đó để lấy giá trị thực
                    condition_value = VARIABLES.ReplaceByRealValue(condition_value, Pairs).trim();
                }
                String[] clauses = mCurrentResponse.response.matched.replaceAll(STANDARD_REGEX.CHECK_IS_CONDITION_CLAUSE, "").trim().split("\\|\\|");
                for (String clause : clauses) {
                    String[] extract = clause.trim().split(">>");
                    if (extract.length != 2) {
                        Log.i(TAG, "Lỗi trong quá trình phần giải mệnh đề này! Nên sẽ được bỏ qua");
                        continue;
                    }
                    if (extract[0].trim().matches("\\~\\>.*.\\<\\~")) {
                        if (condition_value.matches(extract[0].trim().replace("~>", "").replace("<~", ""))) {
                            reply = extract[1];
                            break;
                        }
                    } else if (condition_value.equals(extract[0].trim()) || extract[0].trim().equals("$")) {
                        reply = extract[1];
                    }
                }
            } else {
                // Không phải biểu thức điều kiện, trả lời theo cách thông thường
                reply = mCurrentResponse.response.matched;
            }
            // Thay thế các giá trị biến bằng giá trị thực
            reply = VARIABLES.ReplaceByRealValue(reply, Pairs).trim();
            if (!mCurrentResponse.phrase.event_key.isEmpty() && EVENT_KEY.PROCESSING(CURRENT_USER_INPUT_SENTENCE, mCurrentResponse, reply))
                return;
            // Thực hiện trả lời bằng giọng nói
            mContext.runOnUiThread(() -> {
//                BODY.MOUNTH.TalkAndShow(reply);
                BODY.MOUNTH.TALK(reply);
                //BODY.Mouth.Talk(reply);
            });
        } else {
            BODY.MOUNTH.TALK("Xin lỗi! Không thể phản hồi ngay được.");
//            BODY.Mouth.Talk("Xin lỗi! Không thể phản hồi ngay được.");
        }
    }

    //region Backend processing
    static HashMap<String, String> GetPairVariable(String content, String matched_phrase) {
        HashMap<String, String> pairs = new HashMap<String, String>();
        // Thêm tất cả các biến được sử dụng trong câu trùng khớp vào làm khóa của hashmap
        Matcher matcherVariable = Pattern.compile(STANDARD_REGEX.CHECK_IS_VARIABLE_TYPE).matcher(matched_phrase);
        while (matcherVariable.find()) {
            pairs.put(matcherVariable.group(0), "");
        }
        // Làm cho câu gần nhất với chuẩn
        Matcher matcher = Pattern.compile(
                matched_phrase.replaceAll(STANDARD_REGEX.CHECK_IS_VARIABLE_TYPE, ".*."),
                Pattern.MULTILINE)
                .matcher(content);
        ArrayList<String> variableValues = new ArrayList<>();
        if (matcher.find()) {
            for (String variableValue : matcher.group(0)
                    .trim()
                    .replaceAll(("(" + matched_phrase.replaceAll(STANDARD_REGEX.CHECK_IS_VARIABLE_TYPE, ")|(") + ")")
                            .replace("|()", "")
                            .replace("()|", ""), "||||")
                    .split("\\|\\|\\|\\|")) {
                if (!variableValue.isEmpty())
                    variableValues.add(variableValue);
            }
        }
        // Nếu số lượng biến và số lượng giá trị là như nhau, ta tiến hành so khớp và thêm vào tập giá trị
        if (pairs.size() == variableValues.size()) {
            int i = 0;
            for (Map.Entry<String, String> pair : pairs.entrySet()) {
                pairs.put(pair.getKey(), variableValues.get(i++));
            }
            return pairs;
        }
        return new HashMap<>();
    }

    class _phrase {
        public int id;
        public String matched;
        public String event_key;
        public String event_json;
        public String different_reates;
    }

    class _response {
        public int id;
        public String matched;
    }

    public _phrase phrase;
    public _response response;

    public RESPONSE() {
        phrase = new _phrase();
        response = new _response();
    }

    public RESPONSE(String json) {
        phrase = new _phrase();
        response = new _response();
        try {
            JSONObject responseJSON = new JSONObject(json);
            // --- Phrase
            JSONObject phrase = responseJSON.getJSONObject("phrase");
            this.phrase.id = phrase.getInt("id");
            this.phrase.matched = phrase.getString("matched");
            this.phrase.event_key = phrase.getString("event_key");
            this.phrase.event_json = phrase.getString("event_json");
            this.phrase.different_reates = phrase.getString("different_rates");
            // -- Response
            JSONObject response = responseJSON.getJSONObject("response");
            this.response.id = response.getInt("id");
            this.response.matched = response.getString("matched");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static class WORK extends AsyncTask<String, Integer, Void> {
        @SuppressLint("StaticFieldLeak")
        private ActivityHome mContext;

        public WORK(ActivityHome mContext) {
            this.mContext = mContext;
        }

        @Override
        protected Void doInBackground(String... strings) {
            HttpURLConnection client = null;
            String response = "";
            try {
                // Create json parameter
                JSONObject JSONbody = new JSONObject();
                JSONbody.put("user_id", NETWORK_CONFIG.CLIENT.USER_ID);
                JSONbody.put("sentence", strings[0]);
//                JSONbody.put("token", NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN);

                URL url = new URL(NETWORK_CONFIG.GET_RESPONSE_API);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("POST");
                client.usingProxy();
                client.setConnectTimeout(1000);
                client.setRequestProperty("User-Agent", "Mozilla/5.0");
                client.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                client.setRequestProperty("Accept", "application/json");
                client.setDoInput(true);
                client.setDoOutput(true);

                OutputStream os = client.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, StandardCharsets.UTF_8));
                writer.write(JSONbody.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = client.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                } else {
                    response = "";
                }
            } catch (Exception e) {
                Log.e(TAG, "ERROR: " + e.getMessage());
                e.printStackTrace();
            } finally {
                if (client != null) // Make sure the connection is not null.
                    client.disconnect();
            }
            CURRENT_USER_INPUT_SENTENCE = strings[0];
            Log.w(TAG, "RESPONSE:\n" + response);
            String JSONstring = response;
            PROCESSING(mContext, JSONstring);
            return null;
        }
    }
    //endregion
}