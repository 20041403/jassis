package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Home;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.speech.tts.TextToSpeech;

import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;

import java.util.Locale;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.JassisDeepListenServices.mAudioManager;

public class SpeechRespone {
    private TextToSpeech tts;
    ActivityHome mContext;

    public SpeechRespone(ActivityHome mContext) {
        this.mContext = mContext;
        TextToSpeech.OnInitListener textToSpeechListenser = new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR)
                    tts.setLanguage(new Locale("vi_VN"));
            }
        };
        if (tts == null)
            tts = new TextToSpeech(mContext, textToSpeechListenser);
    }

    public void Talk(String content) {
        if (mAudioManager == null)
            mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, AudioManager.FLAG_PLAY_SOUND);
        }

//        Toasty.success(mContext, content, Toast.LENGTH_SHORT, false).show();

        tts.setPitch(JAssistant.PITCH);
        tts.setSpeechRate(JAssistant.SPEECH_RATE);
        tts.speak(content, TextToSpeech.QUEUE_FLUSH, null, "");
    }

    public void ShutUp() {
        if (isTalking())
        {
            tts.stop();
        }
    }

    public void Destroy() {
        ShutUp();
        tts.shutdown();
    }

    boolean isTalking() {
        if (tts != null)
            return tts.isSpeaking();
        return false;
    }

}
