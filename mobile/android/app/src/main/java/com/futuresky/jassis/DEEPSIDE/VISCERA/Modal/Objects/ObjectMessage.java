package com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects;

public class ObjectMessage {
    int msg_id = -1;
    String msg = "";
    int sender_id = -1;
    String send_at = "";
    String received_at = "";

    public ObjectMessage() {

    }

    public ObjectMessage(int msg_id, String msg, int sender_id, String send_at, String received_at) {
        this.msg_id = msg_id;
        this.msg = msg;
        this.sender_id = sender_id;
        this.send_at = send_at;
        this.received_at = received_at;
    }

    public int getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(int msg_id) {
        this.msg_id = msg_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }

    public String getSend_at() {
        return send_at;
    }

    public void setSend_at(String send_at) {
        this.send_at = send_at;
    }

    public String getReceived_at() {
        return received_at;
    }

    public void setReceived_at(String received_at) {
        this.received_at = received_at;
    }
}
