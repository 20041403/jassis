package com.futuresky.jassis.DEEPSIDE.VISCERA.View.AccountLogRes;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.ObjectOverview;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;
import com.meetic.marypopup.MaryPopup;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import es.dmoral.toasty.Toasty;

public class WelcomeNoneLogin {
    public static boolean isShow = false;
    public static void Show(final Activity mContext, View from) {
        // Infilate layout
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.module_layout_welcome_none_login, null);
        // Create Dialog
        final MaryPopup popup = MaryPopup.with(mContext)
                .content(v)
                .from(from)
                .blackOverlayColor(0)
                .backgroundColor(0)
                .center(true)
                .draggable(true)
                .scaleDownDragging(true)
                .closeDuration(500)
                .cancellable(false);
        // 1. Find and init view
        v.findViewById(R.id.module_welcome_none_login_login_btn).setOnClickListener(vx->{
            new ModulLoginDialog().Show(BODY.mContext, BODY.mContext.rlRootLayout);
            popup.close(true);
        });
        v.findViewById(R.id.module_welcome_none_login_register_btn).setOnClickListener(vx->{
            Toasty.info(mContext, "Xin lỗi! Hiện tại đang trong thời gian thử nghiệm nên chưa mở chức năng đăng ký!",
                    Toast.LENGTH_LONG, true).show();
        });
        ///////////////////////////////////
        popup.show();
        isShow = true;
    }
}
