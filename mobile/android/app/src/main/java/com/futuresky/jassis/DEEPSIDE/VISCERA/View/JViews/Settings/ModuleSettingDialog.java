package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.Settings;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;
import com.meetic.marypopup.MaryPopup;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

public class ModuleSettingDialog {
    public static boolean isShow = false;
    public static LinearLayout ivCloseButton;

    public static void Show(final Activity mContext, View from) {
        // Infilate layout
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.module_layout_setting, null);
        // Create Dialog
        final MaryPopup popup = MaryPopup.with(mContext)
                .content(v)
                .from(from)
                .blackOverlayColor(0)
                .backgroundColor(0)
                .center(true)
                .draggable(false)
                .scaleDownDragging(false)
                .closeDuration(500)
                .cancellable(false);
        // 1. Find and init view
        // User avater
        ImageView userAvatar = v.findViewById(R.id.module_layout_setting_user_avatar);
        userAvatar.setImageBitmap(NETWORK_CONFIG.CLIENT.USER_AVATAR);
        // User background
        ImageView userBackground = v.findViewById(R.id.module_layout_setting_user_background_image);
        if (!NETWORK_CONFIG.CLIENT.USER_BACKGROUND.isEmpty()) {
            Glide.with(BODY.mContext)
                    .load(Uri.parse(NETWORK_CONFIG.CLIENT.USER_BACKGROUND))
                    .placeholder(BODY.mContext.getResources().getDrawable(R.drawable.empty, null))
                    .into(userBackground);
        } else {
            userBackground.setImageBitmap(NETWORK_CONFIG.CLIENT.USER_AVATAR);
        }

        // User Displayname
        TextView userDisplayName = v.findViewById(R.id.module_layout_setting_displayname);
        userDisplayName.setText(NETWORK_CONFIG.CLIENT.USER_DISPLAY_NAME);

        // Close button
        v.findViewById(R.id.module_layout_setting_close_btn).setOnClickListener(xv -> {
            popup.close(true);
        });
        v.findViewById(R.id.module_layout_setting_close_bg_btn).setOnClickListener(cx -> {
            v.findViewById(R.id.module_layout_setting_close_btn).performClick();
        });
        // Logout button
        v.findViewById(R.id.module_layout_setting_logout_btn).setOnClickListener(cx -> {
            SweetAlertDialog logout = new SweetAlertDialog(mContext)
                    .setContentText("Thực sự muốn đăng xuất?")
                    .setCancelButton("Hủy", null)
                    .setConfirmButton("Đồng ý", xc -> {
                        NETWORK_CONFIG.CLIENT.ClearUserInfo(mContext);
                        popup.close(true);
                        xc.cancel();
                        Toasty.success(mContext, "Đã đăng xuất khỏi hệ thống.", Toast.LENGTH_SHORT, true).show();
                    });
            logout.setTitle("ĐĂNG XUẤT");
            logout.setCancelable(false);
            logout.create();
            logout.show();
        });

        //
        popup.show();
        isShow = true;
    }

}
