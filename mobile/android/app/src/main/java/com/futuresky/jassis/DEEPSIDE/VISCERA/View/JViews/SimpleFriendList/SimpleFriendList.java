package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews.SimpleFriendList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.futuresky.jassis.DEEPSIDE.BRAIN.NETWORK_CONFIG;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects.SimpleFriendInfo;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.AccountLogRes.ModulLoginDialog;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.R;
import com.meetic.marypopup.MaryPopup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.EchoWebsockerListener.ws;

public class SimpleFriendList {
    public final static String TAG = SimpleFriendInfo.class.getSimpleName();
    public static boolean isShow = false;
    public static List<SimpleFriendInfo> data;
    public static SimpleFriendListAdapter simpleFriendListAdapter;


    public static void getAllSimpleFriends() {
        new GetListFriendsAsync().execute();
    }

    public static void Show(final Activity mContext, View from) {
        if (NETWORK_CONFIG.CLIENT.USER_ID <= 0 || NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN.isEmpty()) {
            Toasty.error(mContext, "Hiện chưa đăng nhập, vui lòng đăng nhập hệ thống!", Toast.LENGTH_SHORT, true).show();
            new ModulLoginDialog().Show(BODY.mContext, BODY.mContext.rlRootLayout);
            return;
        }
        // Infilate layout
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.module_layout_friend_list, null);
        // Create Dialog
        final MaryPopup popup = MaryPopup.with(mContext)
                .content(v)
                .from(from)
                .blackOverlayColor(0)
                .backgroundColor(0)
                .center(false)
                .draggable(false)
                .scaleDownDragging(false)
                .closeDuration(300)
                .cancellable(false);
        // 1. Find and init view
        // A. Close button
        ImageView closeBtn = v.findViewById(R.id.module_close_friend_list);
        closeBtn.setOnClickListener(v1 -> {
            popup.close(true);
            isShow = false;
        });
        //
        v.findViewById(R.id.module_close_friend_list_bg)
                .setOnClickListener(sf -> {
                    closeBtn.performClick();
                });
        // B. RecycleView
        RecyclerView simpleFriendList = v.findViewById(R.id.module_simple_list_friend);
        // Init data
        if (data == null)
            data = new ArrayList<>();

        new GetListFriendsAsync().execute();

        if (simpleFriendListAdapter == null)
            simpleFriendListAdapter = new SimpleFriendListAdapter(data);

//        assert simpleFriendList != null;
//        simpleFriendList.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
//            @Override
//            public void onChildViewAttachedToWindow(@NonNull View view) {
//                if (data.size() > 0)
//                    v.findViewById(R.id.module_simple_list_friend_no_one_online).setVisibility(View.INVISIBLE);
//                else
//                    v.findViewById(R.id.module_simple_list_friend_no_one_online).setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onChildViewDetachedFromWindow(@NonNull View view) {
//                if (data.size() > 0)
//                    v.findViewById(R.id.module_simple_list_friend_no_one_online).setVisibility(View.INVISIBLE);
//                else
//                    v.findViewById(R.id.module_simple_list_friend_no_one_online).setVisibility(View.VISIBLE);
//            }
//        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        if (simpleFriendList != null) {
            simpleFriendList.setLayoutManager(layoutManager);
            simpleFriendList.setAdapter(simpleFriendListAdapter);
        }
        /////////////////////////////////////
        popup.show();
        isShow = true;
    }

    @SuppressLint("StaticFieldLeak")
    public static class GetListFriendsAsync extends AsyncTask<Void, Integer, Boolean> {
        public GetListFriendsAsync() {

        }

        SweetAlertDialog loadingDialog;
        GetListFriendsAsync getListFriendsAsync;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            getListFriendsAsync = this;
            loadingDialog = new SweetAlertDialog(BODY.mContext)
                    .setTitleText("TẢI BẠN BÈ")
                    .showCancelButton(false);
            loadingDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
            loadingDialog.setCancelable(false);
            loadingDialog.create();
            loadingDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (loadingDialog != null && loadingDialog.isShowing()) {
                loadingDialog.cancel();
            }
            if (aBoolean) {
                // Sau khi tải danh sách bạn bè thành công, tiến hành yêu cầu xem ai đang trực tuyến
                try {
                    JSONObject getOnlineFriendRequestObj = new JSONObject();
                    getOnlineFriendRequestObj.put("action", "GETSIMPLEFRIENDLIST");
                    getOnlineFriendRequestObj.put("token", NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN);
                    Log.w(TAG, "GETSIMPLEFRIENDLIST: " + NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN);
                    ws.send(getOnlineFriendRequestObj.toString());
                } catch (Exception e) {
                    Toasty.error(BODY.mContext, "Không kiểm tra trạng thái thành viên được!", Toast.LENGTH_SHORT, true).show();
                }
            } else {
                Toasty.error(BODY.mContext, "Tải không thành công!", Toast.LENGTH_SHORT, true).show();
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            HttpURLConnection client = null;
            StringBuilder response = new StringBuilder();
            try {
                // Create json parameter
                JSONObject JSONbody = new JSONObject();
                JSONbody.put("usr_id", NETWORK_CONFIG.CLIENT.USER_ID);

                URL url = new URL(NETWORK_CONFIG.GET_ALL_FRIENDS_FROM_USER_API);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("POST");
                client.setConnectTimeout(1000);
                client.setRequestProperty("User-Agent", "Mozilla/5.0");
                client.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                client.setRequestProperty("Accept", "application/json");
                client.setRequestProperty("Authorization", NETWORK_CONFIG.CLIENT.USER_CURRENT_TOKEN);
                client.setDoInput(true);
                client.setDoOutput(true);

                OutputStream os = client.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, StandardCharsets.UTF_8));
                writer.write(JSONbody.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = client.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response.append(line);
                    }
                } else {
                    response = new StringBuilder();
                }
            } catch (Exception e) {
                Log.e(TAG, "ERROR: " + e.getMessage());
                e.printStackTrace();
                return false;
            } finally {
                if (client != null) // Make sure the connection is not null.
                    client.disconnect();
            }
            Log.w(TAG, "RESPONSE:\n" + response);
            String JSONstring = response.toString().trim();
            if (JSONstring.equals("-1"))
                return false;
            else {
                try {
                    JSONArray arr = new JSONArray(JSONstring);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject object = arr.getJSONObject(i);
                        SimpleFriendInfo simpleFriendInfo = new SimpleFriendInfo(object.getInt("id"),
                                object.getString("display_name"),
                                "",
                                object.getString("avatar_src"), false);
                        if (data != null && data.size() > 0) {
                            boolean flagIsHave = false;
                            for (SimpleFriendInfo simpleFriendInfo1 : data)
                                if (simpleFriendInfo.getUserId() == simpleFriendInfo1.getUserId()) {
                                    flagIsHave = true;
                                    break;
                                }
                            if (!flagIsHave)
                                data.add(simpleFriendInfo);
                        } else {
                            data.add(simpleFriendInfo);
                        }
                        BODY.mContext.runOnUiThread(() -> {
                            simpleFriendListAdapter.notifyDataSetChanged();
                        });
                    }
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
    }
}
