package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Module.Processing;

import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.BaseLibraries.DestroyUnmeanWords;
import com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.BaseLibraries.StandardSubject;
import com.futuresky.jassis.TNLib;

public class ObjectInfomation {
    public static boolean CheckAsk(String cnt) {
        String Object = "";
        if (cnt.split(" ").length == 1)
            Object = cnt;
        else {
            Object = GetObject(cnt);
        }
        if (Object.isEmpty())
            Object = cnt;
        Object = GetSubject(cnt);
        Object = TNLib.Using.UpperPerWord(Object);
        // --- Get content here

        return false;
    }

    private static String GetSubject(String cnt) {
        String msgX[] = cnt.split(" ");
        if (msgX.length < 2)
            return cnt;
        else if (msgX[0].equals("con") || msgX[0].equals("loài")
                || msgX[0].equals("cái"))
            return cnt.replace(msgX[0], "").trim();
        else
            return cnt;
    }

    public static String GetObject(String cnt) {
        String msg = StandardSubject.DoIt(cnt);
        if (msg.contains("là")) {
            String tmp = msg.replace("sao", "gì")
                    .replace("thế nào", "gì");
            String[] kop = tmp.split("là");
            return kop[0].trim();
        }
        if (msg.contains("thông tin") && msg.contains("về") &&
                msg.indexOf("thông tin") < msg.indexOf("về")) {
            msg = msg.substring(msg.indexOf(" về") + " về".length()).trim();
            if (msg.contains("là"))
                msg = msg.substring(0, msg.indexOf("là"));
            msg = DestroyUnmeanWords.DoIt(msg).trim();
            return msg;
        }
        if (msg.contains("cho tôi") && msg.contains("biết về") &&
                msg.indexOf("cho tôi") < msg.indexOf("biết về")) {
            msg = msg.substring(msg.indexOf("biết về") + "biết về".length()).trim();
            if (msg.contains("là"))
                msg = msg.substring(0, msg.indexOf("là"));
            msg = DestroyUnmeanWords.DoIt(msg).trim();
            return msg;
        }
        if ((msg.contains("giúp tôi") || msg.contains("giúp cho tôi")) && msg.contains("biết") &&
                ((msg.indexOf("cho tôi") < msg.indexOf("biết") || (msg.indexOf("giúp cho tôi") > msg.indexOf("biết"))))) {
            msg = msg.replace("giúp cho tôi", "giúp tôi")
                    .replace("biết về", "biết");
            msg = msg.substring(msg.indexOf("biết") + "biết".length()).trim();
            if (msg.contains("là"))
                msg = msg.substring(0, msg.indexOf("là"));
            msg = DestroyUnmeanWords.DoIt(msg).trim();
            return msg;
        }
        return "";
    }
}
