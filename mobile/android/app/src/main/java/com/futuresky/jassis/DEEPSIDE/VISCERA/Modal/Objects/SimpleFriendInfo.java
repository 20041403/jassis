package com.futuresky.jassis.DEEPSIDE.VISCERA.Modal.Objects;

import java.util.ArrayList;
import java.util.List;

public class SimpleFriendInfo {
    int userId = -1;
    String displayName = "";
    String socketId = "";
    String avatarLink = "";
    boolean status = false;
    List<ObjectMessage> unread = new ArrayList<>();

    public void addUnreadMessage(ObjectMessage objectMessage) {
        unread.add(objectMessage);
    }

    public List<ObjectMessage> getUnread() {
        return unread;
    }

    public void setUnread(List<ObjectMessage> unread) {
        this.unread = unread;
    }

    public void UpdateStatus(String socketId) {
        this.socketId = socketId;
        status = !socketId.isEmpty();
    }

    public static List<SimpleFriendInfo> getDemos() {
        int i = 0;
        List<SimpleFriendInfo> demos = new ArrayList<>();
        demos.add(new SimpleFriendInfo(i++, "Trâm Anh", "xxx", "http://xemanhdep.com/wp-content/uploads/2016/04/anh-girl-xinh-9x-de-thuong.jpg", true));
        demos.add(new SimpleFriendInfo(i++, "Đóa Nhi", "xxx", "https://kienthucphongthuy.vn/wp-content/uploads/2018/10/anh-girl-de-thuong.jpg", true));
        demos.add(new SimpleFriendInfo(i++, "Phi Huyền Trang", "xxx", "http://taihinhanhdep.xyz/wp-content/uploads/2018/02/Hinh-anh-gai-dep-tu-nhien-dang-nguong-mo-nam-2018.jpg", true));
        demos.add(new SimpleFriendInfo(i++, "Cô cô", "xxx", "https://images.headlines.pw/topnews-2017/imgs/b7/fd/b7fd412f02efca815cf6fdccc9f6dff9d99a6c36_640_427.jpg", true));
        demos.add(new SimpleFriendInfo(i++, "Tiểu Long nữ", "xxx", "https://media.cungcau.vn/files/buithuy/2019/04/11/21-0016.jpg", true));
        demos.add(new SimpleFriendInfo(i++, "Chi Pú", "xxx", "https://photo2.tinhte.vn/data/attachment-files/2018/04/4296430_GirlXinh-Part8_ThienITCollection_6-min.png", true));
        return demos;
    }

    public SimpleFriendInfo(int userId, String displayName, String socketId, String avatarLink, boolean status) {
        this.userId = userId;
        this.displayName = displayName;
        this.socketId = socketId;
        this.avatarLink = avatarLink;
        this.status = status;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

    public SimpleFriendInfo() {}

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getSocketId() {
        return socketId;
    }

    public void setSocketId(String socketId) {
        this.socketId = socketId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
