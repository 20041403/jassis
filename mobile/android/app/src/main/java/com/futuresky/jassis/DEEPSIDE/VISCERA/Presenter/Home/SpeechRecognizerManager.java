package com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Home;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.futuresky.jassis.DEEPSIDE.BRAIN.RESPONSE;
import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;
import com.futuresky.jassis.INTERFACE.BODY;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.JassisDeepListenServices.mServicesSpeechRecognizer;

public class SpeechRecognizerManager {
    public final static String TAG = SpeechRecognizer.class.getSimpleName();
    public static SpeechRecognizer mInterfaceSpeechRecognizer;
    protected Intent mInterfaceSpeechRecognizerIntent;
    private ActivityHome mContext;

    int mCount = 0;
    protected String language = "vi";

    Timer jTimer;
    private boolean isReadyForStartDeepListen = false;

    public void callTimer() {
//        if (jTimer != null || mIsListening || BODY.MOUNTH.isTalking() || BODY.Mouth.isTalking() || JAssistant.CURRENT_CHANNEL != JAssistant.CHANNEL.INTERFACE) {
//            Log.w(TAG, "callTimer: Không thể tiếp tục timer vì\n" +
//                    "1. Đang nghe là " + mIsListening + "\n" +
//                    "Đang nói là " + BODY.Mouth.isTalking() + "\n" +
//                    "Và kênh hiện tại là " + JAssistant.CURRENT_CHANNEL);
//            return;
//        }
//        jTimer = new Timer();
//        isReadyForStartDeepListen = false;
//        jTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                if (mIsListening ||
//                        BODY.MOUNTH.isTalking() ||
//                        BODY.Mouth.isTalking())
//                    return;
//                Log.d(TAG, "run: Timer is running!");
//                if (!isReadyForStartDeepListen) {
//                    Log.d(TAG, "run: For finished first times!");
//                    isReadyForStartDeepListen = true;
//                } else {
//                    Log.d(TAG, "run: For start services!");
//                    JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.DEEP_LISTEN;
//                    ActivityHome.IS_MATCHED_CALL = false;
//                    if (jTimer != null)
//                        jTimer.cancel();
//                    jTimer = null;
//                    mContext.runOnUiThread(() -> {
//                        if (mContext.mJassisListenServices != null)
//                            mContext.mJassisListenServices.startListening();
//                    });
//                }
//            }
//        }, 2000, 3000);
    }

    public SpeechRecognizerManager(ActivityHome mContext) {
        this.mContext = mContext;

        // Create new intent
        mInterfaceSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mContext.getPackageName());
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language);
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, language);
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, language);
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, language);
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true); // For streaming result
        mInterfaceSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, JAssistant.TIME_WAIT_AFTER_LISTEN);

    }

    protected void InitSpeechRecognizer() {
        while (mInterfaceSpeechRecognizer == null) {
            mInterfaceSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mContext);
            mInterfaceSpeechRecognizer.setRecognitionListener(new SpeechRecognitionListener());
        }
    }

    public void startListening() {
        if (BODY.Mouth.isTalking() || BODY.MOUNTH.isTalking()) {
            Log.w(TAG, "JASSIS đang nói! Bỏ qua việc nghe.");
            destroyListen();
            return;
        }

        if (JAssistant.CURRENT_CHANNEL == JAssistant.CHANNEL.DEEP_LISTEN) {
            JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.INTERFACE; // Chuyển sang kênh tương tác
            Log.w(TAG, "startListening: Đã chuyển sang kênh " + JAssistant.CURRENT_CHANNEL);
        }

        // Đợi mọi view được thiết lập
        if (!ActivityHome.IS_RUNNING) {
            // Nếu màn hình chính của ứng dụng khôn chạy -> Thực hiện gọi
            Intent intent = new Intent(mContext, ActivityHome.class);
            intent.setAction(Intent.ACTION_SCREEN_ON);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            mContext.startActivity(intent);
        }
        SystemClock.sleep(5); // Nghỉ
        // Xóa lắng nghe của services
        if (mServicesSpeechRecognizer != null) {
            destroyListen();
            SystemClock.sleep(5);
            Log.w(TAG, "startListening: Đã xóa lắng nghe của services");
        }
        if (mInterfaceSpeechRecognizer != null) {
            Log.e(TAG, "startListening: Đang cố hủy lắng nghe cũ");
            destroyListen();
//            if (mInterfaceSpeechRecognizer != null)
//            {
//                Log.e(TAG, "startListening: Lắng nghe cũ không thể hủy bỏ. Chuyển kênh sang nghe dưới nền");
//                JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.DEEP_LISTEN;
//                return;
//            }
        }
        SystemClock.sleep(5);
        Log.i(TAG, "Đang khởi tạo lắng nghe mới");
        InitSpeechRecognizer(); // Khởi tạo lắng nghe mới
        try {
            mInterfaceSpeechRecognizer.startListening(mInterfaceSpeechRecognizerIntent); // Tiến hành thực hiện lắngg nghe
        } catch (Exception e) {
            startListening();
        }
    }

    public void stopListen() {
        if (mInterfaceSpeechRecognizer != null) {
            mInterfaceSpeechRecognizer.stopListening();
            mInterfaceSpeechRecognizer.cancel();
        }
        callTimer();
    }

    public void stopListenWithOutTimer() {
        if (mInterfaceSpeechRecognizer != null) {
            mInterfaceSpeechRecognizer.cancel();
            mInterfaceSpeechRecognizer.stopListening();
        }
    }

    public void destroyListen() {
        stopListenWithOutTimer();
        if (mInterfaceSpeechRecognizer != null) {
            mInterfaceSpeechRecognizer.destroy();
        }
        mInterfaceSpeechRecognizer = null;
    }

    private class SpeechRecognitionListener implements RecognitionListener {
        @Override
        public void onReadyForSpeech(Bundle params) {
            Log.w(TAG, "onReadyForSpeech: Tiến hành set up bắt đầu nghe, hiển thị các biểu trưng");
            mContext.runOnUiThread(() -> mContext.mPresenter.ShowTalk());
        }

        @Override
        public void onBeginningOfSpeech() {
        }

        @Override
        public void onRmsChanged(float rmsdB) {
            if (mContext.prgbarVoiceShow != null)
                mContext.prgbarVoiceShow.setProgress((int) rmsdB);
        }

        @Override
        public void onBufferReceived(byte[] buffer) {
            Log.i(TAG, "onBufferReceived: Buffre Recived");
        }

        @Override
        public void onEndOfSpeech() {
            Log.w(TAG, "onEndOfSpeech: Kết thúc lắng nghe");
            mContext.runOnUiThread(() -> {
                mContext.mPresenter.FinishTalk();
                callTimer();
            });
        }

        @Override
        public void onError(int error) {
            if (error == SpeechRecognizer.ERROR_RECOGNIZER_BUSY) {
                Log.e(TAG, "onError: Ghi âm đang bận! Thực hiện hủy tất cả trước đó và bắt đầu lại");
                SystemClock.sleep(30);
                startListening();
                return;
            } else if (error == SpeechRecognizer.ERROR_NO_MATCH) {
                Log.e(TAG, "onError: Không thể tìm ra lỗi trong quá trình ghi âm!");
                Log.e(TAG, "onError: Tự động thử lại!");
                startListening();
                return;
            } else if (error == SpeechRecognizer.ERROR_NETWORK) {
                Log.e(TAG, "onError: Lỗi mạng!");
            }
            mContext.mPresenter.FinishTalk();
            callTimer();
        }

        @Override
        public void onResults(Bundle results) {
            if (results != null) {
                ArrayList<String> resultArray = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                assert resultArray != null;
                Log.w(TAG, "Nghe được đầy đủ là: " + resultArray.get(0));
                new RESPONSE.WORK(mContext).execute(resultArray.get(0));
                mContext.ltvWhatUserTalk.animateText(resultArray.get(0));
                mContext.runOnUiThread(() -> {
                    mContext.mPresenter.FinishTalk();
                    destroyListen();
                    SystemClock.sleep(30);
                    callTimer();
                });
            }
        }

        @Override
        public void onPartialResults(Bundle partialResults) { // Streamming result
            if (partialResults != null) {
                ArrayList<String> texts = partialResults.getStringArrayList("android.speech.extra.UNSTABLE_TEXT");
                assert texts != null;
                if (texts != null) {
                    Log.w(TAG, "Tương tác nghe tức thì là: " + texts.get(0));
                    mContext.ltvWhatUserTalk.animateText(texts.get(0));
                }
            }
        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }
    }
}