package com.futuresky.jassis.DEEPSIDE.BRAIN;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.futuresky.jassis.DEEPSIDE.JAssistant;
import com.futuresky.jassis.DEEPSIDE.VISCERA.View.Home.ActivityHome;
import com.futuresky.jassis.INTERFACE.BODY;
import com.futuresky.jassis.TNLib;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

import static com.futuresky.jassis.DEEPSIDE.VISCERA.Presenter.Services.JassisDeepListenServices.mAudioManager;

public class SPEECH {
    public static String AUDIO_ENCODING = JAssistant.AUDIO_ENCODING.MP3;
    public static float PITCH = JAssistant.PITCH;
    public static float SPEECH_RATE = JAssistant.SPEECH_RATE;
    public static String LANGUAGE_CODE = JAssistant.LANGUAGE_CODE;
    public static String VOICE_NAME = JAssistant.VOICE_NAME;
    private MediaPlayer mediaPlayer;
    int smallI = -1;
    ActivityHome mContext;

    public SPEECH(ActivityHome mContext) {
        this.mediaPlayer = new MediaPlayer();
        this.mContext = mContext;
    }

    public final static String TAG = SPEECH.class.getSimpleName();

    //region Talk and show
    // Lỗi trong phần nói từng câu.. phần này hiện tại không thể sử dụng được (fuck up)
    public void TalkAndShow(String content) {
        String[] allcontent = content.trim().split("(\\.\\s)|(\\.$)");
        smallI = 0;
        new TalkAndShowProcessing().execute(allcontent);
    }

    class TalkAndShowProcessing extends AsyncTask<String, Integer, Boolean> {
        boolean IS_NEW_SPEECH = false;

        @Override
        protected Boolean doInBackground(String... strings) {
            while (smallI < strings.length) {
//                if (isTalking())
//                    continue;
                String base64 = getExistedSpeech(strings[smallI]);
                if (base64.isEmpty()) {
                    IS_NEW_SPEECH = true;
                    base64 = getNewSpeech(strings[smallI]);
                    // Add it to database
                    new AppendSpeechToDatabase().execute(strings[smallI], base64);
                }
                PlayBase64Audio(base64, strings[smallI]);
                smallI++;
            }
            return true;
        }
    }
    //endregion


    String getExistedSpeech(String content) {
        HttpURLConnection client = null;
        StringBuilder response = new StringBuilder();
        try {
            // Create request body
            JSONObject object = new JSONObject();
            object.put("content", SimilarString(content));
            object.put("token", "");
            // Check
            URL url = new URL(NETWORK_CONFIG.GET_EXISTED_SPEECH_API);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("POST");
            client.setConnectTimeout(1000);
            client.setRequestProperty("User-Agent", "Mozilla/5.0");
            client.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            client.setRequestProperty("Accept", "application/json");
            client.setDoInput(true);
            client.setDoOutput(true);

            OutputStream os = client.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, StandardCharsets.UTF_8));
            writer.write(object.toString());

            writer.flush();
            writer.close();
            os.close();
            int responseCode = client.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response.append(line);
                }
            } else {
                response = new StringBuilder();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            if (client != null) // Make sure the connection is not null.
                client.disconnect();
        }
        if (response.length() < 20 || response.equals("-1"))
            return "";
        return response.toString();
    }

    String getNewSpeech(String content) {
        //===============
        String RequestBody = "{\n" +
                "  \"audioConfig\": {\n" +
                "    \"audioEncoding\": \"" + AUDIO_ENCODING + "\",\n" +
                "    \"pitch\": " + PITCH + ",\n" +
                "    \"speakingRate\": " + SPEECH_RATE + "\n" +
                "  },\n" +
                "  \"input\": {\n" +
                "    \"text\": \"" + content + "\"\n" +
                "  },\n" +
                "  \"voice\": {\n" +
                "    \"languageCode\": \"" + LANGUAGE_CODE + "\",\n" +
                "    \"name\": \"" + VOICE_NAME + "\"\n" +
                "  }\n" +
                "}";
        //===============
        HttpURLConnection client = null;
        StringBuilder response = new StringBuilder();
        String speechRawString = "";
        try {
            URL url = new URL(NETWORK_CONFIG.CLOUD_SPEECH + "?key=" + NETWORK_CONFIG.CLOUD_TEXT_TO_SPEECH_API_KEY);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("POST");
            client.usingProxy();
            client.setConnectTimeout(1000);
            client.setRequestProperty("User-Agent", "Mozilla/5.0");
            client.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            client.setRequestProperty("Accept", "application/json");
            client.setDoInput(true);
            client.setDoOutput(true);

            OutputStream os = client.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, StandardCharsets.UTF_8));
            writer.write(RequestBody);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = client.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response.append(line);
                }
            } else {
                response = new StringBuilder();
            }
            speechRawString = response.toString();
            JSONObject speechJSONraw = new JSONObject(speechRawString);
            speechRawString = speechJSONraw.getString("audioContent");
        } catch (Exception e) {
            Log.e(TAG, "ERROR: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (client != null) // Make sure the connection is not null.
                client.disconnect();
        }
        return speechRawString;
    }

    public void ONLY_TALK(String content) {
        JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.INTERFACE;
        // Mở âm thanh
        if (mAudioManager == null)
            mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mAudioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_UNMUTE, AudioManager.FLAG_PLAY_SOUND);
        }
        // Phát âm thanh
        new CHECK_EXISTE_SPEECH().execute(content);
    }

    public void TALK(String content) {
        Log.w(TAG, "TALK_CONTENT: " + content);
        // Nếu view chưa được cài đặt thì đợi
        if (mContext.stvWhatAITalk == null) {
            final Timer[] TimerWaitFinishTalk = {new Timer()};
            TimerWaitFinishTalk[0].schedule(new TimerTask() {
                @Override
                public void run() {
                    Log.w(TAG, "run: Đợi đến khi view được cập nhật");
                    while (mContext.stvWhatAITalk == null)
                        SystemClock.sleep(5);
                    Log.w(TAG, "run: View đã cập nhật xong! Tiến hành update và nói");
                    while (BODY.MOUNTH.isTalking())
                        SystemClock.sleep(10);
                    mContext.stvWhatAITalk.post(() -> {
                        mContext.stvWhatAITalk.animateText(content);
                    });
                    TimerWaitFinishTalk[0].cancel();
                    TimerWaitFinishTalk[0].purge();
                    TimerWaitFinishTalk[0] = null;
                    // Phát âm thanh
                    ONLY_TALK(content);
                }
            }, 15, 100);
        }
        // Nếu có rồi thì tiến hành nói luôn
        else {
            mContext.stvWhatAITalk.animateText(content);
            ONLY_TALK(content);
        }
    }


    public class CHECK_EXISTE_SPEECH extends AsyncTask<String, Integer, Boolean> {
        String content = "";
        String base64 = "";

        @Override
        protected Boolean doInBackground(String... strings) {
            content = strings[0];
            base64 = getExistedSpeech(content);
            if (base64.isEmpty())
                return false;
            else
                return true;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            if (b) {
                PlayBase64Audio(base64, content);
            } else {
                new preTALK().execute(content);
            }
            super.onPostExecute(b);
        }
    }

    public void stop() {
        mediaPlayer.stop();
        mediaPlayer.reset();
    }

    public boolean isTalking() {
        if (mediaPlayer != null)
            return mediaPlayer.isPlaying();
        return false;
    }

    void PlayBase64Audio(String base64, String content_default) {
        try {
            // Lưu vào file tạm trước khi cho đọc, vì URL quá dài nên dẫn đến mediaPlayer.prepare() không chạy được!
            byte[] BytesOfSpeech = Base64.decode(base64.trim(), Base64.DEFAULT);

            String path = TNLib.Using.createDirectoryAndSaveFile(BytesOfSpeech, "/Jassis/Voices",  "temp.mp3");
            if (!path.isEmpty()) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.reset();
//            mediaPlayer = MediaPlayer.create(mContext, Uri.parse(Mytemp.getPath()));
//            if (mediaPlayer == null)
//                Toast.makeText(mContext, "Fuck null!", Toast.LENGTH_SHORT).show();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(mContext, Uri.parse(path));
                mediaPlayer.prepare();
//            mediaPlayer.setOnPreparedListener(MediaPlayer::start);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(mp -> {
//                JAssistant.CURRENT_CHANNEL = JAssistant.CHANNEL.DEEP_LISTEN;
//                ActivityHome.IS_MATCHED_CALL = false;
//                mContext.runOnUiThread(() -> {
//                    if (mContext.mJassisListenServices != null)
//                        mContext.mJassisListenServices.startListening();
//                });
                });
            } else {
                Toast.makeText(mContext, "Trống!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "PlayBase64Audio: " + e.getMessage());
            BODY.Mouth.Talk(content_default);
        }
    }

    public class preTALK extends AsyncTask<String, Integer, String> {
        String content = "";

        @Override
        protected String doInBackground(String... strings) {
            content = strings[0];
            return getNewSpeech(content);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                // Add it to database
                new AppendSpeechToDatabase().execute(content, s);
                // Play audio
                PlayBase64Audio(s, content);
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }

    }

    String SimilarString(String str) {
        str = str.replaceAll("\\! |\\!$|\\.\\s|\\.$|\\?|\\,", " ")
                .trim()
                .replaceAll("\\s{2}", " ")
                .replaceAll("\\s{2}", " ")
                .trim().replaceAll("\\s", "_").toLowerCase();
        Log.d(TAG, "AFTER SIMILAR: " + str);
        return str;
    }

    @SuppressLint("StaticFieldLeak")
    public class AppendSpeechToDatabase extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            HttpURLConnection client = null;
            StringBuilder response = new StringBuilder();
            strings[0] = SimilarString(strings[0]);
            try {
                // Create JSON request body
                JSONObject body = new JSONObject();
                body.put("content", strings[0]);
                body.put("speech_in_base64", strings[1]);
                body.put("audio_encoding", AUDIO_ENCODING);
                body.put("pitch", PITCH);
                body.put("speech_rate", SPEECH_RATE);
                body.put("language_code", LANGUAGE_CODE);
                body.put("voice_name", VOICE_NAME);
                body.put("create_by", NETWORK_CONFIG.CLIENT.USER_ID);
                //
                URL url = new URL(NETWORK_CONFIG.APPEN_SPEECH_API);
                client = (HttpURLConnection) url.openConnection();
                client.setRequestMethod("POST");
                client.usingProxy();
                client.setConnectTimeout(1000);
                client.setRequestProperty("User-Agent", "Mozilla/5.0");
                client.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                client.setRequestProperty("Accept", "application/json");
                client.setDoInput(true);

                OutputStream os = client.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, StandardCharsets.UTF_8));
                writer.write(body.toString());

                writer.flush();
                writer.close();
                os.close();
                int responseCode = client.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    Log.i(TAG, "doInBackground: Success to send speech to database!");
                } else {
                    Log.i(TAG, "doInBackground: Fail to send speech to database!");
                }
            } catch (Exception e) {
                Log.e(TAG, "ERROR: " + e.getMessage());
                e.printStackTrace();
            } finally {
                if (client != null) // Make sure the connection is not null.
                    client.disconnect();
            }
            return false;
        }
    }
}
