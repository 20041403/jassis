package com.futuresky.jassis.DEEPSIDE.VISCERA.View.JViews;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.futuresky.jassis.R;
import com.meetic.marypopup.MaryPopup;

public class LongMsgContentShow {
    public static boolean isShow = false;

    public static void Show(final Activity mContext, String fullText, View from) {
        // Infilate layout
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View v = layoutInflater.inflate(R.layout.layout_long_msg_content_show, null);
        // Create Dialog
        final MaryPopup popup = MaryPopup.with(mContext)
                .content(v)
                .from(from)
                .blackOverlayColor(0)
                .backgroundColor(0)
                .center(true)
                .draggable(true)
                .scaleDownDragging(true)
                .closeDuration(500)
                .cancellable(false);
        // 1. Find and init view
        v.findViewById(R.id.long_content_show_close_button).setOnClickListener(vx -> {
            popup.close(true);
        });
        ((TextView) v.findViewById(R.id.long_content_show_content_full_text)).setText(fullText);

        popup.show();
        isShow = true;
    }
}
