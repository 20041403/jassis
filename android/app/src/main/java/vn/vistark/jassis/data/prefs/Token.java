package vn.vistark.jassis.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

public class Token {
    private final SharedPreferences mPrefs;
    public final String PREF_USER_TOKEN = "PREF_USER_TOKEN";

    public Token(Context mContext) {
        mPrefs = mContext.getSharedPreferences(this.getClass().getSimpleName(), Context.MODE_PRIVATE);
    }

    public boolean setToken(String token) {
        return mPrefs.edit().putString(PREF_USER_TOKEN, token).commit();
    }

    public String getToken() {
        return mPrefs.getString(PREF_USER_TOKEN, null);
    }
}
