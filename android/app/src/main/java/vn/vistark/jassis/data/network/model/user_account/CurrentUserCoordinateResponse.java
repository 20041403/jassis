package vn.vistark.jassis.data.network.model.user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentUserCoordinateResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("result")
    @Expose
    private Result result;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("ua_current_coordinate")
        @Expose
        private String uaCurrentCoordinate;

        public String getUaCurrentCoordinate() {
            return uaCurrentCoordinate;
        }

        public void setUaCurrentCoordinate(String uaCurrentCoordinate) {
            this.uaCurrentCoordinate = uaCurrentCoordinate;
        }

    }
}
