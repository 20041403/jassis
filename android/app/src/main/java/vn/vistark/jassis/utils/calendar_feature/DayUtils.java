package vn.vistark.jassis.utils.calendar_feature;

import java.util.Calendar;

public final class DayUtils {
    public final static String[] daysName = {"", "Chủ Nhật", "Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy"};
    public static String getCurrentDayName() {
        Calendar calendar = Calendar.getInstance();
        int pos = calendar.get(Calendar.DAY_OF_WEEK);
        return daysName[pos];
    }
    public static String getDayName(Calendar calendar) {
        int pos = calendar.get(Calendar.DAY_OF_WEEK);
        return daysName[pos];
    }
}
