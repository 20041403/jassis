package vn.vistark.jassis.ui.home_screen;

import android.os.Bundle;
import android.speech.SpeechRecognizer;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.github.ybq.android.spinkit.SpinKitView;
import com.hanks.htextview.scale.ScaleTextView;
import com.hanks.htextview.typer.TyperTextView;
import com.tomer.fadingtextview.FadingTextView;

import java.util.ArrayList;

import vn.vistark.jassis.R;
import vn.vistark.jassis.ui.home_screen.feature.FeaturePresenter;
import vn.vistark.jassis.utils.ErrorCollectionUtils;
import vn.vistark.jassis.utils.ExitUtils;
import vn.vistark.jassis.utils.ScreenUtils;
import vn.vistark.jassis.utils.speech_recognizer.SpeechRecognizerCallback;
import vn.vistark.jassis.utils.speech_recognizer.SpeechRecognizerManager;

public class HomeScreenActivity extends AppCompatActivity implements SpeechRecognizerCallback {
    private final static String TAG = HomeScreenActivity.class.getSimpleName();

    // Khai báo các lớp trình bày
    private HomeScreenPresenter mHomePresenter;

    // Khai báo trình nhận diện giọng nói
    private SpeechRecognizerManager mSpeechRecognizerManager;

    // Danh sách khai báo khung nhìn cho màn hình chính
    private SpinKitView skvBlinkAttention; // Hiệu ứng nháy gây chú ý để nhấn nút nói
    private ImageView ivBtnAudioRecord; // Nút nhấn để tiến hành ghi âm/AI tiến hành lắng nghe
    private FadingTextView ftvHintOfVoiceRecord;
    private TextView tvStatus;
    private ProgressBar pbVoiceRms;
    private ScaleTextView stvAiTalked;
    private TyperTextView ttvUserTalked;

    // Danh sách khai báo các khung nhìn chứa tính năng
    public ViewPager vpFeatureDisplay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        ScreenUtils.hideTitleBarAndTransparentStatusBar(this);
        mSpeechRecognizerManager = new SpeechRecognizerManager(this, this);
        initViews();
        initEvents();
        initPresenters();
    }

    private void initPresenters() {
        mHomePresenter = new HomeScreenPresenter(this);
        new FeaturePresenter(this);
    }

    private void initViews() {
        // ánh xạ khung nhìn cho các control chính
        skvBlinkAttention = findViewById(R.id.act_ahs_skv);
        ivBtnAudioRecord = findViewById(R.id.act_ahs_audio_record_btn);
        ftvHintOfVoiceRecord = findViewById(R.id.act_ahs_ftv_hint_of_voice_record);
        tvStatus = findViewById(R.id.act_ahs_tv_status);
        pbVoiceRms = findViewById(R.id.act_ahs_voice_rms);
        stvAiTalked = findViewById(R.id.act_ahs_v_talked);
        ttvUserTalked = findViewById(R.id.act_ahs_user_talked);

        // ánh xạ khung nhìn chứa tính năng
        vpFeatureDisplay = findViewById(R.id.featureDisplay);
    }

    void initEvents() {
        SpeechRecognizerButton();
    }


    //region Khu vực dành cho điều khiển các view
    public void showResponse(String msg) {
        stvAiTalked.animateText(msg);
    }

    void redayViewForListen() {
        skvBlinkAttention.setVisibility(View.INVISIBLE);
        ftvHintOfVoiceRecord.stop();
        ftvHintOfVoiceRecord.setVisibility(View.INVISIBLE);
        tvStatus.setVisibility(View.INVISIBLE);
        pbVoiceRms.setVisibility(View.VISIBLE);
        pbVoiceRms.setMax(10);
        skvBlinkAttention.setVisibility(View.INVISIBLE);
        ivBtnAudioRecord.setColorFilter(getResources().getColor(R.color.listening_voice_record));
    }

    void endViewForListen() {
        skvBlinkAttention.setVisibility(View.VISIBLE);
        ftvHintOfVoiceRecord.restart();
        ftvHintOfVoiceRecord.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.INVISIBLE);
        pbVoiceRms.setVisibility(View.GONE);
        skvBlinkAttention.setVisibility(View.VISIBLE);
        ivBtnAudioRecord.setColorFilter(getResources().getColor(R.color.default_voice_record));
    }

    void showStatus(String text) {
        ftvHintOfVoiceRecord.stop();
        ftvHintOfVoiceRecord.setVisibility(View.INVISIBLE);
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(text);
        pbVoiceRms.setVisibility(View.GONE);
    }

    void showDefaultHintOfVoiceRecord() {
        showCustomHintOfVoiceRecord(getResources().getStringArray(R.array.touch_to_talk));
    }

    void showCustomHintOfVoiceRecord(String[] hint) {
        ftvHintOfVoiceRecord.restart();
        ftvHintOfVoiceRecord.setVisibility(View.VISIBLE);
        tvStatus.setVisibility(View.INVISIBLE);
        ftvHintOfVoiceRecord.setTexts(hint);
        pbVoiceRms.setVisibility(View.GONE);
    }

    void disableRecordButton() {
        skvBlinkAttention.setVisibility(View.GONE);
        ivBtnAudioRecord.setColorFilter(getResources().getColor(R.color.disable_voice_record));
        ivBtnAudioRecord.setEnabled(false);
    }

    void enableRecordButton() {
        skvBlinkAttention.setVisibility(View.VISIBLE);
        ivBtnAudioRecord.setColorFilter(getResources().getColor(R.color.default_voice_record));
        ivBtnAudioRecord.setEnabled(true);
    }
    //endregion


    //region Khu vực dành cho các sự kiện của view
    void SpeechRecognizerButton() {
        ivBtnAudioRecord.setOnClickListener(view -> {
            if (mHomePresenter == null) {
                new ErrorCollectionUtils(TAG, "mHomePresenter của HomeScreenActivity bị null");
            } else if (pbVoiceRms.getVisibility() == View.VISIBLE) {
                mSpeechRecognizerManager.stopListen();
            } else {
                mSpeechRecognizerManager.startListen();
            }
        });
    }
    //endregion

    //region Dành cho khu vực xử lý callback của SpeechRecognizerManager
    @Override
    protected void onStart() {
        super.onStart();
        showStatus("Đang tiến hành bật nhận diện giọng nói ...");
        mSpeechRecognizerManager.Initalizer(); // Khởi tạo bộ lắng nghe lần đầu ngay lập tức
    }

    @Override
    public void onFinishInitializer() {
        if (SpeechRecognizerManager.mSpeechRecognizer == null) {
            Toast.makeText(this, "Khởi tạo không thành công!", Toast.LENGTH_SHORT).show();
            disableRecordButton();
            showStatus("Không thể bật bộ nhận diện giọng nói");
        } else {
            showDefaultHintOfVoiceRecord();
            enableRecordButton();
        }
    }

    @Override
    public void onStartListentSuccess() {
        redayViewForListen();
    }

    @Override
    public void onRmsChanged(float v) {
        pbVoiceRms.setProgress((int) v);
    }

    @Override
    public void onEndOfSpeech() {
        endViewForListen();
    }

    @Override
    public void onError(int error) {
        endViewForListen();
    }

    @Override
    public void onResults(Bundle results) {
        if (results != null) {
            ArrayList<String> resultArray = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            assert resultArray != null;
            ttvUserTalked.animateText(resultArray.get(0));
            mHomePresenter.findResponse(resultArray.get(0));
        }
        endViewForListen();
    }

    @Override
    public void onPartialResults(Bundle partialResults) {

    }
    //endregion


    @Override
    public void onBackPressed() {
        ExitUtils.Process(this);
    }
}
