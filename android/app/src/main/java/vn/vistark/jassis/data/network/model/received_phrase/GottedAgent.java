package vn.vistark.jassis.data.network.model.received_phrase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GottedAgent {
    @SerializedName("a_name")
    @Expose
    private String aName;
    @SerializedName("a_description")
    @Expose
    private String aDescription;
    @SerializedName("a_create_at")
    @Expose
    private String aCreateAt;
    @SerializedName("a_update_at")
    @Expose
    private String aUpdateAt;
    @SerializedName("a_id")
    @Expose
    private String aId;
    @SerializedName("i_total")
    @Expose
    private String iTotal;
    @SerializedName("p_total")
    @Expose
    private String pTotal;
    @SerializedName("r_total")
    @Expose
    private String rTotal;

    public String getAName() {
        return aName;
    }

    public void setAName(String aName) {
        this.aName = aName;
    }

    public String getADescription() {
        return aDescription;
    }

    public void setADescription(String aDescription) {
        this.aDescription = aDescription;
    }

    public String getACreateAt() {
        return aCreateAt;
    }

    public void setACreateAt(String aCreateAt) {
        this.aCreateAt = aCreateAt;
    }

    public String getAUpdateAt() {
        return aUpdateAt;
    }

    public void setAUpdateAt(String aUpdateAt) {
        this.aUpdateAt = aUpdateAt;
    }

    public String getAId() {
        return aId;
    }

    public void setAId(String aId) {
        this.aId = aId;
    }

    public String getITotal() {
        return iTotal;
    }

    public void setITotal(String iTotal) {
        this.iTotal = iTotal;
    }

    public String getPTotal() {
        return pTotal;
    }

    public void setPTotal(String pTotal) {
        this.pTotal = pTotal;
    }

    public String getRTotal() {
        return rTotal;
    }

    public void setRTotal(String rTotal) {
        this.rTotal = rTotal;
    }
}
