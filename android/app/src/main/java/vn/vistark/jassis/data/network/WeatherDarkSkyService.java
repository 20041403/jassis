package vn.vistark.jassis.data.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import vn.vistark.jassis.data.network.model.weather_darksky.WeatherResponse;

public interface WeatherDarkSkyService {
    // Api dành cho việc lấy thông tin thời tiết của Dark Sky API
    @GET("/weather/forecast")
    Call<WeatherResponse> getForecast(@Query("ua_current_coordinate") String ua_current_coordinate);
}
