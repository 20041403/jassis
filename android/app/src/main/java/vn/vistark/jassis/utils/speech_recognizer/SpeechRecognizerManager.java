package vn.vistark.jassis.utils.speech_recognizer;

import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import androidx.appcompat.app.AppCompatActivity;

import vn.vistark.jassis.utils.ErrorCollectionUtils;

import static vn.vistark.jassis.utils.speech_recognizer.SpeechRecognizerProperties.defaultLanguage;
import static vn.vistark.jassis.utils.speech_recognizer.SpeechRecognizerProperties.timeWaitAfterListen;

public class SpeechRecognizerManager {
    public final static String TAG = SpeechRecognizerManager.class.getSimpleName();

    private SpeechRecognizerCallback speechRecognizerCallback;
    public static SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    private AppCompatActivity mContext;

    public SpeechRecognizerManager(AppCompatActivity context, SpeechRecognizerCallback speechRecognizerCallback) {
        this.mContext = context;
        this.speechRecognizerCallback = speechRecognizerCallback;
        // Create new intent
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mContext.getPackageName());
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, defaultLanguage);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, defaultLanguage);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, defaultLanguage);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_ONLY_RETURN_LANGUAGE_PREFERENCE, defaultLanguage);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true); // For streaming result
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, timeWaitAfterListen);
    }

    public void Initalizer() {
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mContext);
        mSpeechRecognizer.setRecognitionListener(new SpeechRecognitionListener(speechRecognizerCallback));
        if (mSpeechRecognizer != null) {
            this.speechRecognizerCallback.onFinishInitializer();
        }
    }

    public void startListen() {
        try {
            // Tiến hành thử thục hiện lắng nghe
            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        } catch (Exception e) {
            // Tiên tiến hành lắng nghe đã bị lỗi
            new ErrorCollectionUtils(TAG, e.getMessage());
        }
    }

    public void stopListen() {
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.stopListening();
        }
    }

    public void destroyListen() {
        if (mSpeechRecognizer != null) {
            stopListen();
            mSpeechRecognizer.destroy();
        }
        mSpeechRecognizer = null;
    }

    public boolean isListen() {
        return mSpeechRecognizer == null;
    }
}
