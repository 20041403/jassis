package vn.vistark.jassis.data.network.model.received_phrase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FindBody {
    @SerializedName("phrase")
    @Expose
    private String phrase;
    @SerializedName("coordinate")
    @Expose
    private String coordinate;
    @SerializedName("current_place_name")
    @Expose
    private String currentPlaceName;

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getCurrentPlaceName() {
        return currentPlaceName;
    }

    public void setCurrentPlaceName(String currentPlaceName) {
        this.currentPlaceName = currentPlaceName;
    }
}
