package vn.vistark.jassis.data.network.model.user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Lớp này định nghĩa cho phần thân của request body để gửi lên phần đăng  nhập, đồng thời cũng là đối tượng nhận dữ liệu của khung đăng nhập
 */

public class EmailLoginBody {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}