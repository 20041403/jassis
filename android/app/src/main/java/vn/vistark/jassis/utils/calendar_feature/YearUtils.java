package vn.vistark.jassis.utils.calendar_feature;

import java.util.Calendar;

public final class YearUtils {
    public final static String zodiacFirst[] = {"Canh", "Tân", "Nhâm,", "Quý", "Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ"};
    public final static String zodiacSecond[] = {"Thân", "Dậu", "Tuất", "Hợi", "Tý", "Sửu", "Dần", "Mão", "Thìn", "Tị", "Ngọ", "Mùi"};


    public static String getCurrentYear() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.YEAR) + "";
    }

    public static String getCurrentYearName() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        return zodiacFirst[currentYear % 10] + " " + zodiacSecond[currentYear % 12];
    }

    public static String getYear(Calendar calendar) {
        return calendar.get(Calendar.YEAR) + "";
    }

    public static String getYearName(Calendar calendar) {
        int currentYear = calendar.get(Calendar.YEAR);
        return getYearName(currentYear);
    }

    public static String getYearName(int year) {
        return zodiacFirst[year % 10] + " " + zodiacSecond[year % 12];
    }

    public static boolean isLeapYear() {
        Calendar calendar = Calendar.getInstance();
        return isLeapYear(calendar.get(Calendar.YEAR));
    }

    public static boolean isLeapYear(int year) {
        // Năm nhuận là năm chia hết cho 4,
        // Nếu đã chia hết cho 100 thì cũng phải chia hết cho 400
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
