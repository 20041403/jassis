package vn.vistark.jassis.ui.home_screen.feature.solar_calendar;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.vistark.jassis.R;
import vn.vistark.jassis.data.network.CelebrateService;
import vn.vistark.jassis.data.network.VistarkAPI;
import vn.vistark.jassis.data.network.model.celebrate.CelebrateResponse;
import vn.vistark.jassis.utils.VibrateUtils;
import vn.vistark.jassis.utils.calendar_feature.DateUtils;
import vn.vistark.jassis.utils.calendar_feature.DayUtils;
import vn.vistark.jassis.utils.calendar_feature.MonthUtils;
import vn.vistark.jassis.utils.calendar_feature.YearUtils;

public class PerSolarDayFragment extends Fragment {
    private static final String ARG_CALENDAR_NEXT = "ARG_CALENDAR_NEXT";

    private int mCalendarNext;

    private CelebrateService mCelebrateService;

    private TextView tvDayName;
    private TextView tvDateNumber;
    private TextView tvCelebrateName;
    private TextView tvMonthAndYearString;
    private TextView tvYearNameAndCheckIsLeapYear;
    private TextView tvDayTimeAbsolute;
    private LinearLayout lnDayPerDayEventBtn;

    private long firstClickedMillis = 0;


    public PerSolarDayFragment() {
    }

    public static PerSolarDayFragment newInstance(int nextNDay) {
        PerSolarDayFragment fragment = new PerSolarDayFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CALENDAR_NEXT, nextNDay);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCalendarNext = getArguments().getInt(ARG_CALENDAR_NEXT, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_per_solar_day, container, false);
        iniViews(v);
        initDatas();
        return v;
    }

    private void iniViews(View v) {
        tvDayName = v.findViewById(R.id.dayName);
        tvDateNumber = v.findViewById(R.id.dateNumber);
        tvCelebrateName = v.findViewById(R.id.celebrateName);
        tvMonthAndYearString = v.findViewById(R.id.monthAndYearString);
        tvYearNameAndCheckIsLeapYear = v.findViewById(R.id.yearNameAndCheckIsLeapYear);
        tvDayTimeAbsolute = v.findViewById(R.id.dayTimeAbsolute);
        lnDayPerDayEventBtn = v.findViewById(R.id.dayPerDayEventBtn);
        lnDayPerDayEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (System.currentTimeMillis() - firstClickedMillis < 1000) {
                    SolarCalendarFragment.updateDayPosition(0);
                }
                firstClickedMillis = System.currentTimeMillis();
            }
        });
    }

    public void initDatas() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, mCalendarNext);

        tvDayName.setText(DayUtils.getDayName(calendar));
        tvDateNumber.setText(DateUtils.getDateNumber(calendar));
        tvCelebrateName.setText("");
        tvMonthAndYearString.setText("Tháng " + MonthUtils.getMonth(calendar) + " năm " + YearUtils.getYear(calendar));
        tvYearNameAndCheckIsLeapYear.setText("(" + YearUtils.getYearName(calendar) + " - " + (YearUtils.isLeapYear(calendar.get(Calendar.YEAR)) ? "Năm nhuận" : "Không nhuận") + ")");
        tvDayTimeAbsolute.setText(DateUtils.getAbsoluteDayString(mCalendarNext).toUpperCase());
        findCelebrate(calendar);
    }

    private void findCelebrate(Calendar calendar) {
        if (mCelebrateService == null)
            mCelebrateService = VistarkAPI.getCelebrateService();
        String prepareDate = DateUtils.getDateMonthYearString(calendar);

        mCelebrateService.find(prepareDate).enqueue(new Callback<CelebrateResponse>() {
            @Override
            public void onResponse(Call<CelebrateResponse> call, Response<CelebrateResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() >= 0 && response.body().getResult().size() > 0) {
                    CelebrateResponse.Result result = response.body().getResult().get(0);
                    tvCelebrateName.setText(result.getName());
                    Log.d(ARG_CALENDAR_NEXT, "onResponse: " + result.getName());
                    tvCelebrateName.setVisibility(View.VISIBLE);
                    if (!result.getName().isEmpty() && tvCelebrateName.getVisibility() == View.VISIBLE)
                        VibrateUtils.vibrateNotify(tvCelebrateName.getContext()); // Rung báo hiệu có ngày kỷ niệm
                } else {
                    tvCelebrateName.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CelebrateResponse> call, Throwable t) {
                tvCelebrateName.setVisibility(View.GONE);
            }
        });
    }
}
