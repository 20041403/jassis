package vn.vistark.jassis.utils.text_to_speech;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Base64;
import android.widget.Toast;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import vn.vistark.jassis.R;
import vn.vistark.jassis.utils.IOUtils;
import vn.vistark.jassis.utils.speech_recognizer.SpeechRecognizerManager;

public final class SpeechPlayer {
    private final static String TAG = SpeechPlayer.class.getSimpleName();

    private static MediaPlayer mediaPlayer;

    public static void playBase64AudioAndListenAfterThen(Context context, String base64Audio, SpeechRecognizerManager speechRecognizerManager) {
        playBase64Audio(context, base64Audio);
        // Kiểm tra xem đã nói xong chưa bằng timer định kỳ
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!mediaPlayer.isPlaying()) {
                    // Không nói thì tiến hành chấm dứt timer và lắng nghe
                    timer.cancel();
                    if (speechRecognizerManager.isListen()) {
                        // Nếu đã đang nghe, thì tiến hành ngắt
                        speechRecognizerManager.stopListen();
                    }
                    // Tiến hành thực hiện lắng nghe lại
                    speechRecognizerManager.startListen();
                }
            }
        }, 500, 500);
    }

    public static void playBase64Audio(Context mContext, String base64Audio) {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
        }
        // Khai báo đường dẫn chứa tệp phát âm thanh tạm thời
        String cacheAudio = "/" + mContext.getResources().getString(R.string.app_name) + "/caches";
        // Tiến hành giải mã vào lưu tệp vào file
        byte[] bytesOfSpeech = Base64.decode(base64Audio.trim(), Base64.DEFAULT);
        String savedPath = IOUtils.saveFileInDirectory(mContext, cacheAudio, "playme.mp3", bytesOfSpeech);
        if (!savedPath.isEmpty()) {
            stop(); // Dừng phát cũ nếu đã có

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mediaPlayer.setDataSource(mContext, Uri.parse(savedPath));
                mediaPlayer.prepare();
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(mp -> {
                    // Sau khi phát xong
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(mContext, "Lưu trữ file tạm không thành công! Có thể do chưa cấp quyền", Toast.LENGTH_SHORT).show();
        }
    }

    public static void stop() {
        if (mediaPlayer != null) {
            // Nêu đang ở chế độ lập thì tiến hành bỏ chế độ lặp đi
            if (mediaPlayer.isLooping()) {
                mediaPlayer.setLooping(false);
            }

            // Nếu media đang được chạy
            if (mediaPlayer.isPlaying()) {
                // Dừng
                mediaPlayer.stop();
            }
            // Reset
            mediaPlayer.reset();
        }
    }

    public static boolean isPlaying() {
        if (mediaPlayer != null) {
            return mediaPlayer.isPlaying();
        } else {
            return false;
        }
    }

    public static void destroy() {
    }

    private SpeechPlayer() {

    }
}
