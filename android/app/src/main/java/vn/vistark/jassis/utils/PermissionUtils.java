package vn.vistark.jassis.utils;

import android.content.pm.PackageManager;
import android.os.Build;

import androidx.appcompat.app.AppCompatActivity;

import vn.vistark.jassis.data.runtime.model.AppPermission;

public class PermissionUtils {
    private final static String TAG = PermissionUtils.class.getSimpleName();

    public static void RequestAllPermission(AppCompatActivity context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.requestPermissions(AppPermission.getStringsPermission(), AppPermission.TOTAL_PERMISSION_REQUEST_CODE);
        }
    }

    public static boolean isGrantedAll(int requestCode, int[] grantResults) {
        if (requestCode == AppPermission.TOTAL_PERMISSION_REQUEST_CODE) {
            int accepted = 0;
            for (int grantResult : grantResults) {
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    accepted++;
                }
            }
            return accepted >= AppPermission.getListInstances().size();
        }
        return false;
    }
}
