package vn.vistark.jassis.data.network.model.received_phrase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TextToSpeechResponse {

    public class Result {
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("result")
        @Expose
        private Result result;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }

        @SerializedName("as_audio_encoding")
        @Expose
        private String asAudioEncoding;
        @SerializedName("as_pitch")
        @Expose
        private Integer asPitch;
        @SerializedName("as_speaking_rate")
        @Expose
        private Integer asSpeakingRate;
        @SerializedName("as_language_code")
        @Expose
        private String asLanguageCode;
        @SerializedName("as_voice_sound_name")
        @Expose
        private String asVoiceSoundName;
        @SerializedName("as_text_code")
        @Expose
        private String asTextCode;
        @SerializedName("gottedResponseAudio")
        @Expose
        private String gottedResponseAudio;

        public String getAsAudioEncoding() {
            return asAudioEncoding;
        }

        public void setAsAudioEncoding(String asAudioEncoding) {
            this.asAudioEncoding = asAudioEncoding;
        }

        public Integer getAsPitch() {
            return asPitch;
        }

        public void setAsPitch(Integer asPitch) {
            this.asPitch = asPitch;
        }

        public Integer getAsSpeakingRate() {
            return asSpeakingRate;
        }

        public void setAsSpeakingRate(Integer asSpeakingRate) {
            this.asSpeakingRate = asSpeakingRate;
        }

        public String getAsLanguageCode() {
            return asLanguageCode;
        }

        public void setAsLanguageCode(String asLanguageCode) {
            this.asLanguageCode = asLanguageCode;
        }

        public String getAsVoiceSoundName() {
            return asVoiceSoundName;
        }

        public void setAsVoiceSoundName(String asVoiceSoundName) {
            this.asVoiceSoundName = asVoiceSoundName;
        }

        public String getAsTextCode() {
            return asTextCode;
        }

        public void setAsTextCode(String asTextCode) {
            this.asTextCode = asTextCode;
        }

        public String getGottedResponseAudio() {
            return gottedResponseAudio;
        }

        public void setGottedResponseAudio(String gottedResponseAudio) {
            this.gottedResponseAudio = gottedResponseAudio;
        }

    }
}
