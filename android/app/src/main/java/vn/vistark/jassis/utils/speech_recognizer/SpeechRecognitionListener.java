package vn.vistark.jassis.utils.speech_recognizer;

import android.os.Bundle;
import android.speech.RecognitionListener;

public class SpeechRecognitionListener implements RecognitionListener {
    private SpeechRecognizerCallback speechRecognizerCallback;

    SpeechRecognitionListener(SpeechRecognizerCallback speechRecognizerCallback) {
        this.speechRecognizerCallback = speechRecognizerCallback;
    }

    @Override
    public void onReadyForSpeech(Bundle bundle) {
        speechRecognizerCallback.onStartListentSuccess();
    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float v) {
        speechRecognizerCallback.onRmsChanged(v);
    }

    @Override
    public void onBufferReceived(byte[] bytes) {

    }

    @Override
    public void onEndOfSpeech() {
        speechRecognizerCallback.onEndOfSpeech();
    }

    @Override
    public void onError(int i) {
        speechRecognizerCallback.onError(i);
    }

    @Override
    public void onResults(Bundle bundle) {
        speechRecognizerCallback.onResults(bundle);
    }

    @Override
    public void onPartialResults(Bundle bundle) {
        speechRecognizerCallback.onPartialResults(bundle);
    }

    @Override
    public void onEvent(int i, Bundle bundle) {

    }
}
