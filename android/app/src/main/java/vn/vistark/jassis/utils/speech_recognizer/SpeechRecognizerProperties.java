package vn.vistark.jassis.utils.speech_recognizer;

public class SpeechRecognizerProperties {
    public static final String defaultLanguage = "vi"; // Ngôn ngữ mặc định cho nhận diện giọng nói
    public static final long timeWaitAfterListen = 3000L;   // Thời gian chờ tối đa sau khi lắng nghe xong
    public static final long maxInitSpeechRecognizerWaitTime = 15000L; // Thời gian chờ khởi tạo bộ nhận diện giọng nói tối đa
}
