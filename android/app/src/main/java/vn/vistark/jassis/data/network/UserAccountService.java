package vn.vistark.jassis.data.network;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import vn.vistark.jassis.data.network.model.user_account.CurrentUserCoordinateBody;
import vn.vistark.jassis.data.network.model.user_account.CurrentUserCoordinateResponse;
import vn.vistark.jassis.data.network.model.user_account.EmailLoginBody;
import vn.vistark.jassis.data.network.model.user_account.LoginResponse;
import vn.vistark.jassis.data.network.model.user_account.ProfileResponse;
import vn.vistark.jassis.data.network.model.user_account.TokenVerifyResponse;
import vn.vistark.jassis.data.network.model.user_account.UsernameLoginBody;

/**
 * Lớp này tiến hành định nghĩa các service của API account
 */
public interface UserAccountService {
    // Api dành cho việc đăng nhập sử dụng tên tài khoản thông thường
    @POST("/user_account/login")
    Call<LoginResponse> login(@Body UsernameLoginBody data);

    // Api dành cho việc đăng nhập sử dụng Email
    @POST("/user_account/login")
    Call<LoginResponse> login(@Body EmailLoginBody data);

    // Api dành cho việc xác thực token
    @GET("/user_account/token_verify")
    Call<TokenVerifyResponse> tokenVerify();

    // Api dành cho việc lấy profile
    @GET("/user_account/profile")
    Call<ProfileResponse> getProfile(@Query("up_id") int up_id);

    // Api dành cho việc lấy tọa độ hiên tại của người dùng
    @GET("/user_account/current_coordinate")
    Call<CurrentUserCoordinateResponse> getCurrentUserCoordinate();

    // Api dành cho việc cập nhật tọa độ hiên tại của người dùng
    @PUT("/user_account/current_coordinate")
    Call<CurrentUserCoordinateResponse> updateCurrentUserCoordinate(@Body CurrentUserCoordinateBody currentUserCoordinateBody);

    // Api dành cho việc cập nhật tọa độ hiên tại của người dùng
    @PUT("/user_account/current_coordinate")
    Call<CurrentUserCoordinateResponse> updateCurrentUserCoordinate();
}
