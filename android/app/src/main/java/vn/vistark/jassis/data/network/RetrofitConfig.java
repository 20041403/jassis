package vn.vistark.jassis.data.network;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vn.vistark.jassis.data.prefs.Prefs;

/**
 * Lớp này tiến hành khởi tạo singleton cho Retrofit2
 * Singleton: là một mẫu thiết kế phần mềm để hạn chế sự khởi tạo của lớp đối tượng. Điều này rất hữu ích khi cần một đối tượng chính xác để điều phối hành động trên toàn hệ thống.
 */

public class RetrofitConfig {
    private static Retrofit retrofit = null;

    public static void reset() {
        retrofit = null;
    }

    public static Retrofit getClient(String baseUrl) {
        if (retrofit == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            Retrofit.Builder retrofitBuilder = new Retrofit
                    .Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create());
            if (Prefs.GlobalToken.getToken() != null) {
                httpClient.addInterceptor(chain -> {
                    Request request = chain.request().newBuilder().addHeader("access_token", Prefs.GlobalToken.getToken()).build();
                    return chain.proceed(request);
                });
                retrofitBuilder = retrofitBuilder.client(httpClient.build());
            }
            retrofit = retrofitBuilder.build();
        }
        return retrofit;
    }
}
