package vn.vistark.jassis.ui.home_screen.feature.lunar_calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PerLunarDaySlidePagerAdapter extends FragmentStatePagerAdapter {
    private int clicked = 0;
    private long firstClickedMillis = -1;

    PerLunarDaySlidePagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return PerLunarDayFragment.newInstance(position - (int) (LunarCalendarConfig.MAX_NEXT_DAYS / 2));
    }

    @Override
    public int getCount() {
        return LunarCalendarConfig.MAX_NEXT_DAYS;
    }
}
