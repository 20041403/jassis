package vn.vistark.jassis.ui.home_screen.feature.solar_calendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import vn.vistark.jassis.R;

public class SolarCalendarFragment extends Fragment {
    private final static String TAG = SolarCalendarFragment.class.getSimpleName();

    private PagerAdapter pagerAdapterPerDayShow;

    public static ViewPager vpPerDay;

    public SolarCalendarFragment() {
    }

    public static SolarCalendarFragment newInstance() {
        SolarCalendarFragment fragment = new SolarCalendarFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_solar_calendar, container, false);
        initViews(v);
        return v;
    }


    private void initViews(View v) {
        vpPerDay = v.findViewById(R.id.showPerDay);
        pagerAdapterPerDayShow = new PerSolarDaySlidePagerAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.POSITION_UNCHANGED);
        vpPerDay.setAdapter(pagerAdapterPerDayShow);
        vpPerDay.setCurrentItem(((int) SolarCalendarConfig.MAX_NEXT_DAYS / 2), false);
    }

    static void updateDayPosition(int pos) {
        vpPerDay.setCurrentItem(((int) SolarCalendarConfig.MAX_NEXT_DAYS / 2) - pos, true);
    }
}
