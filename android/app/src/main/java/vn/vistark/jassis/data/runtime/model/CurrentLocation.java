package vn.vistark.jassis.data.runtime.model;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.vistark.jassis.Config;
import vn.vistark.jassis.data.network.UserAccountService;
import vn.vistark.jassis.data.network.VistarkAPI;
import vn.vistark.jassis.data.network.model.user_account.CurrentUserCoordinateBody;
import vn.vistark.jassis.data.network.model.user_account.CurrentUserCoordinateResponse;
import vn.vistark.jassis.utils.ErrorCollectionUtils;

import static android.content.Context.LOCATION_SERVICE;

public class CurrentLocation {
    public final static String TAG = CurrentLocation.class.getSimpleName();

    public static LocationManager mLocationManager;
    public static Location lastedKnowLocation;
    public static String lastedKnowCoordinate;

    public static void getCurrentUserCoordinate() {
        UserAccountService userAccountService = VistarkAPI.getAccountService();
        userAccountService.getCurrentUserCoordinate().enqueue(new Callback<CurrentUserCoordinateResponse>() {
            @Override
            public void onResponse(Call<CurrentUserCoordinateResponse> call, Response<CurrentUserCoordinateResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() >= 0) {
                    lastedKnowCoordinate = response.body().getResult().getUaCurrentCoordinate();
                    Log.i(TAG, "onResponse: Lấy lại vị trí cũ của người dùng: " + lastedKnowCoordinate);
                }
            }

            @Override
            public void onFailure(Call<CurrentUserCoordinateResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: Tiến hành lấy vị trí đã thu thập của người dùng không thành công.");
            }
        });
    }

    public static void updateCurrentUserCoordinate() {
        UserAccountService userAccountService = VistarkAPI.getAccountService();
        Call<CurrentUserCoordinateResponse> currentUserCoordinateResponseCall;
        String tempCoordinateStr = getStringCoordinate();
        if (tempCoordinateStr == null) {
            currentUserCoordinateResponseCall = userAccountService.updateCurrentUserCoordinate();
        } else {
            CurrentUserCoordinateBody currentUserCoordinateBody = new CurrentUserCoordinateBody();
            currentUserCoordinateBody.setUaCurrentCoordinate(tempCoordinateStr);
            currentUserCoordinateResponseCall = userAccountService.updateCurrentUserCoordinate(currentUserCoordinateBody);
        }
        Log.w(TAG, "updateCurrentUserCoordinate: Đang tiến hành cập nhật vị trí");
        // Tiến hành truy vấn lấy kết quả
        currentUserCoordinateResponseCall.enqueue(new Callback<CurrentUserCoordinateResponse>() {
            @Override
            public void onResponse(Call<CurrentUserCoordinateResponse> call, Response<CurrentUserCoordinateResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getStatus() >= 0) {
                    lastedKnowCoordinate = response.body().getResult().getUaCurrentCoordinate();
                    Log.i(TAG, "onResponse: Đã cập nhật vị trí thành công với giá trị là: " + lastedKnowCoordinate);
                }
            }

            @Override
            public void onFailure(Call<CurrentUserCoordinateResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: Cập nhật vị trí không thành công");
            }
        });
    }

    public static String getStringCoordinate() {
        if (lastedKnowLocation != null) {
            return lastedKnowLocation.getLatitude() + "," + lastedKnowLocation.getLongitude();
        } else {
            return null;
        }
    }

    public CurrentLocation(Context context) {
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "Bạn vẫn chưa cấp quyền lấy vị trí cho tôi", Toast.LENGTH_SHORT).show();
                getCurrentUserCoordinate();
                return;
            }
        }
        if (mLocationManager != null) {
            Location lastedKnow = mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (lastedKnow != null) {
                lastedKnowLocation = lastedKnow;
                updateCurrentUserCoordinate();
//                Log.d(TAG, "CurrentLocation: " + lastedKnow.getLatitude() + "," + lastedKnow.getLongitude());
            } else {
//                Toast.makeText(context, "Không lấy được vị trí cuối cùng của bạn", Toast.LENGTH_SHORT).show();
            }
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Config.Location.LOCATION_REFRESH_TIME, Config.Location.LOCATION_REFRESH_DISTANCE, mLocationListener);
        } else {
            Toast.makeText(context, "Khởi tạo bộ cập nhật vị trí thất bại", Toast.LENGTH_SHORT).show();
        }
    }

    public LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            updateCurrentUserCoordinate();
            lastedKnowLocation = location;
//            Log.d(TAG, "onLocationChanged: " + location.getLatitude() + "," + location.getLongitude());
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    public static Address getAddress(Context mContext, double latitude, double logitude) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            return geocoder.getFromLocation(latitude, logitude, 1).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Address getMyAddress(Context mContext) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            return geocoder.getFromLocation(lastedKnowLocation.getLatitude(), lastedKnowLocation.getLongitude(), 1).get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAddress(Context mContext) {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lastedKnowLocation.getLatitude(), lastedKnowLocation.getLongitude(), 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName(); // Tên quốc gia
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            updateCurrentUserCoordinate();

            return add;
        } catch (Exception e) {
            new ErrorCollectionUtils(TAG, e.getMessage());
            return "Không xác định được tên khu vực cụ thể";
        }
    }

    public static String convertDDtoDMS() {
        if (lastedKnowLocation == null)
            return "Không xác định";
        else
            return convertDDtoDMS(lastedKnowLocation.getLatitude(), lastedKnowLocation.getLongitude());
    }

    public static String convertDDtoDMS(double latitude, double longitude) {
        StringBuilder builder = new StringBuilder();

        String latitudeDegrees = Location.convert(Math.abs(latitude), Location.FORMAT_SECONDS);
        String[] latitudeSplit = latitudeDegrees.split(":");
        builder.append(latitudeSplit[0]);
        builder.append("°");
        builder.append(latitudeSplit[1]);
        builder.append("'");
        builder.append(latitudeSplit[2]);
        builder.append("\"");

        builder.append(" ");

        if (latitude < 0) {
            builder.append("Nam ");
        } else {
            builder.append("Bắc ");
        }


        String longitudeDegrees = Location.convert(Math.abs(longitude), Location.FORMAT_SECONDS);
        String[] longitudeSplit = longitudeDegrees.split(":");
        builder.append(longitudeSplit[0]);
        builder.append("°");
        builder.append(longitudeSplit[1]);
        builder.append("'");
        builder.append(longitudeSplit[2]);
        builder.append("\"");

        builder.append(" ");


        if (longitude < 0) {
            builder.append("Tây ");
        } else {
            builder.append("Đông ");
        }

        return builder.toString();
    }
}
