package vn.vistark.jassis.utils.calendar_feature;

import java.util.Calendar;

public final class DateUtils {
    public static String getCurrentDateNumber() {
        Calendar calendar = Calendar.getInstance();
        int dateNumber = calendar.get(Calendar.DAY_OF_MONTH);
        if (dateNumber < 10) {
            return "0" + dateNumber;
        } else {
            return "" + dateNumber;
        }
    }

    public static String getCurrentDateMonthYearString() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH) + "-" +
                (calendar.get(Calendar.MONTH) + 1) + "-" +
                calendar.get(Calendar.YEAR);
    }

    public static String getDateNumber(Calendar calendar) {
        int dateNumber = calendar.get(Calendar.DAY_OF_MONTH);
        if (dateNumber < 10) {
            return "0" + dateNumber;
        } else {
            return "" + dateNumber;
        }
    }

    public static String getDateMonthYearString(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_MONTH) + "-" +
                (calendar.get(Calendar.MONTH) + 1) + "-" +
                calendar.get(Calendar.YEAR);
    }

    public static String getAbsoluteDayString(int xDays) {
        if (xDays == 0)
            return "Hôm nay";
        else if (xDays > 0) {
            // Định hướng ngày tương lai
            switch (xDays) {
                case 1:
                    return "Ngày mai";
                case 2:
                    return "Ngày mốt";
                default:
                    return xDays + " ngày nữa";
            }
        } else {
            // Định hướng ngày quá khứ
            switch (xDays) {
                case -1:
                    return "Hôm qua";
                case -2:
                    return "Hôm kia";
                default:
                    return (-xDays) + " ngày trước";
            }
        }
    }
}
