package vn.vistark.jassis.data.network.model.user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurrentUserCoordinateBody {
    @SerializedName("ua_current_coordinate")
    @Expose
    private String uaCurrentCoordinate;

    public String getUaCurrentCoordinate() {
        return uaCurrentCoordinate;
    }

    public void setUaCurrentCoordinate(String uaCurrentCoordinate) {
        this.uaCurrentCoordinate = uaCurrentCoordinate;
    }
}
