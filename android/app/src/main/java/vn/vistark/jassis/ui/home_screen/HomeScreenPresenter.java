package vn.vistark.jassis.ui.home_screen;

import android.app.ProgressDialog;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.vistark.jassis.data.network.VistarkAPI;
import vn.vistark.jassis.data.network.model.received_phrase.FindBody;
import vn.vistark.jassis.data.network.model.received_phrase.FindResponse;
import vn.vistark.jassis.data.runtime.model.CurrentLocation;
import vn.vistark.jassis.utils.CommonUtils;
import vn.vistark.jassis.utils.ErrorCollectionUtils;
import vn.vistark.jassis.utils.text_to_speech.SpeechPlayer;

public class HomeScreenPresenter {
    private final static String TAG = HomeScreenPresenter.class.getSimpleName();
    private final HomeScreenActivity mHomeScreenActivity;

    public HomeScreenPresenter(HomeScreenActivity homeScreenView) {
        mHomeScreenActivity = homeScreenView;
    }

    public void findResponse(String msg) {
        // Hiển thị màn hình đang load
        ProgressDialog progressDialog = CommonUtils.showLoadingDialog(mHomeScreenActivity);
        progressDialog.show();

        // Tiến hành xây dựng phần thân của request
        FindBody findBody = new FindBody();
        findBody.setPhrase(msg);
        findBody.setCoordinate(CurrentLocation.getStringCoordinate());
        findBody.setCurrentPlaceName(CurrentLocation.getAddress(mHomeScreenActivity));

        // Tiến hành gửi request để lấy kết quả
        VistarkAPI.getFindResponseService().findResponse(findBody).enqueue(new Callback<FindResponse>() {
            @Override
            public void onResponse(Call<FindResponse> call, Response<FindResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        mHomeScreenActivity.showResponse(response.body().getFindResult().getGottedResponse().getRResponse());
                        SpeechPlayer.playBase64Audio(mHomeScreenActivity, response.body().getFindResult().getGottedResponseAudio());
                    } else {
                        Toast.makeText(mHomeScreenActivity, "Không có phản hồi", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mHomeScreenActivity, "Lấy phản hối không thành công!", Toast.LENGTH_SHORT).show();
                }
                // Đóng màn hình load khi thực hiện xong
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<FindResponse> call, Throwable t) {
                new ErrorCollectionUtils(TAG, t.getMessage());
                // Đóng màn hình load khi thực hiện xong
                progressDialog.dismiss();
            }
        });
    }

}
