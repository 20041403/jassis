package vn.vistark.jassis.service;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.service.voice.VoiceInteractionSession;
import android.service.voice.VoiceInteractionSessionService;

public class VistarkVoiceInteractionSessionService extends VoiceInteractionSessionService {
    public VistarkVoiceInteractionSessionService() {
    }

    @Override
    public VoiceInteractionSession onNewSession(Bundle bundle) {
        return new VoiceInteractionSession(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
