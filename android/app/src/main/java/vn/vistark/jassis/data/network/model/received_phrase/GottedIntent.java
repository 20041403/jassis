package vn.vistark.jassis.data.network.model.received_phrase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GottedIntent {
    @SerializedName("i_name")
    @Expose
    private String iName;
    @SerializedName("i_description")
    @Expose
    private String iDescription;
    @SerializedName("i_create_at")
    @Expose
    private String iCreateAt;
    @SerializedName("a_id")
    @Expose
    private String aId;
    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("a_name")
    @Expose
    private String aName;
    @SerializedName("p_total")
    @Expose
    private String pTotal;
    @SerializedName("r_total")
    @Expose
    private String rTotal;

    public String getIName() {
        return iName;
    }

    public void setIName(String iName) {
        this.iName = iName;
    }

    public String getIDescription() {
        return iDescription;
    }

    public void setIDescription(String iDescription) {
        this.iDescription = iDescription;
    }

    public String getICreateAt() {
        return iCreateAt;
    }

    public void setICreateAt(String iCreateAt) {
        this.iCreateAt = iCreateAt;
    }

    public String getAId() {
        return aId;
    }

    public void setAId(String aId) {
        this.aId = aId;
    }

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getAName() {
        return aName;
    }

    public void setAName(String aName) {
        this.aName = aName;
    }

    public String getPTotal() {
        return pTotal;
    }

    public void setPTotal(String pTotal) {
        this.pTotal = pTotal;
    }

    public String getRTotal() {
        return rTotal;
    }

    public void setRTotal(String rTotal) {
        this.rTotal = rTotal;
    }
}
