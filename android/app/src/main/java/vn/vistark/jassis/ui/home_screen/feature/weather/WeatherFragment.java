package vn.vistark.jassis.ui.home_screen.feature.weather;

import android.location.Address;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.vistark.jassis.R;
import vn.vistark.jassis.data.network.VistarkAPI;
import vn.vistark.jassis.data.network.WeatherDarkSkyService;
import vn.vistark.jassis.data.network.model.weather_darksky.Currently;
import vn.vistark.jassis.data.network.model.weather_darksky.Daily;
import vn.vistark.jassis.data.network.model.weather_darksky.Hourly;
import vn.vistark.jassis.data.network.model.weather_darksky.Result;
import vn.vistark.jassis.data.network.model.weather_darksky.WeatherResponse;
import vn.vistark.jassis.data.runtime.model.CurrentLocation;
import vn.vistark.jassis.data.runtime.model.Weather;

public class WeatherFragment extends Fragment {
    private TextView tvLocationName;
    private TextView tvTemprature;
    private TextView tvHumidity;
    private TextView tvWinSpeech;
    private TextView tvForecastMessage;

    private LinearLayout lnRootShow;
    private LinearLayout lnNotAvailableAnnoucement;
    private TextView tvAnnoucementMessage;

    private WeatherDarkSkyService mWeatherDarkSkyService;

    public WeatherFragment() {
    }

    public static WeatherFragment newInstance() {
        WeatherFragment fragment = new WeatherFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_weather, container, false);
        initViews(v);
        initData();
        return v;
    }

    private void setWetherResponse() {
        Result result = Weather.gottedWeatherResponse.getResult();
        Currently currently = result.getCurrently();
        Hourly hourly = result.getHourly();
        Daily daily = result.getDaily();
        // Thiết lập các thông số vào khung nhìn
        String currentLocal = "";
        Address address = CurrentLocation.getMyAddress(tvTemprature.getContext());
        if (address != null) {
            if (address.getAdminArea() != null) {
                currentLocal = address.getAdminArea();
            } else if (address.getCountryName() != null) {
                currentLocal = address.getCountryName() + "\r\n(" + CurrentLocation.convertDDtoDMS() + ")";
            } else {
                currentLocal = CurrentLocation.convertDDtoDMS();
            }
        } else {
            currentLocal = CurrentLocation.convertDDtoDMS();
        }
        setTvLocationName(currentLocal);
        setTvTemprature(currently.getTemperature().intValue() + "");
        setTvHumidity(Math.round((currently.getHumidity() * 100)) + "%");
        setTvWinSpeech(currently.getWindSpeed().toString());
        setTvForecastMessage(currently.getSummary() + ". " + hourly.getSummary() + " " + daily.getSummary());
        // Sau đó tiến hành hiển thị dữ liệu lên
        showRoot();
    }

    private void initData() {
        if (mWeatherDarkSkyService == null) {
            mWeatherDarkSkyService = VistarkAPI.getWeatherDarkSkyService();
        }
        if (CurrentLocation.getStringCoordinate() == null || CurrentLocation.getStringCoordinate().isEmpty()) {
            showNotAvailableAnnoucement("Không thể xác định được vị trí hiện tại của bạn");
        } else {
            // Nếu đã có
            if (Weather.gottedWeatherResponse != null) {
                // Đồng thời thông tin không quá cũ
                if (System.currentTimeMillis() - Weather.lastedRequest < Weather.MAX_WEATHER_LIFE) {
                    setWetherResponse(); // Tiến hành hiển thị
                } else {
                    // Nếu quá cũ thì xóa đi
                    Weather.gottedWeatherResponse = null;
                }
            }
            // Nếu chưa có thông tin
            if (Weather.gottedWeatherResponse == null) {
                // Tiến hành lấy mới
                mWeatherDarkSkyService.getForecast(CurrentLocation.getStringCoordinate()).enqueue(new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                        if (response.isSuccessful()) {
                            WeatherResponse weatherResponse = response.body();
                            if (weatherResponse != null && weatherResponse.getStatus() >= 0) {
                                Weather.lastedRequest = System.currentTimeMillis();
                                Weather.gottedWeatherResponse = weatherResponse;
                                setWetherResponse();
                            } else {
                                showNotAvailableAnnoucement("Lấy thông tin thời tiết không thành công");
                            }
                        } else {
                            showNotAvailableAnnoucement("Lỗi khi cố lấy thông tin thời tiết");
                        }
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        showNotAvailableAnnoucement("Lỗi khi lấy thông tin thời tiết từ máy chủ");
                    }
                });
            }

        }
    }

    private void initViews(View v) {
        tvLocationName = v.findViewById(R.id.locationName);
        tvTemprature = v.findViewById(R.id.temperature);
        tvHumidity = v.findViewById(R.id.humidity);
        tvWinSpeech = v.findViewById(R.id.windSpeech);
        tvForecastMessage = v.findViewById(R.id.forecastMessage);
        lnRootShow = v.findViewById(R.id.rootShow);
        lnNotAvailableAnnoucement = v.findViewById(R.id.notAvailableAnnoucement);
        tvAnnoucementMessage = v.findViewById(R.id.annoucementMessage);
        showNotAvailableAnnoucement();
    }

    private void showRoot() {
        lnRootShow.post(() -> {
            lnRootShow.setVisibility(View.VISIBLE);
            lnNotAvailableAnnoucement.setVisibility(View.GONE);
        });
    }

    private void showNotAvailableAnnoucement() {
        String loadingWeahter = getResources().getString(R.string.loading_weather);
        showNotAvailableAnnoucement(loadingWeahter);

    }

    private void showNotAvailableAnnoucement(String message) {
        lnNotAvailableAnnoucement.post(() -> {
            tvAnnoucementMessage.setText(message);
            lnNotAvailableAnnoucement.setVisibility(View.VISIBLE);
            lnRootShow.setVisibility(View.GONE);
        });
    }

    private void setTvLocationName(String locationName) {
        tvLocationName.post(() -> tvLocationName.setText(locationName));
    }

    public void setTvTemprature(String temprature) {
        this.tvTemprature.post(() -> tvTemprature.setText(temprature));
    }

    public void setTvHumidity(String humidity) {
        this.tvHumidity.post(() -> tvHumidity.setText(humidity));
    }

    public void setTvWinSpeech(String winSpeech) {
        this.tvWinSpeech.post(() -> tvWinSpeech.setText(winSpeech));
    }

    public void setTvForecastMessage(String forecastMessage) {
        this.tvForecastMessage.post(() -> tvForecastMessage.setText(forecastMessage));
    }
}
