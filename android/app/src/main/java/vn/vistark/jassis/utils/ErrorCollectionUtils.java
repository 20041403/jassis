package vn.vistark.jassis.utils;

import android.os.Build;
import android.util.Log;

import java.util.Date;

import vn.vistark.jassis.data.runtime.model.User;

public class ErrorCollectionUtils {
    private int userId;
    private String deveice;
    private String hardware;
    private String product;
    private String baseOs;
    private int sdkInt;
    private String buildCodeName;
    private String errorDescription;
    private Date current;

    public ErrorCollectionUtils(String TAG, String error) {
        this.userId = User.current.getUaId();
        this.deveice = Build.DEVICE;
        this.hardware = Build.HARDWARE;
        this.product = Build.PRODUCT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.baseOs = Build.VERSION.BASE_OS;
        }
        this.sdkInt = Build.VERSION.SDK_INT;
        this.buildCodeName = Build.VERSION.CODENAME;
        this.errorDescription = error;
        this.current = new Date();
        Log.e(TAG, "ErrorCollectionUtils: " + error);
    }

}
