package vn.vistark.jassis.utils.calendar_feature;

import java.util.Calendar;

public final class MonthUtils {
    public static String getCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        return (calendar.get(Calendar.MONTH) + 1) + "";
    }

    public static String getMonth(Calendar calendar) {
        return (calendar.get(Calendar.MONTH) + 1) + "";
    }
}
