package vn.vistark.jassis.data.network.model.received_phrase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GottedPhrase {
    @SerializedName("p_phrase")
    @Expose
    private String pPhrase;
    @SerializedName("p_create_at")
    @Expose
    private String pCreateAt;
    @SerializedName("p_create_by")
    @Expose
    private String pCreateBy;
    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("p_tsv")
    @Expose
    private String pTsv;
    @SerializedName("p_id")
    @Expose
    private String pId;

    public String getPPhrase() {
        return pPhrase;
    }

    public void setPPhrase(String pPhrase) {
        this.pPhrase = pPhrase;
    }

    public String getPCreateAt() {
        return pCreateAt;
    }

    public void setPCreateAt(String pCreateAt) {
        this.pCreateAt = pCreateAt;
    }

    public String getPCreateBy() {
        return pCreateBy;
    }

    public void setPCreateBy(String pCreateBy) {
        this.pCreateBy = pCreateBy;
    }

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getPTsv() {
        return pTsv;
    }

    public void setPTsv(String pTsv) {
        this.pTsv = pTsv;
    }

    public String getPId() {
        return pId;
    }

    public void setPId(String pId) {
        this.pId = pId;
    }
}
