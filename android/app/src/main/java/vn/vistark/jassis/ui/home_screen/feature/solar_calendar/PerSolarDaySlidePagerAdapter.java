package vn.vistark.jassis.ui.home_screen.feature.solar_calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PerSolarDaySlidePagerAdapter extends FragmentStatePagerAdapter {
    private int clicked = 0;
    private long firstClickedMillis = -1;

    PerSolarDaySlidePagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return PerSolarDayFragment.newInstance(position - (int) (SolarCalendarConfig.MAX_NEXT_DAYS / 2));
    }

    @Override
    public int getCount() {
        return SolarCalendarConfig.MAX_NEXT_DAYS;
    }
}
