package vn.vistark.jassis.data.network.model.received_phrase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FindResult {
    @SerializedName("userTalked")
    @Expose
    private String userTalked;
    @SerializedName("gottedAgent")
    @Expose
    private GottedAgent gottedAgent;
    @SerializedName("gottedIntent")
    @Expose
    private GottedIntent gottedIntent;
    @SerializedName("gottedPhrase")
    @Expose
    private GottedPhrase gottedPhrase;
    @SerializedName("gottedResponse")
    @Expose
    private GottedResponse gottedResponse;
    @SerializedName("gottedResponseAudio")
    @Expose
    private String gottedResponseAudio;

    public String getUserTalked() {
        return userTalked;
    }

    public void setUserTalked(String userTalked) {
        this.userTalked = userTalked;
    }

    public GottedAgent getGottedAgent() {
        return gottedAgent;
    }

    public void setGottedAgent(GottedAgent gottedAgent) {
        this.gottedAgent = gottedAgent;
    }

    public GottedIntent getGottedIntent() {
        return gottedIntent;
    }

    public void setGottedIntent(GottedIntent gottedIntent) {
        this.gottedIntent = gottedIntent;
    }

    public GottedPhrase getGottedPhrase() {
        return gottedPhrase;
    }

    public void setGottedPhrase(GottedPhrase gottedPhrase) {
        this.gottedPhrase = gottedPhrase;
    }

    public GottedResponse getGottedResponse() {
        return gottedResponse;
    }

    public void setGottedResponse(GottedResponse gottedResponse) {
        this.gottedResponse = gottedResponse;
    }

    public String getGottedResponseAudio() {
        return gottedResponseAudio;
    }

    public void setGottedResponseAudio(String gottedResponseAudio) {
        this.gottedResponseAudio = gottedResponseAudio;
    }
}
