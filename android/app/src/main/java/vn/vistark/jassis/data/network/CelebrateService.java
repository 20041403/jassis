package vn.vistark.jassis.data.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import vn.vistark.jassis.data.network.model.celebrate.CelebrateResponse;

public interface CelebrateService {
    @GET("/celebrate/find")
    Call<CelebrateResponse> find(@Query("date") String date);

}
