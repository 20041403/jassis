package vn.vistark.jassis.data.network;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import vn.vistark.jassis.data.network.model.received_phrase.FindBody;
import vn.vistark.jassis.data.network.model.received_phrase.FindResponse;
import vn.vistark.jassis.data.network.model.received_phrase.TextToSpeechResponse;

public interface FindResponseService {
    @POST("/received_phrase/find")
    Call<FindResponse> findResponse(@Body FindBody data);

    @GET("/received_phrase/text_to_speech")
    Call<TextToSpeechResponse> textToSpeechResponse(@Query("msg") String msg);
}
