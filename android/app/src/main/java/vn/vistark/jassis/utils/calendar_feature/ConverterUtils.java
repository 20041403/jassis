package vn.vistark.jassis.utils.calendar_feature;

import java.util.Calendar;

import vn.vistark.jassis.utils.Number;

import static vn.vistark.jassis.utils.calendar_feature.MoonUtils.LunarYear;

public final class ConverterUtils {
    private final static String TAG = ConverterUtils.class.getSimpleName();

    // Đổi dương lịch sang âm lịch
    public static int[] SolarToLunar(Calendar calendar) {
        return SolarToLunar(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
    }

    public static int[] SolarToLunar(int D, int M, int Y) {
        int yy = Y;
        int[][] ly = LunarYear(Y); // Please cache the result of this computation for later use!!!
        int[] month11 = ly[ly.length - 1];
        double jdToday = UniversalToJuliusDayWitGMT(D, M, Y, 7.0);
        double jdMonth11 = UniversalToJuliusDayWitGMT(month11[0], month11[1], month11[2], 7.0);
        if (jdToday >= jdMonth11) {
            ly = LunarYear(Y + 1);
            yy = Y + 1;
        }
        int i = ly.length - 1;
        while (jdToday < UniversalToJuliusDayWitGMT(ly[i][0], ly[i][1], ly[i][2], 7.0)) {
            i--;
        }
        int dd = (int) (jdToday - UniversalToJuliusDayWitGMT(ly[i][0], ly[i][1], ly[i][2], 7.0)) + 1;
        int mm = ly[i][3];
        if (mm >= 11) {
            yy--;
        }

        return new int[]{dd, mm, yy, ly[i][4]};
    }

    // Đổi âm lịch sang dương lịch
    public static int[] LunarToSolar(int D, int M, int Y, int leap) {
        int yy = Y;
        if (M >= 11) {
            yy = Y + 1;
        }
        int[][] lunarYear = LunarYear(yy);
        int[] lunarMonth = null;
        for (int i = 0; i < lunarYear.length; i++) {
            int[] lm = lunarYear[i];
            if (lm[3] == M && lm[4] == leap) {
                lunarMonth = lm;
                break;
            }
        }
        if (lunarMonth != null) {
            double jd = UniversalToJuliusDayWitGMT(lunarMonth[0], lunarMonth[1], lunarMonth[2], 7.0);
            return JuliusDayToUniversalWithGMT(jd + D - 1, 7.0);
        } else {
            throw new RuntimeException("Incorrect input!");
        }
    }

    // Đổi ngày dương lịch ra số ngày Julius GMT
    public static double UniversalToJuliusDay(int day, int month, int year) {
        double juliusDay;
        if (year > 1582 || (year == 1582 && month > 10) || (year == 1582 && month == 10 && day > 14)) {
            juliusDay = 367 * year - Number.INT(7 * (year + Number.INT((month + 9) / 12)) / 4) - Number.INT(3 * (Number.INT((year + (month - 9) / 7) / 100) + 1) / 4) + Number.INT(275 * month / 9) + day + 1721028.5;
        } else {
            juliusDay = 367 * year - Number.INT(7 * (year + 5001 + Number.INT((month - 9) / 7)) / 4) + Number.INT(275 * month / 9) + day + 1729776.5;
        }
        return juliusDay;
    }

    public static double UniversalToJuliusDay(Calendar calendar) {
        double juliusDay;
        int
                year = calendar.get(Calendar.YEAR),
                month = calendar.get(Calendar.MONTH) + 1,
                day = calendar.get(Calendar.DAY_OF_MONTH);
        if (year > 1582 || (year == 1582 && month > 10) || (year == 1582 && month == 10 && day > 14)) {
            juliusDay = 367 * year - Number.INT(7 * (year + Number.INT((month + 9) / 12)) / 4) - Number.INT(3 * (Number.INT((year + (month - 9) / 7) / 100) + 1) / 4) + Number.INT(275 * month / 9) + day + 1721028.5;
        } else {
            juliusDay = 367 * year - Number.INT(7 * (year + 5001 + Number.INT((month - 9) / 7)) / 4) + Number.INT(275 * month / 9) + day + 1729776.5;
        }
        return juliusDay;
    }

    // Đổi ngày dương lịch ra số ngày Julius GMT+X
    public static double UniversalToJuliusDayWitGMT(Calendar calendar, double GMT) {
        return UniversalToJuliusDay(calendar) - GMT / 24.0;
    }

    public static double UniversalToJuliusDayWitGMT(int day, int month, int year, double GMT) {
        return UniversalToJuliusDay(day, month, year) - GMT / 24.0;
    }

    // Đổi ngày Julius ra ngày dương lịch GMT+X
    public static int[] JuliusDayToUniversalWithGMT(double juliusDay, double GMT) {
        return JuliusDayToUniversal(juliusDay + GMT / 24.0);
    }

    // Đổi ngày Julius ra ngày dương lịch GMT
    public static int[] JuliusDayToUniversal(double juliusDay) {
        int Z, A, alpha, B, C, D, E, dd, mm, yyyy;
        double F;
        Z = Number.INT(juliusDay + 0.5);
        F = (juliusDay + 0.5) - Z;
        if (Z < 2299161) {
            A = Z;
        } else {
            alpha = Number.INT((Z - 1867216.25) / 36524.25);
            A = Z + 1 + alpha - Number.INT(alpha / 4);
        }
        B = A + 1524;
        C = Number.INT((B - 122.1) / 365.25);
        D = Number.INT(365.25 * C);
        E = Number.INT((B - D) / 30.6001);
        dd = Number.INT(B - D - Number.INT(30.6001 * E) + F);
        if (E < 14) {
            mm = E - 1;
        } else {
            mm = E - 13;
        }
        if (mm < 3) {
            yyyy = C - 4715;
        } else {
            yyyy = C - 4716;
        }
        return new int[]{dd, mm, yyyy};
    }
}
