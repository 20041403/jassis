package vn.vistark.jassis.data.runtime.model;

import vn.vistark.jassis.data.network.model.weather_darksky.WeatherResponse;

public class Weather {
    public final static long MAX_WEATHER_LIFE = 30*60*1000; // 30 phút
    public static WeatherResponse gottedWeatherResponse;
    public static long lastedRequest = -1;
}
