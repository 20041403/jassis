package vn.vistark.jassis.data.runtime.model;

import androidx.fragment.app.Fragment;

public class Feature {
    private Fragment featureFragment;
    private int featureDrawableIcon;
    private String featureName;

    public Feature(Fragment featureFragment, int featureDrawableIcon, String featureName) {
        this.featureFragment = featureFragment;
        this.featureDrawableIcon = featureDrawableIcon;
        this.featureName = featureName;
    }

    public Fragment getFeatureFragment() {
        return featureFragment;
    }

    public void setFeatureFragment(Fragment featureFragment) {
        this.featureFragment = featureFragment;
    }

    public int getFeatureDrawableIcon() {
        return featureDrawableIcon;
    }

    public void setFeatureDrawableIcon(int featureDrawableIcon) {
        this.featureDrawableIcon = featureDrawableIcon;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }
}
