package vn.vistark.jassis.ui.login_screen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import es.dmoral.toasty.Toasty;
import vn.vistark.jassis.R;
import vn.vistark.jassis.utils.ExitUtils;
import vn.vistark.jassis.utils.PermissionUtils;
import vn.vistark.jassis.utils.ScreenUtils;

public class LoginActivity extends AppCompatActivity {
    LoginPresenter mPresenter;

    private LinearLayout lnSplashLoading;
    private RelativeLayout rlLoginInput;
    private EditText edtUsernameOrEmail;
    private EditText edtPassword;
    private TextView tvForgotPassword;
    private Button btnLogin;
    private TextView tvRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ScreenUtils.hideTitleBarAndTransparentStatusBar(this);
        initPresenter();
        initViews();
        initEvents();
    }

    private void initEvents() {
        btnLoginEvents();
    }

    private void btnLoginEvents() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPresenter.ValidateInput(edtUsernameOrEmail.getText().toString(), edtPassword.getText().toString())) {
                    mPresenter.Login(edtUsernameOrEmail.getText().toString(), edtPassword.getText().toString());
                } else {
                    // Nếu validate không thành công, thì bỏ qua, không làm gì
                }
            }
        });
    }

    private void initPresenter() {
        mPresenter = new LoginPresenter(this);
    }

    private void initViews() {
        lnSplashLoading = findViewById(R.id.splash_loading);
        rlLoginInput = findViewById(R.id.login_input);
        edtUsernameOrEmail = findViewById(R.id.edtUsernameOrEmail);
        edtPassword = findViewById(R.id.edtPassoword);
        tvForgotPassword = findViewById(R.id.tvForgorPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvRegister = findViewById(R.id.tvRegister);
    }

    public void showSplash() {
        lnSplashLoading.post(() -> lnSplashLoading.setVisibility(View.VISIBLE));
        rlLoginInput.post(() -> rlLoginInput.setVisibility(View.GONE));
    }

    public void showLogin() {
        lnSplashLoading.post(() -> lnSplashLoading.setVisibility(View.GONE));
        rlLoginInput.post(() -> rlLoginInput.setVisibility(View.VISIBLE));
    }

    public void showLoginWithTimer(long DelayMilliSeconds) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                showLogin(); // Chờ 1 khoản thời gian rồi hiển thị khung đăng nhập
            }
        }, DelayMilliSeconds);
    }

    @Override
    public void onBackPressed() {
        ExitUtils.Process(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PermissionUtils.isGrantedAll(requestCode, grantResults)) {
            mPresenter.ControlSplash();
        } else {
            Toasty.warning(this, "Vui lòng cung cấp đầy đủ các quyền để ứng dụng có thể hoạt động.", Toasty.LENGTH_SHORT, false).show();
            PermissionUtils.RequestAllPermission(this);
        }
    }
}
