package vn.vistark.jassis.data.network.model.received_phrase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GottedResponse {
    @SerializedName("r_response")
    @Expose
    private String rResponse;
    @SerializedName("i_id")
    @Expose
    private String iId;
    @SerializedName("r_create_at")
    @Expose
    private String rCreateAt;
    @SerializedName("r_create_by")
    @Expose
    private Object rCreateBy;
    @SerializedName("r_event")
    @Expose
    private Object rEvent;
    @SerializedName("r_tsv")
    @Expose
    private String rTsv;
    @SerializedName("r_id")
    @Expose
    private String rId;

    public String getRResponse() {
        return rResponse;
    }

    public void setRResponse(String rResponse) {
        this.rResponse = rResponse;
    }

    public String getIId() {
        return iId;
    }

    public void setIId(String iId) {
        this.iId = iId;
    }

    public String getRCreateAt() {
        return rCreateAt;
    }

    public void setRCreateAt(String rCreateAt) {
        this.rCreateAt = rCreateAt;
    }

    public Object getRCreateBy() {
        return rCreateBy;
    }

    public void setRCreateBy(Object rCreateBy) {
        this.rCreateBy = rCreateBy;
    }

    public Object getREvent() {
        return rEvent;
    }

    public void setREvent(Object rEvent) {
        this.rEvent = rEvent;
    }

    public String getRTsv() {
        return rTsv;
    }

    public void setRTsv(String rTsv) {
        this.rTsv = rTsv;
    }

    public String getRId() {
        return rId;
    }

    public void setRId(String rId) {
        this.rId = rId;
    }
}
