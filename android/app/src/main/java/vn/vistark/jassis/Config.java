package vn.vistark.jassis;

import android.content.Context;

import vn.vistark.jassis.data.prefs.Prefs;

public final class Config {

    public static final String BASE_VISTARK_API_URL = "http://45.76.188.27:2000";
    public static final long DEFAULT_SPLASH_SCREEN_TIMER = 2000;

    public static class Location {
        public final static int LOCATION_REFRESH_TIME = 100;
        public final static float LOCATION_REFRESH_DISTANCE = 1;
    }

    private Config() {

    }

    public static void init(Context mContext) {
        // Khởi tạo các thành phần sharedPreferences có sẵn
        new Prefs(mContext);
        // Khởi tạo bộ phát âm thanh cơ bản

    }
}
