package vn.vistark.jassis.ui.home_screen.feature.lunar_calendar;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.Calendar;

import vn.vistark.jassis.R;
import vn.vistark.jassis.utils.calendar_feature.ConverterUtils;
import vn.vistark.jassis.utils.calendar_feature.DateUtils;
import vn.vistark.jassis.utils.calendar_feature.DayUtils;
import vn.vistark.jassis.utils.calendar_feature.MoonUtils;
import vn.vistark.jassis.utils.calendar_feature.YearUtils;

public class PerLunarDayFragment extends Fragment {
    private static final String ARG_CALENDAR_NEXT = "ARG_CALENDAR_NEXT";

    private int mCalendarNext;

    private TextView tvDayName;
    private TextView tvDateNumber;
    private TextView tvCelebrateName;
    private TextView tvLunarDayName;
    private TextView tvDayTimeAbsolute;
    private TextView tvLunarMonthNumber;
    private TextView tvLunarMonthName;
    private TextView tvLunarYearName;
    private TextView tvLunarYearNumber;

    private LinearLayout lnDayPerDayEventBtn;

    private long firstClickedMillis = 0;

    public PerLunarDayFragment() {
    }

    public static PerLunarDayFragment newInstance(int calendarNext) {
        PerLunarDayFragment fragment = new PerLunarDayFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CALENDAR_NEXT, calendarNext);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCalendarNext = getArguments().getInt(ARG_CALENDAR_NEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_per_lunar_day, container, false);
        iniViews(v);
        initDatas();
        return v;
    }

    private void iniViews(View v) {
        tvDayName = v.findViewById(R.id.dayName);
        tvDateNumber = v.findViewById(R.id.dateNumber);
        tvCelebrateName = v.findViewById(R.id.celebrateName);
        tvLunarDayName = v.findViewById(R.id.lunarDayName);
        tvDayTimeAbsolute = v.findViewById(R.id.dayTimeAbsolute);
        tvLunarMonthNumber = v.findViewById(R.id.lunarMonthNumber);
        lnDayPerDayEventBtn = v.findViewById(R.id.dayPerDayEventBtn);
        tvLunarMonthName = v.findViewById(R.id.lunarMonthName);
        tvLunarYearName = v.findViewById(R.id.lunarYearName);
        tvLunarYearNumber = v.findViewById(R.id.lunarYearNumber);

        lnDayPerDayEventBtn.setOnClickListener(view -> {
            if (System.currentTimeMillis() - firstClickedMillis < 1000) {
                LunarCalendarFragment.updateDayPosition(0);
            }
            firstClickedMillis = System.currentTimeMillis();
        });
    }

    @SuppressLint("DefaultLocale")
    private void initDatas() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, mCalendarNext);

        int[] lunarCalendar = ConverterUtils.SolarToLunar(calendar);

        tvDayName.setText(DayUtils.getDayName(calendar));
        tvDateNumber.setText(String.format("%d", lunarCalendar[0]));
        tvLunarMonthNumber.setText(String.format("%d", lunarCalendar[1]));
        tvLunarMonthName.setText(MoonUtils.getLunarMonthName(lunarCalendar[1], lunarCalendar[2]));
        tvLunarYearName.setText(YearUtils.getYearName(lunarCalendar[2]));
        tvLunarYearNumber.setText(String.format("%d", lunarCalendar[2]));

        tvCelebrateName.setText("");
        tvLunarDayName.setText(MoonUtils.getLunarDayName(calendar));
        tvDayTimeAbsolute.setText(DateUtils.getAbsoluteDayString(mCalendarNext).toUpperCase());
    }

}
