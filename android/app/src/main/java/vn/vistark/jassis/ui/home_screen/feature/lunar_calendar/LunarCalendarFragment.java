package vn.vistark.jassis.ui.home_screen.feature.lunar_calendar;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import vn.vistark.jassis.R;

public class LunarCalendarFragment extends Fragment {
    private PagerAdapter pagerAdapterPerDayShow;

    public static ViewPager vpPerDay;

    public LunarCalendarFragment() {
    }

    public static Fragment newInstance() {
        return new LunarCalendarFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lunar_calendar, container, false);
        initViews(v);
        return v;
    }

    private void initViews(View v) {
        vpPerDay = v.findViewById(R.id.showPerDay);
        pagerAdapterPerDayShow = new PerLunarDaySlidePagerAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        vpPerDay.setAdapter(pagerAdapterPerDayShow);
        vpPerDay.setCurrentItem(((int) LunarCalendarConfig.MAX_NEXT_DAYS / 2), false);
    }

    public static void updateDayPosition(int pos) {
        vpPerDay.setCurrentItem(((int) LunarCalendarConfig.MAX_NEXT_DAYS / 2) - pos, true);
    }

}
