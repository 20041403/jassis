package vn.vistark.jassis.utils.speech_recognizer;

import android.os.Bundle;
import android.speech.SpeechRecognizer;

public interface SpeechRecognizerCallback {
    void onFinishInitializer();
    void onStartListentSuccess();
    void onRmsChanged(float v);
    void onEndOfSpeech();
    void onError(int error);
    void onResults(Bundle bundle);
    void onPartialResults(Bundle partialResults);
}
