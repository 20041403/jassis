package vn.vistark.jassis.data.network;

import vn.vistark.jassis.Config;

/**
 * Tiến hành public các API
 */

public class VistarkAPI {
    // Đẩy về danh sách dịch vụ của các mục API
    public static UserAccountService getAccountService() {
        return RetrofitConfig.getClient(Config.BASE_VISTARK_API_URL).create(UserAccountService.class);
    }

    public static FindResponseService getFindResponseService() {
        return RetrofitConfig.getClient(Config.BASE_VISTARK_API_URL).create(FindResponseService.class);
    }

    public static CelebrateService getCelebrateService() {
        return RetrofitConfig.getClient(Config.BASE_VISTARK_API_URL).create(CelebrateService.class);
    }

    public static WeatherDarkSkyService getWeatherDarkSkyService() {
        return RetrofitConfig.getClient(Config.BASE_VISTARK_API_URL).create(WeatherDarkSkyService.class);
    }
}
