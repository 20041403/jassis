package vn.vistark.jassis.utils.calendar_feature;

import java.util.Calendar;

import vn.vistark.jassis.utils.Number;

public final class MoonUtils {
    public final static String TAG = MoonUtils.class.getSimpleName();

    public final static String zodiacFirst[] = {"Giáp", "Ất", "Bính", "Đinh", "Mậu", "Kỷ", "Canh", "Tân", "Nhâm", "Quý"};
    public final static String zodiacSecond[] = {"Tý", "Sửu", "Dần", "Mão", "Thìn", "Tị", "Ngọ", "Mùi", "Thân", "Dậu", "Tuất", "Hợi"};
    public final static String zodiacSecondForLunarMonthName[] = {"Dần", "Mão", "Thìn", "Tị", "Ngọ", "Mùi", "Thân", "Dậu", "Tuất", "Hợi", "Tý", "Sửu"};

    // Tính điểm Sóc (tức Hội diện) rơi vào ngày nào.  Thuật toán sau cho phép tính thời điểm (tính bằng số ngày Julius) của Sóc thứ k tính từ điểm Sóc lúc 13:51 GMT ngày 1/1/1900 (ngày Julius 2415021.076998695)
    public static double NewMoon(int k) {
        double T = k / 1236.85; // Time in Julian centuries from 1900 January 0.5
        double T2 = T * T;
        double T3 = T2 * T;
        double dr = Math.PI / 180;
        double Jd1 = 2415020.75933 + 29.53058868 * k + 0.0001178 * T2 - 0.000000155 * T3;
        Jd1 = Jd1 + 0.00033 * Math.sin((166.56 + 132.87 * T - 0.009173 * T2) * dr); // Mean new moon
        double M = 359.2242 + 29.10535608 * k - 0.0000333 * T2 - 0.00000347 * T3; // Sun's mean anomaly
        double Mpr = 306.0253 + 385.81691806 * k + 0.0107306 * T2 + 0.00001236 * T3; // Moon's mean anomaly
        double F = 21.2964 + 390.67050646 * k - 0.0016528 * T2 - 0.00000239 * T3; // Moon's argument of latitude
        double C1 = (0.1734 - 0.000393 * T) * Math.sin(M * dr) + 0.0021 * Math.sin(2 * dr * M);
        C1 = C1 - 0.4068 * Math.sin(Mpr * dr) + 0.0161 * Math.sin(dr * 2 * Mpr);
        C1 = C1 - 0.0004 * Math.sin(dr * 3 * Mpr);
        C1 = C1 + 0.0104 * Math.sin(dr * 2 * F) - 0.0051 * Math.sin(dr * (M + Mpr));
        C1 = C1 - 0.0074 * Math.sin(dr * (M - Mpr)) + 0.0004 * Math.sin(dr * (2 * F + M));
        C1 = C1 - 0.0004 * Math.sin(dr * (2 * F - M)) - 0.0006 * Math.sin(dr * (2 * F + Mpr));
        C1 = C1 + 0.0010 * Math.sin(dr * (2 * F - Mpr)) + 0.0005 * Math.sin(dr * (2 * Mpr + M));
        double deltat;
        if (T < -11) {
            deltat = 0.001 + 0.000839 * T + 0.0002261 * T2 - 0.00000845 * T3 - 0.000000081 * T * T3;
        } else {
            deltat = -0.000278 + 0.000265 * T + 0.000262 * T2;
        }
        ;
        return Jd1 + C1 - deltat;
    }

    // Tính vị trí mặt trời tại một thời điểm
    // Dùng hàm NewMoon ta đã xác định được các ngày chứa điểm Sóc, như thế ta biết ngày bắt đầu các tháng âm lịch. Để biết tên các tháng này và biết liệu chúng có phải tháng nhuận không chúng ta cần dựa vào các qui tắc: (1) tháng 11 là tháng chứa Đông chí và (2) tháng nhuận là tháng đầu tiên không chứa Trung khí trong năm nhuận. Để tính xem tháng âm lịch nào chứa Đông chí và tháng vào không chứa Trung khí chúng ta tính vị trí mặt trời trên đường hoàng đạo tại các điểm bắt đầu tháng âm lịch và so sánh chúng với các tọa độ định nghĩa các điểm Trung khí.
    // Thuật toán sau cho phép tính vị trí của mặt trời trên quĩ đạo của nó tại một thời điểm bất kỳ (được thể hiện bằng số ngày Julius của thời điểm đó). Kết quả mà thuật toán trả lại là một góc Rađian giữa 0 và 2*PI. Vị trí mặt trời được sử dụng để chia năm thời tiết thành 24 Khí (12 Tiết khí và 12 Trung khí). Một Trung khí là thời điểm mà kinh độ mặt trời có một trong những giá trị sau: 0*PI/6, 1*PI/6, 2*PI/6, 3*PI/6, 4*PI/6, 5*PI/6, 6*PI/6, 7*PI/6, 8*PI/6, 9*PI/6, 10*PI/6, 11*PI/6. Các điểm "Phân-Chí" được định nghĩa bằng các tọa độ sau: Xuân phân: 0, Hạ chí: PI/2, Thu phân: PI, Đông chí: 3*PI/2.
    public static double SunLongitude(double jdn) {
        double T = (jdn - 2451545.0) / 36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
        double T2 = T * T;
        double dr = Math.PI / 180; // degree to radian
        double M = 357.52910 + 35999.05030 * T - 0.0001559 * T2 - 0.00000048 * T * T2; // mean anomaly, degree
        double L0 = 280.46645 + 36000.76983 * T + 0.0003032 * T2; // mean longitude, degree
        double DL = (1.914600 - 0.004817 * T - 0.000014 * T2) * Math.sin(dr * M);
        DL = DL + (0.019993 - 0.000101 * T) * Math.sin(dr * 2 * M) + 0.000290 * Math.sin(dr * 3 * M);
        double L = L0 + DL; // true longitude, degree
        L = L * dr;
        L = L - Math.PI * 2 * (Number.INT(L / (Math.PI * 2))); // Normalize to (0, 2*PI)
        return L;
    }

    // Tính tháng âm lịch chứa ngày đông chí
    // Đông chí của một năm y thường rơi vào khoảng ngày 20-22 tháng 12 năm đó. Chúng ta nhớ lại rằng Đông chí là thời điểm mà kinh độ của mặt trời trên đường hoàng đạo là 3*PI/2. Như vậy để tính tháng âm lịch chứa Đông chí ta có thể dùng phương pháp sau:
    public static int[] LunarMonth11(int Y) {
        double off = ConverterUtils.UniversalToJuliusDayWitGMT(31, 12, Y, 7.0) - 2415021.076998695;
        int k = Number.INT(off / 29.530588853);
        double jd = NewMoon(k);
        int[] ret = ConverterUtils.JuliusDayToUniversalWithGMT(jd, 7.0);
        double sunLong = SunLongitude(ConverterUtils.UniversalToJuliusDayWitGMT(ret[0], ret[1], ret[2], 7.0)); // sun longitude at local midnight
        if (sunLong > 3 * Math.PI / 2) {
            jd = NewMoon(k - 1);
        }
        return ConverterUtils.JuliusDayToUniversalWithGMT(jd, 7.0);
    }

    // Đổi ngày dương lịch ra âm lịch và ngược lại
    // Tính năm âm lịch
    // Để thuận tiện cho việc chuyển đổi ngày cho năm âm lịch Y (năm mà ngày Tết rơi vào năm dương lịch Y) chúng ta tạo một bảng với cấu trúc như sau: bảng có 5 hàng và 13 cột (cho năm âm lịch thường) hoặc 14 cột (cho năm nhuận). Mỗi cột chứa 5 số nguyên: DD, MM, YY, NM, LEAP với ý nghĩa sau: ngày dương lịch DD/MM/YY là ngày bắt đầu tháng âm lịch; NM là tên của tháng âm đó (1 đến 12), và LEAP là 1 nếu tháng âm lịch đó là tháng nhuận và 1 nếu tháng đó là tháng thường. Cột đầu tiên thể hiện tháng âm lịch chứa Đông chí năm Y-1 và cột cuối cùng là tháng chứa Đông chí năm Y.
    public static final double[] SUNLONG_MAJOR = new double[]{
            0, Math.PI / 6, 2 * Math.PI / 6, 3 * Math.PI / 6, 4 * Math.PI / 6, 5 * Math.PI / 6, Math.PI, 7 * Math.PI / 6, 8 * Math.PI / 6, 9 * Math.PI / 6, 10 * Math.PI / 6, 11 * Math.PI / 6
    };

    public static int[][] LunarYear(int Y) {
        int[][] ret = null;
        int[] month11A = LunarMonth11(Y - 1);
        double jdMonth11A = ConverterUtils.UniversalToJuliusDayWitGMT(month11A[0], month11A[1], month11A[2], 7.0);
        int k = Number.INT(0.5 + (jdMonth11A - 2415021.076998695) / 29.530588853);
        int[] month11B = LunarMonth11(Y);
        double off = ConverterUtils.UniversalToJuliusDayWitGMT(month11B[0], month11B[1], month11B[2], 7.0) - jdMonth11A;
        boolean leap = off > 365.0;
        if (!leap) {
            ret = new int[13][5];
        } else {
            ret = new int[14][5];
        }
        ret[0] = new int[]{month11A[0], month11A[1], month11A[2], 0, 0};
        ret[ret.length - 1] = new int[]{month11B[0], month11B[1], month11B[2], 0, 0};
        for (int i = 1; i < ret.length - 1; i++) {
            double nm = NewMoon(k + i);
            int[] a = ConverterUtils.JuliusDayToUniversalWithGMT(nm, 7.0);
            ret[i] = new int[]{a[0], a[1], a[2], 0, 0};
        }
        for (int i = 0; i < ret.length; i++) {
            ret[i][3] = Number.MOD(i + 11, 12);
        }
        if (leap) {
            initLeapYear(ret);
        }
        return ret;
    }

    private static void initLeapYear(int[][] ret) {
        double[] sunLongitudes = new double[ret.length];
        for (int i = 0; i < ret.length; i++) {
            int[] a = ret[i];
            double jdAtMonthBegin = ConverterUtils.UniversalToJuliusDayWitGMT(a[0], a[1], a[2], 7.0);
            sunLongitudes[i] = SunLongitude(jdAtMonthBegin);
        }
        boolean found = false;
        for (int i = 0; i < ret.length; i++) {
            if (found) {
                ret[i][3] = Number.MOD(i + 10, 12);
                continue;
            }
            double sl1 = sunLongitudes[i];
            double sl2 = sunLongitudes[i + 1];
            boolean hasMajorTerm = Math.floor(sl1 / Math.PI * 6) != Math.floor(sl2 / Math.PI * 6);
            if (!hasMajorTerm) {
                found = true;
                ret[i][4] = 1;
                ret[i][3] = Number.MOD(i + 10, 12);
            }
        }
    }

    public static String getLunarDayName(Calendar calendar) {
        return zodiacFirst[Number.INT(ConverterUtils.UniversalToJuliusDay(calendar) + 9.5) % 10] + " " +
                zodiacSecond[Number.INT(ConverterUtils.UniversalToJuliusDay(calendar) + 1.5) % 12];
    }

    public static String getLunarMonthName(int lunarMonth, int lunarYear) {
        return zodiacFirst[(lunarYear * 12 + lunarMonth + 3) % 10] + " " +
                zodiacSecondForLunarMonthName[lunarMonth - 1];
    }

}
