package vn.vistark.jassis.data.network.model.user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }
    public class Result {

        @SerializedName("up_surename")
        @Expose
        private String upSurename;
        @SerializedName("up_name")
        @Expose
        private String upName;
        @SerializedName("up_birthday")
        @Expose
        private String upBirthday;
        @SerializedName("up_avatar_addr")
        @Expose
        private String upAvatarAddr;
        @SerializedName("up_phone")
        @Expose
        private String upPhone;
        @SerializedName("up_home_curr_addr")
        @Expose
        private String upHomeCurrAddr;
        @SerializedName("up_home_town_addr")
        @Expose
        private String upHomeTownAddr;
        @SerializedName("up_work_place_addr")
        @Expose
        private String upWorkPlaceAddr;
        @SerializedName("up_id")
        @Expose
        private String upId;
        @SerializedName("up_gender")
        @Expose
        private Integer upGender;

        public String getUpSurename() {
            return upSurename;
        }

        public void setUpSurename(String upSurename) {
            this.upSurename = upSurename;
        }

        public String getUpName() {
            return upName;
        }

        public void setUpName(String upName) {
            this.upName = upName;
        }

        public String getUpBirthday() {
            return upBirthday;
        }

        public void setUpBirthday(String upBirthday) {
            this.upBirthday = upBirthday;
        }

        public String getUpAvatarAddr() {
            return upAvatarAddr;
        }

        public void setUpAvatarAddr(String upAvatarAddr) {
            this.upAvatarAddr = upAvatarAddr;
        }

        public String getUpPhone() {
            return upPhone;
        }

        public void setUpPhone(String upPhone) {
            this.upPhone = upPhone;
        }

        public String getUpHomeCurrAddr() {
            return upHomeCurrAddr;
        }

        public void setUpHomeCurrAddr(String upHomeCurrAddr) {
            this.upHomeCurrAddr = upHomeCurrAddr;
        }

        public String getUpHomeTownAddr() {
            return upHomeTownAddr;
        }

        public void setUpHomeTownAddr(String upHomeTownAddr) {
            this.upHomeTownAddr = upHomeTownAddr;
        }

        public String getUpWorkPlaceAddr() {
            return upWorkPlaceAddr;
        }

        public void setUpWorkPlaceAddr(String upWorkPlaceAddr) {
            this.upWorkPlaceAddr = upWorkPlaceAddr;
        }

        public String getUpId() {
            return upId;
        }

        public void setUpId(String upId) {
            this.upId = upId;
        }

        public Integer getUpGender() {
            return upGender;
        }

        public void setUpGender(Integer upGender) {
            this.upGender = upGender;
        }
    }
}

