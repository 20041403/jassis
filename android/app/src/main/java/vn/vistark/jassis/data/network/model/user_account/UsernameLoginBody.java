package vn.vistark.jassis.data.network.model.user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Lớp này định nghĩa cho phần thân của request body để gửi lên phần đăng  nhập, đồng thời cũng là đối tượng nhận dữ liệu của khung đăng nhập
 */

public class UsernameLoginBody {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}