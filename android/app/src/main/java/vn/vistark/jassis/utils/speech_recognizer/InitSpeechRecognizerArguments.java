package vn.vistark.jassis.utils.speech_recognizer;

import android.speech.SpeechRecognizer;

import androidx.appcompat.app.AppCompatActivity;

public class InitSpeechRecognizerArguments {
    SpeechRecognizerCallback speechRecognizerCallback;
    SpeechRecognizer mSpeechReconizer;
    AppCompatActivity mContext;

    public InitSpeechRecognizerArguments(SpeechRecognizerCallback speechRecognizerCallback, SpeechRecognizer mSpeechReconizer, AppCompatActivity mContext) {
        this.speechRecognizerCallback = speechRecognizerCallback;
        this.mSpeechReconizer = mSpeechReconizer;
        this.mContext = mContext;
    }
}
