package vn.vistark.jassis.data.runtime.model;

import vn.vistark.jassis.data.network.model.user_account.LoginResponse;

public class User {
    public static User current = new User();
    int uaId = -1;
    String uaUsername;
    String usEmail;
    int uaRoles;
    String token;

    public static User ConvertFromLoginResult(LoginResponse.Result result) {
        User user = new User();
        user.setUaId(Integer.parseInt(result.getUaId()));
        user.setUaUsername(result.getUaUsername());
        user.setUsEmail(result.getUaEmail());
        return user;
    }

    public User() {
    }

    public User(int uaId, String uaUsername, String usEmail, int uaRoles, String token) {
        this.uaId = uaId;
        this.uaUsername = uaUsername;
        this.usEmail = usEmail;
        this.uaRoles = uaRoles;
        this.token = token;
    }

    public int getUaId() {
        return uaId;
    }

    public void setUaId(int uaId) {
        this.uaId = uaId;
    }

    public String getUaUsername() {
        return uaUsername;
    }

    public void setUaUsername(String uaUsername) {
        this.uaUsername = uaUsername;
    }

    public String getUsEmail() {
        return usEmail;
    }

    public void setUsEmail(String usEmail) {
        this.usEmail = usEmail;
    }

    public int getUaRoles() {
        return uaRoles;
    }

    public void setUaRoles(int uaRoles) {
        this.uaRoles = uaRoles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
