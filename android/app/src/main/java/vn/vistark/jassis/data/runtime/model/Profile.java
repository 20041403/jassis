package vn.vistark.jassis.data.runtime.model;

import vn.vistark.jassis.data.network.model.user_account.ProfileResponse;

public class Profile {
    public static Profile current;
    int uiId = -1;
    String upSureName;
    String upName;
    String upBirthDay;
    String upAvatarAddress;
    String upPhone;
    String upHomeCurrentAddress;
    String upHomeTownAddress;
    String upWorkPlaceAddress;
    int upGender;

    public static Profile getProfileFromProfileResponse(ProfileResponse.Result result) {
        Profile profile = new Profile(
                Integer.parseInt(result.getUpId()),
                result.getUpSurename(),
                result.getUpName(),
                result.getUpBirthday(),
                result.getUpAvatarAddr(),
                result.getUpPhone(),
                result.getUpHomeCurrAddr(),
                result.getUpHomeTownAddr(),
                result.getUpWorkPlaceAddr(),
                result.getUpGender()
        );
        return profile;
    }

    public Profile(int uiId, String upSureName, String upName, String upBirthDay, String upAvatarAddress, String upPhone, String upHomeCurrentAddress, String upHomeTownAddress, String upWorkPlaceAddress, int upGender) {
        this.uiId = uiId;
        this.upSureName = upSureName;
        this.upName = upName;
        this.upBirthDay = upBirthDay;
        this.upAvatarAddress = upAvatarAddress;
        this.upPhone = upPhone;
        this.upHomeCurrentAddress = upHomeCurrentAddress;
        this.upHomeTownAddress = upHomeTownAddress;
        this.upWorkPlaceAddress = upWorkPlaceAddress;
        this.upGender = upGender;
    }

    public static Profile getCurrent() {
        return current;
    }

    public static void setCurrent(Profile current) {
        Profile.current = current;
    }

    public int getUiId() {
        return uiId;
    }

    public void setUiId(int uiId) {
        this.uiId = uiId;
    }

    public String getUpSureName() {
        return upSureName;
    }

    public void setUpSureName(String upSureName) {
        this.upSureName = upSureName;
    }

    public String getUpName() {
        return upName;
    }

    public void setUpName(String upName) {
        this.upName = upName;
    }

    public String getUpBirthDay() {
        return upBirthDay;
    }

    public void setUpBirthDay(String upBirthDay) {
        this.upBirthDay = upBirthDay;
    }

    public String getUpAvatarAddress() {
        return upAvatarAddress;
    }

    public void setUpAvatarAddress(String upAvatarAddress) {
        this.upAvatarAddress = upAvatarAddress;
    }

    public String getUpPhone() {
        return upPhone;
    }

    public void setUpPhone(String upPhone) {
        this.upPhone = upPhone;
    }

    public String getUpHomeCurrentAddress() {
        return upHomeCurrentAddress;
    }

    public void setUpHomeCurrentAddress(String upHomeCurrentAddress) {
        this.upHomeCurrentAddress = upHomeCurrentAddress;
    }

    public String getUpHomeTownAddress() {
        return upHomeTownAddress;
    }

    public void setUpHomeTownAddress(String upHomeTownAddress) {
        this.upHomeTownAddress = upHomeTownAddress;
    }

    public String getUpWorkPlaceAddress() {
        return upWorkPlaceAddress;
    }

    public void setUpWorkPlaceAddress(String upWorkPlaceAddress) {
        this.upWorkPlaceAddress = upWorkPlaceAddress;
    }

    public int getUpGender() {
        return upGender;
    }

    public void setUpGender(int upGender) {
        this.upGender = upGender;
    }
}
