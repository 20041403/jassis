package vn.vistark.jassis.ui.home_screen.feature;

import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

import vn.vistark.jassis.data.runtime.model.Feature;
import vn.vistark.jassis.ui.home_screen.HomeScreenActivity;
import vn.vistark.jassis.ui.home_screen.feature.lunar_calendar.LunarCalendarFragment;
import vn.vistark.jassis.ui.home_screen.feature.solar_calendar.SolarCalendarFragment;
import vn.vistark.jassis.ui.home_screen.feature.weather.WeatherFragment;

public class FeaturePresenter {
    private HomeScreenActivity mContext;

    public static List<Feature> features;


    private PagerAdapter pagerAdapterFeatureDisplay;

    public FeaturePresenter(HomeScreenActivity mContext) {
        this.mContext = mContext;
        // Init feature list
        features = new ArrayList<>();
        features.add(new Feature(SolarCalendarFragment.newInstance(),-1,"Dương lịch"));
        features.add(new Feature(LunarCalendarFragment.newInstance(),-1,"Âm lịch"));
        features.add(new Feature(WeatherFragment.newInstance(),-1,"Âm lịch"));

        // Init feature display
        pagerAdapterFeatureDisplay = new FeatureSlidePagerAdapter(mContext.getSupportFragmentManager(), FragmentStatePagerAdapter.POSITION_NONE);
        mContext.vpFeatureDisplay.setAdapter(pagerAdapterFeatureDisplay);
    }
}
