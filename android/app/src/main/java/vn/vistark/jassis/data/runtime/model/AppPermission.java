package vn.vistark.jassis.data.runtime.model;

import android.Manifest;

import java.util.ArrayList;
import java.util.List;

public class AppPermission {
    public final static int TOTAL_PERMISSION_REQUEST_CODE = 140398;

    public static String[] getStringsPermission() {
        List<AppPermission> appPermissions = getListInstances();
        String[] permissions = new String[appPermissions.size()];
        for (int i = 0; i < appPermissions.size(); i++) {
            permissions[i] = appPermissions.get(i).getKey();
        }
        return permissions;
    }

    public static List<AppPermission> getListInstances() {
        List<AppPermission> appPermissions = new ArrayList<>();
        int permissionCode = 1000;
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.ACCESS_NETWORK_STATE, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.INTERNET, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.READ_EXTERNAL_STORAGE, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.WRITE_EXTERNAL_STORAGE, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.ACCESS_COARSE_LOCATION, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.ACCESS_FINE_LOCATION, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.READ_CONTACTS, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.RECORD_AUDIO, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.CAMERA, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.CALL_PHONE, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.VIBRATE, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.READ_EXTERNAL_STORAGE, ""));
        appPermissions.add(new AppPermission(permissionCode++, Manifest.permission.WAKE_LOCK, ""));

        return appPermissions;
    }

    private int code = -1;
    private String key = "";
    private String description = "";

    public AppPermission(int code, String key, String description) {
        this.code = code;
        this.key = key;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
