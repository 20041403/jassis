package vn.vistark.jassis.ui.home_screen.feature;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class FeatureSlidePagerAdapter extends FragmentStatePagerAdapter {
    FeatureSlidePagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    public FeatureSlidePagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return FeaturePresenter.features.get(position).getFeatureFragment();
    }

    @Override
    public int getCount() {
        return FeaturePresenter.features.size();
    }
}
