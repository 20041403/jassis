package vn.vistark.jassis.data.network.model.user_account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Đây là phần nhận kết quả trả về của phần xác thực đăng nhập
 */

public class LoginResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("token")
    @Expose
    private String token;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    // -----------------------------
    public class Result {

        @SerializedName("ua_id")
        @Expose
        private String uaId;
        @SerializedName("ua_username")
        @Expose
        private String uaUsername;
        @SerializedName("ua_email")
        @Expose
        private String uaEmail;
        @SerializedName("ua_create_at")
        @Expose
        private String uaCreateAt;
        @SerializedName("ua_update_at")
        @Expose
        private String uaUpdateAt;

        public String getUaId() {
            return uaId;
        }

        public void setUaId(String uaId) {
            this.uaId = uaId;
        }

        public String getUaUsername() {
            return uaUsername;
        }

        public void setUaUsername(String uaUsername) {
            this.uaUsername = uaUsername;
        }

        public String getUaEmail() {
            return uaEmail;
        }

        public void setUaEmail(String uaEmail) {
            this.uaEmail = uaEmail;
        }

        public String getUaCreateAt() {
            return uaCreateAt;
        }

        public void setUaCreateAt(String uaCreateAt) {
            this.uaCreateAt = uaCreateAt;
        }

        public String getUaUpdateAt() {
            return uaUpdateAt;
        }

        public void setUaUpdateAt(String uaUpdateAt) {
            this.uaUpdateAt = uaUpdateAt;
        }

    }
}