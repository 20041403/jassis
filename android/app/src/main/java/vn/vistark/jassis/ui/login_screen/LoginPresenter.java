package vn.vistark.jassis.ui.login_screen;

import android.app.ProgressDialog;
import android.content.Intent;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.vistark.jassis.Config;
import vn.vistark.jassis.data.network.RetrofitConfig;
import vn.vistark.jassis.data.network.VistarkAPI;
import vn.vistark.jassis.data.network.model.user_account.EmailLoginBody;
import vn.vistark.jassis.data.network.model.user_account.LoginResponse;
import vn.vistark.jassis.data.network.model.user_account.ProfileResponse;
import vn.vistark.jassis.data.network.model.user_account.TokenVerifyResponse;
import vn.vistark.jassis.data.network.model.user_account.UsernameLoginBody;
import vn.vistark.jassis.data.prefs.Prefs;
import vn.vistark.jassis.data.runtime.model.CurrentLocation;
import vn.vistark.jassis.data.runtime.model.Profile;
import vn.vistark.jassis.data.runtime.model.User;
import vn.vistark.jassis.ui.home_screen.HomeScreenActivity;
import vn.vistark.jassis.utils.CommonUtils;
import vn.vistark.jassis.utils.CryptoUtis;
import vn.vistark.jassis.utils.ErrorCollectionUtils;
import vn.vistark.jassis.utils.PermissionUtils;

class LoginPresenter {
    private final static String TAG = LoginActivity.class.getSimpleName();

    private LoginActivity mContext;

    private CurrentLocation mCurrentLocation;

    LoginPresenter(LoginActivity mContext) {
        this.mContext = mContext;
        Config.init(mContext);
        PermissionUtils.RequestAllPermission(mContext);
    }

    public void ControlSplash() {
        if (Prefs.GlobalToken.getToken() == null) {
            // Nếu người dùng chưa đăng nhập lần nào, tiến hành cho hiển thị splash screen một thời gian rồi mới chuyển sang màn hình đăng nhập
            mContext.showLoginWithTimer(Config.DEFAULT_SPLASH_SCREEN_TIMER);
        } else {
            // Nếu người dùng đã đăng nhập trước đó, tiến hành kiểm tra token của họ còn hiệu lực không
            RetrofitConfig.reset();
            tokenVerify();
        }
    }

    private void tokenVerify() {
        long startMillis = System.currentTimeMillis();
        VistarkAPI.getAccountService().tokenVerify().enqueue(new Callback<TokenVerifyResponse>() {
            @Override
            public void onResponse(Call<TokenVerifyResponse> call, Response<TokenVerifyResponse> response) {
                if (response.isSuccessful()) {
                    // Nếu thành công
                    if (response.body().getStatus() < 0) {
                        // Nếu trạng thái bé hơn 0, tức xảy ra lỗi, cho đăng nhập lại
                        ContinuteSplashIfNotEnoughDelayTime(startMillis);
                    } else {
                        // Nếu mọi thứ đều ổn, tiến hành lấy các thông tin cần thiết của user_account
                        User.current = new User();
                        User.current.setUaId(Integer.parseInt(response.body().getResult().getId()));
                        User.current.setUsEmail(response.body().getResult().getEmail());
                        User.current.setUaUsername(response.body().getResult().getUsername());

                        // Tiến hành lấy Profile
                        getUserProfile();
                    }
                } else {
                    // Trả về kết quả không thành công thì cho đăng nhập lại
                    ContinuteSplashIfNotEnoughDelayTime(startMillis);
                }
            }

            @Override
            public void onFailure(Call<TokenVerifyResponse> call, Throwable t) {
                ContinuteSplashIfNotEnoughDelayTime(startMillis);
            }
        });
    }

    private void getUserProfile() {
        // Tiến hành cập nhật lại config của Retrofit2
        RetrofitConfig.reset();
        // Hiển thị màn hình splash trong khi tiến hành lấy thông tin người dùng
        mContext.showSplash();
        // Tiến hành lấy profile của người dùng
        VistarkAPI.getAccountService().getProfile(User.current.getUaId()).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful()) {
                    Profile.current = Profile.getProfileFromProfileResponse(response.body().getResult().get(0));
                    if (Profile.current.getUiId() < -1 || Profile.current.getUpName() == null || Profile.current.getUpName().isEmpty()) {
                        Toasty.warning(mContext, "Dường như bạn chưa cập nhật thông tin cá nhân.").show();
                    } else {
//                        Toasty.success(mContext, "Chào " + Profile.current.getUpName() + ", rất vui khi bạn quay lại với hệ thống!" + (++i)).show();
                    }
                    gotoHomeScreen();
                } else {
                    Toasty.error(mContext, "Không lấy được thông tin cơ bản").show();
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Toasty.error(mContext, "Lỗi đường truyền khi cố lấy thông tin cơ bản của bạn").show();
                gotoHomeScreen();
            }
        });
    }

    private void gotoHomeScreen() {
        // Tiến hành thực hiện khởi tạo bộ lấy vị trí
        mCurrentLocation = new CurrentLocation(mContext.getApplicationContext());
        // Sau đó chuyển sang màn hình chính
        Intent homeScreenIntent = new Intent(mContext, HomeScreenActivity.class);
        mContext.startActivity(homeScreenIntent);

        // Kết thúc screen hiện tại
        mContext.finish();
    }

    private void ContinuteSplashIfNotEnoughDelayTime(long startTime) {
        if (System.currentTimeMillis() - startTime < Config.DEFAULT_SPLASH_SCREEN_TIMER) {
            mContext.showLoginWithTimer(
                    Config.DEFAULT_SPLASH_SCREEN_TIMER
                            - (System.currentTimeMillis() - startTime)
            );
        }
    }

    boolean ValidateInput(String usernameOrEmail, String password) {
        if (usernameOrEmail.isEmpty() || password.isEmpty()) {
            Toasty.warning(mContext, "Vui lòng nhập đầy đủ thông tin.").show();
        } else if (password.length() < 8) {
            Toasty.warning(mContext, "Độ dài mật khẩu không hợp lệ.").show();
        } else if (!usernameOrEmail.contains("@") && !usernameOrEmail.matches("[A-Za-z0-9_]+")) {
            Toasty.warning(mContext, "Tên đăng nhập không hợp lệ.").show();
        } else if (usernameOrEmail.contains("@") && !usernameOrEmail.matches("((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])")) {
            Toasty.warning(mContext, "Email không hợp lệ").show();
        } else {
            return true;
        }
        return false;
    }

    void Login(String usernameOrEmail, String password) {
        ProgressDialog progressDialog = CommonUtils.showLoadingDialog(mContext);
        progressDialog.show();
        Call<LoginResponse> loginResponseCall;
        if (usernameOrEmail.contains("@")) {
            // Nếu người dùng sử dụng email để đăng nhập
            EmailLoginBody loginBody = new EmailLoginBody();
            loginBody.setEmail(usernameOrEmail);
            loginBody.setPassword(CryptoUtis.MD5(password));
            loginResponseCall = VistarkAPI.getAccountService().login(loginBody);
        } else {
            // Nếu người dùng dùng tài khoản thường để đăng nhập
            UsernameLoginBody loginBody = new UsernameLoginBody();
            loginBody.setUsername(usernameOrEmail);
            loginBody.setPassword(CryptoUtis.MD5(password));
            loginResponseCall = VistarkAPI.getAccountService().login(loginBody);
        }
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body() != null && response.body().getStatus() > 0) {
                    if (response.isSuccessful()) {
                        // Tiến hành công bố dữ liệu sau khi đăng nhập
                        LoginResponse.Result result = response.body().getResult();
                        User.current = User.ConvertFromLoginResult(result);
                        User.current.setToken(response.body().getToken());

                        // Tiến hành lưu token vào sharedPreferences
                        Prefs.GlobalToken.setToken(User.current.getToken());

                        // Tiến hành lấy Profile
                        getUserProfile();
                    } else {
                        Toasty.error(mContext, "Tài khoản hoặc mật khẩu không đúng.").show();
                    }
                } else {
                    Toasty.error(mContext, "Tài khoản hoặc mật khẩu không đúng.").show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                new ErrorCollectionUtils(TAG, t.getMessage());
                // Đóng màn hình load khi thực hiện xong
                progressDialog.dismiss();
            }
        });
    }
}
