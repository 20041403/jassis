package vn.vistark.jassis.service;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.service.voice.VoiceInteractionSession;
import android.widget.Toast;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

public class VistarkVoiceInteractionSession extends VoiceInteractionSession {

    public VistarkVoiceInteractionSession(Context context) {
        super(context);
    }

    public VistarkVoiceInteractionSession(Context context, Handler handler) {
        super(context, handler);
    }

    @Override
    public void onHandleAssist(@NonNull AssistState state) {
        super.onHandleAssist(state);
        try {
            // Fetch structured data
            JSONObject structuredData =
                    new JSONObject(state.getAssistStructure().toString());

            // Display description as Toast
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Toast.makeText(
                        getContext(),
                        structuredData.optString("description"),
                        Toast.LENGTH_LONG
                ).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
