# THIẾT LẬP VPS
## A. Thông số cấu hình phần cứng của VPS
### 1. Hệ điều hành: Centos 7 x64
### 2. Ram: 1G

## B. Lệnh thiết lập CSDL PostgreSQL
### 1. Cài đặt
````sudo yum install postgresql-server postgresql-contrib````
### 2. Khởi tạo
````sudo postgresql-setup initdb````
### 3. Bắt đầu services
````sudo systemctl start postgresql````
### 4. Mở services mặc định phòng khi mất nguồn
````sudo systemctl enable postgresql````
### 5. Đổi mật khẩu cho user quản trị
````sudo passwd postgres````
### 6. Truy cập lệnh dưới danh nghĩa User này
````su - postgres````
hoặc
````su --shell /bin/bash postgres````
### 7. Đổi mật khẩu cho user quản trị trong csdl
````psql -d template1 -c "ALTER USER postgres WITH PASSWORD 'nghianghia057';"````

## C. Cấu hình để mở remote actions
### 1. Tìm tệp cấu hình của PostgresSQL
````find / -name "postgresql.conf"````
- Kết quả là: vi /var/lib/pgsql/data/postgresql.conf
### 2. Sau đó mở lên
````vi /var/lib/pgsql/data/pg_hba.conf````
### 3. Thêm vào CUỐI CÙNG
````listen_addresses = '*'````
### 3. Mở file pg_hba.conf ở cùng thư mục
````vi /var/lib/pgsql/data/pg_hba.conf````
### 4. Thêm dòng này vào cuối
````host all all 0.0.0.0./0 md5````
**(Lệnh trên cho phép nhận truy cập từ mọi nguồn)**
- Để hạn chế, chỉ cần thay dải IP bằng dải của tổ chức
### 5. Khởi động lai dịch vụ của PgSQL
````sudo systemctl restart postgresql````
### 6. Public port
````firewall-cmd --zone=public --add-port=5432/tcp --permanent````
### 7. Khởi động lại firewall để cập nhật
````firewall-cmd --reload````

## D. Thiết lập PgAdmin4
### 1. Thêm địa chỉ gói cài
````yum -y install https://download.postgresql.org/pub/repos/yum/11/redhat/rhel-7-x86_64/pgdg-centos11-11-2.noarch.rpm````
### 2. Cài PgAdmin4
````yum -y install pgadmin4````
### 3. Coppy tệp cấu hình mẫu thành tệp cấu hình chính
````mv /etc/httpd/conf.d/pgadmin4.conf.sample /etc/httpd/conf.d/pgadmin4.conf````
### 4. Tạo các thư mục logs và lib liên quan và thiết lập quyền sở hữu
````mkdir -p /var/lib/pgadmin4/````
````mkdir -p /var/log/pgadmin4/````
````chown -R apache:apache /var/lib/pgadmin4````
````chown -R apache:apache /var/log/pgadmin4````
````chown -R apache:apache /var/lib/pgadmin4/*````
````chown -R apache:apache /var/log/pgadmin4/*````
### 5. Mở rộng nội dung file cấu hình
````vi /usr/lib/python2.7/site-packages/pgadmin4-web/config_distro.py````
### 6. Thêm các dòng sau vào
````LOG_FILE = '/var/log/pgadmin4/pgadmin4.log'````
````SQLITE_PATH = '/var/lib/pgadmin4/pgadmin4.db'````
````SESSION_DB_PATH = '/var/lib/pgadmin4/sessions'````
````STORAGE_DIR = '/var/lib/pgadmin4/storage'````
### 7. Khởi tạo accout 
````python /usr/lib/python2.7/site-packages/pgadmin4-web/setup.py````
- Email: projects.futuresky@gmail.com
- Pass: nghianghia057
### 8. Public port
````firewall-cmd --zone=public --add-port=80/tcp --permanent````
````firewall-cmd --permanent --add-service=http````
### 9. Khởi động lại firewall để cập nhật
````firewall-cmd --reload````
### 10. Khởi dộng httpd
````sudo systemctl start httpd.service````
````systemctl enable httpd````
### 11. Đổi cổng mặc định 80 sang 8080
````nano /etc/httpd/conf/httpd.conf````
````firewall-cmd --zone=public --add-port=8080/tcp --permanent````
````firewall-cmd --reload````

## E. Cài đặt node và npm
````yum install -y gcc-c++ make````
````curl -sL https://rpm.nodesource.com/setup_10.x | sudo -E bash -````
````sudo yum install nodejs````

## F. Install And Configure FTP Server
### 1. Install vsftpd
````yum install vsftpd ftp -y````
### 2. Configure vsftpd
````vi /etc/vsftpd/vsftpd.conf````
Find the following lines and make the changes as shown below:

[...]
Disable anonymous login
````anonymous_enable=NO````

Uncomment
````ascii_upload_enable=YES````
````ascii_download_enable=YES````

Uncomment - Enter your Welcome message - This is optional
````ftpd_banner=Welcome to UNIXMEN FTP service.````

Add at the end of this  file
````use_localtime=YES````

Enable and start the vsftpd service:
````systemctl enable vsftpd````
````systemctl start vsftpd````